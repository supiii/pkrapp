
CURRENT PROCESS
-processing folder 3 in PT
02:24:21 pm: Import file: C:\Users\compuuter\AppData\Local\PokerTracker 4\Temp\2019-10-05_CO_NL10_SH_IHDIYHL57.zip\hh.com_Zachia_284SH_2019.10.05_0.txt

10:04:49 pm: Import file: C:\Users\compuuter\AppData\Local\PokerTracker 4\Temp\2019-08-25_CO_NL10_SH_NODRNMR55.zip\hh.com_Watts_284SH_2019.08.25_0.txt


PROCESS NEW HANDS
C:\Users\compuuter\Desktop\New folder

divide into four folders 
C:\Users\compuuter\Desktop\New folder\1
C:\Users\compuuter\Desktop\New folder\2
C:\Users\compuuter\Desktop\New folder\3
C:\Users\compuuter\Desktop\New folder\4

run PostFlopRowsTest into 1 2 3 or 4 folder

Process next folder



How to calculate EV from PT4 wonBB amount
EV for hero BET/RAISE
    wonBB + heros next bet/raise size
EV for CALL
    wonBB + villains bet/raise size
EV for CHECK
    if hand goes to showdown without betting, calculate hero equity and multiply with pot
    if there is a bet/bets calculate wonBB + next bet/raise



(cash_hand_summary.amt_pot_p) as amt_pot_p,
(cash_hand_summary.amt_pot_f) as amt_pot_f,
(cash_hand_summary.amt_pot_t) as amt_pot_t,
(cash_hand_summary.amt_pot_r) as amt_pot_r,
(cash_hand_player_statistics.val_p_raise_made_pct) as val_p_raise_made_pct,
(cash_hand_player_statistics.val_p_raise_made_2_pct) as val_p_raise_made_2_pct,

(cash_hand_player_statistics.val_f_bet_made_pct) as val_f_bet_made_pct,
(cash_hand_player_statistics.val_f_raise_made_pct) as val_f_raise_made_pct,
(cash_hand_player_statistics.val_f_raise_made_2_pct) as val_f_raise_made_2_pct,

(cash_hand_player_statistics.val_t_bet_made_pct) as val_t_bet_made_pct,
(cash_hand_player_statistics.val_t_raise_made_pct) as val_t_raise_made_pct,
(cash_hand_player_statistics.val_t_raise_made_2_pct) as val_t_raise_made_2_pct,

(cash_hand_player_statistics.val_r_bet_made_pct) as val_r_bet_made_pct,
(cash_hand_player_statistics.val_r_raise_made_pct) as val_r_raise_made_pct,
(cash_hand_player_statistics.val_r_raise_made_2_pct) as val_r_raise_made_2_pct,


amt_f_bet_made          - Amount of bet on the flop
amt_f_raise_made        - Amount of this player's first raise on the flop
amt_f_raise_made_2      - Amount this player 3 or 4 bet on the flop (their last raise).

val_f_bet_made_pct      - Percentage of pot this player bet on flop (the pot value not including their raise)
val_f_raise_made_pct    - Percentage of pot this player raised on the flop as their first raise
val_f_raise_made_2_pct  - Percentage of pot this player 3 or 4 bet on the flop (their last raise)


All
-use WrongLinesRemover
-remove villain folds with same ratio as there are no villains hands on showdown

bestLinesForShowdonwRows
-only showdown hands
-use WrongLinesRemover

Aggression
-Bet/Raise all lines

Aggression/Call/Check
-All simple betting and raising lines
-Call/Check for villain aggression

Aggression/Fold
-All simple betting and raising lines
-Fold for villain aggression

Check/Call
-If Villain is the aggressor
-C, C, C - C, C, X - C, X, X

Semi bluff
-https://www.thepokerbank.com/strategy/plays/semi-bluff/examples/
-All hands where villain fold except hands where hero hits his draw
-If hero hits his draw take all B/R/C lines from hero
-remove villain folds with same ratio as there are no villains hands on showdown




BTN_BB1_0_1_1_0_0_0_2.txt


TODO

PT4 274 GB port:5432 user:postgres Database:PT4 password:dbpass
-postfloprow hands count: 51100701
-PT4 hands 67 miljons

1. amount bet per street and number of bets per street
    need to be converted to BB
    -amt_bet_p, amt_bet_f, amt_bet_t, amt_bet_r
    -amt_bet_ttl
2. load data in two steps again
3. make villainh propable hand better
4. load rest of the data
5. make money


PostflopRowCombiner
-combine
    -combine(file)


JJ
AJ4

Fold: (0) 0.0 NaN
Check: (824) 2402.399999999999 2.915533980582523
Call: (0) 0.0 NaN
Raise: (1259) 5752.280000000012 4.568927720413035

Check: (791) 2422.7000000000007 3.0628318584070806
Call: (0) 0.0 NaN
Raise: (1714) 6385.180000000022 3.725309218203047

Check: (793) 2509.2000000000025 3.1641866330390953
Call: (0) 0.0 NaN
Raise: (1713) 8038.980000000025 4.692924693520155


----------
K9s
flop A93

Fold: (0) 0.0 NaN
Check: (622) 705.6000000000005 1.1344051446945345
Call: (0) 0.0 NaN
Raise: (1741) 1698.7999999999952 0.9757610568638686

Check: (721) 626.0000000000003 0.8682385575589464
Call: (0) 0.0 NaN
Raise: (1768) 1606.7999999999956 0.9088235294117623






Fold: (3) -6.5 -2.1666666666666665
Check: (1187) -766.5000000000009 -0.6457455770850892
Call: (0) 0.0 NaN
Raise: (3223) 7759.9600000000255 2.407682283586728






----------
QJ
flop J74

Rows after removing wrongly played lines: 26799
Reading postflowrows.txt TOOK: 0 seconds
Fold: (2) -4.5 -2.25
Check: (7058) 28448.77999678604 4.030714082854355
Call: (0) 0.0 NaN
Raise: (19739) 62659.946424849775 3.1744235485510806

//// only when vpip gt 28
Check: (3229) 11392.419610030684 3.528157203478069
Call: (0) 0.0 NaN
Raise: (8868) 27359.461224522245 3.085189583279459

----------
IP BTN AA
FLOP A47


ROWS TO REMOVE SIZE: 10408
WrongLinesRemover start...
Rows before removing wrongly played lines: 10095
linesMapRiver size: 1632
linesMapTurn size: 80
WrongLinesRemover TOOK: 0 seconds
Rows after removing wrongly played lines: 10095
Fold: (0) 0.0 NaN
Check: (3027) 18327.568770964997 6.054697314491245
Call: (0) 0.0 NaN
Raise: (7068) 39348.06488252471 5.567071998093479

----------
preflop
Ks9d BTN r
BB C

FLOP
KhQc4s

Rows after removing wrongly played lines: 4359
Fold: (0) 0.0 NaN
Check: (1451) 5303.866999070833 3.6553184004623245
Call: (0) 0.0 NaN
Raise: (2908) 5800.74682271628 1.9947547533412242

KJ
Check: (1450) 5768.244420081078 3.978099600055916
Call: (0) 0.0 NaN
Raise: (2912) 6772.23506838502 2.3256301745827677

AK
Check: (1438) 5292.346723659068 3.680352380847752
Call: (0) 0.0 NaN
Raise: (2937) 8005.116000690143 2.7256098061593947

QQ
Check: (1940) 6840.233569165446 3.525893592353323
Call: (0) 0.0 NaN
Raise: (4138) 15638.753570355915 3.7793024577950494

QT
Check: (2168) 6277.39143876416 2.8954757558875275
Call: (0) 0.0 NaN
Raise: (4377) 8028.038330197194 1.8341417249708005

99
Check: (1611) 3841.8133502125115 2.3847382682883373
Call: (0) 0.0 NaN
Raise: (3082) 1979.1663832419576 0.6421694948870725
----------
weird behaviour:

preflop
QdQh BTN r
BB C

FLOP
QsJd4c

Fold: (0) 0.0 NaN
Check: (0) 0.0 NaN
Call: (0) 0.0 NaN
Raise: (23) 481.1 20.917391304347827

Postflowservice 218 LOST QdQhJdQc2h6d vs 2d2cJdQc2hQh6dLOST wonBB after: -19.9

----------
took 20s

used BufferedReader
    -took 12s

-----------
AdAh  9dJhQh
ALL ROWS: 1537
Rows after removing wrongly played lines: 913
Fold: (0) 0.0 NaN
Check: (379) 450.4000000000003 1.1883905013192622
Call: (0) 0.0 NaN
Raise: (534) 666.1999999999997 1.2475655430711605



------------------
hero    bb
villain btn

holecards   6d7d
flop        AsKh4c

preflop villain R, hero C
flop    hero

Fold: (1922) -2.638813735691987
Check: (0) NaN
Call: (935) -1.782395721925135
Raise: (119) 3.8563025210084056

---------------
Hero QQ
Villain AK

Hand no 1
FLOP    247
-B
TURN    8
-B
RIVER   J
-B

Hand no 2
FLOP    247
-B
TURN    8
-B
RIVER   K
-B



--------------------
4 BET ratio
1 = PREMIUM HANDS
2 = BALANCED, FEW BLUFFS
3 = MORE BLUFFS, SOME VALUE
4+ = LOTS OF BLUFFS

ATS
10 OR BELOW = NIT, VERY STRONG RANGE
20 = MORE BALANCED
30+ = MORE SPECULATIVE HANDS THAN VALUE

--------------------
FlopSuitness    3
FlopBucket      12
positio         20
actionspreflop   3
actions flop     3

---------------------
 hand TT
hero preflop R
villain C
FLOP AJ3
villain X

Fold: (0) 0.0
Check: (23) -40.8
Call: (1) -250.0
Raise: (32) 87.30000000000001
Fold madehand: (0) 0.0
Check madehand: (8) -13.4
Call madehand: (0) 0.0
Raise madehand: (5) 2.1000000000000014
CheckWithoutSD: (7) -8.3
CallWithoutSD: (1) -250.0
RaisWithoutSDe: (9) 41.4
Average equity: 0.06(1)


--------------------- 


PostFlopRows visualisation

Action      Results         madehand    no showdown
--------------------------------------------------
Check       (times)/wonBB
Call
Bet/Raise
Fold


--------------------------------------------------------------
Here are some example flop buckets.

High Connected (Monotone, Two Tone, Rainbow)
AJT
KQT
AQT etc
2 High Cards (Monotone, Two Tone, Rainbow)
QJ4
JT2
AJ6
1 High Card, 2 Middle Connected (Monotone, Two Tone, Rainbow)
K79
Q68
A89

1 High, 2 Low Connected (Monotone, Two Tone, Rainbow)
A34
K23
Q45

Mid Connected (Monotone, Two Tone, Rainbow)
8TJ
79J
789

2 Mid, 1 Low (Monotone, Two Tone, Rainbow)
T92
893
T84

1 Mid, 2 Low Connected (Monotone, Two Tone, Rainbow)
T23
924
J34

Mid low Connected (Monotone, Two Tone, Rainbow)
645
743
623

Low Connected (Monotone, Two Tone, Rainbow)
234
345
235

Paired Flops (Monotone, Two Tone, Rainbow)
T55
JJ3
882

52A 2
632 1
743 0
854 -1
965 -2
T76 -3




643
6-4-3 -1

543
5-4-3 -2

642
6-4-2 0

843
8-4-3 1

632


963 0

975 -13

965 -2

T76


postflop table

-holecards
-preflop actions
-range adv
-equity adv
-in position/out of position
-buckets
-vulnerability
-flopzilla hand strength
-effective stacks

calculateTheFive()


-3 is 8
-5 is 6 when right hole card? wrong: s c, d right: h
-when new hand, board is not cleared
-button is not updated on UI


tasks
-move read-button                       DONE
-clear if holecards have changed
-check that upswing ranges are working
-range advantage and nut advantage
-read postflop cards


screenreading 

1. find table position by pressing
    -by finding a valid card from possible left hole card positions
    -set table position

-------------------

{p1_won_bb=-119.0, id_hand=5385, hand_no=228871446359   , 
id_rangecombo=157, 
id_holecard1=12, 
id_holecard2=51, 
id_action_p=15, 
id_action_p_ep=1, 
id_action_p_mp=18, 
id_action_p_co=1, 
id_action_p_btn=0, 
id_action_p_sb=1, 
id_action_p_bb=1


Folds   78 67.0
Checks  -
Calls   45 -11.0
Bets    -
Raises   





Pkrapp/src/main/java/module-info.java
module com.masa.pkrapp {
    requires java.sql;
    requires javafx.controls;
    requires javafx.fxml;

    opens com.masa.pkrapp to javafx.fxml;
    exports com.masa.pkrapp;
}


Villain Range connectivity
-take away heros holdings (blockers)

Value bet
-villains continue range
-hand vulnerability




-Strong hands you can always bet for value/protection
-Marginal hands you can bet or check, protecting your range
-Bluffs with some immediate or backdoor equity
-Weak hands you’ll want to check and give up on the turn unimproved



Cbetting value/bluff ratios:
Bluff-to-Value Ratios on the Flop, Turn, and River
Your bluff-to-value ratios will be close to balanced if you use these popular rules of thumb:

-2 to 1 bluffs to value on the flop
-1 to 1 bluff to value on the turn
-1 to 2 bluff to value on the river

FLOP

TURN



RIVER
-75% pot size bet, ratio 1:2.33
-pot size bet, ratio 1:2
https://app.booklux.com/book/claarsus_by_chanet_coaching?fbclid=IwAR3XY3lZgqZ6DvOoflZJ1NT5kZ7IR-bdGDN7CfFQJS-lqevn1LRILwrdVug#


https://upswingpoker.com/semi-bluff-poker-strategy/
OOP 
Qh9s6s


Flopzilla statistics

Pair
	1 holecard
	



No similar ranks board
	1 holecard
		Straight flush
		Flush
		Straight
		Set
		Two pair
		Overpair
		Top pair
		PP below TP
		Middle pair
		Weak pair
	
Board paired
	Quads
	Full house
	Three of a kind
	Overpair
	PP below TP
	Middle pair
	Weak pair
	Ace high

Board triples
	Quads
	AA
	KK
	QQ
	JJ
	TT
	66-99
	22-55
	Ace high
 	
Boards Quads
	A high
	K high
	Q high
	J high
	T high
	Nothing

TURN
Two pair on board
	Quads
	Full house top
	Full house bottom
	Overpair
	PP below TP
	Ace high

























