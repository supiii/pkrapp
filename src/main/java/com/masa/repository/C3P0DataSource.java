package com.masa.repository;

import com.masa.type.Globals;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;

/**
 *
 * @author Matti
 */
public class C3P0DataSource {
    //private static C3P0DataSource dataSource;

    private static ComboPooledDataSource comboPooledDataSource;

    private C3P0DataSource() {
        createDS();
    }

    private static void createDS() {
        try {
            comboPooledDataSource = new ComboPooledDataSource();
            comboPooledDataSource
                    .setDriverClass("org.postgresql.Driver");
            comboPooledDataSource
                    .setJdbcUrl(Globals.JDBC_URL);
            comboPooledDataSource.setUser(Globals.DB_USER);
            comboPooledDataSource.setPassword(Globals.DB_PASSWORD);
            // Optional Settings

            comboPooledDataSource.setInitialPoolSize(5);
            comboPooledDataSource.setMinPoolSize(5);
            comboPooledDataSource.setAcquireIncrement(5);
            comboPooledDataSource.setMaxPoolSize(30);
            comboPooledDataSource.setMaxStatements(250);

            //System.out.println("imnital poolsize" + comboPooledDataSource.getInitialPoolSize());
            //System.out.println("max poolsize" + comboPooledDataSource.getMaxPoolSize());
        } catch (PropertyVetoException ex1) {
            ex1.printStackTrace();
        }
    }

    public static ComboPooledDataSource getInstance() {
        if (comboPooledDataSource == null) {
            createDS();
        }
        return comboPooledDataSource;
    }
}
