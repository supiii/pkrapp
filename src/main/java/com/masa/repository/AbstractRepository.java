package com.masa.repository;

import com.masa.type.Street;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matti
 */
public class AbstractRepository {
//    protected final static String DBNAME = "PT4_2019_01_07_094525";

    protected final static String testSql = "SELECT COUNT(*) as count FROM cash_hand_summary";

    protected final String WHERE = " WHERE ";
    protected final String AND = " AND ";
    protected final String OR = " OR ";

//    protected final String CHECK = "X";
//    protected final String CALL = "C";
//    protected final String BET = "B";
//    protected final String RAISE = "R";
//    protected final String FOLD = "F";
    /*public boolean checkStats(Map<HUDStat, Double> hudStats, int vpip, int pfr, StatsFilterPrecision precision) {
        int vpipRangeStep = 3;
        int pfrRangeStep = 3;
        if (precision == StatsFilterPrecision.LOW) {
            vpipRangeStep = 6;
            pfrRangeStep = 6;
        } else if (precision == StatsFilterPrecision.HIGH) {
            vpipRangeStep = 3;
            pfrRangeStep = 3;
        }

        if (!matches(hudStats.get(HUDStat.PREFLOP_VPIP).intValue(), vpip, vpipRangeStep)) {
            return false;
        }
        if (!matches(hudStats.get(HUDStat.PREFLOP_PFR).intValue(), pfr, pfrRangeStep)) {
            return false;
        }

        return true;
    }*/
    // 24, 22
    private boolean matches(int value1, int value2, int rangeStep) {
        int range1 = value1 - rangeStep;
        int range2 = value1 + rangeStep;
        return value2 >= range1 && value2 <= range2;
    }//         22 >= 21 && 22 <= 27

    public String getVillainsActionsSelect(List<String> villainPositions) {
        String sql = "";

        for (String position : villainPositions) {
            sql = sql + ",id_action_p_" + position + " ";
            sql = sql + ",id_action_f_" + position + " ";
            sql = sql + ",id_action_t_" + position + " ";
            sql = sql + ",id_action_r_" + position + " ";
        }

        return sql;
    }

    public String getVillainActionsSelect() {
        String sql = "";

        sql = sql + ",id_action_p_p2";
        sql = sql + ",id_action_f_p2";
        sql = sql + ",id_action_t_p2";
        sql = sql + ",id_action_r_p2";

        return sql;
    }

    public String getVillainsStatsSelect(List<String> villainPositions) {
        String sql = "";

        for (String position : villainPositions) {
            sql = sql + ",vpip_" + position + " ";
            sql = sql + ",pfr_" + position + " ";
        }

        return sql;
    }

    public String getVillainStatsSelect(String villainPositions) {
        String sql = "";

        sql = sql + ",vpip_" + villainPositions + " ";
        sql = sql + ",pfr_" + villainPositions + " ";

        return sql;
    }

    // val_f_hand_group, val_t_hand_group, val_r_hand_group, id_f_hand_strength, id_t_hand_strength, id_r_hand_strength
    public String getHandGroupAndStrengthSelect(Street street) {
        String sql = "";

        if (street == Street.FLOP) {
            sql = sql + ",id_f_hand_strength,val_f_hand_group ";
        }
        if (street == Street.TURN) {
            sql = sql + ",id_t_hand_strength,val_t_hand_group ";
        }
        if (street == Street.RIVER) {
            sql = sql + ",id_r_hand_strength,val_r_hand_group ";
        }

        return sql;
    }

    // flg_f_flush_draw, flg_t_flush_draw, flg_f_straight_draw, flg_t_straight_draw
    public String getDrawsSelect(Street street) {
        String sql = "";

        if (street == Street.FLOP) {
            sql = sql + ",flg_f_flush_draw, flg_f_straight_draw ";
        }
        if (street == Street.TURN) {
            sql = sql + ",flg_t_flush_draw, flg_t_straight_draw ";
        }

        return sql;
    }

    public String getStrActionsSelect(Street street) {
        //str_actions_f, str_actions_t, str_actions_r
        if (street == Street.FLOP) {
            return ",str_actions_f ";
        }
        if (street == Street.TURN) {
            return ",str_actions_t ";
        }
        if (street == Street.RIVER) {
            return ",str_actions_r ";
        }
        return "";
    }

//    protected final int QUERY_TIMEOUT = 7;
//    public boolean checkActions(ResultSet rs, QueryBuilderActions actions, List<String> villainPositions) throws SQLException {
//        String id_action_pf = "id_action_pf";
//        String id_action_f = "id_action_f";
//        String id_action_t = "id_action_t";
//        String id_action_r = "id_action_r";
//
//        List<String> villain_id_action_pList = new ArrayList<>();
//        List<String> villain_id_action_fList = new ArrayList<>();
//        List<String> villain_id_action_tList = new ArrayList<>();
//        List<String> villain_id_action_rList = new ArrayList<>();
//
//        for (String villainPos : villainPositions) {
//            villain_id_action_pList.add("id_action_p_" + villainPos);
//            villain_id_action_fList.add("id_action_f_" + villainPos);
//            villain_id_action_tList.add("id_action_t_" + villainPos);
//            villain_id_action_rList.add("id_action_r_" + villainPos);
//        }
//
//        if (!actionFound(actions.villainPreflopActionLookups, villain_id_action_pList, rs)) {
//            return false;
//        }
//        if (!actionFound(actions.villainFlopActionLookups, villain_id_action_fList, rs)) {
//            return false;
//        }
//        if (!actionFound(actions.villainTurnActionLookups, villain_id_action_tList, rs)) {
//            return false;
//        }
//        if (!actionFound(actions.villainRiverActionLookups, villain_id_action_rList, rs)) {
//            return false;
//        }
//
////        if (actions.villainFlopActionLookups != null && !actions.villainFlopActionLookups.isEmpty()) {
////            if (!found(actions.villainFlopActionLookups, rs.getInt(villain_id_action_f))) {
////                return false;
////            }
////        }
////        if (actions.villainTurnActionLookups != null && !actions.villainTurnActionLookups.isEmpty()) {
////            if (!found(actions.villainTurnActionLookups, rs.getInt(villain_id_action_t))) {
////                return false;
////            }
////        }
////        if (actions.villainRiverActionLookups != null && !actions.villainRiverActionLookups.isEmpty()) {
////            if (!found(actions.villainRiverActionLookups, rs.getInt(villain_id_action_r))) {
////                return false;
////            }
////        }
//        if (actions.preflopActionLookups != null && !actions.preflopActionLookups.isEmpty()) {
//            if (!found(actions.preflopActionLookups, rs.getInt(id_action_pf))) {
//                return false;
//            }
//        }
//        if (actions.flopActionLookups != null && !actions.flopActionLookups.isEmpty()) {
//            if (!found(actions.flopActionLookups, rs.getInt(id_action_f))) {
//                return false;
//            }
//        }
//        if (actions.turnActionLookups != null && !actions.turnActionLookups.isEmpty()) {
//            if (!found(actions.turnActionLookups, rs.getInt(id_action_t))) {
//                return false;
//            }
//        }
//        if (actions.riverActionLookups != null && !actions.riverActionLookups.isEmpty()) {
//            if (!found(actions.riverActionLookups, rs.getInt(id_action_r))) {
//                return false;
//            }
//        }
//
//        return true;
//    }
    private boolean actionFound(List<Integer> actions, List<String> villain_id_actionList, ResultSet rs) throws SQLException {
        if (actions == null || actions.isEmpty()) {
            return true;
        }
        for (String action : villain_id_actionList) {
            if (found(actions, rs.getInt(action))) {
                return true;
            }
        }

        return false;
    }

    private boolean found(List<Integer> list, int action) {
        for (int i : list) {
            if (i == action) {
                return true;
            }
        }
        return false;
    }

    public void insertGenericTableData(List<Map<String, Object>> list, String tableName) throws SQLException {
        Connection connection = getConnection();
        //connection.setAutoCommit(false);
        PreparedStatement preparedStatement = null;

        StringBuilder sql = new StringBuilder("INSERT INTO ").append(tableName).append(" (");
        StringBuilder placeholders = new StringBuilder();

        for (Iterator<String> iter = list.get(0).keySet().iterator(); iter.hasNext();) {
            //String s = iter.next();
            // System.out.println("iter.next(): " + iter.next());
            sql.append(iter.next());

            placeholders.append("?");

            if (iter.hasNext()) {
                sql.append(",");
                placeholders.append(",");
            }
        }

        sql.append(") VALUES (").append(placeholders).append(")");

        //System.out.println(sql.toString());
        preparedStatement = connection.prepareStatement(sql.toString());

        final int batchSize = 500;
        int count = 0;

        for (Map<String, Object> map : list) {
            int i = 1;

            for (Object value : map.values()) {
                if (value instanceof String) {
                    preparedStatement.setString(i++, (String) value);
                } else if (value instanceof Integer) {
                    preparedStatement.setInt(i++, (Integer) value);
                } else {
                    preparedStatement.setObject(i++, value);
                }

            }

            preparedStatement.addBatch();

            if (++count % batchSize == 0) {
//                System.out.println("Executing count " + count);
                try {
                    preparedStatement.executeBatch();
                } catch (SQLException ex) {
                    Logger.getLogger(AbstractRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        preparedStatement.executeBatch();
        preparedStatement.close();
        connection.close();

//        System.out.println("AFFECTED ROWS: " + affectedRows.length);
    }

    public void insertGenericTableDataOneByOne(List<Map<String, Object>> list, String tableName) throws SQLException {
        Connection connection = getConnection();
        //connection.setAutoCommit(false);

        for (Map<String, Object> map : list) {
            //if ((int) map.get("id_hand") == 5385) {

            PreparedStatement preparedStatement;
            StringBuilder sql = new StringBuilder("INSERT INTO ").append(tableName).append(" (");
            StringBuilder placeholders = new StringBuilder();

            for (Iterator<String> iter = map.keySet().iterator(); iter.hasNext();) {
                //String s = iter.next();
                // System.out.println("iter.next(): " + iter.next());
                sql.append(iter.next());

                placeholders.append("?");

                if (iter.hasNext()) {
                    sql.append(",");
                    placeholders.append(",");
                }
            }

            sql.append(") VALUES (").append(placeholders).append(")");

            //System.out.println(sql.toString());
            preparedStatement = connection.prepareStatement(sql.toString());
            int i = 1;

            for (Object value : map.values()) {
                if (value instanceof String) {
                    preparedStatement.setString(i++, (String) value);
                } else if (value instanceof Integer) {
                    preparedStatement.setInt(i++, (Integer) value);
                } else {
                    preparedStatement.setObject(i++, value);
                }

            }

            System.out.println(preparedStatement.toString());

            preparedStatement.execute();
            preparedStatement.close();
            //}

//        System.out.println("AFFECTED ROWS: " + affectedRows.length);
        }
        connection.close();
    }

    public Connection getConnection() throws SQLException {
        return C3P0DataSource.getInstance()
                .getConnection();
    }

//    public Connection getConnection() throws SQLException, PropertyVetoException {
//        ComboPooledDataSource dataSource = DatabaseUtility.getDataSource();
//        return dataSource.getConnection();
//    }
    public Properties getProperties(String path) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
        } catch (FileNotFoundException ex) {
//            throw new FileNotFoundException(ex.getMessage());
        }
        Properties properties = new Properties();
        try {
            properties.load(fis);
        } catch (IOException ex) {
//            throw new IOException(ex.getCause());
        }

        return properties;
    }

    public void printResultSet(ResultSet rs) throws SQLException {
        for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
            System.out.println(" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i));
        }
    }

    public void deleteAllDataFromTable(String tableName) throws SQLException {
        // PreparedStatement pst = null;
        Connection connection = getConnection();

        Statement stmt = connection.createStatement();
        stmt.executeUpdate("DELETE FROM " + tableName);

    }

    /*protected static String getPosition(Seat seat, ActionRound ac) {
        PositionPreFlop positionPreflop = ac.getSeats().get(seat).getPosition();

        if (positionPreflop == PositionPreFlop.UTG) {
            return "EP";
        }
        if (positionPreflop == PositionPreFlop.UTGplus1) {
            return "MP";
        }
        if (positionPreflop == PositionPreFlop.CO) {
            return "CO";
        }
        if (positionPreflop == PositionPreFlop.BTN) {
            return "BTN";
        }
        if (positionPreflop == PositionPreFlop.SB) {
            return "SB";
        }
        if (positionPreflop == PositionPreFlop.BB) {
            return "BB";
        }
        return null;
    }*/
}
