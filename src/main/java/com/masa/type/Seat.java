package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum Seat {
    ONE("1"),
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6");

    private String value;

    private Seat(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
