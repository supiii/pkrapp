package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum ThreeTypeValue {
    SMALL,
    MEDIUM,
    BIG
}
