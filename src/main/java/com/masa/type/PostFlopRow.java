package com.masa.type;

import com.masa.pkrapp.App;
import com.masa.pokertracker.PTUtil;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class PostFlopRow {

    private int id;

    // 1
    public boolean won;
    // 2
    public double wonBB;
    // 3
    //public int rangeComboId;
    // 4
    public String position;
    // 5
    public String villainPosition;
    // 6
    public MadeHandSpecific madeHandSpecificFlop;
    // 7
    public MadeHandSpecific madeHandSpecificTurn;
    // 8
    public MadeHandSpecific madeHandSpecificRiver;
    // 9
    public String actionsPreflop;
    // 10
    public String actionsFlop;
    // 11
    public String actionsTurn;
    // 12
    public String actionsRiver;
    // 13
    public String actionsVillainPreflop;
    // 14
    public String actionsVillainFlop;
    // 15
    public String actionsVillainTurn;
    // 16
    public String actionsVillainRiver;
    // 17
    public boolean overCardsFlop;
    // 18
    public boolean backdoorFlushDrawFlop;
    // 21
    public boolean gutshotDrawFlop;
    // 22
    public boolean openEndedStraightDrawFlop;
    // 23
    public boolean flushDrawFlop;
    // 24
    public boolean flushNutDrawFlop;
    // 25
    public boolean flushSecondNutDrawFlop;
    // 26
    public boolean flushThirdNutDrawFlop;
    // 27
    public boolean flushWeakDrawFlop;
    // 28
    public boolean overCardsTurn;
    // 29
    public boolean backdoorFlushDrawTurn;
    // 30
    public boolean gutshotDrawTurn;
    // 31
    public boolean openEndedStraightDrawTurn;
    // 32
    public boolean flushDrawTurn;
    // 33
    public boolean flushNutDrawTurn;
    // 34
    public boolean flushSecondNutDrawTurn;
    // 35
    public boolean flushThirdNutDrawTurn;
    // 36
    public boolean flushWeakDrawTurn;
    // 37
    public boolean showdown;
    // 38
    //public Double equityPreflop;
    // 39
    //public Double equityFlop;
    // 40
    //public Double equityTurn;
    // 41
    //public Double equityRiver;
    // 42
    public int holeCard1Id;
    // 43
    public int holeCard2Id;
    // 44
    public int flopCard1Id;
    // 45
    public int flopCard2Id;
    // 46
    public int flopCard3Id;
    // 47
    public int turnCardId;
    // 48
    public int riverCardId;
    // 49
    public int villainHoleCard1Id;
    // 50
    public int villainHoleCard2Id;
    // 51
    public int villainRangeComboId;
    // 52
    public int playersPreflop;
    // 53
    public int playersOnFlop;
    // 54
    //public String handNo;
    // 55
    public boolean aceFlop;
    // 56
    public boolean kingFlop;
    // 57
    public boolean queenFlop;
    // 58
    public boolean jackFlop;
    // 59
    public int nroOfSameSuitsFlop;
    // 60
    public int nroOfSameRanksFlop;
    // 61
    public boolean straightPossibleFlop;
    // 62
    public int ranksAddedValueFlop;
    // 63
    public boolean aceTurn;
    // 64
    public boolean kingTurn;
    // 65
    public boolean turnCardIsHighest;
    // 66
    public boolean turnCardIsSecondHighest;
    // 67
    public boolean turnCardIsThirdHighest;
    // 68
    public int nroOfSameSuitsTurn;
    // 69
    public boolean boardHasPairTurn;
    // 70
    public boolean boardHasTwoPairsTurn;
    // 71
    public boolean boardHasTriplesTurn;
    // 72
    public boolean straightPossibleWithOneCardTurn;
    // 73
    public boolean straightPossibleWithTwoCardsTurn;
    // 74
    public boolean aceRiver;
    // 75
    public boolean kingRiver;
    // 76
    public boolean riverCardIsHighest;
    // 77
    public boolean riverCardIsSecondHighest;
    // 78
    public boolean riverCardIsThirdeHighest;
    // 79
    public int nroOfSameSuitsRiver;
    // 80
    public boolean boardHasPairRiver;
    // 81
    public boolean boardHasTwoPairsRiver;
    // 82
    public boolean boardHasTriplesRiver;
    // 83
    public boolean boardHasFullhouseRiver;
    // 84
    public boolean straightPossibleWithOneCardRiver;
    // 85
    public boolean straightPossibleWithTwoCardsRiver;
    // 86
    public boolean straightOnBoardRiver;
    // 87
    public boolean noRanksOrOneRankBetweenFlop;
    // 88
    public boolean twoOrThreeRanksBetweenFlop;
    // 89
    public boolean noCardsBetweenTurn;
    // 90
    public boolean oneCardBetweenTurn;
    // 91
    public boolean noCardsBetweenRiver;
    // 92
    public boolean oneCardBetweenRiver;
    // 93
    public int villainVPIP;
    // 94
    public int villainPFR;
    // 95
    public int villainAF;
    // 96
    public int villain3Bet;
    // 97
    public int villainFoldTo3Bet;
    // 98
    public int villain4Bet;
    // 99
    public int villainFoldTo4Bet;
    // 100
    public int villainCbetFlop;
    // 101
    public int villainCbetTurn;
    // 102
    public int villainFoldToCbetFlop;
    // 103
    public int villainFoldToCbetTurn;
    // 104
    public int ranksAddedValueCategoryId;
    public double potPreflopBB;
    public double potFlopBB;
    public double potTurnBB;
    public double potRiverBB;
    public double betMadeFlopPct;
    public double betMadeTurnPct;
    public double betMadeRiverPct;
    public double raiseMadePreFlopPct;
    public double raiseMadeFlopPct;
    public double raiseMadeTurnPct;
    public double raiseMadeRiverPct;
    public double raise2MadePreFlopPct;
    public double raise2MadeFlopPct;
    public double raise2MadeTurnPct;
    public double raise2MadeRiverPct;

    public double villainBetMadeFlopPct;
    public double villainBetMadeTurnPct;
    public double villainBetMadeRiverPct;
    public double villainRaiseMadePreFlopPct;
    public double villainRaiseMadeFlopPct;
    public double villainRaiseMadeTurnPct;
    public double villainRaiseMadeRiverPct;
    public double villainRaise2MadePreFlopPct;
    public double villainRaise2MadeFlopPct;
    public double villainRaise2MadeTurnPct;
    public double villainRaise2MadeRiverPct;

    public BoardTextureFlop boardTextureFlop;
    public BoardTextureTurn boardTextureTurn;
    public BoardTextureRiver boardTextureRiver;
    public MadeHandSpecificAndDraw villainMadeHandSpecificAndDrawFlop;
    public MadeHandSpecificAndDraw villainMadeHandSpecificAndDrawTurn;
    public MadeHandSpecific villainMadeHandSpecificRiver;

    public Set<Draw> drawsFlop;
    public Set<Draw> drawsTurn;

    public boolean alteredVillainHand;

    public String getActions(Street street) {
        if (street == Street.PREFLOP) {
            return actionsPreflop;
        } else if (street == Street.FLOP) {
            return actionsFlop;
        } else if (street == Street.TURN) {
            return actionsTurn;
        } else if (street == Street.RIVER) {
            return actionsRiver;
        }

        return "";
    }

    public void setActions(Street street, String actions) {
        if (street == Street.PREFLOP) {
            actionsPreflop = actions;
        } else if (street == Street.FLOP) {
            actionsFlop = actions;
        } else if (street == Street.TURN) {
            actionsTurn = actions;
        } else if (street == Street.RIVER) {
            actionsRiver = actions;
        }
    }

    public String getVillainActions(Street street) {
        if (street == Street.PREFLOP) {
            return actionsVillainPreflop;
        } else if (street == Street.FLOP) {
            return actionsVillainFlop;
        } else if (street == Street.TURN) {
            return actionsVillainTurn;
        } else if (street == Street.RIVER) {
            return actionsVillainRiver;
        }

        return "";
    }

    @Override
    public String toString() {
        return "PostFlopRow{" + "wonBB=" + wonBB + ", position=" + position + ", villainPosition=" + villainPosition + ", madeHandSpecificFlop=" + madeHandSpecificFlop + ", madeHandSpecificTurn=" + madeHandSpecificTurn + ", madeHandSpecificRiver=" + madeHandSpecificRiver + ", actionsPreflop=" + actionsPreflop + ", actionsFlop=" + actionsFlop + ", actionsTurn=" + actionsTurn + ", actionsRiver=" + actionsRiver + ", actionsVillainPreflop=" + actionsVillainPreflop + ", actionsVillainFlop=" + actionsVillainFlop + ", actionsVillainTurn=" + actionsVillainTurn + ", actionsVillainRiver=" + actionsVillainRiver + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.won ? 1 : 0);
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.wonBB) ^ (Double.doubleToLongBits(this.wonBB) >>> 32));
        hash = 17 * hash + Objects.hashCode(this.position);
        hash = 17 * hash + Objects.hashCode(this.villainPosition);
        hash = 17 * hash + Objects.hashCode(this.madeHandSpecificFlop);
        hash = 17 * hash + Objects.hashCode(this.madeHandSpecificTurn);
        hash = 17 * hash + Objects.hashCode(this.madeHandSpecificRiver);
        hash = 17 * hash + Objects.hashCode(this.actionsPreflop);
        hash = 17 * hash + Objects.hashCode(this.actionsFlop);
        hash = 17 * hash + Objects.hashCode(this.actionsTurn);
        hash = 17 * hash + Objects.hashCode(this.actionsRiver);
        hash = 17 * hash + Objects.hashCode(this.actionsVillainPreflop);
        hash = 17 * hash + Objects.hashCode(this.actionsVillainFlop);
        hash = 17 * hash + Objects.hashCode(this.actionsVillainTurn);
        hash = 17 * hash + Objects.hashCode(this.actionsVillainRiver);
        hash = 17 * hash + (this.overCardsFlop ? 1 : 0);
        hash = 17 * hash + (this.backdoorFlushDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.gutshotDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.openEndedStraightDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.flushDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.flushNutDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.flushSecondNutDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.flushThirdNutDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.flushWeakDrawFlop ? 1 : 0);
        hash = 17 * hash + (this.overCardsTurn ? 1 : 0);
        hash = 17 * hash + (this.backdoorFlushDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.gutshotDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.openEndedStraightDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.flushDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.flushNutDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.flushSecondNutDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.flushThirdNutDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.flushWeakDrawTurn ? 1 : 0);
        hash = 17 * hash + (this.showdown ? 1 : 0);
        hash = 17 * hash + this.holeCard1Id;
        hash = 17 * hash + this.holeCard2Id;
        hash = 17 * hash + this.flopCard1Id;
        hash = 17 * hash + this.flopCard2Id;
        hash = 17 * hash + this.flopCard3Id;
        hash = 17 * hash + this.turnCardId;
        hash = 17 * hash + this.riverCardId;
        hash = 17 * hash + this.villainHoleCard1Id;
        hash = 17 * hash + this.villainHoleCard2Id;
        hash = 17 * hash + this.villainRangeComboId;
        hash = 17 * hash + this.playersPreflop;
        hash = 17 * hash + this.playersOnFlop;
        hash = 17 * hash + (this.aceFlop ? 1 : 0);
        hash = 17 * hash + (this.kingFlop ? 1 : 0);
        hash = 17 * hash + (this.queenFlop ? 1 : 0);
        hash = 17 * hash + (this.jackFlop ? 1 : 0);
        hash = 17 * hash + this.nroOfSameSuitsFlop;
        hash = 17 * hash + this.nroOfSameRanksFlop;
        hash = 17 * hash + (this.straightPossibleFlop ? 1 : 0);
        hash = 17 * hash + this.ranksAddedValueFlop;
        hash = 17 * hash + (this.aceTurn ? 1 : 0);
        hash = 17 * hash + (this.kingTurn ? 1 : 0);
        hash = 17 * hash + (this.turnCardIsHighest ? 1 : 0);
        hash = 17 * hash + (this.turnCardIsSecondHighest ? 1 : 0);
        hash = 17 * hash + (this.turnCardIsThirdHighest ? 1 : 0);
        hash = 17 * hash + this.nroOfSameSuitsTurn;
        hash = 17 * hash + (this.boardHasPairTurn ? 1 : 0);
        hash = 17 * hash + (this.boardHasTwoPairsTurn ? 1 : 0);
        hash = 17 * hash + (this.boardHasTriplesTurn ? 1 : 0);
        hash = 17 * hash + (this.straightPossibleWithOneCardTurn ? 1 : 0);
        hash = 17 * hash + (this.straightPossibleWithTwoCardsTurn ? 1 : 0);
        hash = 17 * hash + (this.aceRiver ? 1 : 0);
        hash = 17 * hash + (this.kingRiver ? 1 : 0);
        hash = 17 * hash + (this.riverCardIsHighest ? 1 : 0);
        hash = 17 * hash + (this.riverCardIsSecondHighest ? 1 : 0);
        hash = 17 * hash + (this.riverCardIsThirdeHighest ? 1 : 0);
        hash = 17 * hash + this.nroOfSameSuitsRiver;
        hash = 17 * hash + (this.boardHasPairRiver ? 1 : 0);
        hash = 17 * hash + (this.boardHasTwoPairsRiver ? 1 : 0);
        hash = 17 * hash + (this.boardHasTriplesRiver ? 1 : 0);
        hash = 17 * hash + (this.boardHasFullhouseRiver ? 1 : 0);
        hash = 17 * hash + (this.straightPossibleWithOneCardRiver ? 1 : 0);
        hash = 17 * hash + (this.straightPossibleWithTwoCardsRiver ? 1 : 0);
        hash = 17 * hash + (this.straightOnBoardRiver ? 1 : 0);
        hash = 17 * hash + (this.noRanksOrOneRankBetweenFlop ? 1 : 0);
        hash = 17 * hash + (this.twoOrThreeRanksBetweenFlop ? 1 : 0);
        hash = 17 * hash + (this.noCardsBetweenTurn ? 1 : 0);
        hash = 17 * hash + (this.oneCardBetweenTurn ? 1 : 0);
        hash = 17 * hash + (this.noCardsBetweenRiver ? 1 : 0);
        hash = 17 * hash + (this.oneCardBetweenRiver ? 1 : 0);
        hash = 17 * hash + this.villainVPIP;
        hash = 17 * hash + this.villainPFR;
        hash = 17 * hash + this.villain3Bet;
        hash = 17 * hash + this.villainFoldTo3Bet;
        hash = 17 * hash + this.villain4Bet;
        hash = 17 * hash + this.villainFoldTo4Bet;
        hash = 17 * hash + this.villainCbetFlop;
        hash = 17 * hash + this.villainCbetTurn;
        hash = 17 * hash + this.villainFoldToCbetFlop;
        hash = 17 * hash + this.villainFoldToCbetTurn;
        hash = 17 * hash + this.ranksAddedValueCategoryId;
        hash = 17 * hash + Objects.hashCode(this.boardTextureFlop);
        hash = 17 * hash + Objects.hashCode(this.boardTextureTurn);
        hash = 17 * hash + Objects.hashCode(this.boardTextureRiver);
        hash = 17 * hash + Objects.hashCode(this.drawsFlop);
        hash = 17 * hash + Objects.hashCode(this.drawsTurn);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PostFlopRow other = (PostFlopRow) obj;
        if (this.won != other.won) {
            return false;
        }
        if (!Objects.equals(this.position, other.position)) {
            return false;
        }
        if (!Objects.equals(this.villainPosition, other.villainPosition)) {
            return false;
        }
        if (!Objects.equals(this.actionsPreflop, other.actionsPreflop)) {
            return false;
        }
        if (!Objects.equals(this.actionsFlop, other.actionsFlop)) {
            return false;
        }
        if (!Objects.equals(this.actionsTurn, other.actionsTurn)) {
            return false;
        }
        if (!Objects.equals(this.actionsRiver, other.actionsRiver)) {
            return false;
        }
        if (!Objects.equals(this.actionsVillainPreflop, other.actionsVillainPreflop)) {
            return false;
        }
        if (!Objects.equals(this.actionsVillainFlop, other.actionsVillainFlop)) {
            return false;
        }
        if (!Objects.equals(this.actionsVillainTurn, other.actionsVillainTurn)) {
            return false;
        }
        if (!Objects.equals(this.actionsVillainRiver, other.actionsVillainRiver)) {
            return false;
        }
        return true;
    }

    private int intValue(String s) {
        return Integer.parseInt(s);
    }

    private int intValue(boolean b) {
        return b == true ? 1 : 0;
    }

    private boolean booleanValue(String s) {
        return s.equals("1");
    }

    public Double round(Double value, int places) {
        if (value == null) {
            return null;
        }
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd;

        try {
            bd = BigDecimal.valueOf(value);
        } catch (Exception e) {
            return null;
        }

        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public CardSet getBoardByStreet(Street street) {
        if (street == Street.FLOP) {
            return getFlopCardSet();
        }
        if (street == Street.TURN) {
            return getTurnCardSet();
        }
        if (street == Street.RIVER) {
            return getRiverCardSet();
        }

        return null;
    }

    public double getPotByStreet(Street street) {
        if (street == Street.PREFLOP) {
            return potPreflopBB;
        } else if (street == Street.FLOP) {
            return potFlopBB;
        }
        if (street == Street.TURN) {
            return potTurnBB;
        } else {
            return potRiverBB;
        }
    }

    /*
    public double betMadeFlopPct;
    public double betMadeTurnPct;
    public double betMadeRiverPct;
    public double raiseMadePreFlopPct;
    public double raiseMadeFlopPct;
    public double raiseMadeTurnPct;
    public double raiseMadeRiverPct;
    public double raise2MadePreFlopPct;
    public double raise2MadeFlopPct;
    public double raise2MadeTurnPct;
    public double raise2MadeRiverPct;

     */
    public double getBetMadeByStreet(Street street) {
        if (street == Street.FLOP) {
            return betMadeFlopPct;
        }
        if (street == Street.TURN) {
            return betMadeTurnPct;
        } else {
            return betMadeRiverPct;
        }
    }

    public double getVillainBetMadeByStreet(Street street) {
        if (street == Street.FLOP) {
            return villainBetMadeFlopPct;
        } else if (street == Street.TURN) {
            return villainBetMadeTurnPct;
        } else {
            return villainBetMadeRiverPct;
        }
    }

    public double getRaiseMadeByStreet(Street street) {
        if (street == Street.PREFLOP) {
            return raiseMadePreFlopPct;
        } else if (street == Street.FLOP) {
            return raiseMadeFlopPct;
        } else if (street == Street.TURN) {
            return raiseMadeTurnPct;
        } else {
            return raiseMadeRiverPct;
        }
    }

    public double getVillainRaiseMadeByStreet(Street street) {
        if (street == Street.PREFLOP) {
            return villainRaiseMadePreFlopPct;
        } else if (street == Street.FLOP) {
            return villainRaiseMadeFlopPct;
        } else if (street == Street.TURN) {
            return villainRaiseMadeTurnPct;
        } else {
            return villainRaiseMadeRiverPct;
        }
    }

    public double getRaise2MadeByStreet(Street street) {
        if (street == Street.PREFLOP) {
            return raise2MadePreFlopPct;
        } else if (street == Street.FLOP) {
            return raise2MadeFlopPct;
        } else if (street == Street.TURN) {
            return raise2MadeTurnPct;
        } else {
            return raise2MadeRiverPct;
        }
    }

    public double getVillainRaise2MadeByStreet(Street street) {
        if (street == Street.PREFLOP) {
            return villainRaise2MadePreFlopPct;
        } else if (street == Street.FLOP) {
            return villainRaise2MadeFlopPct;
        } else if (street == Street.TURN) {
            return villainRaise2MadeTurnPct;
        } else {
            return villainRaise2MadeRiverPct;
        }
    }

    public CardSet getBoardCardSet() {
        if (flopCard1Id != 0 && turnCardId != 0 && riverCardId != 0) {
            return getRiverCardSet();
        } else if (flopCard1Id != 0 && turnCardId != 0 && riverCardId == 0) {
            return getTurnCardSet();
        } else {
            return getFlopCardSet();
        }
    }

    public CardSet getFlopCardSet() {
        return new CardSet(
                PTUtil.getCard(flopCard1Id),
                PTUtil.getCard(flopCard2Id),
                PTUtil.getCard(flopCard3Id));
    }

    public CardSet getTurnCardSet() {
        return new CardSet(
                PTUtil.getCard(flopCard1Id),
                PTUtil.getCard(flopCard2Id),
                PTUtil.getCard(flopCard3Id),
                PTUtil.getCard(turnCardId));
    }

    public CardSet getRiverCardSet() {
        return new CardSet(
                PTUtil.getCard(flopCard1Id),
                PTUtil.getCard(flopCard2Id),
                PTUtil.getCard(flopCard3Id),
                PTUtil.getCard(turnCardId),
                PTUtil.getCard(riverCardId));
    }

    public void printAllActions() {
        System.out.println("Hero actions: " + actionsFlop + "|" + actionsTurn + "|" + actionsRiver + " VillainActions: " + actionsVillainFlop + "|" + actionsVillainTurn + "|" + actionsVillainRiver);
    }

    public String getAllActionsAsString() {
        return "Hero actions: " + actionsFlop + "|" + actionsTurn + "|" + actionsRiver + " VillainActions: " + actionsVillainFlop + "|" + actionsVillainTurn + "|" + actionsVillainRiver;
    }

    public void printActionsAndCards() {
        if (villainHoleCard1Id == 0 || villainHoleCard2Id == 0) {
            System.out.println("villainhole cars 0 #############################################################");
            return;
        }
        System.out.println(getAllActionsAsString() + " - " + getRiverCardSet().toStringWithoutCommas()
                + " H: " + App.holeCards.toStringWithoutCommas()
                + " V: " + PTUtil.getCard(villainHoleCard1Id).toString() + PTUtil.getCard(villainHoleCard2Id).toString());
    }

    public String getAllActions() {
        return actionsPreflop + "|" + actionsFlop + "|" + actionsTurn + "|" + actionsRiver;
    }

    public String getAllVillainActions() {
        return actionsVillainPreflop + "|" + actionsVillainFlop + "|" + actionsVillainTurn + "|" + actionsVillainRiver;
    }

    public void printBoard() {
        System.out.println(getBoardCardSet().toStringWithoutCommas());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean sawFlop() {
        return flopCard1Id != 0;
    }

    public boolean sawTurn() {
        return turnCardId != 0;
    }

    public boolean sawRiver() {
        return riverCardId != 0;
    }
}
