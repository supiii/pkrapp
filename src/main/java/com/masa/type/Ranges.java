package com.masa.type;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mi.poker.calculation.HandParser;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author compuuter
 */
public class Ranges {

    // AA,AKo,AKs...
    public List<WeightedCombination> heroRange;
    public List<WeightedCombination> villainRange;

    // "AsAc|AsKc"
    public String heroRangeString = "";
    public String villainRangeString = "";

    // AsAd,AsKc...
    public Set<CardSet> heroRangeCardSet;
    public Set<CardSet> villainRangeCardSet;

    public void parsePossibleCombinationsOnFlopHero(CardSet flopCards) {
        heroRangeCardSet = new HashSet<>();
        villainRangeCardSet = new HashSet<>();
        boolean monotoneOrTwoToneFlop = false;
        Suit multipleSuit = null;

        for (Suit value : Suit.values()) {
            if (similarSuits(value, flopCards) > 1) {
                multipleSuit = value;
                monotoneOrTwoToneFlop = true;
            }
        }

        checkRange(heroRange, heroRangeCardSet, flopCards, monotoneOrTwoToneFlop, multipleSuit);
        checkRange(villainRange, villainRangeCardSet, flopCards, monotoneOrTwoToneFlop, multipleSuit);
        createStringRanges();
    }

    private void checkRange(List<WeightedCombination> weightedRanges, Set<CardSet> cardSetsToAdd, CardSet flopCardSet, boolean monotoneOrTwoToneFlop, Suit multipleSuit) {
        for (WeightedCombination wc : weightedRanges) {
            List<CardSet> parsedHandsCardSets = HandParser.parsePossibleHandsReturnList(wc.hand);

            int added = 0;
            if (monotoneOrTwoToneFlop) {
                // take all combos with suit == multipleSuits
                for (CardSet parsedHandsCardSet : parsedHandsCardSets) {
                    if (parsedHandsCardSet.get(0).suitOf() == multipleSuit
                            || parsedHandsCardSet.get(1).suitOf() == multipleSuit) {

                        if (allowedInRange(flopCardSet, parsedHandsCardSet)) {
                            boolean addSuccess = cardSetsToAdd.add(parsedHandsCardSet);
                            if (addSuccess) {
                                added++;
                            }
                        }
                    }
                }
            }
            for (CardSet parsedHandsCardSet : parsedHandsCardSets) {
                if (allowedInRange(flopCardSet, parsedHandsCardSet)) {
                    boolean addSuccess = cardSetsToAdd.add(parsedHandsCardSet);
                    if (addSuccess) {
                        added++;
                    }
                }

                if (added == wc.combinations) {
                    break;
                }
            }

        }
    }

    private boolean allowedInRange(CardSet flopCardSet, CardSet cs) {
        for (Card card : flopCardSet) {
            if (cs.contains(card)) {
                return false;
            }
        }
        return true;
    }

    private void createStringRanges() {
        heroRangeString = "";
        for (CardSet cs : heroRangeCardSet) {
            if (heroRangeString.isEmpty()) {
                heroRangeString = heroRangeString + cs.toStringWithoutCommas();
            } else {
                heroRangeString = heroRangeString + "|" + cs.toStringWithoutCommas();
            }
        }
        villainRangeString = "";
        for (CardSet cs : villainRangeCardSet) {
            if (villainRangeString.isEmpty()) {
                villainRangeString = villainRangeString + cs.toStringWithoutCommas();
            } else {
                villainRangeString = villainRangeString + "|" + cs.toStringWithoutCommas();
            }
        }
    }

    private int similarSuits(Suit suit, CardSet cards) {
        int times = 0;
        for (Card card : cards) {
            if (suit == card.suitOf()) {
                times++;
            }
        }
        return times;
    }

    public void printRanges() {
        System.out.println("Hero's range ");
        heroRange.forEach(weightedCombination -> {
            System.out.print(weightedCombination.hand + " " + weightedCombination.combinations + ", ");
        });
        System.out.println("Villain's range ");
        villainRange.forEach(weightedCombination -> {
            System.out.print(weightedCombination.hand + " " + weightedCombination.combinations + ", ");
        });

        System.out.println("heroRangeString " + heroRange);
        System.out.println("villainRangeString " + villainRange);
    }
}
