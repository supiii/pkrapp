package com.masa.type;

/**
 *
 * @author Matti
 */
public enum PositionPostFlop {
    SB,
    BB,
    EP,
    MP,
    CO,
    BTN
}
