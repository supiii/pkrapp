package com.masa.type;

/**
 *
 * @author compuuter
 */
public class WeightedCombination {

    // T9o
    public String hand;
    public int combinations;
    public double weightedCombinations;

    public WeightedCombination(String hand) {
        this.hand = hand;
        this.combinations = getCombinations(hand);
        this.weightedCombinations = combinations;
    }

    public WeightedCombination(String hand, double weight) {
        this.hand = hand;
        this.combinations = getCombinations(hand);
        this.weightedCombinations = weight * combinations;
    }

    private int getCombinations(String hand) {
        // KK
        if (hand.length() == 2) {
            return 6;
        } // ex. T9o
        else if (hand.substring(hand.length() - 1).equals("o")) {
            return 12;
        }

        // T9s
        return 4;
    }
}
