package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum Aggressiveness {
    VERY_LOW("Very low aggression"),
    LOW("Low aggression"),
    MEDIUM("Medium aggression"),
    HIGH("Low aggression"),
    VERY_HIGH("Low aggression");

    private String value;

    private Aggressiveness(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
