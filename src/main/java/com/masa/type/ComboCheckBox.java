package com.masa.type;

/**
 *
 * @author compuuter
 */
public class ComboCheckBox {

    public String label;
    public boolean checked;
    public int combos;

    public ComboCheckBox() {
        this.label = "";
        this.checked = false;
        this.combos = 0;
    }

    public ComboCheckBox(String label, boolean checked) {
        this.label = label;
        this.checked = checked;
        this.combos = 0;
    }

    public ComboCheckBox(String label, boolean checked, int combos) {
        this.label = label;
        this.checked = checked;
        this.combos = combos;
    }
}
