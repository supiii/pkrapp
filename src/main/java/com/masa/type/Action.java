package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum Action {
    RAISE("Raise"),
    RAISE_OR_CALL("Raise or Call"),
    RAISE_OR_FOLD("Raise or Fold"),
    RAISE_CALL_OR_FOLD("Raise, Call or Fold"),
    CALL("Call"),
    CALL_OR_FOLD("Call or Fold"),
    FOLD("FOLD");

    private String value;

    private Action(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
