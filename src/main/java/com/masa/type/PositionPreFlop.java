package com.masa.type;

/**
 *
 * @author Matti
 */
public enum PositionPreFlop {
    UTG,
    UTGplus1,
    CO,
    BTN,
    SB,
    BB
}
