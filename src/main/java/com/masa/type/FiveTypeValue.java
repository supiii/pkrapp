package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum FiveTypeValue {
    VERY_BAD,
    BAD,
    MEDIUM,
    GOOD,
    VERY_GOOD
}
