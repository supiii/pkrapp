package com.masa.type;

import com.masa.screenreader.Rgb;

/**
 *
 * @author compuuter
 */
public class Point {

    public int x;
    public int y;
    public Rgb rgb;
    public boolean matches;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        this.matches = true;
    }

    public Point(int x, int y, Rgb rgb) {
        this.x = x;
        this.y = y;
        this.matches = true;
        this.rgb = rgb;
    }

    public Point(int x, int y, boolean matches) {
        this.x = x;
        this.y = y;
        this.matches = matches;
    }
}
