package com.masa.type;

/**
 *
 * @author Matti
 */
public enum Street {
    PREFLOP,
    FLOP,
    TURN,
    RIVER
}
