package com.masa.type;

/**
 * https://www.pokervip.com/strategy-articles/texas-hold-em-no-limit-advanced/flop-buckets
 *
 * @author compuuter
 */
public enum FlopBucket {
    HI_CONNECTED("High Connected"),
    TWO_HI_CARDS("2 High Cards"),
    ONE_HI_TWO_MIDDLE_CONNECTED("1 High Card, 2 Middle Connected"),
    ONE_HI_TWO_LOW_CONNECTED("1 High, 2 Low Connected"),
    ONE_HI_NOT_CONNECTED("1 High, Not Connected"),
    MID_CONNECTED("Mid Connected"),
    TWO_MID_ONE_LOW("2 Mid, 1 low"),
    ONE_MID_TWO_LOW_CONNECTED("1 Mid, 2 Low Connected"),
    MID_LOW_CONNECTED("Mid Low Connected"),
    LOW_CONNECTED("Low Connected"),
    MID_LOW_NOT_CONNECTED("Mid Low Not Connected"),
    PAIRED("Paired"),
    TRIPLES("Triples");

    private final String value;

    private FlopBucket(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
