package com.masa.type;

/**
 *
 * @author compuuter
 */
public class Frequency {

    public final double min;
    public final double max;

    public Frequency(double min, double max) {
        this.min = min;
        this.max = max;
    }
}
