package com.masa.type;

import com.masa.screenreader.util.ImageUtil;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author Matti
 */
public final class Globals {

    public final static NLLimit LIMIT = NLLimit.NL10;
    public final static TableType TABLE_TYPE = TableType.CASH_6_MAX_ZOOM;

    public final static String DB_NAME = "PT4_2024_01_10_070535"; // "PT4 DB"
    public final static String DB_USER = "postgres";
    public final static String DB_PASSWORD = "dbpass";
    public final static String JDBC_URL = "jdbc:postgresql://localhost/" + DB_NAME + "?allowMultiQueries=true";

    public final static Set<CardSet> possibleCombos = new HashSet<>();

    public final static String DOT = ".";
    public final static String DOLLAR = "$";
    public final static String ZERO = "0";
    public final static String ONE = "1";
    public final static String TWO = "2";
    public final static String THREE = "3";
    public final static String FOUR = "4";
    public final static String FIVE = "5";
    public final static String SIX = "6";
    public final static String SEVEN = "7";
    public final static String EIGHT = "8";
    public final static String NINE = "9";

    public final static Card TWO_CLUB = new Card(Rank.TWO, Suit.CLUB);
    public final static Card THREE_CLUB = new Card(Rank.THREE, Suit.CLUB);
    public final static Card FOUR_CLUB = new Card(Rank.FOUR, Suit.CLUB);
    public final static Card FIVE_CLUB = new Card(Rank.FIVE, Suit.CLUB);
    public final static Card SIX_CLUB = new Card(Rank.SIX, Suit.CLUB);
    public final static Card SEVEN_CLUB = new Card(Rank.SEVEN, Suit.CLUB);
    public final static Card EIGHT_CLUB = new Card(Rank.EIGHT, Suit.CLUB);
    public final static Card NINE_CLUB = new Card(Rank.NINE, Suit.CLUB);
    public final static Card TEN_CLUB = new Card(Rank.TEN, Suit.CLUB);
    public final static Card JACK_CLUB = new Card(Rank.JACK, Suit.CLUB);
    public final static Card QUEEN_CLUB = new Card(Rank.QUEEN, Suit.CLUB);
    public final static Card KING_CLUB = new Card(Rank.KING, Suit.CLUB);
    public final static Card ACE_CLUB = new Card(Rank.ACE, Suit.CLUB);

    public final static Card TWO_HEART = new Card(Rank.TWO, Suit.HEART);
    public final static Card THREE_HEART = new Card(Rank.THREE, Suit.HEART);
    public final static Card FOUR_HEART = new Card(Rank.FOUR, Suit.HEART);
    public final static Card FIVE_HEART = new Card(Rank.FIVE, Suit.HEART);
    public final static Card SIX_HEART = new Card(Rank.SIX, Suit.HEART);
    public final static Card SEVEN_HEART = new Card(Rank.SEVEN, Suit.HEART);
    public final static Card EIGHT_HEART = new Card(Rank.EIGHT, Suit.HEART);
    public final static Card NINE_HEART = new Card(Rank.NINE, Suit.HEART);
    public final static Card TEN_HEART = new Card(Rank.TEN, Suit.HEART);
    public final static Card JACK_HEART = new Card(Rank.JACK, Suit.HEART);
    public final static Card QUEEN_HEART = new Card(Rank.QUEEN, Suit.HEART);
    public final static Card KING_HEART = new Card(Rank.KING, Suit.HEART);
    public final static Card ACE_HEART = new Card(Rank.ACE, Suit.HEART);

    public final static Card TWO_DIAMOND = new Card(Rank.TWO, Suit.DIAMOND);
    public final static Card THREE_DIAMOND = new Card(Rank.THREE, Suit.DIAMOND);
    public final static Card FOUR_DIAMOND = new Card(Rank.FOUR, Suit.DIAMOND);
    public final static Card FIVE_DIAMOND = new Card(Rank.FIVE, Suit.DIAMOND);
    public final static Card SIX_DIAMOND = new Card(Rank.SIX, Suit.DIAMOND);
    public final static Card SEVEN_DIAMOND = new Card(Rank.SEVEN, Suit.DIAMOND);
    public final static Card EIGHT_DIAMOND = new Card(Rank.EIGHT, Suit.DIAMOND);
    public final static Card NINE_DIAMOND = new Card(Rank.NINE, Suit.DIAMOND);
    public final static Card TEN_DIAMOND = new Card(Rank.TEN, Suit.DIAMOND);
    public final static Card JACK_DIAMOND = new Card(Rank.JACK, Suit.DIAMOND);
    public final static Card QUEEN_DIAMOND = new Card(Rank.QUEEN, Suit.DIAMOND);
    public final static Card KING_DIAMOND = new Card(Rank.KING, Suit.DIAMOND);
    public final static Card ACE_DIAMOND = new Card(Rank.ACE, Suit.DIAMOND);

    public final static Card TWO_SPADE = new Card(Rank.TWO, Suit.SPADE);
    public final static Card THREE_SPADE = new Card(Rank.THREE, Suit.SPADE);
    public final static Card FOUR_SPADE = new Card(Rank.FOUR, Suit.SPADE);
    public final static Card FIVE_SPADE = new Card(Rank.FIVE, Suit.SPADE);
    public final static Card SIX_SPADE = new Card(Rank.SIX, Suit.SPADE);
    public final static Card SEVEN_SPADE = new Card(Rank.SEVEN, Suit.SPADE);
    public final static Card EIGHT_SPADE = new Card(Rank.EIGHT, Suit.SPADE);
    public final static Card NINE_SPADE = new Card(Rank.NINE, Suit.SPADE);
    public final static Card TEN_SPADE = new Card(Rank.TEN, Suit.SPADE);
    public final static Card JACK_SPADE = new Card(Rank.JACK, Suit.SPADE);
    public final static Card QUEEN_SPADE = new Card(Rank.QUEEN, Suit.SPADE);
    public final static Card KING_SPADE = new Card(Rank.KING, Suit.SPADE);
    public final static Card ACE_SPADE = new Card(Rank.ACE, Suit.SPADE);

    public final static int TABLE_WIDTH = 640;
    public final static int TABLE_HEIGHT = 468; // new value 468
    public final static int HUD_WIDTH = 250;
    public final static int HUD_HEIGHT = 40;
    public final static int TABLE_1_POSITION_X = 0;
    public final static int TABLE_1_POSITION_Y = 0;
    public final static int TABLE_2_POSITION_X = TABLE_WIDTH;
    public final static int TABLE_2_POSITION_Y = 0;
    public final static int TABLE_3_POSITION_X = 2 * TABLE_WIDTH;
    public final static int TABLE_3_POSITION_Y = 0;
    public final static int TABLE_4_POSITION_X = 0;
    public final static int TABLE_4_POSITION_Y = TABLE_HEIGHT;
    public final static int TABLE_5_POSITION_X = TABLE_WIDTH;
    public final static int TABLE_5_POSITION_Y = TABLE_HEIGHT;
    public final static int TABLE_6_POSITION_X = 2 * TABLE_WIDTH;
    public final static int TABLE_6_POSITION_Y = TABLE_HEIGHT;
    public final static int TABLE_1_HUD_POSITION_X = 0;
    public final static int TABLE_1_HUD_POSITION_Y = TABLE_HEIGHT - HUD_HEIGHT;
    public final static int TABLE_2_HUD_POSITION_X = TABLE_2_POSITION_X + TABLE_1_HUD_POSITION_X;
    public final static int TABLE_2_HUD_POSITION_Y = TABLE_2_POSITION_Y + TABLE_1_HUD_POSITION_Y;
    public final static int TABLE_3_HUD_POSITION_X = TABLE_3_POSITION_X + TABLE_1_HUD_POSITION_X;
    public final static int TABLE_3_HUD_POSITION_Y = TABLE_3_POSITION_Y + TABLE_1_HUD_POSITION_Y;
    public final static int TABLE_4_HUD_POSITION_X = TABLE_4_POSITION_X + TABLE_1_HUD_POSITION_X;
    public final static int TABLE_4_HUD_POSITION_Y = TABLE_4_POSITION_Y + TABLE_1_HUD_POSITION_Y;
    public final static int TABLE_5_HUD_POSITION_X = TABLE_5_POSITION_X + TABLE_1_HUD_POSITION_X;
    public final static int TABLE_5_HUD_POSITION_Y = TABLE_5_POSITION_Y + TABLE_1_HUD_POSITION_Y;
    public final static int TABLE_6_HUD_POSITION_X = TABLE_6_POSITION_X + TABLE_1_HUD_POSITION_X;
    public final static int TABLE_6_HUD_POSITION_Y = TABLE_6_POSITION_Y + TABLE_1_HUD_POSITION_Y;

    public final static int DEALER_BUTTON_X_SEAT_2 = 498;
    public final static int DEALER_BUTTON_Y_SEAT_2 = 169;

    public final static int DEALER_BUTTON_X_SEAT_3 = 469;
    public final static int DEALER_BUTTON_Y_SEAT_3 = 271;

    public final static int DEALER_BUTTON_X_SEAT_4 = 261;
    public final static int DEALER_BUTTON_Y_SEAT_4 = 293;

    public final static int DEALER_BUTTON_X_SEAT_5 = 137;
    public final static int DEALER_BUTTON_Y_SEAT_5 = 227;

    public final static int DEALER_BUTTON_X_SEAT_6 = 206;
    public final static int DEALER_BUTTON_Y_SEAT_6 = 138;

    public static final Color STACK_FONT_COLOR = new Color(192, 248, 181);
    public final static Color FOLDED_STACK_COLOR_RANGE_1 = new Color(94, 128, 94);
    public final static Color FOLDED_STACK_COLOR_RANGE_2 = new Color(135, 165, 126);
    public final static Color DEALER_BUTTON_COLOR_RANGE_1 = new Color(195, 0, 0);
    public final static Color DEALER_BUTTON_COLOR_RANGE_2 = new Color(245, 116, 212);
    // Table green scale:
    // R 1 - 2
    // G 56 - 115
    // B 18 - 37
    public final static Color TABLE_COLOR_RANGE_1 = new Color(1, 50, 14);
    public final static Color TABLE_COLOR_RANGE_2 = new Color(10, 140, 50);
    public final static int FLOP_CARD_READING_POINT_Y_PS_ZOOM = 169;

    public static final String POT_NUMBERS_FOLDER = "src/main/resources/PS_new_skin\\pot_numbers\\";
    public static final String STACK_NUMBERS_FOLDER = "src/main/resources/PS_new_skin\\stack_numbers\\";
    public static final String STACK_FOLDED_NUMBERS_FOLDER = "src/main/resources/PS_new_skin\\stack_numbers\\folded\\";
    public static final String STACK_HERO_NUMBERS_FOLDER = "src/main/resources/PS_new_skin\\stack_numbers\\hero\\";
    public static final String CARDS_FOLDER = "src/main/resources/PS_new_skin/cards/";
    public static final String CARD_REFERENCE_FOLDER = "C:\\Users\\compuuter\\sources\\Pkrapp\\src\\test\\resources\\PS_new_skin\\card_screenshots\\";
    private static final String ZERO_IMAGE_FILE = "0.png";
    private static final String ONE_IMAGE_FILE = "1.png";
    private static final String TWO_IMAGE_FILE = "2.png";
    private static final String THREE_IMAGE_FILE = "3.png";
    private static final String FOUR_IMAGE_FILE = "4.png";
    private static final String FIVE_IMAGE_FILE = "5.png";
    private static final String SIX_IMAGE_FILE = "6.png";
    private static final String SEVEN_IMAGE_FILE = "7.png";
    private static final String EIGHT_IMAGE_FILE = "8.png";
    private static final String NINE_IMAGE_FILE = "9.png";
    private static final String DOT_IMAGE_FILE = "dot.png";
    private static final String DOLLAR_IMAGE_FILE = "dollar.png";

    public static final String PNG = ".png";
    public static final BufferedImage TWO_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TWO_SPADE.toString() + PNG);
    public static final BufferedImage THREE_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + THREE_SPADE.toString() + PNG);
    public static final BufferedImage FOUR_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FOUR_SPADE.toString() + PNG);
    public static final BufferedImage FIVE_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FIVE_SPADE.toString() + PNG);
    public static final BufferedImage SIX_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SIX_SPADE.toString() + PNG);
    public static final BufferedImage SEVEN_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SEVEN_SPADE.toString() + PNG);
    public static final BufferedImage EIGHT_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + EIGHT_SPADE.toString() + PNG);
    public static final BufferedImage NINE_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + NINE_SPADE.toString() + PNG);
    public static final BufferedImage TEN_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TEN_SPADE.toString() + PNG);
    public static final BufferedImage JACK_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + JACK_SPADE.toString() + PNG);
    public static final BufferedImage QUEEN_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + QUEEN_SPADE.toString() + PNG);
    public static final BufferedImage KING_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + KING_SPADE.toString() + PNG);
    public static final BufferedImage ACE_SPADE_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + ACE_SPADE.toString() + PNG);

    public static final BufferedImage TWO_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TWO_HEART.toString() + PNG);
    public static final BufferedImage THREE_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + THREE_HEART.toString() + PNG);
    public static final BufferedImage FOUR_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FOUR_HEART.toString() + PNG);
    public static final BufferedImage FIVE_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FIVE_HEART.toString() + PNG);
    public static final BufferedImage SIX_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SIX_HEART.toString() + PNG);
    public static final BufferedImage SEVEN_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SEVEN_HEART.toString() + PNG);
    public static final BufferedImage EIGHT_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + EIGHT_HEART.toString() + PNG);
    public static final BufferedImage NINE_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + NINE_HEART.toString() + PNG);
    public static final BufferedImage TEN_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TEN_HEART.toString() + PNG);
    public static final BufferedImage JACK_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + JACK_HEART.toString() + PNG);
    public static final BufferedImage QUEEN_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + QUEEN_HEART.toString() + PNG);
    public static final BufferedImage KING_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + KING_HEART.toString() + PNG);
    public static final BufferedImage ACE_HEART_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + ACE_HEART.toString() + PNG);

    public static final BufferedImage TWO_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TWO_CLUB.toString() + PNG);
    public static final BufferedImage THREE_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + THREE_CLUB.toString() + PNG);
    public static final BufferedImage FOUR_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FOUR_CLUB.toString() + PNG);
    public static final BufferedImage FIVE_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FIVE_CLUB.toString() + PNG);
    public static final BufferedImage SIX_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SIX_CLUB.toString() + PNG);
    public static final BufferedImage SEVEN_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SEVEN_CLUB.toString() + PNG);
    public static final BufferedImage EIGHT_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + EIGHT_CLUB.toString() + PNG);
    public static final BufferedImage NINE_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + NINE_CLUB.toString() + PNG);
    public static final BufferedImage TEN_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TEN_CLUB.toString() + PNG);
    public static final BufferedImage JACK_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + JACK_CLUB.toString() + PNG);
    public static final BufferedImage QUEEN_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + QUEEN_CLUB.toString() + PNG);
    public static final BufferedImage KING_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + KING_CLUB.toString() + PNG);
    public static final BufferedImage ACE_CLUB_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + ACE_CLUB.toString() + PNG);

    public static final BufferedImage TWO_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TWO_DIAMOND.toString() + PNG);
    public static final BufferedImage THREE_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + THREE_DIAMOND.toString() + PNG);
    public static final BufferedImage FOUR_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FOUR_DIAMOND.toString() + PNG);
    public static final BufferedImage FIVE_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + FIVE_DIAMOND.toString() + PNG);
    public static final BufferedImage SIX_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SIX_DIAMOND.toString() + PNG);
    public static final BufferedImage SEVEN_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + SEVEN_DIAMOND.toString() + PNG);
    public static final BufferedImage EIGHT_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + EIGHT_DIAMOND.toString() + PNG);
    public static final BufferedImage NINE_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + NINE_DIAMOND.toString() + PNG);
    public static final BufferedImage TEN_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + TEN_DIAMOND.toString() + PNG);
    public static final BufferedImage JACK_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + JACK_DIAMOND.toString() + PNG);
    public static final BufferedImage QUEEN_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + QUEEN_DIAMOND.toString() + PNG);
    public static final BufferedImage KING_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + KING_DIAMOND.toString() + PNG);
    public static final BufferedImage ACE_DIAMOND_IMAGE = ImageUtil.getImageResource(CARDS_FOLDER + ACE_DIAMOND.toString() + PNG);

    public static final BufferedImage POT_ZERO_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + ZERO_IMAGE_FILE);
    public static final BufferedImage POT_ONE_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + ONE_IMAGE_FILE);
    public static final BufferedImage POT_TWO_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + TWO_IMAGE_FILE);
    public static final BufferedImage POT_THREE_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + THREE_IMAGE_FILE);
    public static final BufferedImage POT_FOUR_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + FOUR_IMAGE_FILE);
    public static final BufferedImage POT_FIVE_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + FIVE_IMAGE_FILE);
    public static final BufferedImage POT_SIX_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + SIX_IMAGE_FILE);
    public static final BufferedImage POT_SEVEN_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + SEVEN_IMAGE_FILE);
    public static final BufferedImage POT_EIGHT_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + EIGHT_IMAGE_FILE);
    public static final BufferedImage POT_NINE_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + NINE_IMAGE_FILE);
    public static final BufferedImage POT_DOT_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + DOT_IMAGE_FILE);
    public static final BufferedImage POT_DOLLAR_IMAGE = ImageUtil.getImageResource(POT_NUMBERS_FOLDER + DOLLAR_IMAGE_FILE);

    public static final BufferedImage STACK_ZERO_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + ZERO_IMAGE_FILE);
    public static final BufferedImage STACK_ONE_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + ONE_IMAGE_FILE);
    public static final BufferedImage STACK_TWO_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + TWO_IMAGE_FILE);
    public static final BufferedImage STACK_THREE_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + THREE_IMAGE_FILE);
    public static final BufferedImage STACK_FOUR_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + FOUR_IMAGE_FILE);
    public static final BufferedImage STACK_FIVE_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + FIVE_IMAGE_FILE);
    public static final BufferedImage STACK_SIX_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + SIX_IMAGE_FILE);
    public static final BufferedImage STACK_SEVEN_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + SEVEN_IMAGE_FILE);
    public static final BufferedImage STACK_EIGHT_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + EIGHT_IMAGE_FILE);
    public static final BufferedImage STACK_NINE_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + NINE_IMAGE_FILE);
    public static final BufferedImage STACK_DOT_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + DOT_IMAGE_FILE);
    public static final BufferedImage STACK_DOLLAR_IMAGE = ImageUtil.getImageResource(STACK_NUMBERS_FOLDER + DOLLAR_IMAGE_FILE);

    public static final BufferedImage STACK_FOLDED_ZERO_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + ZERO_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_ONE_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + ONE_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_TWO_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + TWO_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_THREE_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + THREE_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_FOUR_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + FOUR_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_FIVE_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + FIVE_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_SIX_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + SIX_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_SEVEN_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + SEVEN_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_EIGHT_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + EIGHT_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_NINE_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + NINE_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_DOT_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + DOT_IMAGE_FILE);
    public static final BufferedImage STACK_FOLDED_DOLLAR_IMAGE = ImageUtil.getImageResource(STACK_FOLDED_NUMBERS_FOLDER + DOLLAR_IMAGE_FILE);

    public static final BufferedImage STACK_HERO_ZERO_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + ZERO_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_ONE_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + ONE_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_TWO_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + TWO_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_THREE_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + THREE_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_FOUR_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + FOUR_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_FIVE_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + FIVE_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_SIX_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + SIX_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_SEVEN_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + SEVEN_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_EIGHT_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + EIGHT_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_NINE_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + NINE_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_DOT_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + DOT_IMAGE_FILE);
    public static final BufferedImage STACK_HERO_DOLLAR_IMAGE = ImageUtil.getImageResource(STACK_HERO_NUMBERS_FOLDER + DOLLAR_IMAGE_FILE);

    public static final BufferedImage FOLDED_TEXT_IMAGE = ImageUtil.getImageResource("src/main/resources/PS_new_skin/fold_text/f.png");
}
