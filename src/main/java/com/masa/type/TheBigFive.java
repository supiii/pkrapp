package com.masa.type;

/**
 *
 * @author compuuter
 */
public class TheBigFive {

    public boolean inPosition;
    public double rangeAdvantage;
    public double rawAdvantage;
    public EquityBucket villainEquityBucket;
    public double vulnerability;
    public double effectiveStacksBB;
}
