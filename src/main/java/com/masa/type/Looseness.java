package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum Looseness {
    VERY_LOOSE,
    LOOSE,
    NORMAL,
    TIGHT,
    VERY_TIGHT
}
