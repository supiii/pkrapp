package com.masa.type;

import com.masa.pokertracker.model.NextMove;
import com.masa.pokertracker.model.PTActionType;

/**
 *
 * @author compuuter
 */
public class NextPostFlopMoves {

    private NextMove foldMoves;
    private NextMove checkMoves;
    private NextMove callMoves;
    private NextMove raiseMoves;

    private NextMove foldMovesMadeHand;
    private NextMove checkMovesMadeHand;
    private NextMove callMovesMadeHand;
    private NextMove raiseMovesMadeHand;

    private NextMove checkMovesWithoutSD;
    private NextMove callMovesWithoutSD;
    private NextMove raiseMovesWithoutSD;

    private int countAverageEquity;
    private double totalEquity;
    private double averageEquity;

    public NextPostFlopMoves() {
        foldMoves = new NextMove(PTActionType.FOLD);
        checkMoves = new NextMove(PTActionType.CHECK);
        callMoves = new NextMove(PTActionType.CALL);
        raiseMoves = new NextMove(PTActionType.RAISE);

        foldMovesMadeHand = new NextMove(PTActionType.FOLD);
        checkMovesMadeHand = new NextMove(PTActionType.CHECK);
        callMovesMadeHand = new NextMove(PTActionType.CALL);
        raiseMovesMadeHand = new NextMove(PTActionType.RAISE);

        checkMovesWithoutSD = new NextMove(PTActionType.CHECK);
        callMovesWithoutSD = new NextMove(PTActionType.CALL);
        raiseMovesWithoutSD = new NextMove(PTActionType.RAISE);

        countAverageEquity = 0;
        averageEquity = 0.0;
        totalEquity = 0.0;
    }

    /*public int getCountAverageEquity() {
        return countAverageEquity;
    }

    public void setCountAverageEquity(int countAverageEquity) {
        this.countAverageEquity = countAverageEquity;
    }

    public double getAverageEquity() {
        return averageEquity;
    }

    public void setAverageEquity(double averageEquity) {
        this.averageEquity = averageEquity;
    }

    public void addEquity(double equity) {
        countAverageEquity++;
        totalEquity = totalEquity + equity;
        averageEquity = totalEquity / countAverageEquity;
    }

    public NextMove getCheckMovesWithoutSD() {
        return checkMovesWithoutSD;
    }

    public NextMove getCallMovesWithoutSD() {
        return callMovesWithoutSD;
    }

    public NextMove getRaiseMovesWithoutSD() {
        return raiseMovesWithoutSD;
    }*/
    public NextMove getFoldMoves() {
        return foldMoves;
    }

    public NextMove getCheckMoves() {
        return checkMoves;
    }

    public NextMove getCallMoves() {
        return callMoves;
    }

    public NextMove getRaiseMoves() {
        return raiseMoves;
    }

    /*public NextMove getFoldMovesMadeHand() {
        return foldMovesMadeHand;
    }

    public NextMove getCheckMovesMadeHand() {
        return checkMovesMadeHand;
    }

    public NextMove getCallMovesMadeHand() {
        return callMovesMadeHand;
    }

    public NextMove getRaiseMovesMadeHand() {
        return raiseMovesMadeHand;
    }*/
}
