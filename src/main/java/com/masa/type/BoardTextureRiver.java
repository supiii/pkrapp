package com.masa.type;

/**
 *
 * @author compuuter
 */
public class BoardTextureRiver {

    public boolean aceRiver;
    public boolean kingRiver;
    public boolean riverCardIsHighest;
    public boolean riverCardIsSecondHighest;
    public boolean riverCardIsThirdeHighest;
    public int nroOfSameSuitsRiver;
    public boolean boardHasPairRiver;
    public boolean boardHasTwoPairsRiver;
    public boolean boardHasTriplesRiver;
    public boolean boardHasFullhouseRiver;
    public boolean straightPossibleWithOneCardRiver;
    public boolean straightPossibleWithTwoCardsRiver;
    public boolean straightOnBoardRiver;

    public BoardTextureRiver(PostFlopRow postflopRow) {
        this.aceRiver = postflopRow.aceRiver;
        this.kingRiver = postflopRow.kingRiver;
        this.riverCardIsHighest = postflopRow.riverCardIsHighest;
        this.riverCardIsSecondHighest = postflopRow.riverCardIsSecondHighest;
        this.riverCardIsThirdeHighest = postflopRow.riverCardIsThirdeHighest;
        this.nroOfSameSuitsRiver = postflopRow.nroOfSameSuitsRiver;
        this.boardHasPairRiver = postflopRow.boardHasPairRiver;
        this.boardHasTwoPairsRiver = postflopRow.boardHasTwoPairsRiver;
        this.boardHasTriplesRiver = postflopRow.boardHasTriplesRiver;
        this.boardHasFullhouseRiver = postflopRow.boardHasFullhouseRiver;
        this.straightPossibleWithOneCardRiver = postflopRow.straightPossibleWithOneCardRiver;
        this.straightPossibleWithTwoCardsRiver = postflopRow.straightPossibleWithTwoCardsRiver;
        this.straightOnBoardRiver = postflopRow.straightOnBoardRiver;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.aceRiver ? 1 : 0);
        hash = 67 * hash + (this.kingRiver ? 1 : 0);
        hash = 67 * hash + (this.riverCardIsHighest ? 1 : 0);
        hash = 67 * hash + (this.riverCardIsSecondHighest ? 1 : 0);
        hash = 67 * hash + (this.riverCardIsThirdeHighest ? 1 : 0);
        hash = 67 * hash + this.nroOfSameSuitsRiver;
        hash = 67 * hash + (this.boardHasPairRiver ? 1 : 0);
        hash = 67 * hash + (this.boardHasTwoPairsRiver ? 1 : 0);
        hash = 67 * hash + (this.boardHasTriplesRiver ? 1 : 0);
        hash = 67 * hash + (this.boardHasFullhouseRiver ? 1 : 0);
        hash = 67 * hash + (this.straightPossibleWithOneCardRiver ? 1 : 0);
        hash = 67 * hash + (this.straightPossibleWithTwoCardsRiver ? 1 : 0);
        hash = 67 * hash + (this.straightOnBoardRiver ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BoardTextureRiver other = (BoardTextureRiver) obj;
        if (this.aceRiver != other.aceRiver) {
            return false;
        }
        /*if (this.kingRiver != other.kingRiver) {
            return false;
        }*/
        if (this.riverCardIsHighest != other.riverCardIsHighest) {
            return false;
        }
        /*if (this.riverCardIsSecondHighest != other.riverCardIsSecondHighest) {
            return false;
        }*/
 /*if (this.riverCardIsThirdeHighest != other.riverCardIsThirdeHighest) {
            return false;
        }*/

        if (this.nroOfSameSuitsRiver < 3 && other.nroOfSameSuitsRiver > 2) {
            return false;

        } else if (this.nroOfSameSuitsRiver != other.nroOfSameSuitsRiver) {
            return false;
        }

        if (this.boardHasPairRiver != other.boardHasPairRiver) {
            return false;
        }
        if (this.boardHasTwoPairsRiver != other.boardHasTwoPairsRiver) {
            return false;
        }
        if (this.boardHasTriplesRiver != other.boardHasTriplesRiver) {
            return false;
        }
        if (this.boardHasFullhouseRiver != other.boardHasFullhouseRiver) {
            return false;
        }
        if (this.straightPossibleWithOneCardRiver != other.straightPossibleWithOneCardRiver) {
            return false;
        }
        if (this.straightPossibleWithTwoCardsRiver != other.straightPossibleWithTwoCardsRiver) {
            return false;
        }
        if (this.straightOnBoardRiver != other.straightOnBoardRiver) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BoardTextureRiver{" + "aceRiver=" + aceRiver + ", kingRiver=" + kingRiver + ", riverCardIsHighest=" + riverCardIsHighest + ", riverCardIsSecondHighest=" + riverCardIsSecondHighest + ", riverCardIsThirdeHighest=" + riverCardIsThirdeHighest + ", nroOfSameSuitsRiver=" + nroOfSameSuitsRiver + ", boardHasPairRiver=" + boardHasPairRiver + ", boardHasTwoPairsRiver=" + boardHasTwoPairsRiver + ", boardHasTriplesRiver=" + boardHasTriplesRiver + ", boardHasFullhouseRiver=" + boardHasFullhouseRiver + ", straightPossibleWithOneCardRiver=" + straightPossibleWithOneCardRiver + ", straightPossibleWithTwoCardsRiver=" + straightPossibleWithTwoCardsRiver + ", straightOnBoardRiver=" + straightOnBoardRiver + '}';
    }

}
