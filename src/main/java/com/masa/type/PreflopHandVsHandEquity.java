package com.masa.type;

/**
 *
 * @author compuuter
 */
public class PreflopHandVsHandEquity {

    public final String hand1;
    public final String hand2;
    public final double hand1Equity;
    public final double hand2Equity;

    public PreflopHandVsHandEquity(String hand1, String hand2, double hand1Equity, double hand2Equity) {
        this.hand1 = hand1;
        this.hand2 = hand2;
        this.hand1Equity = hand1Equity;
        this.hand2Equity = hand2Equity;
    }
}
