package com.masa.type;

import java.util.HashSet;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class BetRange {

    public Set<CardSet> value;
    public Set<CardSet> bluff;

    public BetRange() {
        this.value = new HashSet<>();
        this.bluff = new HashSet<>();
    }

    public BetRange(Set<CardSet> value, Set<CardSet> bluff) {
        this.value = value;
        this.bluff = bluff;
    }

    public double getBluffRatio() {
        double countAll = value.size() + bluff.size();
        double countBluff = bluff.size();

        return countBluff / countAll;
    }
}
