package com.masa.type;

import java.util.Objects;
import java.util.Set;

/**
 *
 * @author compuuter
 */
public class MadeHandSpecificAndDraw {

    public final MadeHandSpecific madeHandSpecific;
    public final Set<Draw> draws;

    public MadeHandSpecificAndDraw(MadeHandSpecific madeHandSpecific, Set<Draw> draws) {
        this.madeHandSpecific = madeHandSpecific;
        this.draws = draws;
    }

    @Override
    public String toString() {
        return "MadeHandSpecificAndDraw{" + "madeHandSpecific=" + madeHandSpecific + ", draws=" + draws + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.madeHandSpecific);
        hash = 29 * hash + Objects.hashCode(this.draws);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MadeHandSpecificAndDraw other = (MadeHandSpecificAndDraw) obj;
        if (this.madeHandSpecific != other.madeHandSpecific) {
            return false;
        }
        if (!Objects.equals(this.draws, other.draws)) {
            return false;
        }
        return true;
    }

}
