package com.masa.type;

/**
 *
 * @author Matti
 */
public enum RangeConnectivity {
    GOOD,
    MEDIUM,
    BAD
}
