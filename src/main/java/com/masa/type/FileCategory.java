package com.masa.type;

import java.util.Objects;

/**
 *
 * @author compuuter
 */
public class FileCategory {

    private final BoardTextureFlop boardTextureFlop;
    private final String position;
    private final String villainPosition;

    public FileCategory(BoardTextureFlop boardTextureFlop, String position, String villainPosition) {
        this.boardTextureFlop = boardTextureFlop;
        this.position = position;
        this.villainPosition = villainPosition;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.boardTextureFlop);
        hash = 67 * hash + Objects.hashCode(this.position);
        hash = 67 * hash + Objects.hashCode(this.villainPosition);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileCategory other = (FileCategory) obj;
        if (!Objects.equals(this.position, other.position)) {
            return false;
        }
        if (!Objects.equals(this.villainPosition, other.villainPosition)) {
            return false;
        }
        if (!Objects.equals(this.boardTextureFlop, other.boardTextureFlop)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return position + "_" + villainPosition + boardTextureFlop.toString();
    }

}
