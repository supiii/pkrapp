package com.masa.type;

/**
 * https://www.pokervip.com/strategy-articles/texas-hold-em-no-limit-advanced/flop-buckets
 *
 * @author compuuter
 */
public enum FlopSuitness {
    MONOTONE("Monotone"),
    TWO_TONE("Two tone"),
    RAINBOW("Rainbow");

    private final String value;

    private FlopSuitness(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
