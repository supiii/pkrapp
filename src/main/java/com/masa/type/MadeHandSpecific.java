package com.masa.type;

/**
 *
 * @author Matti
 */
public enum MadeHandSpecific {
    /*
    Hand buckets
    Strong hands (>75% equity in the hand)
    -top pairs and better
    Moderate hands (50%-75% equity in the hand)

    Weak hands (33%-50% equity in the hand)
    Trash hands (<33% equity in the hand)


     */

    NO_MADE_HAND("No hand"),
    LOW_HIGH("Low high"),
    MEDIUM_HIGH("Medium high"),
    JACK_HIGH("Jack high"),
    QUEEN_HIGH("Queen high"),
    KING_HIGH("King high"),
    ACE_HIGH("Ace high"),
    WEAK_PAIR("Weak pair"),
    MIDDLE_PAIR("Middle pair"),
    PP_BELOW_TP("PP below TP"),
    TOP_PAIR("Top pair"),
    TOP_PAIR_LOW_KICKER("Top pair_low_kicker"),
    TOP_PAIR_MEDIUM_KICKER("Top pair_medium_kicker"),
    TOP_PAIR_JACK_KICKER("Top pair_jack_kicker"),
    TOP_PAIR_QUEEN_KICKER("Top pair_queen_kicker"),
    TOP_PAIR_KING_KICKER("Top pair_king_kicker"),
    TOP_PAIR_ACE_KICKER("Top pair_ace_kicker"),
    OVERPAIR("Overpair"),
    TWO_PAIR("Two pair"),
    SET("Set"),
    THREE_OF_A_KIND("Three of a kind"),
    STRAIGHT("Straight"),
    FLUSH("Flush"),
    FLUSH_NUT("Nut flush"),
    FLUSH_2ND_NUT("2nd nut flush"),
    FLUSH_3RD_NUT("3rd nut flush"),
    FLUSH_4TH_NUT("4th nut flush"),
    FLUSH_WEAK("Weak flush"),
    FULL_HOUSE("Fullhouse"),
    FULL_HOUSE_OVERPAIR("Fullhouse overpair"),
    FULL_HOUSE_UNDERPAIR("Fullhouse underpair"),
    FULL_HOUSE_SET("Fullhouse set"),
    FULL_HOUSE_TP("Fullhouse TP"),
    FULL_HOUSE_MIDDLE_PAIR("Fullhouse Middle pair"),
    FULL_HOUSE_WEAK_PAIR("Fullhouse weak pair"),
    FULL_HOUSE_PP_BELOW_TP("Fullhouse PP below TP"),
    FULL_HOUSE_PP_BELOW_TRIPS("Fullhouse PP below trips"),
    FULL_HOUSE_AA("AA"),
    FULL_HOUSE_KK("KK"),
    FULL_HOUSE_QQ("QQ"),
    FULL_HOUSE_JJ("JJ"),
    FULL_HOUSE_TT("TT"),
    FULL_HOUSE_66_99("66-99"),
    FULL_HOUSE_22_55("22-55"),
    FULL_HOUSE_TOP("Fullhouse top"),
    FULL_HOUSE_STRONG("Fullhouse strong"),
    FULL_HOUSE_BOTTOM("Fullhouse bottom"),
    FULL_HOUSE_OTHER("Fullhouse other"),
    QUADS("Quads"),
    STRAIGHT_FLUSH("Straight flush");

    private String value;

    private MadeHandSpecific(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
