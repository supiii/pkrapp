package com.masa.type;

/**
 *
 * @author compuuter
 */
public class BoardTextureTurn {

    public boolean aceTurn;
    public boolean kingTurn;
    public boolean turnCardIsHighest;
    public boolean turnCardIsSecondHighest;
    public boolean turnCardIsThirdHighest;
    public int nroOfSameSuitsTurn;
    public boolean boardHasPairTurn;
    public boolean boardHasTwoPairsTurn;
    public boolean boardHasTriplesTurn;
    public boolean straightPossibleWithOneCardTurn;
    public boolean straightPossibleWithTwoCardsTurn;

    public BoardTextureTurn(PostFlopRow postflopRow) {
        this.aceTurn = postflopRow.aceTurn;
        this.kingTurn = postflopRow.kingTurn;
        this.turnCardIsHighest = postflopRow.turnCardIsHighest;
        this.turnCardIsSecondHighest = postflopRow.turnCardIsSecondHighest;
        this.turnCardIsThirdHighest = postflopRow.turnCardIsThirdHighest;
        this.nroOfSameSuitsTurn = postflopRow.nroOfSameSuitsTurn;
        this.boardHasPairTurn = postflopRow.boardHasPairTurn;
        this.boardHasTwoPairsTurn = postflopRow.boardHasTwoPairsTurn;
        this.boardHasTriplesTurn = postflopRow.boardHasTriplesTurn;
        this.straightPossibleWithOneCardTurn = postflopRow.straightPossibleWithOneCardTurn;
        this.straightPossibleWithTwoCardsTurn = postflopRow.straightPossibleWithTwoCardsTurn;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.aceTurn ? 1 : 0);
        hash = 97 * hash + (this.kingTurn ? 1 : 0);
        hash = 97 * hash + (this.turnCardIsHighest ? 1 : 0);
        hash = 97 * hash + (this.turnCardIsSecondHighest ? 1 : 0);
        hash = 97 * hash + (this.turnCardIsThirdHighest ? 1 : 0);
        hash = 97 * hash + this.nroOfSameSuitsTurn;
        hash = 97 * hash + (this.boardHasPairTurn ? 1 : 0);
        hash = 97 * hash + (this.boardHasTwoPairsTurn ? 1 : 0);
        hash = 97 * hash + (this.boardHasTriplesTurn ? 1 : 0);
        hash = 97 * hash + (this.straightPossibleWithOneCardTurn ? 1 : 0);
        hash = 97 * hash + (this.straightPossibleWithTwoCardsTurn ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BoardTextureTurn other = (BoardTextureTurn) obj;
        if (this.aceTurn != other.aceTurn) {
            return false;
        }
        /*if (this.kingTurn != other.kingTurn) {
            return false;
        }*/
        if (this.turnCardIsHighest != other.turnCardIsHighest) {
            return false;
        }
        if (this.turnCardIsSecondHighest != other.turnCardIsSecondHighest) {
            return false;
        }
        /*if (this.turnCardIsThirdHighest != other.turnCardIsThirdHighest) {
            return false;
        }*/
        if (this.nroOfSameSuitsTurn != other.nroOfSameSuitsTurn) {
            return false;
        }
        if (this.boardHasPairTurn != other.boardHasPairTurn) {
            return false;
        }
        if (this.boardHasTwoPairsTurn != other.boardHasTwoPairsTurn) {
            return false;
        }
        if (this.boardHasTriplesTurn != other.boardHasTriplesTurn) {
            return false;
        }
        if (this.straightPossibleWithOneCardTurn != other.straightPossibleWithOneCardTurn) {
            return false;
        }
        /*if (this.straightPossibleWithTwoCardsTurn != other.straightPossibleWithTwoCardsTurn) {
            return false;
        }*/
        return true;
    }

}
