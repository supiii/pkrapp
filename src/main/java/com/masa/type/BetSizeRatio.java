package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum BetSizeRatio {
    ONE_THIRD("1/3"),
    TWO_THIRDS("2/3"),
    POT("Pot");

    private String value;

    private BetSizeRatio(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
