package com.masa.type;

/**
 *
 * @author Matti
 */
public enum TableType {
    CASH_6_MAX,
    CASH_6_MAX_ZOOM,
    CASH_9_MAX,
    TOURNAMENT_6_MAX,
    TOURNAMENT_9MAX,
    SPIN_AND_GO
}
