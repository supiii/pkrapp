package com.masa.type;

/**
 *
 * @author Matti
 */
public enum NLLimit {
    NL2,
    NL5,
    NL10,
    NL25,
    NL50,
    NL100,
    NL200
}
