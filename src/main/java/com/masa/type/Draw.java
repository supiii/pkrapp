package com.masa.type;

/**
 *
 * @author Matti
 */
public enum Draw {
    NO_DRAW("No draw"),
    OVERCARDS("Overcards"),
    //TODO:
    FLUSH_DRAW_BACKDOOR("BD flush"),
    Gutshot("Straight 4 outs"),
    OESD("Straight 8 outs"),
    FLUSH_DRAW("Flush"),
    FLUSH_DRAW_2ND_NUT("2nd nut flushdraw"),
    FLUSH_DRAW_3RD_NUT("3rd nut flushdraw"),
    FLUSH_DRAW_WEAK("Weak flushdraw"),
    FLUSH_DRAW_NUT("Nut flushdraw");

    private final String value;

    private Draw(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
