package com.masa.type;

/**
 *
 * @author compuuter
 */
public enum MadeHand {
    NO_MADE_HAND, PAIR, TWO_PAIR, THREE_OF_A_KIND, STRAIGHT,
    FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH;
}
