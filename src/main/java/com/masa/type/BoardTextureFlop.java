package com.masa.type;

/**
 *
 * @author compuuter
 */
public class BoardTextureFlop {

    public boolean aceFlop;
    public boolean kingFlop;
    public int nroOfSameSuitsFlop;
    public int nroOfSameRanksFlop;
    public boolean straightPossibleFlop;
    public boolean noCardsBetweenFlop;
    public boolean oneCardBetweenFlop;
    public int ranksAddedValueCategoryId;
    public int ranksAddedValueFlop;

    public BoardTextureFlop(PostFlopRow postflopRow) {
        this.aceFlop = postflopRow.aceFlop;
        this.kingFlop = postflopRow.kingFlop;
        this.nroOfSameSuitsFlop = postflopRow.nroOfSameSuitsFlop;
        this.nroOfSameRanksFlop = postflopRow.nroOfSameRanksFlop;
        this.straightPossibleFlop = postflopRow.straightPossibleFlop;
        this.noCardsBetweenFlop = postflopRow.noRanksOrOneRankBetweenFlop;
        this.oneCardBetweenFlop = postflopRow.twoOrThreeRanksBetweenFlop;
        this.ranksAddedValueCategoryId = postflopRow.ranksAddedValueCategoryId;
        this.ranksAddedValueFlop = postflopRow.ranksAddedValueFlop;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.aceFlop ? 1 : 0);
        hash = 37 * hash + (this.kingFlop ? 1 : 0);
        hash = 37 * hash + this.nroOfSameSuitsFlop;
        hash = 37 * hash + this.nroOfSameRanksFlop;
        hash = 37 * hash + (this.straightPossibleFlop ? 1 : 0);
        hash = 37 * hash + (this.noCardsBetweenFlop ? 1 : 0);
        hash = 37 * hash + (this.oneCardBetweenFlop ? 1 : 0);
        hash = 37 * hash + this.ranksAddedValueCategoryId;
        hash = 37 * hash + this.ranksAddedValueFlop;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BoardTextureFlop other = (BoardTextureFlop) obj;
        if (this.aceFlop != other.aceFlop) {
            return false;
        }
        if (this.kingFlop != other.kingFlop) {
            return false;
        }
        if (this.nroOfSameSuitsFlop != other.nroOfSameSuitsFlop) {
            return false;
        }
        if (this.nroOfSameRanksFlop != other.nroOfSameRanksFlop) {
            return false;
        }
        if (this.straightPossibleFlop != other.straightPossibleFlop) {
            return false;
        }
        if (this.noCardsBetweenFlop != other.noCardsBetweenFlop) {
            return false;
        }
        if (this.oneCardBetweenFlop != other.oneCardBetweenFlop) {
            return false;
        }
        /*if (this.ranksAddedValueCategoryId != other.ranksAddedValueCategoryId) {
            return false;
        }*/
        //return equalsPlusMinus(this.ranksAddedValueFlop, other.ranksAddedValueFlop, 2);

        return true;
    }

    private boolean equalsPlusMinus(int value1, int value2, int plusMinus) {
        int diff = Math.abs(value1 - value2);

        return diff > plusMinus;
    }

    @Override
    public String toString() {
        return getIntString(aceFlop) + "_"
                + getIntString(kingFlop) + "_"
                + nroOfSameSuitsFlop + "_"
                + nroOfSameRanksFlop + "_"
                + getIntString(straightPossibleFlop) + "_"
                + getIntString(noCardsBetweenFlop) + "_"
                + getIntString(oneCardBetweenFlop) + "_"
                + ranksAddedValueCategoryId;
    }

    // ace=0 king=0 samesuits=2 nroOfSameRanksFlop=1 straightPossibleFlop=0 noRanksOrOneRankBetweenFlop=0 twoOrThreeRanksBetweenFlop=0
    //
    private String getIntString(boolean b) {
        return b ? "1" : "0";
    }

}
