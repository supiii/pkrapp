package com.masa.component;

import static com.masa.component.BoardCards.createBoardCards;
import static com.masa.component.CardButtons.createCardButtons;
import static com.masa.component.GlobalActions.createGlobalActions;
import static com.masa.component.ReadTableButton.createReadButton;
import static com.masa.component.SeatButtongGroup.createHeroSeatButtonGroup;
import static com.masa.component.SeatButtongGroup.createSeatButtonGroup;
import com.masa.pkrapp.App;
import static com.masa.pkrapp.App.cardsRows;
import com.masa.type.HUDStat;
import com.masa.type.Seat;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author compuuter
 */
public class TablePanel {

    public static GridPane createTablePanel(Stage stage) {
        GridPane gridPane = new GridPane();
        gridPane.setOpacity(50.00d);
        cardsRows = createCardButtons();

        Button readButton = createReadButton();
        HBox statControlRow = new HBox(createStatsControl(HUDStat.PREFLOP_VPIP), createStatsControl(HUDStat.PREFLOP_PFR), createStatsControl(HUDStat.PREFLOP_3BET));

        //gridPane.add(resolvedActionLabel, 3, 1);
        //gridPane.add(createInfoPanel(), 3, 0);
        gridPane.add(createSeatButtonGroup(Seat.ONE), 1, 0);
        gridPane.add(createBoardCards(), 1, 1, 1, 2);
        gridPane.add(createSeatButtonGroup(Seat.TWO), 2, 1);
        gridPane.add(createSeatButtonGroup(Seat.THREE), 2, 2);
        gridPane.add(createHeroSeatButtonGroup(), 1, 3);
        gridPane.add(createGlobalActions(stage), 2, 3);
        gridPane.add(createSeatButtonGroup(Seat.FIVE), 0, 2);
        gridPane.add(createSeatButtonGroup(Seat.SIX), 0, 1);
        gridPane.add(statControlRow, 0, 4, 4, 1);
        //gridPane.add(cardsRows, 0, 4, 4, 1);
        gridPane.add(readButton, 0, 3);
        //gridPane.setStyle("-fx-grid-lines-visible: true");
        gridPane.setHgap(40);
        gridPane.setVgap(20);

        return gridPane;
    }

    private static HBox createStatsControl(HUDStat hudStat) {
        Label statLabel;
        if (hudStat == HUDStat.PREFLOP_VPIP) {
            statLabel = new Label("" + App.villainVPIP);
        } else if (hudStat == HUDStat.PREFLOP_PFR) {
            statLabel = new Label("" + App.villainPFR);
        } else {
            statLabel = new Label("" + App.villain3Bet);
        }
        statLabel.setFont(Font.font("Arial", 18));
        Button minusButton = new Button("-");
        minusButton.getStyleClass().add("stat-button");
        minusButton.setOnAction((ActionEvent event) -> {
            int value = 0;
            if (hudStat == HUDStat.PREFLOP_VPIP) {
                value = App.villainVPIP - 1;
                App.villainVPIP = value;
            } else if (hudStat == HUDStat.PREFLOP_PFR) {
                value = App.villainPFR - 1;
                App.villainPFR = value;
            } else if (hudStat == HUDStat.PREFLOP_3BET) {
                value = App.villain3Bet - 1;
                App.villain3Bet = value;
            }
            statLabel.setText("" + value);

        });
        Button addButton = new Button("+");
        addButton.getStyleClass().add("stat-button");
        addButton.setOnAction((ActionEvent event) -> {
            int value = 0;
            if (hudStat == HUDStat.PREFLOP_VPIP) {
                value = App.villainVPIP + 1;
                App.villainVPIP = value;
            } else if (hudStat == HUDStat.PREFLOP_PFR) {
                value = App.villainPFR + 1;
                App.villainPFR = value;
            } else if (hudStat == HUDStat.PREFLOP_3BET) {
                value = App.villain3Bet + 1;
                App.villain3Bet = value;
            }
            statLabel.setText("" + value);
        });
        HBox statControl = new HBox(minusButton, statLabel, addButton);
        statControl.setPadding(new Insets(0, 0, -16, 22));

        return statControl;
    }
}
