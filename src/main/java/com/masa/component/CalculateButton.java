package com.masa.component;

import static com.masa.pkrapp.App.villainContinueRangeSlider;
import static com.masa.util.Util.calculate;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

/**
 *
 * @author compuuter
 */
public class CalculateButton {

    public static Button createCalculateButton() {
        Button calculateButton = new Button("Calculate");
        calculateButton.getStyleClass().add("calculate-button");
        calculateButton.setOnAction((ActionEvent event) -> {
            calculate();

            System.out.println("Villain cont range val: " + villainContinueRangeSlider.getValue());
        });

        return calculateButton;
    }
}
