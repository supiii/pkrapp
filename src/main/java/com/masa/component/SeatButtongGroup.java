package com.masa.component;

import static com.masa.pkrapp.App.dealerGroup;
import static com.masa.pkrapp.App.dealerSeat;
import static com.masa.pkrapp.App.heroPos;
import static com.masa.pkrapp.App.leftHoleCardImgView;
import static com.masa.pkrapp.App.preflopAggressors;
import static com.masa.pkrapp.App.rightHoleCardImgView;
import static com.masa.pkrapp.App.villainPos;
import com.masa.type.Seat;
import com.masa.util.Util;
import static com.masa.util.Util.calculate;
import static com.masa.util.Util.determinePos;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author compuuter
 */
public class SeatButtongGroup {

    public static VBox createHeroSeatButtonGroup() {
        HBox holeCardsHBox = new HBox(leftHoleCardImgView, rightHoleCardImgView);
        holeCardsHBox.setPadding(new Insets(0, 0, -16, 22));
        return new VBox(holeCardsHBox, createSeatButtonGroup(Seat.FOUR));
    }

    public static HBox createSeatButtonGroup(Seat seat) {
        Button checkButton = new Button("X");
        checkButton.getStyleClass().add("action-button");
        checkButton.setOnAction((ActionEvent event) -> {
            System.out.println("Check pressed...");
            if (dealerSeat == null) {
                return;
            }
            Util.addAction("X", seat);
            if (!seat.equals(Seat.FOUR)) {
                if (villainPos == null) {
                    villainPos = determinePos(seat, dealerSeat);
                }
                Util.addAction("X", villainPos);
            }
            //getRanges();
            calculate();
        });
        Button callButton = new Button("C");
        callButton.getStyleClass().add("action-button");
        callButton.setOnAction((ActionEvent event) -> {
            System.out.println("Call pressed...");
            if (dealerSeat == null) {
                return;
            }
            Util.addAction("C", seat);
            if (!seat.equals(Seat.FOUR)) {
                villainPos = determinePos(seat, dealerSeat);
                Util.addAction("C", villainPos);
            }
            //getRanges();
        });
        Button raiseButton = new Button("R");
        raiseButton.getStyleClass().add("action-button");
        raiseButton.setOnAction((ActionEvent event) -> {
            if (dealerSeat == null) {
                return;
            }
            Util.addAction("R", seat);
            if (!seat.equals(Seat.FOUR)) {
                villainPos = determinePos(seat, dealerSeat);
                Util.addAction("R", villainPos);

            }
            switch (preflopAggressors.length()) {
                case 1:
                    raiseButton.getStyleClass().add("button-red");
                    break;
                case 2:
                    raiseButton.getStyleClass().add("button-purple");
                    break;
                case 3:
                    raiseButton.getStyleClass().add("button-yellow");
                    break;
                default:
                    break;
            }

            //getRanges();
            calculate();

        });
        ToggleButton dealerButton = new ToggleButton("D");
        dealerButton.setToggleGroup(dealerGroup);
        dealerButton.setId(seat.value());
        dealerButton.getStyleClass().add("dealer-button");
        dealerButton.setOnAction((ActionEvent event) -> {
            dealerSeat = seat;
            heroPos = determinePos(Seat.FOUR, dealerSeat);
            calculate();

        });
        if (seat == Seat.FOUR) {
            dealerButton.fire();
        }

        //dealerGroup.getToggles().add(dealerButton);
        HBox hBox = new HBox(checkButton, callButton, raiseButton, dealerButton);
        hBox.setSpacing(5);

        return hBox;
    }
}
