package com.masa.component;

import static com.masa.pkrapp.App.restart;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 *
 * @author compuuter
 */
public class RestartButton {

    public static Button createRestartButton(Stage stage) {
        Button restartButton = new Button("clear");
        restartButton.getStyleClass().add("restart-button");
        restartButton.setOnAction((ActionEvent event) -> {
            restart(stage);
        });

        return restartButton;
    }
}
