package com.masa.component;

import static com.masa.component.RestartButton.createRestartButton;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author compuuter
 */
public class GlobalActions {

    public static VBox createGlobalActions(Stage stage) {
        Button restartButton = createRestartButton(stage);
        //Button readButton = createReadButton();
        //Button calculateButton = createCalculateButton();

        VBox globalActions = new VBox(/*readButton, */restartButton);
        globalActions.setSpacing(5);

        return globalActions;
    }
}
