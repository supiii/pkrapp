package com.masa.component;

import static com.masa.component.CardButtons.createCardButtons;
import com.masa.pkrapp.App;
import static com.masa.pkrapp.App.cardsRows;
import static com.masa.pkrapp.App.equityVsContRangeFlop;
import static com.masa.pkrapp.App.equityVsContRangeRiver;
import static com.masa.pkrapp.App.equityVsContRangeTurn;
import static com.masa.pkrapp.App.nutAdvantageLabel;
import static com.masa.pkrapp.App.rangeEquityLabelFlop;
import static com.masa.pkrapp.App.rangeEquityLabelRiver;
import static com.masa.pkrapp.App.rangeEquityLabelTurn;
import static com.masa.pkrapp.App.rawEquityLabelFlop;
import static com.masa.pkrapp.App.rawEquityLabelRiver;
import static com.masa.pkrapp.App.rawEquityLabelTurn;
import static com.masa.pkrapp.App.resolvedActionLabel;
import static com.masa.pkrapp.App.villainNutAdvantageLabel;
import static com.masa.pkrapp.App.vulnerabilityLabel;
import com.masa.type.HUDStat;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.StringConverter;

/**
 *
 * @author compuuter
 */
public class InfoPanel {

    public static VBox createInfoPanel() {
        Slider villainTypeSlider = new Slider(0, 2, 0);
        villainTypeSlider.setMajorTickUnit(1.0);
        villainTypeSlider.setMinorTickCount(1);
        villainTypeSlider.setSnapToTicks(true);
        villainTypeSlider.setShowTickMarks(true);
        villainTypeSlider.setShowTickLabels(true);
        villainTypeSlider.setValue(1.0);
        villainTypeSlider.setLabelFormatter(new StringConverter<Double>() {
            @Override
            public String toString(Double n) {
                if (n < 0.5) {
                    return "Tight";
                }
                if (n < 1.5) {
                    return "Normal";
                }

                return "Loose";
            }

            @Override
            public Double fromString(String s) {
                switch (s) {
                    case "Tight":
                        return 0d;
                    case "Normal":
                        return 1d;
                    case "Loose":
                        return 2d;

                    default:
                        return 1d;
                }
            }
        });

        // VPIP
        Label villainVPIPLabel = new Label("" + App.villainVPIP);
        Button vpipMinusButton = new Button("-");
        //vpipAddButton.getStyleClass().add("restart-button");
        vpipMinusButton.setOnAction((ActionEvent event) -> {
            int value = App.villainVPIP - 1;
            App.villainVPIP = value;
            villainVPIPLabel.setText("" + value);
        });
        Button vpipAddButton = new Button("+");
        //vpipAddButton.getStyleClass().add("restart-button");
        vpipAddButton.setOnAction((ActionEvent event) -> {
            int value = App.villainVPIP + 1;
            App.villainVPIP = value;
            villainVPIPLabel.setText("" + value);
        });
        HBox vpipRow = new HBox(vpipMinusButton, villainVPIPLabel, vpipAddButton);

        // PFR
        Label villainPFRLabel = new Label("" + App.villainVPIP);
        Button pfrMinusButton = new Button("-");
        //vpipAddButton.getStyleClass().add("restart-button");
        pfrMinusButton.setOnAction((ActionEvent event) -> {
            int vpip = App.villainVPIP - 1;
            App.villainPFR = vpip;
            villainPFRLabel.setText("" + vpip);
        });
        Button pfrAddButton = new Button("+");
        //vpipAddButton.getStyleClass().add("restart-button");
        pfrAddButton.setOnAction((ActionEvent event) -> {
            int value = App.villainPFR + 1;
            App.villainPFR = value;
            villainPFRLabel.setText("" + value);
        });
        HBox pfrRow = new HBox(pfrMinusButton, villainPFRLabel, pfrAddButton);

        Label resolvedActionText = new Label("PF Action: ");
        resolvedActionText.setMinSize(90.0, 0);
        HBox resolvedActionRow = new HBox(resolvedActionText, resolvedActionLabel);
        resolvedActionRow.setAlignment(Pos.CENTER_LEFT);
        resolvedActionRow.setMinHeight(25.0);

        double dataColumnWidth = 80.0;

        Label empty = new Label(" ");
        empty.setMinSize(90.0, 0);
        Label flop = new Label("Flop");
        flop.setMinSize(dataColumnWidth, 0);
        Label turn = new Label("Turn");
        turn.setMinSize(dataColumnWidth, 0);
        Label river = new Label("River");
        river.setMinSize(dataColumnWidth, 0);
        HBox streetsRows = new HBox(empty, flop, turn, river);

        Label rawEquityText = new Label("Raw Equity: ");
        rawEquityText.setMinSize(90.0, 0);
        rawEquityLabelFlop.setMinSize(dataColumnWidth, 0);
        rawEquityLabelTurn.setMinSize(dataColumnWidth, 0);
        rawEquityLabelRiver.setMinSize(dataColumnWidth, 0);
        HBox rawEquityRow = new HBox(rawEquityText, rawEquityLabelFlop, rawEquityLabelTurn, rawEquityLabelRiver);

        Label rangeEquityText = new Label("Range Equity: ");
        rangeEquityText.setMinSize(90.0, 0);
        rangeEquityLabelFlop.setMinSize(dataColumnWidth, 0);
        rangeEquityLabelTurn.setMinSize(dataColumnWidth, 0);
        rangeEquityLabelRiver.setMinSize(dataColumnWidth, 0);
        HBox rangeEquityRow = new HBox(rangeEquityText, rangeEquityLabelFlop, rangeEquityLabelTurn, rangeEquityLabelRiver);

        Label nutAdvantageText = new Label("Nut advantage: ");
        nutAdvantageText.setMinSize(90.0, 0);
        HBox nutAdvantageRow = new HBox(nutAdvantageText, nutAdvantageLabel);

        Label villainNutAdvantageText = new Label("Villain Nut adv: ");
        villainNutAdvantageText.setMinSize(90.0, 0);
        HBox villainNutAdvantageRow = new HBox(villainNutAdvantageText, villainNutAdvantageLabel);

        Label villainConnectivityText = new Label("V. connectivity: ");
        villainConnectivityText.setMinSize(90.0, 0);
        HBox villainConnectivityTextRow = new HBox(villainConnectivityText, App.villainRangeConnectivityLabel);

        Label vulnerabilityText = new Label("Vulnerability: ");
        vulnerabilityText.setMinSize(90.0, 0);
        HBox vulnerabilityTextRow = new HBox(vulnerabilityText, vulnerabilityLabel);

        Label equityVsContRangeText = new Label("Eq vs c range");
        equityVsContRangeText.setMinSize(90.0, 0);
        equityVsContRangeFlop.setMinSize(dataColumnWidth, 0);
        equityVsContRangeTurn.setMinSize(dataColumnWidth, 0);
        equityVsContRangeRiver.setMinSize(dataColumnWidth, 0);
        HBox equityVsContRangeRow = new HBox(equityVsContRangeText,
                equityVsContRangeFlop, equityVsContRangeTurn, equityVsContRangeRiver);

        /*HBox rangeCheckboxes = new HBox();
        if (ranges.villainRangeCardSet != null) {
            rangeCheckboxes.getChildren().addAll(createVillainContinueRangeCheckboxes(), createVillainContinueDrawRangeCheckboxes());
        }*/
        //HBox statControlRow = new HBox(createStatsControl(HUDStat.PREFLOP_VPIP), createStatsControl(HUDStat.PREFLOP_PFR), createStatsControl(HUDStat.PREFLOP_3BET));
        cardsRows = createCardButtons();
        VBox infoPanel = new VBox(cardsRows/*, statControlRow, resolvedActionRow, streetsRows, rawEquityRow, rangeEquityRow,
                nutAdvantageRow, villainNutAdvantageRow, villainConnectivityTextRow,
                vulnerabilityTextRow, equityVsContRangeRow*/);

        VBox.setMargin(resolvedActionRow, new Insets(10, 0, 0, 0));

        return infoPanel;
    }

    private static HBox createStatsControl(HUDStat hudStat) {
        Label statLabel;
        if (hudStat == HUDStat.PREFLOP_VPIP) {
            statLabel = new Label("" + App.villainVPIP);
        } else if (hudStat == HUDStat.PREFLOP_PFR) {
            statLabel = new Label("" + App.villainPFR);
        } else {
            statLabel = new Label("" + App.villain3Bet);
        }
        statLabel.setFont(Font.font("Arial", 18));
        Button minusButton = new Button("-");
        minusButton.getStyleClass().add("stat-button");
        minusButton.setOnAction((ActionEvent event) -> {
            if (hudStat == HUDStat.PREFLOP_VPIP) {
                int value = 0;
                if (hudStat == HUDStat.PREFLOP_VPIP) {
                    value = App.villainVPIP - 1;
                    App.villainVPIP = value;
                } else if (hudStat == HUDStat.PREFLOP_PFR) {
                    value = App.villainPFR - 1;
                    App.villainPFR = value;
                } else if (hudStat == HUDStat.PREFLOP_3BET) {
                    value = App.villain3Bet - 1;
                    App.villain3Bet = value;
                }
                statLabel.setText("" + value);
            }
        });
        Button addButton = new Button("+");
        addButton.getStyleClass().add("stat-button");
        addButton.setOnAction((ActionEvent event) -> {
            int value = 0;
            if (hudStat == HUDStat.PREFLOP_VPIP) {
                value = App.villainVPIP + 1;
                App.villainVPIP = value;
            } else if (hudStat == HUDStat.PREFLOP_PFR) {
                value = App.villainPFR + 1;
                App.villainPFR = value;
            } else if (hudStat == HUDStat.PREFLOP_3BET) {
                value = App.villain3Bet + 1;
                App.villain3Bet = value;
            }
            statLabel.setText("" + value);
        });
        HBox statControl = new HBox(minusButton, statLabel, addButton);
        statControl.setPadding(new Insets(0, 0, -16, 22));

        return statControl;
    }
}
