package com.masa.component;

import static com.masa.pkrapp.App.board;
import static com.masa.pkrapp.App.df;
import static com.masa.pkrapp.App.drawCheckBoxValues;
import static com.masa.pkrapp.App.equityVsContRangeFlop;
import static com.masa.pkrapp.App.equityVsContRangeRiver;
import static com.masa.pkrapp.App.equityVsContRangeTurn;
import static com.masa.pkrapp.App.holeCards;
import static com.masa.pkrapp.App.ranges;
import com.masa.type.ComboCheckBox;
import com.masa.type.Draw;
import com.masa.util.DrawAnalyzer;
import com.masa.util.RangeUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class VillainContinueDrawRangeCheckboxes {

    public static VBox createVillainContinueDrawRangeCheckboxes() {
        createDrawCheckBoxValues(board, holeCards);
        Label label = new Label("V.Cont.draws");
        VBox.setMargin(label, new Insets(10, 0, 0, 0));
        VBox vBox = new VBox(label);
        List<CheckBox> checkBoxes = new ArrayList<>();

        for (Map.Entry<Draw, ComboCheckBox> entry : drawCheckBoxValues.entrySet()) {
            Draw key = entry.getKey();
            ComboCheckBox comboCheckBox = entry.getValue();
            String checkBoxLabel = comboCheckBox.label + "(" + comboCheckBox.combos + ")";
            CheckBox checkBox = new CheckBox(checkBoxLabel);
            checkBox.setSelected(comboCheckBox.checked);
            checkBox.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                        drawCheckBoxValues.get(key).checked = newValue;
                        double equity = RangeUtil.calculateEquityVsVillainContinueRange();
                        switch (board.size()) {
                            case 3:
                                equityVsContRangeFlop.setText("" + df.format(equity));
                                break;
                            case 4:
                                equityVsContRangeTurn.setText("" + df.format(equity));
                                break;
                            case 5:
                                equityVsContRangeRiver.setText("" + df.format(equity));
                                break;
                            default:
                                break;
                        }
                        System.out.println("Equity vs cont range: " + equity);
                    });

            checkBoxes.add(checkBox);
        }
        Collections.reverse(checkBoxes);
        vBox.getChildren().addAll(checkBoxes);

        return vBox;
    }

    public static void createDrawCheckBoxValues(CardSet board, CardSet holeCards) {
        // from all cards in deck. check MadeHandSpecific
        CardSet deck = CardSet.freshDeck();
        deck.removeAll(board);
        deck.removeAll(holeCards);

        // All possible combinations in deck
        Set<CardSet> allPossibleCombosInDeck = new HashSet<>();
        for (Card card1 : deck) {
            for (Card card2 : deck) {
                if (card1.equals(card2)) {
                    continue;
                }
                if (card1.rankOf().pipValue() > card2.rankOf().pipValue()) {
                    allPossibleCombosInDeck.add(new CardSet(card1, card2));
                } else {
                    allPossibleCombosInDeck.add(new CardSet(card2, card1));
                }

            }
        }

        Map<Draw, Integer> map = new HashMap<>();
        // Create map with empty values
        for (Draw value : Draw.values()) {
            map.put(value, 0);
        }
        // Fill map with values
        for (CardSet cardSet : allPossibleCombosInDeck) {
            //MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, cardSet);
            //map.put(madeHandSpecific, map.get(madeHandSpecific) + 1);

            Set<Draw> draws = DrawAnalyzer.getDraws(board, cardSet, holeCards);
            for (Draw draw : draws) {
                map.put(draw, map.get(draw) + 1);
            }
        }

        for (Map.Entry<Draw, Integer> entry : map.entrySet()) {
            Draw key = entry.getKey();
            Integer value = entry.getValue();

            if (value > 0) {
                ComboCheckBox checkbox = new ComboCheckBox(key.value(), false);
                drawCheckBoxValues.put(key, checkbox);
            }
        }

        Set<CardSet> villainRangeCopy = new HashSet<>(ranges.villainRangeCardSet);
        RangeUtil.removeHandsFromRangeThatContainCardFromGivenCardSet(villainRangeCopy, holeCards);

        for (CardSet cardSet : villainRangeCopy) {
            Set<Draw> draws = DrawAnalyzer.getDraws(board, cardSet, holeCards);
            for (Draw draw : draws) {
                drawCheckBoxValues.get(draw).combos++;
            }
        }
    }
}
