package com.masa.component;

import com.masa.pkrapp.App;
import static com.masa.pkrapp.App.dealerGroup;
import static com.masa.pkrapp.App.dealerSeat;
import static com.masa.pkrapp.App.firstFlopCard;
import static com.masa.pkrapp.App.firstFlopCardImgView;
import static com.masa.pkrapp.App.heroPos;
import static com.masa.pkrapp.App.holeCardLeft;
import static com.masa.pkrapp.App.holeCardRight;
import static com.masa.pkrapp.App.leftHoleCardImgView;
import static com.masa.pkrapp.App.rightHoleCardImgView;
import static com.masa.pkrapp.App.secondFlopCard;
import static com.masa.pkrapp.App.secondFlopCardImgView;
import static com.masa.pkrapp.App.thirdFlopCard;
import static com.masa.pkrapp.App.thirdFlopCardImgView;
import com.masa.screenreader.ImageSaver;
import com.masa.screenreader.ScreenReader;
import com.masa.screenreader.TableReader;
import com.masa.type.Seat;
import static com.masa.util.Util.cleanUp;
import static com.masa.util.Util.determinePos;
import java.awt.image.BufferedImage;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class ReadTableButton {

    private static final ScreenReader screenReader = new ScreenReader();
    private static final TableReader tableReader = new TableReader();

    public static Button createReadButton() {
        Button restartButton = new Button("Read");
        restartButton.getStyleClass().add("read-button");
        restartButton.setOnAction((ActionEvent event) -> {
            screenReader.readFullScreenImage();
            BufferedImage screenImg = screenReader.getFullScreenImage();
            ImageSaver.saveImage(
                    screenImg.getSubimage(0, 0, 1000, 800), "fs" + System.currentTimeMillis());
            CardSet holeCardset = tableReader.readHoleCardset(screenImg);
            Card leftHoleCard = holeCardset.get(0);
            Card rightHoleCard = holeCardset.get(1);

            // new hand?
            if (holeCardLeft == null || !holeCardset.isEmpty() && !holeCardLeft.equals(leftHoleCard.toString()) && !holeCardRight.equals(rightHoleCard.toString())) {
                cleanUp();
                dealerSeat = tableReader.readButtonSeat(screenImg);

                switch (dealerSeat) {
                    case ONE:
                        //dealerSeat = Seat.TWO;
                        dealerGroup.getToggles().get(1).setSelected(true);
                        break;
                    case TWO:
                        //dealerSeat = Seat.THREE;
                        dealerGroup.getToggles().get(2).setSelected(true);
                        break;
                    case THREE:
                        //dealerSeat = Seat.FOUR;
                        dealerGroup.getToggles().get(3).setSelected(true);
                        break;
                    case FOUR:
                        //dealerSeat = Seat.FIVE;
                        dealerGroup.getToggles().get(4).setSelected(true);
                        break;
                    case FIVE:
                        //dealerSeat = Seat.SIX;
                        dealerGroup.getToggles().get(5).setSelected(true);
                        break;
                    case SIX:
                        //dealerSeat = Seat.ONE;
                        dealerGroup.getToggles().get(0).setSelected(true);
                        break;
                    default:
                        break;
                }

                heroPos = determinePos(Seat.FOUR, dealerSeat);
                System.out.println("New hand *** Dealer is " + dealerSeat);
                Image leftHoleCardImg = new Image(App.class.getResourceAsStream(leftHoleCard.toString() + ".png"));
                holeCardLeft = leftHoleCard.toString();
                leftHoleCardImgView.setImage(leftHoleCardImg);
                Image rightHoleCardImg = new Image(App.class.getResourceAsStream(rightHoleCard.toString() + ".png"));
                holeCardRight = rightHoleCard.toString();
                rightHoleCardImgView.setImage(rightHoleCardImg);
            } else {
                // Flop?
                CardSet flopCardset = tableReader.readFlopCardset(screenImg);
                if (!flopCardset.isEmpty() && firstFlopCard == null) {
                    firstFlopCard = flopCardset.get(0).toString();
                    Image firstFlopCardImg = new Image(App.class.getResourceAsStream(firstFlopCard + ".png"));
                    firstFlopCardImgView.setImage(firstFlopCardImg);

                    secondFlopCard = flopCardset.get(1).toString();
                    Image secondFlopCardImg = new Image(App.class.getResourceAsStream(secondFlopCard + ".png"));
                    secondFlopCardImgView.setImage(secondFlopCardImg);

                    thirdFlopCard = flopCardset.get(2).toString();
                    Image thirdFlopCardImg = new Image(App.class.getResourceAsStream(thirdFlopCard + ".png"));
                    thirdFlopCardImgView.setImage(thirdFlopCardImg);
                }

                //
            }

            // flop

            /*BufferedImage tableImage = screenReader.readTableImage(TABLE_NRO);
            tableReader.setTableImage(tableImage);
            Seat dealerSeat = tableReader.readDealerPosition();
            Card readLeftHoleCard = tableReader.readLeftHoleCard();
            System.out.println(dealerSeat + " " + readLeftHoleCard.toString());
            //dealerSeat = Seat.ONE;
            heroPos = determinePos(Seat.FOUR, dealerSeat);
            if (null == dealerSeat) {
            dealerSeat = Seat.FOUR;
            } else {
            switch (dealerSeat) {
            case ONE:
            dealerGroup.getToggles().get(1).setSelected(true);
            break;
            case TWO:
            dealerGroup.getToggles().get(2).setSelected(true);
            break;
            case THREE:
            dealerGroup.getToggles().get(3).setSelected(true);
            break;
            case FOUR:
            dealerGroup.getToggles().get(4).setSelected(true);
            break;
            case FIVE:
            dealerGroup.getToggles().get(5).setSelected(true);
            break;
            case SIX:
            dealerGroup.getToggles().get(0).setSelected(true);
            break;
            default:
            break;
            }
            }*/
        });

        return restartButton;
    }
}
