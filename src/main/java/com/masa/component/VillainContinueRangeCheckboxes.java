package com.masa.component;

import static com.masa.pkrapp.App.board;
import static com.masa.pkrapp.App.comboCheckBoxValues;
import static com.masa.pkrapp.App.df;
import static com.masa.pkrapp.App.equityVsContRangeFlop;
import static com.masa.pkrapp.App.equityVsContRangeRiver;
import static com.masa.pkrapp.App.equityVsContRangeTurn;
import static com.masa.pkrapp.App.holeCards;
import static com.masa.pkrapp.App.ranges;
import com.masa.type.ComboCheckBox;
import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import com.masa.util.RangeUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class VillainContinueRangeCheckboxes {

    public static VBox createVillainContinueRangeCheckboxes() {
        createComboCheckBoxValues(board, holeCards);
        Label label = new Label("V.Cont.range");
        VBox.setMargin(label, new Insets(10, 0, 0, 0));
        VBox vBox = new VBox(label);
        //List<CheckBox> checkBoxes = new ArrayList<>();
        List<HBox> rows = new ArrayList<>();
        double allCombos = 0.0;
        for (Map.Entry<MadeHandSpecific, ComboCheckBox> entry : comboCheckBoxValues.entrySet()) {
            allCombos = allCombos + entry.getValue().combos;
        }

        for (Map.Entry<MadeHandSpecific, ComboCheckBox> entry : comboCheckBoxValues.entrySet()) {
            MadeHandSpecific key = entry.getKey();
            ComboCheckBox comboCheckBox = entry.getValue();
            String checkBoxLabel = comboCheckBox.label + "(" + comboCheckBox.combos + ")";
            CheckBox checkBox = new CheckBox(checkBoxLabel);
            checkBox.setSelected(comboCheckBox.checked);
            checkBox.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                        comboCheckBoxValues.get(key).checked = newValue;
                        double equity = RangeUtil.calculateEquityVsVillainContinueRange();
                        switch (board.size()) {
                            case 3:
                                equityVsContRangeFlop.setText("" + df.format(equity));
                                break;
                            case 4:
                                equityVsContRangeTurn.setText("" + df.format(equity));
                                break;
                            case 5:
                                equityVsContRangeRiver.setText("" + df.format(equity));
                                break;
                            default:
                                break;
                        }
                        System.out.println("Equity vs cont range: " + equity);
                    });

            //checkBoxes.add(checkBox);
            double percentageOfAll = comboCheckBox.combos / allCombos;
            ProgressBar bar = new ProgressBar(percentageOfAll);
            HBox row = new HBox(checkBox, bar);
            rows.add(row);
        }
        //Collections.reverse(checkBoxes);
        vBox.getChildren().addAll(rows);

        return vBox;
    }

    public static void createComboCheckBoxValues(CardSet board, CardSet holeCards) {
        // from all cards in deck. check MadeHandSpecific
        CardSet deck = CardSet.freshDeck();
        deck.removeAll(board);
        deck.removeAll(holeCards);

        // All possible combinations in deck
        Set<CardSet> allPossibleCombosInDeck = new HashSet<>();
        for (Card card1 : deck) {
            for (Card card2 : deck) {
                if (card1.equals(card2)) {
                    continue;
                }
                //CardSet combo = new CardSet();
                if (card1.rankOf().pipValue() > card2.rankOf().pipValue()) {
                    allPossibleCombosInDeck.add(new CardSet(card1, card2));
                } else {
                    allPossibleCombosInDeck.add(new CardSet(card2, card1));
                }

            }
        }

        Map<MadeHandSpecific, Integer> map = new HashMap<>();
        // Create map with empty values
        for (MadeHandSpecific value : MadeHandSpecific.values()) {
            map.put(value, 0);
        }
        // Fill map with values
        for (CardSet cardSet : allPossibleCombosInDeck) {
            MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, cardSet);
            map.put(madeHandSpecific, map.get(madeHandSpecific) + 1);
        }

        for (Map.Entry<MadeHandSpecific, Integer> entry : map.entrySet()) {
            MadeHandSpecific key = entry.getKey();
            Integer value = entry.getValue();

            if (value > 0) {
                ComboCheckBox checkbox = new ComboCheckBox(key.value(), false);
                comboCheckBoxValues.put(key, checkbox);
            }
        }

        Set<CardSet> villainRangeCopy = new HashSet<>(ranges.villainRangeCardSet);
        RangeUtil.removeHandsFromRangeThatContainCardFromGivenCardSet(villainRangeCopy, holeCards);

        for (CardSet cardSet : villainRangeCopy) {
            MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, cardSet);
            //String key = madeHandSpecific.name();
            comboCheckBoxValues.get(madeHandSpecific).combos++;
        }
    }

}
