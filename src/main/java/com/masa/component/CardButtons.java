package com.masa.component;

import com.masa.pkrapp.App;
import static com.masa.pkrapp.App.RANKS;
import static com.masa.pkrapp.App.SUITS;
import static com.masa.pkrapp.App.backImg;
import static com.masa.pkrapp.App.board;
import static com.masa.pkrapp.App.cardImgHeight;
import static com.masa.pkrapp.App.cardImgWidth;
import static com.masa.pkrapp.App.firstFlopCard;
import static com.masa.pkrapp.App.firstFlopCardImgView;
import static com.masa.pkrapp.App.holeCardLeft;
import static com.masa.pkrapp.App.holeCardRight;
import static com.masa.pkrapp.App.holeCards;
import static com.masa.pkrapp.App.leftHoleCardImgView;
import static com.masa.pkrapp.App.rightHoleCardImgView;
import static com.masa.pkrapp.App.riverCard;
import static com.masa.pkrapp.App.riverCardImgView;
import static com.masa.pkrapp.App.secondFlopCard;
import static com.masa.pkrapp.App.secondFlopCardImgView;
import static com.masa.pkrapp.App.thirdFlopCard;
import static com.masa.pkrapp.App.thirdFlopCardImgView;
import static com.masa.pkrapp.App.turnCard;
import static com.masa.pkrapp.App.turnCardImgView;
import static com.masa.util.Util.calculate;
import javafx.event.ActionEvent;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class CardButtons {

    public static VBox createCardButtons() {
        VBox sRow = new VBox();

        for (char suit : SUITS) {
            HBox suitsRow = new HBox();
            suitsRow.setId("" + suit);
            for (char rank : RANKS) {
                String cardString = String.valueOf(rank) + String.valueOf(suit);
                Image image = new Image(App.class.getResourceAsStream(cardString + ".png"));

                ImageView imgView = new ImageView(image);
                imgView.setFitWidth(cardImgWidth);
                imgView.setFitHeight(cardImgHeight);
                ToggleButton tb = new ToggleButton("", imgView);

                tb.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                tb.setStyle("  -fx-border-style: none; -fx-border-width: 0px;");
                tb.setId("" + rank);
                tb.setOnAction((ActionEvent event) -> {
                    if (holeCardLeft != null && holeCardLeft.equals(cardString)) {
                        holeCardLeft = null;
                        leftHoleCardImgView.setImage(backImg);
                        return;
                    }
                    if (holeCardRight != null && holeCardRight.equals(cardString)) {
                        holeCardRight = null;
                        rightHoleCardImgView.setImage(backImg);
                        return;
                    }
                    if (firstFlopCard != null && firstFlopCard.equals(cardString)) {
                        firstFlopCard = null;
                        firstFlopCardImgView.setImage(backImg);
                        return;
                    }
                    if (secondFlopCard != null && secondFlopCard.equals(cardString)) {
                        secondFlopCard = null;
                        secondFlopCardImgView.setImage(backImg);
                        return;
                    }
                    if (thirdFlopCard != null && thirdFlopCard.equals(cardString)) {
                        thirdFlopCard = null;
                        thirdFlopCardImgView.setImage(backImg);
                        return;
                    }
                    if (turnCard != null && turnCard.equals(cardString)) {
                        turnCard = null;
                        turnCardImgView.setImage(backImg);
                        return;
                    }
                    if (riverCard != null && riverCard.equals(cardString)) {
                        riverCard = null;
                        riverCardImgView.setImage(backImg);
                        return;
                    }

                    if (holeCardLeft == null) {

                        holeCardLeft = cardString;
                        leftHoleCardImgView.setImage(image);
                    } else if (holeCardRight == null) {
                        holeCardRight = cardString;
                        rightHoleCardImgView.setImage(image);
                        Card holeCardLeftCard = new Card(holeCardLeft);
                        Card holeCardRightCard = new Card(holeCardRight);
                        holeCards = new CardSet(holeCardLeftCard, holeCardRightCard);
                    } else if (firstFlopCard == null) {
                        firstFlopCard = cardString;
                        firstFlopCardImgView.setImage(image);

                    } else if (secondFlopCard == null) {
                        secondFlopCard = cardString;
                        secondFlopCardImgView.setImage(image);
                    } else if (thirdFlopCard == null) {
                        thirdFlopCard = cardString;
                        thirdFlopCardImgView.setImage(image);
                        //remove duplicates from ranges
                        board = new CardSet(new Card(firstFlopCard), new Card(secondFlopCard), new Card(thirdFlopCard));
                        //ranges.parsePossibleCombinationsOnFlopHero(board);

                    } else if (turnCard == null) {
                        turnCard = cardString;
                        turnCardImgView.setImage(image);
                        board.add(new Card(turnCard));
                        //ranges.parsePossibleCombinationsOnFlopHero(board);

                    } else if (riverCard == null) {
                        riverCard = cardString;
                        riverCardImgView.setImage(image);
                        board.add(new Card(riverCard));
                        //ranges.parsePossibleCombinationsOnFlopHero(board);

                    }
                    calculate();
                });
                tb.getStyleClass().add("card-button");

                suitsRow.getChildren().add(tb);
            }
            sRow.getChildren().add(suitsRow);
        }
        return sRow;
    }

}
