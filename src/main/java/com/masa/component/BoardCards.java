package com.masa.component;

import static com.masa.pkrapp.App.firstFlopCardImgView;
import static com.masa.pkrapp.App.riverCardImgView;
import static com.masa.pkrapp.App.secondFlopCardImgView;
import static com.masa.pkrapp.App.thirdFlopCardImgView;
import static com.masa.pkrapp.App.turnCardImgView;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;

/**
 *
 * @author compuuter
 */
public class BoardCards {

    public static HBox createBoardCards() {
        /*ToggleButton firstFlopCardButton = createButton(firstFlopCardImgView, 1);
        ToggleButton secondFlopCardButton = createButton(secondFlopCardImgView, 2);
        ToggleButton thirdFlopCardButton = createButton(thirdFlopCardImgView, 3);
        ToggleButton turnCardButton = createButton(turnCardImgView, 4);
        ToggleButton riverCardButton = createButton(riverCardImgView, 4);*/

        ToggleButton firstCard = new ToggleButton("", firstFlopCardImgView);
        firstCard.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        firstCard.setStyle("  -fx-border-style: none; -fx-border-width: 0px;");
        firstCard.getStyleClass().add("card-button");
        /*firstCard.setOnAction((ActionEvent event) -> {
            if (firstFlopCard != null) {
                Card card = new Card(firstFlopCard);
                Suit suit = card.suitOf();
                Rank rank = card.rankOf();
                for (Node node : cardsRows.getChildren()) {
                    if (node.getId().equals(suit.toString())) {
                        HBox hBox = (HBox) node;
                        for (Node cardNode : hBox.getChildren()) {
                            if (cardNode.getId().equals(rank.toString())) {
                                ToggleButton button = (ToggleButton) node;
                                button.fire();
                            }
                        }
                    }
                }
            }
            firstFlopCard = null;
            firstFlopCardImgView.setImage(App.backImg);
        });*/

        ToggleButton secondCard = new ToggleButton("", secondFlopCardImgView);
        secondCard.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        secondCard.setStyle("  -fx-border-style: none; -fx-border-width: 0px;");
        secondCard.getStyleClass().add("card-button");
        /*secondCard.setOnAction((ActionEvent event) -> {
            if (secondFlopCard != null) {
                Card card = new Card(firstFlopCard);
                Suit suit = card.suitOf();
                Rank rank = card.rankOf();
                for (Node node : cardsRows.getChildren()) {
                    if (node.getId().equals(suit.toString())) {
                        HBox hBox = (HBox) node;
                        for (Node cardNode : hBox.getChildren()) {
                            if (cardNode.getId().equals(rank.toString())) {
                                ToggleButton button = (ToggleButton) node;
                                button.fire();
                            }
                        }
                    }
                }
                secondFlopCardImgView.setImage(App.backImg);
            }
        });*/

        ToggleButton thirdCard = new ToggleButton("", thirdFlopCardImgView);
        thirdCard.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        thirdCard.setStyle("  -fx-border-style: none; -fx-border-width: 0px;");
        thirdCard.getStyleClass().add("card-button");
        /*thirdCard.setOnAction((ActionEvent event) -> {
            if (turnCard != null) {
                Card card = new Card(turnCard);
                Suit suit = card.suitOf();
                Rank rank = card.rankOf();
                for (Node node : cardsRows.getChildren()) {
                    if (node.getId().equals(suit.toString())) {
                        HBox hBox = (HBox) node;
                        for (Node cardNode : hBox.getChildren()) {
                            if (cardNode.getId().equals(rank.toString())) {
                                ToggleButton button = (ToggleButton) node;
                                button.fire();
                            }
                        }
                    }
                }
                thirdFlopCardImgView.setImage(App.backImg);
            }
        });*/

        ToggleButton fourthCard = new ToggleButton("", turnCardImgView);
        fourthCard.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        fourthCard.setStyle("  -fx-border-style: none; -fx-border-width: 0px;");
        fourthCard.getStyleClass().add("card-button");
        /*fourthCard.setOnAction((ActionEvent event) -> {
            if (turnCard != null) {
                Card card = new Card(firstFlopCard);
                Suit suit = card.suitOf();
                Rank rank = card.rankOf();
                for (Node node : cardsRows.getChildren()) {
                    if (node.getId().equals(suit.toString())) {
                        HBox hBox = (HBox) node;
                        for (Node cardNode : hBox.getChildren()) {
                            if (cardNode.getId().equals(rank.toString())) {
                                ToggleButton button = (ToggleButton) node;
                                button.fire();
                            }
                        }
                    }
                }
                turnCardImgView.setImage(App.backImg);
            }
        });*/

        ToggleButton fifthCard = new ToggleButton("", riverCardImgView);
        fifthCard.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        fifthCard.setStyle("  -fx-border-style: none; -fx-border-width: 0px;");
        fifthCard.getStyleClass().add("card-button");
        /*fifthCard.setOnAction((ActionEvent event) -> {
            if (riverCard != null) {
                Card card = new Card(riverCard);
                Suit suit = card.suitOf();
                Rank rank = card.rankOf();
                for (Node node : cardsRows.getChildren()) {
                    if (node.getId().equals(suit.toString())) {
                        HBox hBox = (HBox) node;
                        for (Node cardNode : hBox.getChildren()) {
                            if (cardNode.getId().equals(rank.toString())) {
                                ToggleButton button = (ToggleButton) node;
                                button.fire();
                            }
                        }
                    }
                }
                riverCardImgView.setImage(App.backImg);
            }
        });*/

        return new HBox(firstCard, secondCard, thirdCard, fourthCard, fifthCard);
    }

    /*private static ToggleButton createButton(ImageView imgView, int cardNro) {
        ToggleButton toggleButton = new ToggleButton("", imgView);
        toggleButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        toggleButton.setStyle("  -fx-border-style: none; -fx-border-width: 0px;");

        toggleButton.setOnAction((ActionEvent event) -> {

            if (cardString != null && !cardString.isEmpty()) {
                Card card = new Card(cardString);
                Suit suit = card.suitOf();
                Rank rank = card.rankOf();
                for (Node node : cardsRows.getChildren()) {
                    if (node.getId().equals(suit.toString())) {
                        HBox hBox = (HBox) node;
                        for (Node cardNode : hBox.getChildren()) {
                            if (cardNode.getId().equals(rank.toString())) {
                                ToggleButton button = (ToggleButton) node;
                                button.fire();
                            }
                        }
                    }
                }
                imgView.setImage(App.backImg);
            }
        });

        return toggleButton;
    }*/
}
