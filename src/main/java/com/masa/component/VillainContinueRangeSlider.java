package com.masa.component;

import com.masa.type.MadeHandSpecific;
import javafx.geometry.Orientation;
import javafx.scene.control.Slider;
import javafx.util.StringConverter;

/**
 *
 * @author compuuter
 */
public class VillainContinueRangeSlider {

    public static Slider createVillainContinueRangeSlider() {
        Slider villainContinueSlider = new Slider(0, MadeHandSpecific.STRAIGHT_FLUSH.ordinal(), 0);
        villainContinueSlider.setMajorTickUnit(1.0);
        villainContinueSlider.setMinorTickCount(1);
        villainContinueSlider.setSnapToTicks(true);
        villainContinueSlider.setShowTickMarks(true);
        villainContinueSlider.setShowTickLabels(true);
        villainContinueSlider.setValue(2.0);
        villainContinueSlider.setOrientation(Orientation.VERTICAL);
        villainContinueSlider.setPrefHeight(300.0);
        villainContinueSlider.setLabelFormatter(new StringConverter<Double>() {
            @Override
            public String toString(Double n) {
                for (MadeHandSpecific value : MadeHandSpecific.values()) {
                    double ordinal = value.ordinal() * 1.0;
                    if (n * 1.0 < ordinal + 0.5) {
                        return value.name();
                    }
                }
                return "";
            }

            @Override
            public Double fromString(String s) {
                for (MadeHandSpecific value : MadeHandSpecific.values()) {
                    if (value.name().equals(s)) {
                        return (double) value.ordinal() * 1.0;
                    }
                }
                return 0.0d;
            }
        });
        return villainContinueSlider;
    }
}
