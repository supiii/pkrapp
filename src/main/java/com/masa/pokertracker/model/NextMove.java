package com.masa.pokertracker.model;

/**
 *
 * @author compuuter
 */
public class NextMove {

    public PTActionType action;
    public int count;
    public double bbAmountWonOrLoss;

    public NextMove(PTActionType action) {
        this.action = action;
        this.count = 0;
        this.bbAmountWonOrLoss = 0.0;
    }

    public void add(double wonOrLoss) {
        //System.out.println("NextMove debug before - " + action + " bbAmountWonOrLoss: " + bbAmountWonOrLoss + " count: " + count + " wonOrLoss: " + wonOrLoss);
        count++;
        bbAmountWonOrLoss = bbAmountWonOrLoss + wonOrLoss;
        //System.out.println("NextMove debug after - " + action + " bbAmountWonOrLoss: " + bbAmountWonOrLoss + " count: " + count + " wonOrLoss: " + wonOrLoss);
    }
}
