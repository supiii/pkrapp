package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public class PTHandReportTurnDataSumResponse {

    public int id_action_t,
            val_t_hand_group,
            id_t_hand_strength,
            id_t_group_strength,
            id_t_flush_draw_strength,
            count;
    public boolean flg_t_flush_draw,
            flg_t_gutshot_draw,
            flg_t_straight_draw,
            
            tex_all_cards_one_suit_t,
            tex_four_cards_connected_1gap_t,
            tex_four_cards_connected_t,
            tex_paired_bottom_pair_t,
            tex_paired_double_pair_t,
            tex_paired_four_of_a_kind_t,
            tex_paired_middle_pair_t,
            tex_paired_three_of_a_kind_t,
            tex_paired_top_pair_t,
            tex_rainbow_t,
            tex_t_card_creates_second_flush_draw_t,
            tex_t_card_is_second_card_of_suit_t,
            tex_t_card_is_third_card_of_suit_t;

    public int getHandCategoryT() {
        return val_t_hand_group;
    }

    public int getHandStrengthT() {
        return id_t_hand_strength;
    }
}
