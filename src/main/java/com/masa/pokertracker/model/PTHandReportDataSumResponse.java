package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public class PTHandReportDataSumResponse {

    public int 
            id_action_f,
            id_action_t,
            id_action_r,
            val_f_hand_group,
            val_t_hand_group,
            val_r_hand_group,
            id_f_hand_strength,
            id_t_hand_strength,
            id_r_hand_strength,
            id_f_group_strength,
            id_t_group_strength,
            id_r_group_strength,
            count;
    
    public int getHandCategoryF() {
        return val_f_hand_group;
    }
    
    public int getHandCategoryT() {
        return val_t_hand_group;
    }
    
    public int getHandCategoryR() {
        return val_r_hand_group;
    }
    
    public int getHandStrengthF() {
        return id_f_hand_strength;
    }
    
    public int getHandStrengthT() {
        return id_t_hand_strength;
    }
    
    public int getHandStrengthR() {
        return id_r_hand_strength;
    }
}
