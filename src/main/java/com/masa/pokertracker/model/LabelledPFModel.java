package com.masa.pokertracker.model;

/**
 *
 * @author compuuter
 */
public class LabelledPFModel {

    public boolean p1_won;
    public double p1_won_bb;
    public String hand_no;
    public int id_hand,
            id_rangecombo,
            id_holecard1,
            id_holecard2,
            id_action_p,
            id_action_p_ep,
            id_action_p_mp,
            id_action_p_co,
            id_action_p_btn,
            id_action_p_sb,
            id_action_p_bb,
            vpip_ep, pfr_ep, att_to_steal_ep, fold_to_steal_ep, threebet_pf_ep,
            fold_to_threebet_pf_ep, fourbet_pf_ep, fold_to_fourbet_ep, cbet_f_ep,
            fold_to_cbet_f_ep, cbet_t_ep, fold_to_cbet_t_ep,
            vpip_mp, pfr_mp, att_to_steal_mp, fold_to_steal_mp, threebet_pf_mp,
            fold_to_threebet_pf_mp, fourbet_pf_mp, fold_to_fourbet_mp, cbet_f_mp,
            fold_to_cbet_f_mp, cbet_t_mp, fold_to_cbet_t_mp,
            vpip_co, pfr_co, att_to_steal_co, fold_to_steal_co, threebet_pf_co,
            fold_to_threebet_pf_co, fourbet_pf_co, fold_to_fourbet_co, cbet_f_co,
            fold_to_cbet_f_co, cbet_t_co, fold_to_cbet_t_co,
            vpip_btn, pfr_btn, att_to_steal_btn, fold_to_steal_btn, threebet_pf_btn,
            fold_to_threebet_pf_btn, fourbet_pf_btn, fold_to_fourbet_btn, cbet_f_btn,
            fold_to_cbet_f_btn, cbet_t_btn, fold_to_cbet_t_btn,
            vpip_sb, pfr_sb, att_to_steal_sb, fold_to_steal_sb, threebet_pf_sb,
            fold_to_threebet_pf_sb, fourbet_pf_sb, fold_to_fourbet_sb, cbet_f_sb,
            fold_to_cbet_f_sb, cbet_t_sb, fold_to_cbet_t_sb,
            vpip_bb, pfr_bb, att_to_steal_bb, fold_to_steal_bb, threebet_pf_bb,
            fold_to_threebet_pf_bb, fourbet_pf_bb, fold_to_fourbet_bb, cbet_f_bb,
            fold_to_cbet_f_bb, cbet_t_bb, fold_to_cbet_t_bb;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LabelledPFModel{p1_won_bb=").append(p1_won_bb);
        sb.append(", id_hand=").append(id_hand);
        sb.append(", hand_no=").append(hand_no);
        sb.append(", id_rangecombo=").append(id_rangecombo);
        sb.append(", id_holecard1=").append(id_holecard1);
        sb.append(", id_holecard2=").append(id_holecard2);
        sb.append(", id_action_p=").append(id_action_p);
        sb.append(", id_action_p_ep=").append(id_action_p_ep);
        sb.append(", id_action_p_mp=").append(id_action_p_mp);
        sb.append(", id_action_p_co=").append(id_action_p_co);
        sb.append(", id_action_p_btn=").append(id_action_p_btn);
        sb.append(", id_action_p_sb=").append(id_action_p_sb);
        sb.append(", id_action_p_bb=").append(id_action_p_bb);
        sb.append('}');
        return sb.toString();
    }

}
