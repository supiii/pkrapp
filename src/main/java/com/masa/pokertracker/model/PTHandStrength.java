package com.masa.pokertracker.model;

/**
 *  https://www.pokertracker.com/forums/viewtopic.php?f=18&t=17317#p82296
 *  Straight Flush (1), Four of a Kind (2), Full House (3), Flush (4), 
 * Straight (5), Three of a Kind (6), Two Pair (7), One Pair (8) and High Card (9).
 * @author Matti
 */
public enum PTHandStrength {
    HIGH_CARD_NO_OVERCARDS,
    HIGH_CARD_ONE_OVERCARD,
    HIGH_CARD_TWO_OVERCARDS,

/*Within One Pair:
0 - Board Paired or low pocket pair (under the 3rd board card)
1 - Bottom pair
2 - Pair higher than the lowest board card
3 - Top Pair
4 - Pocket Pair, higher than the lowest board card but lower than the 2nd highest board card (low-middle)
5 - Pocket Pair, middle pair - specifically, a pair lower than the top board card and higher than the 2nd highest board card (high-middle)
6 - Pocket Pair, overpair*/
    ONE_PAIR_BOARD_PAIRED_OR_LOWER_PP_UNDER_3RD_BOARD_CARD,
    ONE_PAIR_BOTTOM_PAIR,
    ONE_PAIR_HIGHER_THAN_LOWEST_BOARD_CARD,
    ONE_PAIR_TOP_PAIR,
    ONE_PAIR_PP_HIGHER_THAN_LOWEST_BUT_LOWER_THAN_2ND_HIGHEST,
    ONE_PAIR_PP_MIDDLE_PAIR_LOWER_THAN_HIGHEST_BUT_HIGHER_THAN_2ND_HIGHEST,
    ONE_PAIR_OVERPAIR,

/*Within Two Pair:
0 - Board Double Paired (Note: only valid for turn and river)
1 - Board Paired, low pair (34 on a KK378 board)
2 - Board Paired, middle pair (QJ on a AT8JT board)
3 - Board Paired, top pair (KQ on a JTTQ8 board)
4 - Pocket Pair, low pair (88 on a 6TTJ3 board)
5 - Pocket Pair, middle pair (JJ on an A7633 board)
6 - Pocket Pair, overpair
7 - Low two pair (78 on a K78T3 board)
8 - Top and middle pair (K7 on a K78T3 board)
9 - Top two pair (KT on a K78T3 board)*/
    TWO_PAIR_BOARD_DOUBLE_PAIRED_TURN_OR_RIVER,
    TWO_PAIR_BOARD_PAIRED_LOW_PAIR,
    TWO_PAIR_BOARD_PAIRED_MIDDLE_PAIR,
    TWO_PAIR_BOARD_PAIRED_TOP_PAIR,
    TWO_PAIR_PP_LOW_PAIR,
    TWO_PAIR_PP_MIDDLE_PAIR,
    TWO_PAIR_PP_OVERPAIR,
    TWO_PAIR_LOW,
    TWO_PAIR_TOP_AND_MIDDLE,
    TWO_PAIR_TOP_TWO,
    
    
/*Within Three of a Kind:

0 - Board trips
1 - Board paired, low (43 on 4T64Q board)
2 - Board paired, middle (KQ on Q93AQ board)
3 - Board paired, high (AQ on A932A board)
4 - Low set
5 - Middle set
6 - Top set*/
THREEOAK_BOARD_TRIPS,
THREEOAK_BOARD_PAIRED_LOW,
THREEOAK_BOARD_PAIRED_MIDDLE,
THREEOAK_BOARD_PAIRED_HIGH,
THREEOAK_LOW_SET,
THREEOAK_MIDDLE_SET,
THREEOAK_TOP_SET,
/*Within Straight:

0 - Straight on board (valid for river only)
1 - One card, low end (A4 on a 5678K board)
2 - One card, middle
3 - One card, high end
4 - Two Card Non-nut straight
5 - Two Card Nut straight*/
STRAIGHT_ON_BOARD_RIVER,
STRAIGHT_ONE_CARD_LOW_END,
STRAIGHT_ONE_CARD_MIDDLE,
STRAIGHT_ONE_CARD_HIGH_END,
STRAIGHT_TWO_CARD_NON_NUT,
STRAIGHT_TWO_CARD_NUT_STRAIGHT,
/*Within Flush:

10 - Nut flush
9 - 2nd nut flush
etc.*/
FLUSH_NUT,
FLUSH_2ND_NUT,
FLUSH_3RD_NUT,
//FLUSH_4TH_NUT,
FLUSH_4TH_OR_LOWER_NUT,
/*Within Full House:

0 - Full house on board (valid for river only)
1 - Board trips, pocket pair, low pair
2 - Board trips, pocket pair, middle pair
3 - Board trips, pocket pair, overpair
4 - Board trips, top pair
5 - Board trips, not top pair
6 - Board paired, no set, pair below trips
7 - Board paired, no set, pair above trips
8 - Set under pair (33 on a 3JJT9 board)
9 - Set over pair (AA on an A3347 board)*/
FULLHOUSE_ON_BOARD_RIVER,
FULLHOUSE_BOARD_TRIPS_LOW_PP,
FULLHOUSE_BOARD_TRIPS_MIDDLE_PP,
FULLHOUSE_BOARD_TRIPS_OVERPAIR,
FULLHOUSE_BOARD_TRIPS_TOP_PAIR,
FULLHOUSE_BOARD_TRIPS_NOT_TOP_PAIR,
FULLHOUSE_BOARD_PAIRED_NO_SET_PAIR_BELOW_TRIPS,
FULLHOUSE_BOARD_PAIRED_NO_SET_PAIR_ABOVE_TRIPS,
FULLHOUSE_SET_UNDER_PAIR,
FULLHOUSE_SET_OVER_PAIR,
/*Within Four of a Kind:

0 - Board four of a kind, kicker below other board card (valid on turn and river only)
1 - Board four of a kind, kicker above other board card (K2 on a 33337 board)
2 - Board trips
3 - Board paired*/
FOUROAK,
/*Within Straight Flush:

0 - Straight flush on board (valid for river only)
1 - One card, low end (A4 on a 5678K board)
2 - One card, middle
3 - One card, high end
4 - Two Card Non-nut straight flush
5 - Two Card Nut straight flush*/
STRAIGHT_FLUSH
}
