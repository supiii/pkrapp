package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public class Combination {
    public int groupStrengthId;
    public boolean flushDraw,
            straigthDraw;

    public Combination(int groupStrengthId, boolean flushDraw, boolean straigthDraw) {
        this.groupStrengthId = groupStrengthId;
        this.flushDraw = flushDraw;
        this.straigthDraw = straigthDraw;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.groupStrengthId;
        hash = 73 * hash + (this.flushDraw ? 1 : 0);
        hash = 73 * hash + (this.straigthDraw ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Combination other = (Combination) obj;
        if (this.groupStrengthId != other.groupStrengthId) {
            return false;
        }
        if (this.flushDraw != other.flushDraw) {
            return false;
        }
        if (this.straigthDraw != other.straigthDraw) {
            return false;
        }
        return true;
    }

}
