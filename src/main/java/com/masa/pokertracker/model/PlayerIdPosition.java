package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public class PlayerIdPosition {
    private String playerId;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     * Position    absolute_position    description
        0        0            BUTTON
        1        1            CO
        2        6            UTG+1
        3        8            UTG
        8        8            BB
        9        9            SB
     */
    private String position;
    
    public PlayerIdPosition(String playerId, String position) {
        this.playerId = playerId;
        this.position = position;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    
}
