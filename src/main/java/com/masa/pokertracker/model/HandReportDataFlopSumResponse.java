package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public class HandReportDataFlopSumResponse {

    public int 
            id_action_pf,
            id_action_f,
            val_f_hand_group,
            id_f_hand_strength,
            id_f_group_strength,
            id_f_flush_draw_strength,
            count;
    public boolean
            flg_f_flush_draw,
            flg_f_gutshot_draw,
            flg_f_straight_draw,
         
            tex_any_pair_f,
            tex_three_cards_connected_1gap_f,
            tex_three_cards_connected_f,
            tex_three_cards_suited_f,
            tex_three_of_a_kind_f,
            tex_two_cards_connected_f,
            tex_two_cards_suited_f;
    
    public int getHandCategoryF() {
        return val_f_hand_group;
    }
    
    public int getHandStrengthF() {
        return id_f_hand_strength;
    }
}
