package com.masa.pokertracker.model;

import com.masa.pokertracker.PTUtil;

/**
 *
 * @author Matti
 */
public class QueryBuilderPositions {

    public String buildSql(String heroPosition, String villainPosition) {
        return " val_position_type = '" + PTUtil.getPositionInt(heroPosition) + "' AND flg_f_saw_" + villainPosition.toLowerCase() + " = true ";
    }

    public String buildPosIntString(String heroPosition, String villainPosition) {
        return "" + PTUtil.getPositionInt(heroPosition) + PTUtil.getPositionInt(villainPosition);
    }
}
