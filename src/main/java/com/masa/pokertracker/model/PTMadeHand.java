package com.masa.pokertracker.model;

import com.masa.pokertracker.model.PTHandStrength;

/**
 *
 * @author Matti
 */
public class PTMadeHand {
    
    
    private boolean highcard;
    private boolean onePair;
    private boolean twoPair;
    private boolean threeOfAKind;
    private boolean straight;
    private boolean flush;
    private boolean fullhouse;
    private boolean fourOfAKind;
    private boolean straightFlush;
    /**
     * Within High Card:
0 - No overcards
1 - One overcard
2 - Two overcards

Within One Pair:
0 - Board Paired or low pocket pair (under the 3rd board card)
1 - Bottom pair
2 - Pair higher than the lowest board card
3 - Top Pair
4 - Pocket Pair, higher than the lowest board card but lower than the 2nd highest board card (low-middle)
5 - Pocket Pair, middle pair - specifically, a pair lower than the top board card and higher than the 2nd highest board card (high-middle)
6 - Pocket Pair, overpair

Within Two Pair:
0 - Board Double Paired (Note: only valid for turn and river)
1 - Board Paired, low pair (34 on a KK378 board)
2 - Board Paired, middle pair (QJ on a AT8JT board)
3 - Board Paired, top pair (KQ on a JTTQ8 board)
4 - Pocket Pair, low pair (88 on a 6TTJ3 board)
5 - Pocket Pair, middle pair (JJ on an A7633 board)
6 - Pocket Pair, overpair
7 - Low two pair (78 on a K78T3 board)
8 - Top and middle pair (K7 on a K78T3 board)
9 - Top two pair (KT on a K78T3 board)

Within Three of a Kind:

0 - Board trips
1 - Board paired, low (43 on 4T64Q board)
2 - Board paired, middle (KQ on Q93AQ board)
3 - Board paired, high (AQ on A932A board)
4 - Low set
5 - Middle set
6 - Top set

Within Straight:

0 - Straight on board (valid for river only)
1 - One card, low end (A4 on a 5678K board)
2 - One card, middle
3 - One card, high end
4 - Two Card Non-nut straight
5 - Two Card Nut straight

Within Flush:

10 - Nut flush
9 - 2nd nut flush
etc.

Within Full House:

0 - Full house on board (valid for river only)
1 - Board trips, pocket pair, low pair
2 - Board trips, pocket pair, middle pair
3 - Board trips, pocket pair, overpair
4 - Board trips, top pair
5 - Board trips, not top pair
6 - Board paired, no set, pair below trips
7 - Board paired, no set, pair above trips
8 - Set under pair (33 on a 3JJT9 board)
9 - Set over pair (AA on an A3347 board)

Within Four of a Kind:

0 - Board four of a kind, kicker below other board card (valid on turn and river only)
1 - Board four of a kind, kicker above other board card (K2 on a 33337 board)
2 - Board trips
3 - Board paired

Within Straight Flush:

0 - Straight flush on board (valid for river only)
1 - One card, low end (A4 on a 5678K board)
2 - One card, middle
3 - One card, high end
4 - Two Card Non-nut straight flush
5 - Two Card Nut straight flush

     */
    private HandCategory handCategory;
    private PTHandStrength ptHandStrength;
    private int handStrength;
    private int kickerStrength;
    private int holeCardsUsed;
    private boolean gutshotDraw;
    private boolean straightDraw;
    private boolean twoGutshotDraw;
    private boolean flushDraw;

//                + "(cash_hand_player_combinations.id_f_flush_draw_strength) as \"id_f_flush_draw_strength\" "
    public boolean isHighcard() {
        return highcard;
    }

    public void setHighcard(boolean highcard) {
        this.highcard = highcard;
    }

    public boolean isOnePair() {
        return onePair;
    }

    public void setOnePair(boolean onePair) {
        this.onePair = onePair;
    }

    public boolean isTwoPair() {
        return twoPair;
    }

    public void setTwoPair(boolean twoPair) {
        this.twoPair = twoPair;
    }

    public boolean isThreeOfAKind() {
        return threeOfAKind;
    }

    public void setThreeOfAKind(boolean threeOfAKind) {
        this.threeOfAKind = threeOfAKind;
    }

    public boolean isStraight() {
        return straight;
    }

    public void setStraight(boolean straight) {
        this.straight = straight;
    }

    public boolean isFlush() {
        return flush;
    }

    public void setFlush(boolean flush) {
        this.flush = flush;
    }

    public boolean isFullhouse() {
        return fullhouse;
    }

    public void setFullhouse(boolean fullhouse) {
        this.fullhouse = fullhouse;
    }

    public boolean isFourOfAKind() {
        return fourOfAKind;
    }

    public void setFourOfAKind(boolean fourOfAKind) {
        this.fourOfAKind = fourOfAKind;
    }

    public boolean isStraightFlush() {
        return straightFlush;
    }

    public void setStraightFlush(boolean straightFlush) {
        this.straightFlush = straightFlush;
    }

    public HandCategory getHandCategory() {
        return handCategory;
    }

    public void setHandCategory(HandCategory handCategory) {
        this.handCategory = handCategory;
    }

    public PTHandStrength getPtHandStrength() {
        return ptHandStrength;
    }

    public void setPtHandStrength(PTHandStrength ptHandStrength) {
        this.ptHandStrength = ptHandStrength;
    }

    public int getHandStrength() {
        return handStrength;
    }

    public void setHandStrength(int handStrength) {
        this.handStrength = handStrength;
    }

    public int getKickerStrength() {
        return kickerStrength;
    }

    public void setKickerStrength(int kickerStrength) {
        this.kickerStrength = kickerStrength;
    }

    public int getHoleCardsUsed() {
        return holeCardsUsed;
    }

    public void setHoleCardsUsed(int holeCardsUsed) {
        this.holeCardsUsed = holeCardsUsed;
    }

    public boolean isGutshotDraw() {
        return gutshotDraw;
    }

    public void setGutshotDraw(boolean gutshotDraw) {
        this.gutshotDraw = gutshotDraw;
    }

    public boolean isStraightDraw() {
        return straightDraw;
    }

    public void setStraightDraw(boolean straightDraw) {
        this.straightDraw = straightDraw;
    }

    public boolean isTwoGutshotDraw() {
        return twoGutshotDraw;
    }

    public void setTwoGutshotDraw(boolean twoGutshotDraw) {
        this.twoGutshotDraw = twoGutshotDraw;
    }

    public boolean isFlushDraw() {
        return flushDraw;
    }

    public void setFlushDraw(boolean flushDraw) {
        this.flushDraw = flushDraw;
    }

    
}
