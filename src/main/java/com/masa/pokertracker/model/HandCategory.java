package com.masa.pokertracker.model;

/**
 * PT3 uses 9 groups for hand strength values: 
 * Straight Flush (1), Four of a Kind (2), Full House (3), Flush (4), Straight (5), Three of a Kind (6), Two Pair (7), One Pair (8) and High Card (9).
 * @author Matti
 */
public enum HandCategory {
    STRAIGHT_FLUSH("1"),
    FOUR_OF_A_KIND("2"), 
    FULL_HOUSE("3"), 
    FLUSH("4"), 
    STRAIGHT("5"),
    THREE_OF_A_KIND("6"), 
    TWO_PAIR("7"), 
    PAIR("8"), 
    NO_PAIR("9");
    
    private final String value;

    HandCategory(String v) {
        value = v;
    }
    
    public String value() {
        return value;
    }
}
