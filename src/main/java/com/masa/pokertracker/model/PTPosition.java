package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public enum PTPosition {
    BUTTON("0"),
    CO("1"),
    UTGPLUS1("2"),
    UTG("3"),
    BB("8"),
    SB("9");
    
    private final String value;

    PTPosition(String v) {
        value = v;
    }
    
    public String value() {
        return value;
    }
}
