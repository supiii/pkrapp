package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public enum Site {
    POKERSTARS("100"),
    PARTY("200"),
    FULLTILT("300"),
    EVEREST("500"),
    ONGAME("600"),
    PACIFICPOKER("900");
    
    private final String value;

    Site(String v) {
        value = v;
    }
    
    public String value() {
        return value;
    }
}
