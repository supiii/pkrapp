package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public enum PTActionType {
    FOLD("F"),
    CHECK("X"),
    BET("B"),
    CALL("C"),
    RAISE("R");
    
    private String value;
    
    private PTActionType(String value) {
        this.value = value;
    }
    
    public String value() {
        return value;
    }
}
