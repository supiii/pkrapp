package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public class PTHandReportRiverDataSumResponse {

    public int id_action_r,
            val_r_hand_group,
            id_r_hand_strength,
            id_r_group_strength,
            count;
    public boolean tex_five_cards_connected_r,
            tex_five_cards_suited_r,
            tex_four_cards_connected_r,
            tex_four_cards_suited_r,
            tex_less_than_three_cards_suited_r,
            tex_paired_bottom_pair_r,
            tex_paired_double_paired_r,
            tex_paired_four_of_a_kind_r,
            tex_paired_fullhouse_r,
            tex_paired_middle_pair_r,
            tex_paired_three_of_a_kind_r,
            tex_paired_top_pair_r,
            tex_three_cards_suited_r;

    public int getHandCategoryR() {
        return val_r_hand_group;
    }

    public int getHandStrengthR() {
        return id_r_hand_strength;
    }
}
