package com.masa.pokertracker.model;

/**
 *
 * @author Matti
 */
public class WinningPlayersResponse {
    public int playerId = 0;
    public int hands = 0;
    public int amountWonBB = 0; 
}
