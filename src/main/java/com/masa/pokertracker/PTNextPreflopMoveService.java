package com.masa.pokertracker;

import com.masa.pokertracker.model.LabelledPFModel;
import com.masa.pokertracker.model.NextMove;
import com.masa.pokertracker.model.PTActionType;
import java.util.ArrayList;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class PTNextPreflopMoveService {

    private final LabelledPFRepository repository = new LabelledPFRepository();

    public List<NextMove> getNextMoves(CardSet holeCards, String heroPos,
            String heroActions,
            String actionsEP,
            String actionsMP,
            String actionsCO,
            String actionsBTN,
            String actionsSB,
            String actionsBB) {
        System.out.println("holeCards " + holeCards.toStringWithoutCommas() + " heroPos " + heroPos);
        List<NextMove> nextMoves = new ArrayList<>();

        String rangeCombo = PTUtil.convertToRangeCombo(holeCards.toStringWithoutCommas());
        int rangeComboInt = PTUtil.getRangeComboInt(rangeCombo);
        int positonInt = PTUtil.getPositionInt(heroPos);
        System.out.println("rangeCombo " + rangeCombo + " rangeComboInt " + rangeComboInt + " positionInt " + positonInt);
        List<LabelledPFModel> fetchedMoves = repository.fetchMoves(rangeComboInt, positonInt);

        System.out.println("FetchedMoves size: " + fetchedMoves.size());
        fetchedMoves.forEach(fetchedMove -> {
            System.out.println(fetchedMove.toString());
        });

        NextMove foldMove = new NextMove(PTActionType.FOLD);
        NextMove callMove = new NextMove(PTActionType.CALL);
        NextMove betMove = new NextMove(PTActionType.BET);
        NextMove raiseMove = new NextMove(PTActionType.RAISE);

        String foldAction = heroActions + PTActionType.FOLD.value();
        String callAction = heroActions + PTActionType.CALL.value();
        String betAction = heroActions + PTActionType.BET.value();
        String raiseAction = heroActions + PTActionType.RAISE.value();

        List<Integer> foldingActionIDs = PTUtil.getActionsIDsStartingOrEqualsWith(foldAction);
        List<Integer> callActionIDs = PTUtil.getActionsIDsStartingOrEqualsWith(callAction);
        List<Integer> betActionIDs = PTUtil.getActionsIDsStartingOrEqualsWith(betAction);
        List<Integer> raiseActionIDs = PTUtil.getActionsIDsStartingOrEqualsWith(raiseAction);

        for (LabelledPFModel fetchedMove : fetchedMoves) {
            // villaina actions
            if (heroPos.equalsIgnoreCase("EP")) {
                if (PTUtil.getActionsIDsStartingOrEqualsWith(actionsMP).contains(fetchedMove.id_action_p_mp)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsCO).contains(fetchedMove.id_action_p_co)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBTN).contains(fetchedMove.id_action_p_btn)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsSB).contains(fetchedMove.id_action_p_sb)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBB).contains(fetchedMove.id_action_p_bb)) {
                    updateNextMove(foldingActionIDs, fetchedMove, foldMove, callActionIDs, callMove, betActionIDs, betMove, raiseActionIDs, raiseMove);
                }
            } else if (heroPos.equalsIgnoreCase("MP")) {
                if (PTUtil.getActionsIDsStartingOrEqualsWith(actionsEP).contains(fetchedMove.id_action_p_ep)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsCO).contains(fetchedMove.id_action_p_co)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBTN).contains(fetchedMove.id_action_p_btn)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsSB).contains(fetchedMove.id_action_p_sb)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBB).contains(fetchedMove.id_action_p_bb)) {
                    updateNextMove(foldingActionIDs, fetchedMove, foldMove, callActionIDs, callMove, betActionIDs, betMove, raiseActionIDs, raiseMove);
                }
            } else if (heroPos.equalsIgnoreCase("CO")) {
                if (PTUtil.getActionsIDsStartingOrEqualsWith(actionsEP).contains(fetchedMove.id_action_p_ep)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsMP).contains(fetchedMove.id_action_p_mp)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBTN).contains(fetchedMove.id_action_p_btn)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsSB).contains(fetchedMove.id_action_p_sb)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBB).contains(fetchedMove.id_action_p_bb)) {
                    updateNextMove(foldingActionIDs, fetchedMove, foldMove, callActionIDs, callMove, betActionIDs, betMove, raiseActionIDs, raiseMove);
                }
            } else if (heroPos.equalsIgnoreCase("BTN")) {
                if (PTUtil.getActionsIDsStartingOrEqualsWith(actionsEP).contains(fetchedMove.id_action_p_ep)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsMP).contains(fetchedMove.id_action_p_mp)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsCO).contains(fetchedMove.id_action_p_co)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsSB).contains(fetchedMove.id_action_p_sb)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBB).contains(fetchedMove.id_action_p_bb)) {
                    updateNextMove(foldingActionIDs, fetchedMove, foldMove, callActionIDs, callMove, betActionIDs, betMove, raiseActionIDs, raiseMove);
                }
            } else if (heroPos.equalsIgnoreCase("SB")) {
                if (PTUtil.getActionsIDsStartingOrEqualsWith(actionsEP).contains(fetchedMove.id_action_p_ep)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsMP).contains(fetchedMove.id_action_p_mp)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsCO).contains(fetchedMove.id_action_p_co)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBTN).contains(fetchedMove.id_action_p_btn)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBB).contains(fetchedMove.id_action_p_bb)) {
                    updateNextMove(foldingActionIDs, fetchedMove, foldMove, callActionIDs, callMove, betActionIDs, betMove, raiseActionIDs, raiseMove);
                }
            } else if (heroPos.equalsIgnoreCase("BB")) {
                if (PTUtil.getActionsIDsStartingOrEqualsWith(actionsEP).contains(fetchedMove.id_action_p_ep)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsMP).contains(fetchedMove.id_action_p_mp)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsCO).contains(fetchedMove.id_action_p_co)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsBTN).contains(fetchedMove.id_action_p_btn)
                        && PTUtil.getActionsIDsStartingOrEqualsWith(actionsSB).contains(fetchedMove.id_action_p_sb)) {
                    updateNextMove(foldingActionIDs, fetchedMove, foldMove, callActionIDs, callMove, betActionIDs, betMove, raiseActionIDs, raiseMove);
                }
            }
        }

        System.out.println("Folds count: " + foldMove.count + " bbWon: " + foldMove.bbAmountWonOrLoss);
        System.out.println("Calls count: " + callMove.count + " bbWon: " + callMove.bbAmountWonOrLoss);
        System.out.println("Bets count: " + betMove.count + " bbWon: " + betMove.bbAmountWonOrLoss);
        System.out.println("Raises count: " + raiseMove.count + " bbWon: " + raiseMove.bbAmountWonOrLoss);

        return nextMoves;
    }

    private void updateNextMove(List<Integer> foldingActionIDs, LabelledPFModel fetchedMove, NextMove foldMove,
            List<Integer> callActionIDs, NextMove callMove,
            List<Integer> betActionIDs, NextMove betMove,
            List<Integer> raiseActionIDs, NextMove raiseMove) {
        // hero actions
        if (foldingActionIDs.contains(fetchedMove.id_action_p)) {
            foldMove.add(fetchedMove.p1_won_bb);
        }
        if (callActionIDs.contains(fetchedMove.id_action_p)) {
            callMove.add(fetchedMove.p1_won_bb);
        }
        if (betActionIDs.contains(fetchedMove.id_action_p)) {
            betMove.add(fetchedMove.p1_won_bb);
        }
        if (raiseActionIDs.contains(fetchedMove.id_action_p)) {
            raiseMove.add(fetchedMove.p1_won_bb);
        }
    }
}
