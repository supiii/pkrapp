package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.PTHandStrengthUtil;
import com.masa.pokertracker.model.PTHandStrength;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author Matti
 */
public class PTHandStrengthHighCardAnalyzer {
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        Rank highestBoardCardRank = PTHandStrengthUtil.highestRank(boardCards);
        
        boolean leftCardHigher = holeCards.get(0).rankOf().pipValue() > highestBoardCardRank.pipValue();
        boolean rightCardHigher = holeCards.get(1).rankOf().pipValue() > highestBoardCardRank.pipValue();;
        
        if (leftCardHigher && rightCardHigher) {
            return 2;
        }
        if (!leftCardHigher && !rightCardHigher) {
            return 0;
        }
        return 1;
    }
    
    /**
     *  HIGH_CARD_NO_OVERCARDS,
        HIGH_CARD_ONE_OVERCARD,
        HIGH_CADR_TWO_OVERCARDS,
     * @param holeCards
     * @param boardCards
     * @return 
     */
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        Rank highestBoardCardRank = PTHandStrengthUtil.highestRank(boardCards);
        
        boolean leftCardHigher = holeCards.get(0).rankOf().pipValue() > highestBoardCardRank.pipValue();
        boolean rightCardHigher = holeCards.get(1).rankOf().pipValue() > highestBoardCardRank.pipValue();;
        
        if (leftCardHigher && rightCardHigher) {
            return PTHandStrength.HIGH_CARD_TWO_OVERCARDS;
        }
        if (!leftCardHigher && !rightCardHigher) {
            return PTHandStrength.HIGH_CARD_NO_OVERCARDS;
        }
        return PTHandStrength.HIGH_CARD_ONE_OVERCARD;
    }
}
