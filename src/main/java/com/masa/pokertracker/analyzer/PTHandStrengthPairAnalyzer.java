package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.PTHandStrengthUtil;
import com.masa.pokertracker.model.PTHandStrength;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author Matti
 */
public class PTHandStrengthPairAnalyzer {
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        if (holeCards.get(0).rankOf().pipValue() == holeCards.get(1).rankOf().pipValue()) {
            return pocketPairIDValue(holeCards, boardCards);
        }
        return noPocketPairIDValue(holeCards, boardCards);
    }
    
    public static int pocketPairIDValue(CardSet holeCards, CardSet boardCards) {
        int left = holeCards.get(0).rankOf().pipValue();
        
        int lowestBoard = PTHandStrengthUtil.lowestRank(boardCards).pipValue(); 
        if (left < lowestBoard) {
            return 0;
        }
        int secondHighest = PTHandStrengthUtil.secondHighestRank(boardCards).pipValue();
        if (left > lowestBoard && left < secondHighest) {
            return 4;
        }
        int highestBoard = PTHandStrengthUtil.highestRank(boardCards).pipValue();
        if (left < highestBoard && left > secondHighest) {
            return 5;
        }
        if (left > highestBoard) {
            return 6;
        }
        return 0;
    }
    
    public static int noPocketPairIDValue(CardSet holeCards, CardSet boardCards) {
        int left = holeCards.get(0).rankOf().pipValue();
        int right = holeCards.get(1).rankOf().pipValue();
        int highestBoard = PTHandStrengthUtil.highestRank(boardCards).pipValue();
        int lowestBoard = PTHandStrengthUtil.lowestRank(boardCards).pipValue();
        Rank boardCard1 = boardCards.get(0).rankOf();
        Rank boardCard2 = boardCards.get(1).rankOf();
        Rank boardCard3 = boardCards.get(2).rankOf();
        
        if (PTHandStrengthUtil.countRanks(boardCard1, boardCards) > 1
                || PTHandStrengthUtil.countRanks(boardCard2, boardCards) > 1
                || PTHandStrengthUtil.countRanks(boardCard3, boardCards) > 1) {
            return 0;
        }
        // TODO CHECK:
        // ONE_PAIR_BOARD_PAIRED_OR_LOWER_PP_UNDER_3RD_BOARD_CARD,
        // ONE_PAIR_BOTTOM_PAIR,
        // ONE_PAIR_TOP_PAIR,
        if (left == highestBoard || right == highestBoard) {
            return 3;
        }
        if (left == lowestBoard || right == lowestBoard) {
            return 1;
        }
        // ONE_PAIR_HIGHER_THAN_LOWEST_BOARD_CARD,
        if (left > lowestBoard && right > lowestBoard && (left < highestBoard && right < highestBoard)) {
            return 2;
        }
        
        return 0;
    }
    
    /**
     * ONE_PAIR_BOARD_PAIRED_OR_LOWER_PP_UNDER_3RD_BOARD_CARD,
    ONE_PAIR_BOTTOM_PAIR,
    ONE_PAIR_HIGHER_THAN_LOWEST_BOARD_CARD,
    ONE_PAIR_TOP_PAIR,
    ONE_PAIR_PP_HIGHER_THAN_LOWEST_BUT_LOWER_THAN_2ND_HIGHEST,
    ONE_PAIR_PP_MIDDLE_PAIR_LOWER_THAN_HIGHEST_BUT_HIGHER_THAN_2ND_HIGHEST,
    ONE_PAIR_OVERPAIR,
     * @param holeCards
     * @param boardCards
     * @return 
     */
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        if (holeCards.get(0).rankOf().pipValue() == holeCards.get(1).rankOf().pipValue()) {
            return pocketPair(holeCards, boardCards);
        }
        return noPocketPair(holeCards, boardCards);
    }
    
    public static PTHandStrength pocketPair(CardSet holeCards, CardSet boardCards) {
        int left = holeCards.get(0).rankOf().pipValue();
        
        int lowestBoard = PTHandStrengthUtil.lowestRank(boardCards).pipValue(); 
        if (left < lowestBoard) {
            return PTHandStrength.ONE_PAIR_BOARD_PAIRED_OR_LOWER_PP_UNDER_3RD_BOARD_CARD;
        }
        int secondHighest = PTHandStrengthUtil.secondHighestRank(boardCards).pipValue();
        if (left > lowestBoard && left < secondHighest) {
            return PTHandStrength.ONE_PAIR_PP_HIGHER_THAN_LOWEST_BUT_LOWER_THAN_2ND_HIGHEST;
        }
        int highestBoard = PTHandStrengthUtil.highestRank(boardCards).pipValue();
        if (left < highestBoard && left > secondHighest) {
            return PTHandStrength.ONE_PAIR_PP_MIDDLE_PAIR_LOWER_THAN_HIGHEST_BUT_HIGHER_THAN_2ND_HIGHEST;
        }
        if (left > highestBoard) {
            return PTHandStrength.ONE_PAIR_OVERPAIR;
        }
        return null;
    }
    
    public static PTHandStrength noPocketPair(CardSet holeCards, CardSet boardCards) {
        int left = holeCards.get(0).rankOf().pipValue();
        int right = holeCards.get(1).rankOf().pipValue();
        int highestBoard = PTHandStrengthUtil.highestRank(boardCards).pipValue();
        int lowestBoard = PTHandStrengthUtil.lowestRank(boardCards).pipValue();
        Rank boardCard1 = boardCards.get(0).rankOf();
        Rank boardCard2 = boardCards.get(1).rankOf();
        Rank boardCard3 = boardCards.get(2).rankOf();
        
        if (PTHandStrengthUtil.countRanks(boardCard1, boardCards) > 1
                || PTHandStrengthUtil.countRanks(boardCard2, boardCards) > 1
                || PTHandStrengthUtil.countRanks(boardCard3, boardCards) > 1) {
            return PTHandStrength.ONE_PAIR_BOARD_PAIRED_OR_LOWER_PP_UNDER_3RD_BOARD_CARD;
        }
        
        
        // TODO CHECK:
        // ONE_PAIR_BOARD_PAIRED_OR_LOWER_PP_UNDER_3RD_BOARD_CARD,
        // ONE_PAIR_BOTTOM_PAIR,
        // ONE_PAIR_TOP_PAIR,
        if (left == highestBoard || right == highestBoard) {
            return PTHandStrength.ONE_PAIR_TOP_PAIR;
        }
        if (left == lowestBoard || right == lowestBoard) {
            return PTHandStrength.ONE_PAIR_BOTTOM_PAIR;
        }
        // ONE_PAIR_HIGHER_THAN_LOWEST_BOARD_CARD,
        if (left > lowestBoard && right > lowestBoard && (left < highestBoard && right < highestBoard)) {
            return PTHandStrength.ONE_PAIR_HIGHER_THAN_LOWEST_BOARD_CARD;
        }
        
        return null;
    }
}
