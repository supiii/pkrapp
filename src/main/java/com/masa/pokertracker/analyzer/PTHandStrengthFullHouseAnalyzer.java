package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.PTHandStrengthUtil;
import com.masa.pokertracker.model.PTHandStrength;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author Matti
 */
public class PTHandStrengthFullHouseAnalyzer {
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        if (boardTrips(boardCards)) {
            return whenBoardTripsIDValue(holeCards, boardCards);
        }
        if (boardPaired(boardCards)) {
            return whenBoardPairedIDValue(holeCards, boardCards);
        }
        return 0;
    }

    private static int whenBoardPairedIDValue(CardSet hole, CardSet board) {
        Rank left = hole.get(0).rankOf();
        Rank right = hole.get(1).rankOf();
//        Rank highestBoardRank = PTHandStrengthUtil.highestRank(board);
        boolean pp = left == right;
        Rank pairRank = determinePairRankWhenBoardPaired(board);
        
        if (pp) {    
            if (left.ordinal() < pairRank.ordinal()) {
                return 8;
            }
            return 9;
        }
        else {
            Rank herosPairRank = pairRank == left ? right : left;
            if (herosPairRank.ordinal() < pairRank.ordinal()) {
                return 6;
            }
            return 7;
        }
    }
    
    private static int whenBoardTripsIDValue(CardSet hole, CardSet board) {
        Rank left = hole.get(0).rankOf();
        Rank right = hole.get(1).rankOf();
        Rank highestBoardRank = PTHandStrengthUtil.highestRank(board);
        Rank highestBoardSingleRank = PTHandStrengthUtil.highestSingleRank(board);
        boolean pp = left == right;
        
        if (pp) {
            Rank lowestBoardRank = PTHandStrengthUtil.lowestRank(board);
            
            if (left.ordinal() < lowestBoardRank.ordinal()) {
                return 1;
            }
            if (left.ordinal() > highestBoardRank.ordinal()) {
                return 3;
            }
            return 2;
        }
        
        if (left == highestBoardSingleRank || right == highestBoardSingleRank) {
            return 4;
        }
        
        return 5;
    }
    
    /**
     * 0 - Full house on board (valid for river only)
6 - Board paired, no set, pair below trips
7 - Board paired, no set, pair above trips
8 - Set under pair (33 on a 3JJT9 board)
9 - Set over pair (AA on an A3347 board)
FULLHOUSE_ON_BOARD_RIVER,


     * @param holeCards
     * @param boardCards
     * @return 
     */
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        if (boardTrips(boardCards)) {
            return whenBoardTrips(holeCards, boardCards);
        }
        if (boardPaired(boardCards)) {
            return whenBoardPaired(holeCards, boardCards);
        }
        return PTHandStrength.FULLHOUSE_ON_BOARD_RIVER;
    }

    private static PTHandStrength whenBoardPaired(CardSet hole, CardSet board) {
        Rank left = hole.get(0).rankOf();
        Rank right = hole.get(1).rankOf();
        Rank highestBoardRank = PTHandStrengthUtil.highestRank(board);
        boolean pp = left == right;
        Rank pairRank = determinePairRankWhenBoardPaired(board);
        
        if (pp) {    
            if (left.ordinal() < pairRank.ordinal()) {
                return PTHandStrength.FULLHOUSE_SET_UNDER_PAIR;
            }
            return PTHandStrength.FULLHOUSE_SET_OVER_PAIR;
        }
        else {
            Rank herosPairRank = pairRank == left ? right : left;
            if (herosPairRank.ordinal() < pairRank.ordinal()) {
                return PTHandStrength.FULLHOUSE_BOARD_PAIRED_NO_SET_PAIR_BELOW_TRIPS;
            }
            return PTHandStrength.FULLHOUSE_BOARD_PAIRED_NO_SET_PAIR_ABOVE_TRIPS;
        }
    }
    
    private static PTHandStrength whenBoardTrips(CardSet hole, CardSet board) {
        Rank left = hole.get(0).rankOf();
        Rank right = hole.get(1).rankOf();
        Rank highestBoardRank = PTHandStrengthUtil.highestRank(board);
        Rank highestBoardSingleRank = PTHandStrengthUtil.highestSingleRank(board);
        boolean pp = left == right;
        
        if (pp) {
            Rank lowestBoardRank = PTHandStrengthUtil.lowestRank(board);
            
            if (left.ordinal() < lowestBoardRank.ordinal()) {
                return PTHandStrength.FULLHOUSE_BOARD_TRIPS_LOW_PP;
            }
            if (left.ordinal() > highestBoardRank.ordinal()) {
                return PTHandStrength.FULLHOUSE_BOARD_TRIPS_OVERPAIR;
            }
            return PTHandStrength.FULLHOUSE_BOARD_TRIPS_MIDDLE_PP;
        }
        
        if (left == highestBoardSingleRank || right == highestBoardSingleRank) {
            return PTHandStrength.FULLHOUSE_BOARD_TRIPS_TOP_PAIR;
        }
        
        return PTHandStrength.FULLHOUSE_BOARD_TRIPS_NOT_TOP_PAIR;
    }
    
    private static boolean boardTrips(CardSet board) {
        boolean trips = false;
        boolean pair = false;
        for (Card card : board) {
            if (PTHandStrengthUtil.countRanks(card.rankOf(), board) == 2) {
                pair = true;
            }
            if (PTHandStrengthUtil.countRanks(card.rankOf(), board) == 3) {
                trips = true;
            }
        }
        return trips && !pair;
    }
    
    private static boolean boardPaired(CardSet board) {
        boolean trips = false;
        boolean pair = false;
        for (Card card : board) {
            if (PTHandStrengthUtil.countRanks(card.rankOf(), board) == 2) {
                pair = true;
            }
            if (PTHandStrengthUtil.countRanks(card.rankOf(), board) == 3) {
                trips = true;
            }
        }
        return !trips && pair;
    }

    private static Rank determinePairRankWhenBoardPaired(CardSet board) {
        for (Card c : board) {
            if (PTHandStrengthUtil.countRanks(c.rankOf(), board) == 2) {
                return c.rankOf();
            }
        }
        return null;
    }
    
    private static Rank determineTripsRank(CardSet cards) {
        for (Card c : cards) {
            if (PTHandStrengthUtil.countRanks(c.rankOf(), cards) == 3) {
                return c.rankOf();
            }
        }
        return null;
    }
}
