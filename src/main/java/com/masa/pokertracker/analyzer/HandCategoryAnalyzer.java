package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.model.HandCategory;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author Matti
 */
public class HandCategoryAnalyzer {
    static String[] _cardStrings = { "bad1", "bad2", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace" };
    static Rank[] _cardRanks = { null, null, Rank.TWO, Rank.THREE, Rank.FOUR, Rank.FIVE, Rank.SIX, Rank.SEVEN, Rank.EIGHT, Rank.NINE, Rank.TEN, Rank.JACK, Rank.QUEEN, Rank.KING, Rank.ACE };
    
    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * high-card hand in the {@code com.stevebrecher.poker.HandEval} return
     * value format.
     */
    public static final int NO_PAIR = 0; // HandEval.HandCategory.NO_PAIR.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * one-pair hand in the {@code com.stevebrecher.poker.HandEval} return value
     * format.
     */
    public static final int PAIR = 1; // HandEval.HandCategory.PAIR.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * two-pair hand in the {@code com.stevebrecher.poker.HandEval} return value
     * format.
     */
    public static final int TWO_PAIR = 2; // HandEval.HandCategory.TWO_PAIR.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * trips hand in the {@code com.stevebrecher.poker.HandEval} return value
     * format.
     */
    public static final int THREE_OF_A_KIND = 3; // HandEval.HandCategory.THREE_OF_A_KIND.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * straight hand in the {@code com.stevebrecher.poker.HandEval} return value
     * format.
     */
    public static final int STRAIGHT = 4; // HandEval.HandCategory.STRAIGHT.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * flush hand in the {@code com.stevebrecher.poker.HandEval} return value
     * format.
     */
    public static final int FLUSH = 5; // HandEval.HandCategory.FLUSH.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * full house hand in the {@code com.stevebrecher.poker.HandEval} return
     * value format.
     */
    public static final int FULL_HOUSE = 6; // HandEval.HandCategory.FULL_HOUSE.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * quads hand in the {@code com.stevebrecher.poker.HandEval} return value
     * format.
     */
    public static final int FOUR_OF_A_KIND = 7; // HandEval.HandCategory.FOUR_OF_A_KIND.ordinal();

    /**
     * A constant (that may be used in a {@code switch} statement) indicating a
     * straight flush hand in the {@code com.stevebrecher.poker.HandEval} return
     * value format.
     */
    public static final int STRAIGHT_FLUSH = 8; // HandEval.HandCategory.STRAIGHT_FLUSH.ordinal();

    public static HandCategory handCategory(CardSet holeCards, CardSet boardCards) {
        return handCategory(getRank(holeCards, boardCards));
    }
    
    private static int getRank(CardSet holeCards, CardSet boardCards) {
        int rank = 0;
        CardSet allCards = new CardSet(holeCards);
                
        allCards.addAll(boardCards);
        
        switch(allCards.size()) {
            case 5:
                rank = HandEval.hand5Eval(allCards);
                break;
            case 6:
                rank = HandEval.hand6Eval(allCards);
                break;
            case 7:
                rank = HandEval.hand7Eval(allCards);
                break;
        }
        
        return rank;
    }
    
    public static HandCategory handCategory(int brecherValue) {
        int category = brecherValue >> 24;
        HandCategory handCategory;
        switch (category) {
            case PAIR:
                handCategory = HandCategory.PAIR;
                break;
            case TWO_PAIR:
                handCategory = HandCategory.TWO_PAIR;
                break;
            case THREE_OF_A_KIND:
                handCategory = HandCategory.THREE_OF_A_KIND;
                break;
            case STRAIGHT:
                handCategory = HandCategory.STRAIGHT;
                break;
            case FLUSH:
                handCategory = HandCategory.FLUSH;
                break;
            case FULL_HOUSE:
                handCategory = HandCategory.FULL_HOUSE;
                break;
            case FOUR_OF_A_KIND:
                handCategory = HandCategory.FOUR_OF_A_KIND;
                break;
            case STRAIGHT_FLUSH:
                handCategory = HandCategory.STRAIGHT_FLUSH;
                break;
            default:
                handCategory = HandCategory.NO_PAIR;
        }
        return handCategory;
    }
    
    /**
     * Given a {@code com.stevebrecher.poker.HandEval} return value, return a
     * string that describes the made hand.
     *
     * @param brecherValue a {@code com.stevebrecher.poker.HandEval} return
     * value.
     * @return a string that describes the made hand.
     */
    public static String handString(int brecherValue) {
        StringBuffer sb = new StringBuffer();
        int category = brecherValue >> 24;
        int topNibble = (brecherValue >> 20) & 0xf;
        int botNibble = (brecherValue >> 16) & 0xf;
        int kickers = brecherValue & 0xffff;
        switch (category) {
            case PAIR:
                sb.append("a pair of ");
                sb.append(plural(botNibble));
                sb.append(" with a ");
                sb.append(_cardStrings[kicker(kickers, 1)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 2)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 3)]);
                sb.append(" kicker");
                break;
            case TWO_PAIR:
                sb.append("two pair, ");
                sb.append(plural(topNibble));
                sb.append(" and ");
                sb.append(plural(botNibble));
                sb.append(" with a ");
                sb.append(_cardStrings[kicker(kickers, 1)]);
                sb.append(" kicker");
                break;
            case THREE_OF_A_KIND:
                sb.append("three of a kind, ");
                sb.append(plural(botNibble));
                sb.append(" with a ");
                sb.append(_cardStrings[kicker(kickers, 1)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 2)]);
                sb.append(" kicker");
                break;
            case STRAIGHT:
                sb.append("a straight, ");
                sb.append(_cardStrings[botNibble]);
                sb.append(" high ");
                break;
            case FLUSH:
                sb.append("a flush, ");
                sb.append(_cardStrings[kicker(kickers, 1)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 2)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 3)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 4)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 5)]);
                break;
            case FULL_HOUSE:
                sb.append("a full house, ");
                sb.append(plural(botNibble));
                sb.append(" full of ");
                sb.append(plural(kicker(kickers, 1)));
                break;
            case FOUR_OF_A_KIND:
                sb.append("four of a kind, ");
                sb.append(plural(botNibble));
                sb.append(" with a ");
                sb.append(_cardStrings[kicker(kickers, 1)]);
                sb.append(" kicker");
                break;
            case STRAIGHT_FLUSH:
                sb.append("a straight flush, ");
                sb.append(_cardStrings[botNibble]);
                sb.append(" high ");
            default:
                sb.append("high card ");
                sb.append(_cardStrings[kicker(kickers, 1)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 2)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 3)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 4)]);
                sb.append(" ");
                sb.append(_cardStrings[kicker(kickers, 5)]);
        }
        return sb.toString();
    }

    private static int kicker(int mask, int pos) {
        int found = 0;
        int bit = 0x1000;
        int bnum = 14;
        while (bit != 0) {
            if ((bit & mask) != 0 && ++found == pos) {
                return bnum;
            }
            bnum--;
            bit >>= 1;
        }
        return 0;
    }

    private static String plural(int val) {
        return _cardStrings[val] + (val == 6 ? "es" : "s");
    }
}
