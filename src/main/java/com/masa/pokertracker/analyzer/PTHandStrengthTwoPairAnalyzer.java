package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.PTHandStrengthUtil;
import com.masa.pokertracker.model.PTHandStrength;
import java.util.ArrayList;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

public class PTHandStrengthTwoPairAnalyzer {
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        if (isBoardDoublePaired(boardCards)) {
            return 0;
        }
        if (!PTHandStrengthUtil.isPP(holeCards) && isBoardPaired(boardCards)) {
            return whenBoardPairedIDValue(holeCards, boardCards);
        }
        if (PTHandStrengthUtil.isPP(holeCards)) {
            return whenPPIDValue(holeCards, boardCards);
        }
        
//7 - Low two pair (78 on a K78T3 board)
//8 - Top and middle pair (K7 on a K78T3 board)
//9 - Top two pair (KT on a K78T3 board)*/
        //    ,
//    TWO_PAIR_TOP_AND_MIDDLE,
//    ,
        Rank highestRank = PTHandStrengthUtil.highestRank(boardCards);
        Rank secondHighestRank = PTHandStrengthUtil.secondHighestRank(boardCards);
//        Rank thirdHighestRank = PTHandStrengthUtil.thirdHighestRank(boardCards);
        Rank left = holeCards.get(0).rankOf();
        Rank right = holeCards.get(1).rankOf();
        
        if (left == highestRank && right == secondHighestRank || right == highestRank && left == secondHighestRank) {
            return 9;
        }
        if (left == highestRank || right == highestRank) {
            return 8;
        }
        
        return 7;
    }
    
    /**
     * 
     * @param holeCards
     * @param boardCards
     * @return 
     */
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        if (isBoardDoublePaired(boardCards)) {
            return PTHandStrength.TWO_PAIR_BOARD_DOUBLE_PAIRED_TURN_OR_RIVER;
        }
        if (!PTHandStrengthUtil.isPP(holeCards) && isBoardPaired(boardCards)) {
            return whenBoardPaired(holeCards, boardCards);
        }
        if (PTHandStrengthUtil.isPP(holeCards)) {
            return whenPP(holeCards, boardCards);
        }
        
//7 - Low two pair (78 on a K78T3 board)
//8 - Top and middle pair (K7 on a K78T3 board)
//9 - Top two pair (KT on a K78T3 board)*/
        //    ,
//    TWO_PAIR_TOP_AND_MIDDLE,
//    ,
        Rank highestRank = PTHandStrengthUtil.highestRank(boardCards);
        Rank secondHighestRank = PTHandStrengthUtil.secondHighestRank(boardCards);
        Rank thirdHighestRank = PTHandStrengthUtil.thirdHighestRank(boardCards);
        Rank left = holeCards.get(0).rankOf();
        Rank right = holeCards.get(1).rankOf();
        
        if (left == highestRank && right == secondHighestRank || right == highestRank && left == secondHighestRank) {
            return PTHandStrength.TWO_PAIR_TOP_TWO;
        }
        if (left == highestRank || right == highestRank) {
            return PTHandStrength.TWO_PAIR_TOP_AND_MIDDLE;
        }
        
        return PTHandStrength.TWO_PAIR_LOW;
    }
    
    private static int whenPPIDValue(CardSet holeCards, CardSet boardCards) {
        CardSet all = new CardSet(); 
        all.addAll(holeCards);
        all.addAll(boardCards);
        if (PTHandStrengthUtil.highestRank(all) == holeCards.get(0).rankOf()) {
            return 6;
        }
        if (PTHandStrengthUtil.secondHighestRank(all) == holeCards.get(0).rankOf()) {
            return 5;
        }
        return 4;
    }
    
    //        1 - Board Paired, low pair (34 on a KK378 board)
//        2 - Board Paired, middle pair (QJ on a AT8JT board)
//        3 - Board Paired, top pair (KQ on a JTTQ8 board)
//    TWO_PAIR_BOARD_PAIRED_LOW_PAIR,
//    TWO_PAIR_BOARD_PAIRED_MIDDLE_PAIR,
//    ,
    private static int whenBoardPairedIDValue(CardSet holeCards, CardSet boardCards) {
//        for (Card card : boardCards) {
            Rank highestSingleRank = PTHandStrengthUtil.highestSingleRank(boardCards);
            if (holeCards.get(0).rankOf() == highestSingleRank || holeCards.get(1).rankOf() == highestSingleRank) {
                return 3;
            }
            Rank secondHighestSingleRank = PTHandStrengthUtil.secondHighestSingleRank(boardCards);
            if (holeCards.get(0).rankOf() == highestSingleRank || holeCards.get(1).rankOf() == secondHighestSingleRank) {
                return 2;
            }
            Rank thirdHighestSingleRank = PTHandStrengthUtil.thirdHighestSingleRank(boardCards);
            if (holeCards.get(0).rankOf() == highestSingleRank 
                    || (thirdHighestSingleRank != null && holeCards.get(1).rankOf() == thirdHighestSingleRank)) {
                return 1;
            }
//        }
        return 0;
    }
    
//    4 - Pocket Pair, low pair (88 on a 6TTJ3 board)
//    5 - Pocket Pair, middle pair (JJ on an A7633 board)
//    6 - Pocket Pair, overpair
    //    ,
//    ,
//    ,
    private static PTHandStrength whenPP(CardSet holeCards, CardSet boardCards) {
        CardSet all = new CardSet(); 
        all.addAll(holeCards);
        all.addAll(boardCards);
        if (PTHandStrengthUtil.highestRank(all) == holeCards.get(0).rankOf()) {
            return PTHandStrength.TWO_PAIR_PP_OVERPAIR;
        }
        if (PTHandStrengthUtil.secondHighestRank(all) == holeCards.get(0).rankOf()) {
            return PTHandStrength.TWO_PAIR_PP_MIDDLE_PAIR;
        }
        return PTHandStrength.TWO_PAIR_PP_LOW_PAIR;
    }
    
    //        1 - Board Paired, low pair (34 on a KK378 board)
//        2 - Board Paired, middle pair (QJ on a AT8JT board)
//        3 - Board Paired, top pair (KQ on a JTTQ8 board)
//    TWO_PAIR_BOARD_PAIRED_LOW_PAIR,
//    TWO_PAIR_BOARD_PAIRED_MIDDLE_PAIR,
//    ,
    private static PTHandStrength whenBoardPaired(CardSet holeCards, CardSet boardCards) {
        for (Card card : boardCards) {
            Rank highestSingleRank = PTHandStrengthUtil.highestSingleRank(boardCards);
            if (holeCards.get(0).rankOf() == highestSingleRank || holeCards.get(1).rankOf() == highestSingleRank) {
                return PTHandStrength.TWO_PAIR_BOARD_PAIRED_TOP_PAIR;
            }
            Rank secondHighestSingleRank = PTHandStrengthUtil.secondHighestSingleRank(boardCards);
            if (holeCards.get(0).rankOf() == highestSingleRank || holeCards.get(1).rankOf() == secondHighestSingleRank) {
                return PTHandStrength.TWO_PAIR_BOARD_PAIRED_MIDDLE_PAIR;
            }
            Rank thirdHighestSingleRank = PTHandStrengthUtil.thirdHighestSingleRank(boardCards);
            if (holeCards.get(0).rankOf() == highestSingleRank 
                    || (thirdHighestSingleRank != null && holeCards.get(1).rankOf() == thirdHighestSingleRank)) {
                return PTHandStrength.TWO_PAIR_BOARD_PAIRED_LOW_PAIR;
            }
        }
        return null;
    }
    
    public static boolean isBoardDoublePaired(CardSet boardCards) {
        if (boardCards.size() < 4) {
            return false;
        }
        int pairs = 0;
        List<Rank> checkedRanks = new ArrayList<>();
        // 22667
        for (Card card : boardCards) {
            if (checkedRanks.contains(card.rankOf())) {
                continue;
            }
            int ranks = PTHandStrengthUtil.countRanks(card.rankOf(), boardCards);
            if (ranks > 1) {
                pairs++;
            }
            checkedRanks.add(card.rankOf());
        }
        return pairs > 1;
    }
    
    public static boolean isBoardPaired(CardSet boardCards) {
        int pairs = 0;
        List<Rank> checkedRanks = new ArrayList<>();
        // 22667
        for (Card card : boardCards) {
            if (checkedRanks.contains(card.rankOf())) {
                continue;
            }
            int ranks = PTHandStrengthUtil.countRanks(card.rankOf(), boardCards);
            if (ranks > 1) {
                pairs++;
            }
            checkedRanks.add(card.rankOf());
        }
        return pairs == 1;
    }
}
