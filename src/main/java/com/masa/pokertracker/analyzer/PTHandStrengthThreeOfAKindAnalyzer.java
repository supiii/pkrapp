package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.PTHandStrengthUtil;
import com.masa.pokertracker.model.PTHandStrength;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author Matti
 */
public class PTHandStrengthThreeOfAKindAnalyzer {
    
//    0 - Board trips
//1 - Board paired, low (43 on 4T64Q board)
//2 - Board paired, middle (KQ on Q93AQ board)
//3 - Board paired, high (AQ on A932A board)
//4 - Low set
//5 - Middle set
//6 - Top set*/
//THREEOAK_BOARD_TRIPS,
//THREEOAK_BOARD_PAIRED_LOW,
//THREEOAK_BOARD_PAIRED_MIDDLE,
//THREEOAK_BOARD_PAIRED_HIGH,
//THREEOAK_LOW_SET,
//THREEOAK_MIDDLE_SET,
//THREEOAK_TOP_SET,
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        if (boardTrips(boardCards)) {
            return 0;
        }
        if (boardPaired(boardCards)) {
            return whenBoardPairedIDValue(holeCards, boardCards);
        }
        return whenSetIDValue(holeCards, boardCards);
    }
    
    private static int whenSetIDValue(CardSet holeCards, CardSet boardCards) {
        Rank left = holeCards.get(0).rankOf();
        Rank highest = PTHandStrengthUtil.highestRank(boardCards);
        if (highest == left) {
            return 6;
        }
        Rank secondHighest = PTHandStrengthUtil.secondHighestRank(boardCards);
        if (secondHighest == left) {
            return 5;
        }
        return 4;
    }
    
    private static int whenBoardPairedIDValue(CardSet holeCards, CardSet boardCards) {
        Rank left = holeCards.get(0).rankOf();
        Rank right = holeCards.get(1).rankOf();
        Rank highest = PTHandStrengthUtil.highestRank(boardCards);
        if (highest == left || highest == right) {
            return 3;
        }
        Rank secondHighest = PTHandStrengthUtil.secondHighestRank(boardCards);
        if (secondHighest == left || secondHighest == right) {
            return 2;
        }
        return 1;
    }
    
    
    /**
     * 
     * @param holeCards
     * @param boardCards
     * @return 
     */
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        if (boardTrips(boardCards)) {
            return PTHandStrength.THREEOAK_BOARD_TRIPS;
        }
        if (boardPaired(boardCards)) {
            return whenBoardPaired(holeCards, boardCards);
        }
        return whenSet(holeCards, boardCards);
    }
    
    private static PTHandStrength whenSet(CardSet holeCards, CardSet boardCards) {
        Rank left = holeCards.get(0).rankOf();
        Rank highest = PTHandStrengthUtil.highestRank(boardCards);
        if (highest == left) {
            return PTHandStrength.THREEOAK_TOP_SET;
        }
        Rank secondHighest = PTHandStrengthUtil.secondHighestRank(boardCards);
        if (secondHighest == left) {
            return PTHandStrength.THREEOAK_MIDDLE_SET;
        }
        return PTHandStrength.THREEOAK_LOW_SET;
    }
    
    private static PTHandStrength whenBoardPaired(CardSet holeCards, CardSet boardCards) {
        Rank left = holeCards.get(0).rankOf();
        Rank right = holeCards.get(1).rankOf();
        Rank highest = PTHandStrengthUtil.highestRank(boardCards);
        if (highest == left || highest == right) {
            return PTHandStrength.THREEOAK_BOARD_PAIRED_HIGH;
        }
        Rank secondHighest = PTHandStrengthUtil.secondHighestRank(boardCards);
        if (secondHighest == left || secondHighest == right) {
            return PTHandStrength.THREEOAK_BOARD_PAIRED_MIDDLE;
        }
        return PTHandStrength.THREEOAK_BOARD_PAIRED_LOW;
    }
    
    private static boolean boardTrips(CardSet board) {
        for (Card card : board) {
            if (PTHandStrengthUtil.countRanks(card.rankOf(), board) > 2) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean boardPaired(CardSet board) {
        for (Card card : board) {
            if (PTHandStrengthUtil.countRanks(card.rankOf(), board) == 2) {
                return true;
            }
        }
        return false;
    }
}
