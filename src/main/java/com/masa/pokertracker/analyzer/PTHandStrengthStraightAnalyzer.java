package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.PTHandStrengthUtil;
import com.masa.pokertracker.model.PTHandStrength;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author Matti
 */
public class PTHandStrengthStraightAnalyzer {
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        Rank left = holeCards.get(0).rankOf();
        Rank right = holeCards.get(1).rankOf();
        CardSet cardsWithBoth = new CardSet(boardCards);
        cardsWithBoth.add(holeCards.get(0));
        cardsWithBoth.add(holeCards.get(1));
        int brecherValue = HandEval.handEval(cardsWithBoth);
        
        int botNibble = (brecherValue >> 16) & 0xf;
        Rank highestStraightCard = HandCategoryAnalyzer._cardRanks[botNibble];
        
        boolean leftIsInStraight = false;
        boolean rightIsInStraight = false;
        
        List<Rank> boardRanksInStraight = new ArrayList<>();
        
        
        // check if holecards are in straight
        for (int i = 0;i < 5;i++) {
            int n = highestStraightCard.ordinal() - i; 
            if (n < 0) {
                break;
            }
            Rank rank = Rank.values()[n];
            
            if (left == rank && PTHandStrengthUtil.countRanks(rank, cardsWithBoth) == 1) {
                leftIsInStraight = true;
                continue;
            }
            if (right == rank && PTHandStrengthUtil.countRanks(rank, cardsWithBoth) == 1) {
                rightIsInStraight = true;
                continue;
            }
            boardRanksInStraight.add(rank);
        }
        Collections.sort(boardRanksInStraight);
        
        Rank highestBoardInStraight = boardRanksInStraight.get(boardRanksInStraight.size() -1);
        
        // both holecards in straight
        if (leftIsInStraight && rightIsInStraight) {
            // check if there is gaps between board cards
            // 2 gaps
            if (highestBoardInStraight.ordinal() - boardRanksInStraight.get(0).ordinal() == 4) {
                return 5;
            }
            // 1 gap
            if (highestBoardInStraight.ordinal() - boardRanksInStraight.get(0).ordinal() == 3) {
                // if highest board is not ACE, check if highest is holecard
                if (highestBoardInStraight != Rank.ACE && highestStraightCard == left || highestStraightCard == right) {
                    return 5;
                }
                // if board highest is ACE
                if (highestBoardInStraight == Rank.ACE) {
                    return 5;
                }
            } 
            // no gaps
            if (highestBoardInStraight.ordinal() - boardRanksInStraight.get(0).ordinal() == 2) {
                if (highestBoardInStraight == Rank.ACE) {
                    return 5;
                }
                if (highestBoardInStraight == Rank.KING && left == Rank.ACE || right == Rank.ACE) {
                    return 5;
                }
                if (left.ordinal() > highestBoardInStraight.ordinal() && right.ordinal() > highestBoardInStraight.ordinal()) {
                    return 5;
                }
            }
            return 4;
        }
        if (leftIsInStraight) {
            if (left.ordinal() > highestBoardInStraight.ordinal()) {
                return 3;
            }
            if (left.ordinal() > boardRanksInStraight.get(0).ordinal()) {
                return 2;
            }
            return 1;
        }
        if (rightIsInStraight) {
            if (right.ordinal() > highestBoardInStraight.ordinal()) {
                return 3;
            }
            if (right.ordinal() > boardRanksInStraight.get(0).ordinal()) {
                return 2;
            }
            return 1;
        }
//        if (twoHoleCards(holeCards, boardCards)) {
//            return whenTwoHoleCards(holeCards, boardCards);
//        }
        return 0;
    }
    
    /**
     *0 - Straight on board (valid for river only)
1 - One card, low end (A4 on a 5678K board)
2 - One card, middle
3 - One card, high end
4 - Two Card Non-nut straight
5 - Two Card Nut straight
//STRAIGHT_ON_BOARD_RIVER,
//STRAIGHT_ONE_CARD_LOW_END,
//STRAIGHT_ONE_CARD_MIDDLE,
//STRAIGHT_ONE_CARD_HIGH_END,
//STRAIGHT_TWO_CARD_NON_NUT,
//STRAIGHT_TWO_CARD_NUT_STRAIGHT, 
     * @param holeCards
     * @param boardCards
     * @return 
     */
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        Rank left = holeCards.get(0).rankOf();
        Rank right = holeCards.get(1).rankOf();
        CardSet cardsWithBoth = new CardSet(boardCards);
        cardsWithBoth.add(holeCards.get(0));
        cardsWithBoth.add(holeCards.get(1));
        int brecherValue = HandEval.handEval(cardsWithBoth);
        
        int botNibble = (brecherValue >> 16) & 0xf;
        Rank highestStraightCard = HandCategoryAnalyzer._cardRanks[botNibble];
        
        boolean leftIsInStraight = false;
        boolean rightIsInStraight = false;
        
        List<Rank> boardRanksInStraight = new ArrayList<>();
        
        
        // check if holecards are in straight
        for (int i = 0;i < 5;i++) {
            Rank rank = Rank.values()[highestStraightCard.ordinal() - i];
            
            if (left == rank && PTHandStrengthUtil.countRanks(rank, cardsWithBoth) == 1) {
                leftIsInStraight = true;
                continue;
            }
            if (right == rank && PTHandStrengthUtil.countRanks(rank, cardsWithBoth) == 1) {
                rightIsInStraight = true;
                continue;
            }
            boardRanksInStraight.add(rank);
        }
        Collections.sort(boardRanksInStraight);
        
        Rank highestBoardInStraight = boardRanksInStraight.get(boardRanksInStraight.size() -1);
        
        // both holecards in straight
        if (leftIsInStraight && rightIsInStraight) {
            // check if there is gaps between board cards
            // 2 gaps
            if (highestBoardInStraight.ordinal() - boardRanksInStraight.get(0).ordinal() == 4) {
                return PTHandStrength.STRAIGHT_TWO_CARD_NUT_STRAIGHT;
            }
            // 1 gap
            if (highestBoardInStraight.ordinal() - boardRanksInStraight.get(0).ordinal() == 3) {
                // if highest board is not ACE, check if highest is holecard
                if (highestBoardInStraight != Rank.ACE && highestStraightCard == left || highestStraightCard == right) {
                    return PTHandStrength.STRAIGHT_TWO_CARD_NUT_STRAIGHT;
                }
                // if board highest is ACE
                if (highestBoardInStraight == Rank.ACE) {
                    return PTHandStrength.STRAIGHT_TWO_CARD_NUT_STRAIGHT;
                }
            } 
            // no gaps
            if (highestBoardInStraight.ordinal() - boardRanksInStraight.get(0).ordinal() == 2) {
                if (highestBoardInStraight == Rank.ACE) {
                    return PTHandStrength.STRAIGHT_TWO_CARD_NUT_STRAIGHT;
                }
                if (highestBoardInStraight == Rank.KING && left == Rank.ACE || right == Rank.ACE) {
                    return PTHandStrength.STRAIGHT_TWO_CARD_NUT_STRAIGHT;
                }
                if (left.ordinal() > highestBoardInStraight.ordinal() && right.ordinal() > highestBoardInStraight.ordinal()) {
                    return PTHandStrength.STRAIGHT_TWO_CARD_NUT_STRAIGHT;
                }
            }
            return PTHandStrength.STRAIGHT_TWO_CARD_NON_NUT;
        }
        if (leftIsInStraight) {
            if (left.ordinal() > highestBoardInStraight.ordinal()) {
                return PTHandStrength.STRAIGHT_ONE_CARD_HIGH_END;
            }
            if (left.ordinal() > boardRanksInStraight.get(0).ordinal()) {
                return PTHandStrength.STRAIGHT_ONE_CARD_MIDDLE;
            }
            return PTHandStrength.STRAIGHT_ONE_CARD_LOW_END;
        }
        if (rightIsInStraight) {
            if (right.ordinal() > highestBoardInStraight.ordinal()) {
                return PTHandStrength.STRAIGHT_ONE_CARD_HIGH_END;
            }
            if (right.ordinal() > boardRanksInStraight.get(0).ordinal()) {
                return PTHandStrength.STRAIGHT_ONE_CARD_MIDDLE;
            }
            return PTHandStrength.STRAIGHT_ONE_CARD_LOW_END;
        }
//        if (twoHoleCards(holeCards, boardCards)) {
//            return whenTwoHoleCards(holeCards, boardCards);
//        }
        return PTHandStrength.STRAIGHT_ON_BOARD_RIVER;
    }
    
}
