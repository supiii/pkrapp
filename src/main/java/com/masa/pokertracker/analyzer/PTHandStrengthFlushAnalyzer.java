package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.PTHandStrengthUtil;
import com.masa.pokertracker.model.PTHandStrength;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author Matti
 */
public class PTHandStrengthFlushAnalyzer {
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        Suit flushSuit = determineFlushSuit(holeCards, boardCards);
        Card heroFlushHigh = determineHeroFlushHi(flushSuit, holeCards);
        // Ks2s
        // 7sTsJs
        List<Card> boardFlushCards = getFlushBoardCards(flushSuit, boardCards);
//        CardSet flushBoard = getFlushBoardCardsAsSet(flushSuit, boardCards);
//        Rank highestBoardFlushRank = boardFlushCards.get(boardFlushCards.size() -1).rankOf();
        
        
        int ndNut = 1;
        // reverse iterate Ranks
        outerloop:
        for (int i = Rank.values().length - 1; i >= 0; --i) {
            Rank r = Rank.values()[i];
            
            for (Card c : boardFlushCards) {
                if (c.rankOf() == r) {
                    continue outerloop;
                }
            }
            
            if (r == heroFlushHigh.rankOf()) {
                break;
            }
            
            ndNut++;
        }
        
        if (ndNut == 1) {
            return 10;
        }
        if (ndNut == 2) {
            return 9;
        }
        if (ndNut == 3) {
            return 8;
        }
//        if (ndNut == 4) {
//            return PTHandStrength.FLUSH_4TH_NUT;
//        }
        
        return 7;
    }
    
    /**
     *  FLUSH_NUT,
        FLUSH_2ND_NUT,
        FLUSH_3RD_NUT,
        FLUSH_4TH_NUT,
        FLUSH_5TH_OR_LOWER_NUT, 
     * @param holeCards
     * @param boardCards
     * @return 
     */
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        Suit flushSuit = determineFlushSuit(holeCards, boardCards);
        Card heroFlushHigh = determineHeroFlushHi(flushSuit, holeCards);
        // Ks2s
        // 7sTsJs
        List<Card> boardFlushCards = getFlushBoardCards(flushSuit, boardCards);
//        CardSet flushBoard = getFlushBoardCardsAsSet(flushSuit, boardCards);
//        Rank highestBoardFlushRank = boardFlushCards.get(boardFlushCards.size() -1).rankOf();
        
        
        int ndNut = 1;
        // reverse iterate Ranks
        outerloop:
        for (int i = Rank.values().length - 1; i >= 0; --i) {
            Rank r = Rank.values()[i];
            
            for (Card c : boardFlushCards) {
                if (c.rankOf() == r) {
                    continue outerloop;
                }
            }
            
            if (r == heroFlushHigh.rankOf()) {
                break;
            }
            
            ndNut++;
        }
        
        if (ndNut == 1) {
            return PTHandStrength.FLUSH_NUT;
        }
        if (ndNut == 2) {
            return PTHandStrength.FLUSH_2ND_NUT;
        }
        if (ndNut == 3) {
            return PTHandStrength.FLUSH_3RD_NUT;
        }
//        if (ndNut == 4) {
//            return PTHandStrength.FLUSH_4TH_NUT;
//        }
        
        return PTHandStrength.FLUSH_4TH_OR_LOWER_NUT;
    }
    
    private static List<Card> getFlushBoardCards(Suit flushSuit, CardSet board) {
        List<Card> boardFlushCards = new ArrayList<>();
        for (Card card : board) {
            if (card.suitOf() == flushSuit) {
                boardFlushCards.add(card);
            }
        }
        Collections.sort(boardFlushCards);
        return boardFlushCards;
    }
    
    private static CardSet getFlushBoardCardsAsSet(Suit flushSuit, CardSet board) {
        CardSet boardFlushCards = new CardSet();
        for (Card card : board) {
            if (card.suitOf() == flushSuit) {
                boardFlushCards.add(card);
            }
        }
        return boardFlushCards;
    }
    
    private static Card determineHeroFlushHi(Suit flushSuit, CardSet holeCards) {
        Card left = holeCards.get(0);
        Card right = holeCards.get(1);
        
        if (left.suitOf() == flushSuit && right.suitOf() == flushSuit) {
            return left.rankOf().ordinal() > right.rankOf().ordinal() ? left : right;
        }
        if (left.suitOf() == flushSuit) {
            return left;
        } else {
            return right;
        }
    }
    
    private static Suit determineFlushSuit(CardSet hole, CardSet cards) {
        CardSet allCards = new CardSet(hole);
        allCards.addAll(cards);
        
        int s = PTHandStrengthUtil.countSuits(Suit.SPADE, allCards);
        int c = PTHandStrengthUtil.countSuits(Suit.CLUB, allCards);
        int h = PTHandStrengthUtil.countSuits(Suit.HEART, allCards);
        int d = PTHandStrengthUtil.countSuits(Suit.DIAMOND, allCards);
        
        if (s > 4) {
            return Suit.SPADE;
        }
        if (c > 4) {
            return Suit.CLUB;
        }
        if (h > 4) {
            return Suit.HEART;
        }
        if (d > 4) {
            return Suit.DIAMOND;
        }
        
        return null;
    }
}
