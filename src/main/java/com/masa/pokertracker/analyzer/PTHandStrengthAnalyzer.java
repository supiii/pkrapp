package com.masa.pokertracker.analyzer;

import com.masa.pokertracker.model.PTHandStrength;
import com.masa.pokertracker.model.HandCategory;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;

/**
 *
 * @author Matti
 */
public class PTHandStrengthAnalyzer {
    
    public static int getPTHandStrengthIDValue(CardSet holeCards, CardSet boardCards) {
        PTHandStrength ptHandStrength = null;
        
        int rank = getRank(holeCards, boardCards);
        
        HandCategory handCategory = HandCategoryAnalyzer.handCategory(rank);
        
        int strength = 0;
        
        if (handCategory == HandCategory.NO_PAIR) {
            strength = PTHandStrengthHighCardAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.PAIR) {
            strength =PTHandStrengthPairAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.TWO_PAIR) {
            strength = PTHandStrengthTwoPairAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.THREE_OF_A_KIND) {
            strength = PTHandStrengthThreeOfAKindAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.STRAIGHT) {
            strength = PTHandStrengthStraightAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.FLUSH) {
            strength = PTHandStrengthFlushAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.FULL_HOUSE) {
            strength = PTHandStrengthFullHouseAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.FOUR_OF_A_KIND) {
            strength = PTHandStrengthFourOfAKindAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        if (handCategory == HandCategory.STRAIGHT_FLUSH) {
            strength = PTHandStrengthStraightFlushAnalyzer.getPTHandStrengthIDValue(holeCards, boardCards);
        }
        
//        return Integer.parseInt(handCategory.value() + "" + strenth);
        return strength;
    }
    
    public static PTHandStrength getPTHandStrength(CardSet holeCards, CardSet boardCards) {
        PTHandStrength ptHandStrength = null;
        
        int rank = getRank(holeCards, boardCards);
        
        HandCategory handCategory = HandCategoryAnalyzer.handCategory(rank);
        
        if (handCategory == HandCategory.NO_PAIR) {
            return PTHandStrengthHighCardAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.PAIR) {
            return PTHandStrengthPairAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.TWO_PAIR) {
            return PTHandStrengthTwoPairAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.THREE_OF_A_KIND) {
            return PTHandStrengthThreeOfAKindAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.STRAIGHT) {
            return PTHandStrengthStraightAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.FLUSH) {
            return PTHandStrengthFlushAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.FULL_HOUSE) {
            return PTHandStrengthFullHouseAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.FOUR_OF_A_KIND) {
            return PTHandStrengthFourOfAKindAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        if (handCategory == HandCategory.STRAIGHT_FLUSH) {
            return PTHandStrengthStraightFlushAnalyzer.getPTHandStrength(holeCards, boardCards);
        }
        
        return ptHandStrength;
    }
    
    private static int getRank(CardSet holeCards, CardSet boardCards) {
        int rank = 0;
        CardSet allCards = new CardSet(holeCards);
                
        allCards.addAll(boardCards);
        
        switch(allCards.size()) {
            case 5:
                rank = HandEval.hand5Eval(allCards);
                break;
            case 6:
                rank = HandEval.hand6Eval(allCards);
                break;
            case 7:
                rank = HandEval.hand7Eval(allCards);
                break;
        }
        
        return rank;
    }
    
    public static int getBrecherValueRank(CardSet cardSet) {
        int rank = 0;
        switch(cardSet.size()) {
            case 5:
                rank = HandEval.hand5Eval(cardSet);
                break;
            case 6:
                rank = HandEval.hand6Eval(cardSet);
                break;
            case 7:
                rank = HandEval.hand7Eval(cardSet);
                break;
        }
        
        return rank;
    }
}
