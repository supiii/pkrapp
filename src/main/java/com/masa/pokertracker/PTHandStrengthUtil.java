package com.masa.pokertracker;

import com.masa.pokertracker.model.HandCategory;
import com.masa.pokertracker.model.PTHandStrength;
import com.masa.pokertracker.model.PTMadeHand;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author Matti
 */
public class PTHandStrengthUtil {

    public static PTMadeHand getPTMadeHand(int category, int strengthWithinCategory) {
        PTMadeHand madeHand = new PTMadeHand();
        int strengthAddOrdinal = 0;
        madeHand.setHandCategory(HandCategory.valueOf(String.valueOf(category)));

        switch (madeHand.getHandCategory()) {
            case NO_PAIR:
                break;
            case PAIR:
                strengthAddOrdinal = 3;
                break;
            case TWO_PAIR:
                strengthAddOrdinal = 10;
                break;
            case THREE_OF_A_KIND:
                strengthAddOrdinal = 20;
                break;
            case STRAIGHT:
                strengthAddOrdinal = 27;
                break;
            case FLUSH:
                strengthAddOrdinal = 33;
                break;
            case FULL_HOUSE:
                strengthAddOrdinal = 38;
            case FOUR_OF_A_KIND:
                strengthAddOrdinal = 48;
                break;
            case STRAIGHT_FLUSH:
                strengthAddOrdinal = 49;
                break;
        }

        madeHand.setPtHandStrength(PTHandStrength.values()[strengthAddOrdinal]);

        return madeHand;
    }

    public static boolean isPP(CardSet holeCards) {
        return holeCards.get(0).rankOf() == holeCards.get(1).rankOf();
    }

    public static boolean isSuited(CardSet holeCards) {
        return holeCards.get(0).suitOf() == holeCards.get(1).suitOf();
    }

    public static Rank highestRank(CardSet cardSet) {
        Rank highestRank = null;

        for (Card card : cardSet) {
            if (highestRank == null) {
                highestRank = card.rankOf();
                continue;
            }
            if (card.rankOf().pipValue() > highestRank.pipValue()) {
                highestRank = card.rankOf();
            }
        }

        return highestRank;
    }

    public static Rank highestSingleRank(CardSet cardSet) {
        CardSet cardSetSingles = removePairsAndAbove(cardSet);
        Rank highestRank = null;

        for (Card card : cardSetSingles) {
            if (highestRank == null) {
                highestRank = card.rankOf();
                continue;
            }
            if (card.rankOf().pipValue() > highestRank.pipValue()) {
                highestRank = card.rankOf();
            }
        }

        return highestRank;
    }

    public static Rank lowestRank(CardSet cardSet) {
        CardSet cardSetSingles = leaveSingleRanks(cardSet);
        Rank lowestRank = null;

        for (Card card : cardSetSingles) {
            if (lowestRank == null) {
                lowestRank = card.rankOf();
                continue;
            }
            if (card.rankOf().pipValue() < lowestRank.pipValue()) {
                lowestRank = card.rankOf();
            }
        }

        return lowestRank;
    }

    public static Rank lowestSingleRank(CardSet cardSet) {
        CardSet cardSetSingles = removePairsAndAbove(cardSet);
        Rank lowestRank = null;

        for (Card card : cardSetSingles) {
            if (countRanks(card.rankOf(), cardSet) > 1) {
                continue;
            }
            if (lowestRank == null) {
                lowestRank = card.rankOf();
                break;
            }
            if (card.rankOf().pipValue() < lowestRank.pipValue()) {
                lowestRank = card.rankOf();
            }
        }

        return lowestRank;
    }

    public static Rank secondHighestRank(CardSet cardSet) {
        CardSet cardSetSingles = leaveSingleRanks(cardSet);
        List<Card> cardsSorted = cardSetSingles.stream().collect(Collectors.toList());
        Collections.sort(cardsSorted, (c1, c2) -> c1.rankOf().compareTo(c2.rankOf()));

        return cardsSorted.get(cardsSorted.size() - 2).rankOf();
    }

    public static Rank secondHighestSingleRank(CardSet cardSet) {
        CardSet cardSetSingles = removePairsAndAbove(cardSet);

        List<Card> cardsSorted = cardSetSingles.stream().collect(Collectors.toList());
        Collections.sort(cardsSorted, (c1, c2) -> c1.rankOf().compareTo(c2.rankOf()));

        return cardsSorted.get(cardsSorted.size() - 2).rankOf();
    }

    public static Rank thirdHighestRank(CardSet cardSet) {
        CardSet cardSetSingles = leaveSingleRanks(cardSet);
        List<Card> cardsSorted = cardSetSingles.stream().collect(Collectors.toList());
        Collections.sort(cardsSorted, (c1, c2) -> c1.rankOf().compareTo(c2.rankOf()));

        return cardsSorted.get(cardsSorted.size() - 3).rankOf();
    }

    public static Rank thirdHighestSingleRank(CardSet cardSet) {
        CardSet cardSetSingles = removePairsAndAbove(cardSet);
        List<Card> cardsSorted = cardSetSingles.stream().collect(Collectors.toList());
        Collections.sort(cardsSorted, (c1, c2) -> c1.rankOf().compareTo(c2.rankOf()));

        if (cardsSorted.size() < 4) {
            return null;
        }
        return cardsSorted.get(cardsSorted.size() - 3).rankOf();
    }

    public static int countRanks(Rank rank, CardSet cards) {
        int times = 0;
        for (Card card : cards) {
            if (rank.equals(card.rankOf())) {
                times++;
            }
        }
        return times;
    }

    public static int countSuits(Suit suit, CardSet cards) {
        int times = 0;
        for (Card card : cards) {
            if (suit == card.suitOf()) {
                times++;
            }
        }
        return times;
    }

    private static CardSet leaveSingleRanks(CardSet cardSet) {
        CardSet cardSetSingles = new CardSet();
        for (Card card : cardSet) {
            if (cardSetSingles.containsRank(card.rankOf()) == false) {
                cardSetSingles.add(card);
            }
        }
        return cardSetSingles;
    }

    private static CardSet removePairsAndAbove(CardSet cardSet) {
        CardSet cardSetSingles = new CardSet();
        for (Card card : cardSet) {
            if (countRanks(card.rankOf(), cardSet) < 2) {
                cardSetSingles.add(card);
            }
        }
        return cardSetSingles;
    }
}
