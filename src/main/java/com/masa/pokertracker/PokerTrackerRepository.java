package com.masa.pokertracker;

import com.masa.pokertracker.model.PTPosition;
import com.masa.pokertracker.model.PlayerIdPosition;
import com.masa.pokertracker.model.Site;
import com.masa.repository.AbstractRepository;
import com.masa.txt.service.PostFlopRowService;
import com.masa.type.HUDStat;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author Matti
 */
public class PokerTrackerRepository extends AbstractRepository {

    private static final String TABLE_CASH_HAND_PLAYER_STATISTICS = "cash_hand_player_statistics";

    public void test() throws SQLException {
        PreparedStatement pst = null;
        pst = getConnection().prepareStatement(testSql);

        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            System.out.println(rs.getInt("count"));
        }
    }

    public List<Map<String, Object>> fetchWinningPlayersHandReport(int offset, int step, boolean postflop, int countOffset) throws IOException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<Map<String, Object>> list = new ArrayList<>();
        Properties props;
        if (postflop) {
            props = getProperties("src/main/resources/postflop_hands.properties");
        } else {
            props = getProperties("src/main/resources/postflop_hands_1.properties");
        }

        String sql = (String) props.get("sql");
        //String offsetSql = " (SELECT COUNT(*) FROM labelled_pf) ";

        PostFlopRowService postFlopRowService = new PostFlopRowService();

        //int countOffset = 0;
        //try {
        // TODO: remove this if not done in parts
        //int lastPartNumber = 51100701;
        int rowsSize = postFlopRowService.readAndCountPostFlopRows()/* - lastPartNumber*/;
        System.out.println("rowsSize txt: " + rowsSize);
        //} catch (IOException ex) {
        //System.out.println("ERRORRRRRRRRR");
        //Logger.getLogger(PokerTrackerRepository.class.getName()).log(Level.SEVERE, null, ex);
        //}

        sql = sql + " offset " + countOffset/* + 1000000*/ + " limit " + step;
        System.out.println(sql);
//        sql = sql + " LIMIT 5";

        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(
                    sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            ps.setFetchSize(2000);

            ResultSet rs = ps.executeQuery();
            int rowCount = 0;
            while (rs.next()) {
                rowCount++;
//            if (rs.getMetaData().getColumnCount() > 0) {
//                System.out.println(" " + rs.getMetaData().getColumnName(1) + "=" + rs.getObject(1) + " rowCount: " + rowCount);
//            }
                Map<String, Object> map = new HashMap<>();
                for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                    if (rs.getMetaData().getColumnName(i).equals("str_actions_f") || rs.getMetaData().getColumnName(i).equals("str_actions_t")
                            || rs.getMetaData().getColumnName(i).equals("str_actions_r")) {
                        String s = (String) rs.getObject(i);
                        if (s.length() > 6) {
                            System.out.println("str_actions liian pitka! SKIP!");
                            continue;
                        }
                    }

                    /*if (rs.getMetaData().getColumnName(i).equals("cnt_players_f")) {
                        int playersFlop = (int) rs.getObject(i);
                        if (playersFlop != 2) {
                            continue;
                        }
                    }*/
                    map.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
//                System.out.println(" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i) + " " + rs.getObject(i).getClass());
                }
                list.add(map);
            }

            System.out.println("list size: " + list.size());

            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }
        return list;
    }

    public List<Map<String, Object>> fetchAllMovesHandReport(int offset, int step) {
        Connection connection = null;
        PreparedStatement ps = null;
        List<Map<String, Object>> list = new ArrayList<>();
        Properties props = getProperties("src/main/resources/fetch_all_moves.properties");
        String sql = (String) props.get("sql");
        String offsetSql = " (SELECT COUNT(*) FROM allmoves) ";
        sql = sql + " offset " + offsetSql + " limit " + step;
//        sql = sql + " offset " + offset + " limit " + step;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(
                    sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            ps.setFetchSize(3000);

            ResultSet rs = ps.executeQuery();
            int rowCount = 0;
            while (rs.next()) {
                rowCount++;
//            if (rs.getMetaData().getColumnCount() > 0) {
//                System.out.println(" " + rs.getMetaData().getColumnName(1) + "=" + rs.getObject(1) + " rowCount: " + rowCount);
//            }
                Map<String, Object> map = new HashMap<>();
                for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                    if (rs.getMetaData().getColumnName(i).equals("str_actions_f") || rs.getMetaData().getColumnName(i).equals("str_actions_t")
                            || rs.getMetaData().getColumnName(i).equals("str_actions_r")) {
                        String s = (String) rs.getObject(i);
                        if (s.length() > 6) {
                            System.out.println("str_actions liian pitka! SKIP!");
                            continue;
                        }
                    }
                    map.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
//                System.out.println(" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i));
                }
                list.add(map);
            }

            System.out.println("list size: " + list.size());

            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }
        return list;
    }

    public List<String> getHandsToday(Site site, LocalDateTime start, LocalDateTime end, PTPosition heroPosition, String holecard1, String holecard2) {
        Connection connection = null;
        PreparedStatement ps = null;

        List<String> handIds = new ArrayList<>();
        // hh:mm
        String startTime = start.getHour() + ":" + start.getMinute();
        String endTime = end.getHour() + ":" + end.getMinute();

        String sql = "SELECT (cash_hand_summary.id_hand) as id_hand, (cash_hand_summary.id_site) as id_site, "
                + "(cash_hand_summary.hand_no) as hand_no, (cash_hand_summary.id_gametype) as id_gametype_summary, (timezone('UTC', "
                + "cash_hand_player_statistics.date_played  + INTERVAL '0 HOURS')) as date_played, (cash_hand_player_statistics.holecard_1) as id_holecard1, "
                + "(cash_hand_player_statistics.holecard_2) as id_holecard2, (cash_hand_player_statistics.holecard_3) as id_holecard3, "
                + "(cash_hand_player_statistics.holecard_4) as id_holecard4 FROM cash_hand_player_statistics , cash_hand_summary "
                + "WHERE  (cash_hand_summary.id_hand = cash_hand_player_statistics.id_hand "
                + "AND cash_hand_player_statistics.holecard_1 = '" + holecard1 + "' "
                + "AND cash_hand_player_statistics.holecard_2 = '" + holecard2 + "' "
                + "AND cash_hand_player_statistics.position = '" + heroPosition.value() + "' "
                + "AND cash_hand_summary.id_limit = cash_hand_player_statistics.id_limit) "
                + "AND (cash_hand_player_statistics.id_player = (SELECT id_player FROM player "
                + "WHERE player_name_search=E'supiii'  AND id_site='100')) "
                + "AND ((cash_hand_player_statistics.id_hand IN (SELECT id_hand FROM "
                + "cash_hand_player_statistics WHERE (cash_hand_player_statistics.id_player = (SELECT id_player FROM player WHERE player_name_search=E'supiii' "
                + "AND id_site='100')) AND (cash_hand_player_statistics.id_gametype = 1)AND (cash_hand_player_statistics.id_gametype<>1  "
                + "OR (cash_hand_player_statistics.id_gametype=1 AND (cash_hand_player_statistics.id_limit in (SELECT hlrl.id_limit  "
                + "FROM cash_limit hlrl WHERE (hlrl.flg_nlpl=false and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15)  "
                + "ELSE (CASE WHEN hlrl.limit_currency='INR' THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC'  "
                + "THEN 0.0 ELSE (CASE WHEN hlrl.limit_currency='PLY' THEN 0.0 ELSE hlrl.amt_bb END) END) END) END)<=1.01)  "
                + "or (hlrl.flg_nlpl=true and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15)  "
                + "ELSE (CASE WHEN hlrl.limit_currency='INR' THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC'  "
                + "THEN 0.0 ELSE (CASE WHEN hlrl.limit_currency='PLY' THEN 0.0 ELSE hlrl.amt_bb END) END) END) END)<=0.51)))))AND (((((((      (  "
                + "TIME '" + startTime + "' < TIME '" + endTime + "' AND      (CAST(timezone('UTC', cash_hand_player_statistics.date_played) AS TIME) >= '" + startTime + "' AND  "
                + "(CAST(timezone('UTC', cash_hand_player_statistics.date_played) AS TIME) < '" + endTime + "'))    )    OR    (     TIME '" + startTime + "' >= TIME '" + endTime + "' "
                + "AND      (CAST(timezone('UTC', cash_hand_player_statistics.date_played) AS TIME) < '" + startTime + "' OR  "
                + "CAST(timezone('UTC', cash_hand_player_statistics.date_played) AS TIME) >= '" + endTime + "')    )      ))))))) "
                + "AND ((cash_hand_player_statistics.date_played >= (to_char(current_timestamp, 'YYYY-MM-DD 00:00:00')::timestamp + "
                + "INTERVAL '-3 HOURS') and cash_hand_player_statistics.date_played <= (to_char(current_timestamp, 'YYYY-MM-DD 23:59:59')::timestamp + "
                + "INTERVAL '-3 HOURS')))  ORDER BY (timezone('UTC',  cash_hand_player_statistics.date_played  + INTERVAL '0 HOURS')) desc LIMIT 100)))  "
                + "ORDER BY (timezone('UTC',  cash_hand_player_statistics.date_played  + INTERVAL '0 HOURS')) desc";

        try {
            connection = getConnection();
            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
//            printResultSet(rs);
                handIds.add(rs.getString("id_hand"));
//            players.add(new PlayerIdPosition(rs.getString("id_player"), rs.getString("position")));
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }
        return handIds;
    }

    public List<PlayerIdPosition> getPlayerIdPositionByHand(String handId) {
        Connection connection = null;
        PreparedStatement ps = null;

        List<PlayerIdPosition> players = new ArrayList<>();
        String sql = "SELECT player.id_player, player.position "
                + "FROM cash_hand_player_statistics AS player "
                + "WHERE player.id_hand = '" + handId + "'";

        try {
            connection = getConnection();
            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                PlayerIdPosition pl = new PlayerIdPosition(rs.getString("id_player"), rs.getString("position"));
                pl.setName(getPlayerNameById(Site.POKERSTARS, pl.getPlayerId()));
                players.add(pl);
//            printResultSet(rs);
            }

            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }

        return players;
    }

    public String getPlayerNameById(Site site, String playerId) {
        Connection connection = null;
        PreparedStatement ps = null;

        String player_name = "not found";
        String sql = "SELECT player.player_name "
                + "FROM player "
                + "WHERE player.id_player = '" + playerId + "' ";

        try {
            connection = getConnection();
            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                player_name = rs.getString("player_name");
                printResultSet(rs);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }
        return player_name;
    }

    public Map<HUDStat, Double> getAllHudStats(String playerId) {
        Connection connection = null;
        PreparedStatement ps = null;

        Map<HUDStat, Double> hudStats = new HashMap<>();
        String sqlNew = "SELECT ic.id_player, player.id_site, player.player_name,sum(ic.cnt_vpip) as cnt_vpip,sum(ic.cnt_hands) as cnt_hands,\n"
                + "sum(ic.cnt_walks) as cnt_walks,sum(ic.cnt_pfr) as cnt_pfr,sum(ic.cnt_pfr_opp) as cnt_pfr_opp,sum(ic.cnt_steal_att) as cnt_steal_att,\n"
                + "sum(ic.cnt_steal_opp) as cnt_steal_opp,sum(ic.cnt_steal_def_action_fold) as cnt_steal_def_action_fold,\n"
                + "sum(ic.cnt_steal_def_opp) as cnt_steal_def_opp,sum(ic.cnt_p_3bet) as cnt_p_3bet,sum(ic.cnt_p_3bet_opp) as cnt_p_3bet_opp,sum(ic.cnt_p_3bet_def_action_fold) as cnt_p_3bet_def_action_fold,\n"
                + "sum(ic.cnt_f_3bet_def_action_fold) as cnt_f_3bet_def_action_fold,sum(ic.cnt_t_3bet_def_action_fold) as cnt_t_3bet_def_action_fold,\n"
                + "sum(ic.cnt_r_3bet_def_action_fold) as cnt_r_3bet_def_action_fold,sum(ic.cnt_p_3bet_def_opp) as cnt_p_3bet_def_opp,sum(ic.cnt_f_3bet_def_opp) as cnt_f_3bet_def_opp,\n"
                + "sum(ic.cnt_t_3bet_def_opp) as cnt_t_3bet_def_opp,sum(ic.cnt_r_3bet_def_opp) as cnt_r_3bet_def_opp,\n"
                + "sum(ic.cnt_p_raise_3bet) as cnt_p_raise_3bet,sum(ic.cnt_p_4bet_opp) as cnt_p_4bet_opp,\n"
                + "sum(ic.cnt_p_5bet_opp) as cnt_p_5bet_opp,sum(ic.cnt_p_4bet_def_action_fold) as cnt_p_4bet_def_action_fold,\n"
                + "sum(ic.cnt_f_4bet_def_action_fold) as cnt_f_4bet_def_action_fold,sum(ic.cnt_t_4bet_def_action_fold) as cnt_t_4bet_def_action_fold,\n"
                + "sum(ic.cnt_r_4bet_def_action_fold) as cnt_r_4bet_def_action_fold,sum(ic.cnt_p_4bet_def_opp) as cnt_p_4bet_def_opp,\n"
                + "sum(ic.cnt_f_4bet_def_opp) as cnt_f_4bet_def_opp,sum(ic.cnt_t_4bet_def_opp) as cnt_t_4bet_def_opp,\n"
                + "sum(ic.cnt_r_4bet_def_opp) as cnt_r_4bet_def_opp,sum(ic.cnt_f_cbet) as cnt_f_cbet,\n"
                + "sum(ic.cnt_f_cbet_opp) as cnt_f_cbet_opp,sum(ic.cnt_t_cbet) as cnt_t_cbet,\n"
                + "sum(ic.cnt_t_cbet_opp) as cnt_t_cbet_opp,sum(ic.cnt_r_cbet) as cnt_r_cbet,\n"
                // new

                /*+ "sum(if[cash_hand_player_statistics.flg_f_fold, 1, 0]) as cnt_f_fold,\n"
                + "sum(if[cash_hand_player_statistics.flg_t_fold, 1, 0]) as cnt_t_fold,\n"
                + "sum(if[cash_hand_player_statistics.flg_r_fold, 1, 0]) as cnt_r_fold,\n"
                + "sum(if[cash_hand_player_statistics.flg_f_check, 1, 0]) as cnt_f_check,\n"
                + "sum(if[cash_hand_player_statistics.flg_t_check, 1, 0]) as cnt_t_check,\n"
                + "sum(if[cash_hand_player_statistics.flg_r_check, 1, 0]) as cnt_r_check,\n"
                + "sum(cash_hand_player_statistics.cnt_f_call ) as cnt_f_call,\n"
                + "sum(cash_hand_player_statistics.cnt_t_call ) as cnt_t_call,\n"
                + "sum(cash_hand_player_statistics.cnt_r_call ) as cnt_r_call,\n"
                + "sum(if[cash_hand_player_statistics.flg_f_bet, 1, 0]) as cnt_f_bet,\n"
                + "sum(if[cash_hand_player_statistics.flg_t_bet, 1, 0]) as cnt_t_bet,\n"
                + "sum(if[cash_hand_player_statistics.flg_r_bet, 1, 0]) as cnt_r_bet,\n"
                + "sum(cash_hand_player_statistics.cnt_f_raise) as cnt_f_raise,\n"
                + "sum(cash_hand_player_statistics.cnt_t_raise) as cnt_t_raise,\n"
                + "sum(cash_hand_player_statistics.cnt_r_raise) as cnt_r_raise,\n"*/
                // cnt_f_raise
                // end new
                + "sum(ic.cnt_r_cbet_opp) as cnt_r_cbet_opp,sum(ic.cnt_f_cbet_def_action_fold) as cnt_f_cbet_def_action_fold,\n"
                + "sum(ic.cnt_f_cbet_def_opp) as cnt_f_cbet_def_opp,sum(ic.cnt_t_cbet_def_action_fold) as cnt_t_cbet_def_action_fold,\n"
                + "sum(ic.cnt_t_cbet_def_opp) as cnt_t_cbet_def_opp,sum(ic.cnt_r_cbet_def_action_fold) as cnt_r_cbet_def_action_fold,\n"
                + "sum(ic.cnt_r_cbet_def_opp) as cnt_r_cbet_def_opp FROM (SELECT cash_cache.id_player,sum(cnt_vpip) AS cnt_vpip,"
                + "sum(cnt_hands) AS cnt_hands,sum(cnt_walks) AS cnt_walks,sum(cnt_pfr) AS cnt_pfr,"
                + "sum(cnt_pfr_opp) AS cnt_pfr_opp,sum(cnt_steal_att) AS cnt_steal_att,sum(cnt_steal_opp) AS cnt_steal_opp,"
                + "sum(cnt_steal_def_action_fold) AS cnt_steal_def_action_fold,sum(cnt_steal_def_opp) AS cnt_steal_def_opp,"
                + "sum(cnt_p_3bet) AS cnt_p_3bet,sum(cnt_p_3bet_opp) AS cnt_p_3bet_opp,sum(cnt_p_3bet_def_action_fold) AS cnt_p_3bet_def_action_fold,"
                + "sum(cnt_f_3bet_def_action_fold) AS cnt_f_3bet_def_action_fold,sum(cnt_t_3bet_def_action_fold) AS cnt_t_3bet_def_action_fold,"
                + "sum(cnt_r_3bet_def_action_fold) AS cnt_r_3bet_def_action_fold,sum(cnt_p_3bet_def_opp) AS cnt_p_3bet_def_opp,sum(cnt_f_3bet_def_opp) AS cnt_f_3bet_def_opp,"
                + "sum(cnt_t_3bet_def_opp) AS cnt_t_3bet_def_opp,sum(cnt_r_3bet_def_opp) AS cnt_r_3bet_def_opp,sum(cnt_p_raise_3bet) AS cnt_p_raise_3bet,"
                + "sum(cnt_p_4bet_opp) AS cnt_p_4bet_opp,sum(cnt_p_5bet_opp) AS cnt_p_5bet_opp,sum(cnt_p_4bet_def_action_fold) AS cnt_p_4bet_def_action_fold,"
                + "sum(cnt_f_4bet_def_action_fold) AS cnt_f_4bet_def_action_fold,sum(cnt_t_4bet_def_action_fold) AS cnt_t_4bet_def_action_fold,"
                + "sum(cnt_r_4bet_def_action_fold) AS cnt_r_4bet_def_action_fold,sum(cnt_p_4bet_def_opp) AS cnt_p_4bet_def_opp,"
                + "sum(cnt_f_4bet_def_opp) AS cnt_f_4bet_def_opp,sum(cnt_t_4bet_def_opp) AS cnt_t_4bet_def_opp,sum(cnt_r_4bet_def_opp) AS cnt_r_4bet_def_opp,"
                + "sum(cnt_f_cbet) AS cnt_f_cbet,sum(cnt_f_cbet_opp) AS cnt_f_cbet_opp,sum(cnt_t_cbet) AS cnt_t_cbet,sum(cnt_t_cbet_opp) AS cnt_t_cbet_opp,"
                + "sum(cnt_r_cbet) AS cnt_r_cbet,sum(cnt_r_cbet_opp) AS cnt_r_cbet_opp,sum(cnt_f_cbet_def_action_fold) AS cnt_f_cbet_def_action_fold,"
                + "sum(cnt_f_cbet_def_opp) AS cnt_f_cbet_def_opp,sum(cnt_t_cbet_def_action_fold) AS cnt_t_cbet_def_action_fold,sum(cnt_t_cbet_def_opp) AS cnt_t_cbet_def_opp,"
                + "sum(cnt_r_cbet_def_action_fold) AS cnt_r_cbet_def_action_fold,sum(cnt_r_cbet_def_opp) AS cnt_r_cbet_def_opp  "
                + "FROM  cash_cache "
                + "WHERE  cash_cache.id_player = (SELECT id_player FROM player WHERE player.id_player = " + playerId + " AND id_site='100')  "
                + "AND ((cash_cache.id_gametype = 1)AND (cash_cache.id_gametype<>1 OR (cash_cache.id_limit IN (SELECT hlrl.id_limit FROM cash_limit hlrl "
                + "WHERE (hlrl.flg_nlpl=false and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15) ELSE (CASE WHEN hlrl.limit_currency='INR' "
                + "THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC' THEN 0.0 ELSE (CASE WHEN hlrl.limit_currency='PLY' THEN 0.0 "
                + "ELSE hlrl.amt_bb END) END) END) END)<=1.01) or (hlrl.flg_nlpl=true and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15) "
                + "ELSE (CASE WHEN hlrl.limit_currency='INR' THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC' THEN 0.0 "
                + "ELSE (CASE WHEN hlrl.limit_currency='PLY' THEN 0.0 ELSE hlrl.amt_bb END) END) END) END)<=0.51)))))  "
                + "GROUP BY cash_cache.id_player) ic,player WHERE player.id_player=ic.id_player  GROUP BY ic.id_player,player.id_site,player.player_name";

        try {
            connection = getConnection();
            ps = connection.prepareStatement(sqlNew);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                hudStats.put(HUDStat.HANDS, rs.getDouble("cnt_hands"));
                //hudStats.put(HUDStat.BB_100, rs.getDouble("bb_100"));
                hudStats.put(HUDStat.PREFLOP_VPIP, (rs.getDouble("cnt_vpip") / (rs.getDouble("cnt_hands") - rs.getDouble("cnt_walks"))) * 100);
                hudStats.put(HUDStat.PREFLOP_PFR, (rs.getDouble("cnt_pfr") / rs.getDouble("cnt_pfr_opp")) * 100);
                hudStats.put(HUDStat.PREFLOP_ATT_TO_STEAL, (rs.getDouble("cnt_steal_att") / rs.getDouble("cnt_steal_opp")) * 100);
                /*
                ((cnt_f_bet + cnt_f_raise + cnt_t_bet + cnt_t_raise + cnt_r_bet + cnt_r_raise) /
	(cnt_f_check + cnt_t_check + cnt_r_check + cnt_f_call + cnt_f_fold + cnt_t_call + cnt_t_fold + cnt_r_call + cnt_r_fold
	+ cnt_f_bet + cnt_f_raise + cnt_t_bet + cnt_t_raise + cnt_r_bet + cnt_r_raise)) * 100
                 */
 /*hudStats.put(HUDStat.TOTAL_AFP, ((rs.getDouble("cnt_f_bet") + rs.getDouble("cnt_f_raise") + rs.getDouble("cnt_t_bet") + rs.getDouble("cnt_t_raise")
                        + rs.getDouble("cnt_r_bet") + rs.getDouble("cnt_r_raise"))
                        / (rs.getDouble("cnt_f_check") + rs.getDouble("cnt_t_check") + rs.getDouble("cnt_r_check") + rs.getDouble("cnt_f_call")
                        + rs.getDouble("cnt_f_fold") + rs.getDouble("cnt_t_call") + rs.getDouble("cnt_t_fold") + rs.getDouble("cnt_r_call") + rs.getDouble("cnt_r_fold")
                        + rs.getDouble("cnt_f_bet") + rs.getDouble("cnt_f_raise") + rs.getDouble("cnt_t_bet") + rs.getDouble("cnt_t_raise")
                        + rs.getDouble("cnt_r_bet") + rs.getDouble("cnt_r_raise"))) * 100);*/
                //hudStats.put(HUDStat.PREFLOP_ATT_TO_STEAL_LP, (rs.getDouble("cnt_steal_att_lp") / rs.getDouble("cnt_steal_opp_lp")) * 100);
                //hudStats.put(HUDStat.PREFLOP_ATT_TO_STEAL_UTG, (rs.getDouble("cnt_steal_att_utg") / rs.getDouble("cnt_steal_opp_utg")) * 100);
//            hudStats.put(HUDStat.PREFLOP_ATT_TO_STEAL_UTG1, (rs.getDouble("cnt_steal_att_utg1") / rs.getDouble("cnt_steal_opp_utg1")) * 100);
//            hudStats.put(HUDStat.PREFLOP_ATT_TO_STEAL_CO, (rs.getDouble("cnt_steal_att_co") / rs.getDouble("cnt_steal_opp_co")) * 100);
//            hudStats.put(HUDStat.PREFLOP_ATT_TO_STEAL_BTN, (rs.getDouble("cnt_steal_att_btn") / rs.getDouble("cnt_steal_opp_btn")) * 100);
//            hudStats.put(HUDStat.PREFLOP_ATT_TO_STEAL_SB, (rs.getDouble("cnt_steal_att_sb") / rs.getDouble("cnt_steal_opp_sb")) * 100);
                hudStats.put(HUDStat.PREFLOP_FOLD_TO_STEAL, (rs.getDouble("cnt_steal_def_action_fold") / rs.getDouble("cnt_steal_def_opp")) * 100);
//            hudStats.put(HUDStat.PREFLOP_3BET_STEAL, (rs.getDouble("cnt_steal_def_action_raise") / rs.getDouble("cnt_steal_def_3bet_opp")) * 100);
//            hudStats.put(HUDStat.PREFLOP_3BET_VS_UTG_OR_UTG1_OPEN, (rs.getDouble("cnt_p_3bet_vs_ep_2bet") / rs.getDouble("cnt_p_3bet_opp_vs_ep_2bet")) * 100);
//            hudStats.put(HUDStat.PREFLOP_3BET_VS_LP_STEAL, (rs.getDouble("cnt_steal_def_action_raise_vs_lp") / rs.getDouble("cnt_steal_def_3bet_opp_vs_lp")) * 100);
//            hudStats.put(HUDStat.PREFLOP_3BET_VS_SB_STEAL, (rs.getDouble("cnt_p_3bet_vs_sb_2bet") / rs.getDouble("cnt_p_3bet_opp_vs_sb_2bet")) * 100);
                hudStats.put(HUDStat.PREFLOP_3BET, (rs.getDouble("cnt_p_3bet") / rs.getDouble("cnt_p_3bet_opp")) * 100);
                hudStats.put(HUDStat.PREFLOP_FOLD_TO_3BET, (rs.getDouble("cnt_p_3bet_def_action_fold") / rs.getDouble("cnt_p_3bet_def_opp")) * 100);
                hudStats.put(HUDStat.PREFLOP_4BET, (rs.getDouble("cnt_p_raise_3bet") / (rs.getDouble("cnt_p_4bet_opp") - rs.getDouble("cnt_p_5bet_opp"))) * 100);
                hudStats.put(HUDStat.PREFLOP_FOLD_TO_4BET, (rs.getDouble("cnt_p_4bet_def_action_fold") / rs.getDouble("cnt_p_4bet_def_opp")) * 100);
//            hudStats.put(HUDStat.PREFLOP_5BET, (rs.getDouble("cnt_p_5bet") / rs.getDouble("cnt_p_5bet_opp")) * 100);
//            hudStats.put(HUDStat.PREFLOP_FOLD_TO_5BET, (rs.getDouble("cnt_p_5bet_def_action_fold") / rs.getDouble("cnt_p_5bet_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_BET, (rs.getDouble("cnt_f_bet") / (rs.getDouble("cnt_f_bet") + rs.getDouble("cnt_f_check"))) * 100);
//            hudStats.put(HUDStat.FLOP_FOLD_TO_BET, (rs.getDouble("cnt_f_bet_def_action_fold") / rs.getDouble("cnt_f_bet_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_DONK_BET, (rs.getDouble("cnt_f_donk") / rs.getDouble("cnt_f_donk_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_FOLD_TO_DONK_BET, (rs.getDouble("cnt_f_donk_def_opp_action_fold") / rs.getDouble("cnt_f_donk_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_RAISE_DONK_BET, (rs.getDouble("cnt_f_donk_def_opp_action_raise") / rs.getDouble("cnt_f_donk_def_opp")) * 100);
                hudStats.put(HUDStat.FLOP_CBET, (rs.getDouble("cnt_f_cbet") / rs.getDouble("cnt_f_cbet_opp")) * 100);
                hudStats.put(HUDStat.FLOP_FOLD_TO_CBET, (rs.getDouble("cnt_f_cbet_def_action_fold") / rs.getDouble("cnt_f_cbet_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_CBET_IN_3BET_POT, (rs.getDouble("cnt_f_cbet_3bet_pot") / rs.getDouble("cnt_f_cbet_opp_3bet_pot")) * 100);
//            hudStats.put(HUDStat.FLOP_FOLD_TO_CBET_IN_3BET_POT, (rs.getDouble("cnt_p_3bet_f_cbet_def_action_fold") / rs.getDouble("cnt_p_3bet_f_cbet_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_RAISE_CBET, (rs.getDouble("cnt_f_cbet_def_action_raise") / rs.getDouble("cnt_f_cbet_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_2BET, (rs.getDouble("cnt_f_bet_def_action_raise") / rs.getDouble("cnt_f_bet_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_FOLD_TO_2BET, (rs.getDouble("cnt_f_2bet_def_action_fold") / rs.getDouble("cnt_f_3bet_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_3BET, (rs.getDouble("cnt_f_3bet") / rs.getDouble("cnt_f_3bet_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_FOLD_TO_3BET, (rs.getDouble("cnt_f_3bet_def_action_fold") / rs.getDouble("cnt_f_3bet_def_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_4BET, (rs.getDouble("cnt_f_4bet") / rs.getDouble("cnt_f_4bet_opp")) * 100);
//            hudStats.put(HUDStat.FLOP_FOLD_TO_4BET, (rs.getDouble("cnt_f_4bet_def_action_fold") / rs.getDouble("cnt_f_4bet_def_opp")) * 100);

//            hudStats.put(HUDStat.TURN_BET, (rs.getDouble("cnt_t_bet") / (rs.getDouble("cnt_t_bet") + rs.getDouble("cnt_t_check"))) * 100);
//            hudStats.put(HUDStat.TURN_FOLD_TO_BET, (rs.getDouble("cnt_t_bet_def_action_fold") / rs.getDouble("cnt_t_bet_def_opp")) * 100);
//            hudStats.put(HUDStat.TURN_2BET, (rs.getDouble("cnt_t_bet_def_action_raise") / rs.getDouble("cnt_t_bet_def_opp")) * 100);
//            hudStats.put(HUDStat.TURN_FOLD_TO_2BET, (rs.getDouble("cnt_t_2bet_def_action_fold") / rs.getDouble("cnt_t_3bet_opp")) * 100);
//            hudStats.put(HUDStat.TURN_DONK_BET, (rs.getDouble("cnt_t_donk") / rs.getDouble("cnt_t_donk_opp")) * 100);
                hudStats.put(HUDStat.TURN_CBET, (rs.getDouble("cnt_t_cbet") / rs.getDouble("cnt_t_cbet_opp")) * 100);
                hudStats.put(HUDStat.TURN_FOLD_TO_CBET, (rs.getDouble("cnt_t_cbet_def_action_fold") / rs.getDouble("cnt_t_cbet_def_opp")) * 100);
//            hudStats.put(HUDStat.TURN_RAISE_CBET, (rs.getDouble("cnt_t_cbet_def_action_raise") / rs.getDouble("cnt_t_cbet_def_opp")) * 100);
//            hudStats.put(HUDStat.TURN_FOLD_TO_RAISE_CBET, (rs.getDouble("cnt_t_cbet_fold_to_raise") / rs.getDouble("cnt_t_cbet_face_raise")) * 100);
//            hudStats.put(HUDStat.TURN_3BET, (rs.getDouble("cnt_t_3bet") / rs.getDouble("cnt_t_3bet_opp")) * 100);
//            hudStats.put(HUDStat.TURN_4BET, (rs.getDouble("cnt_t_4bet") / rs.getDouble("cnt_t_4bet_opp")) * 100);
//            hudStats.put(HUDStat.TURN_FLOAT_BET, (rs.getDouble("cnt_t_float") / rs.getDouble("cnt_t_float_opp")) * 100);
//            hudStats.put(HUDStat.TURN_FOLD_FLOAT_BET, (rs.getDouble("cnt_t_float_def_action_fold") / rs.getDouble("cnt_t_float_def_opp")) * 100);

//            hudStats.put(HUDStat.RIVER_BET, (rs.getDouble("cnt_r_bet") / (rs.getDouble("cnt_r_bet") + rs.getDouble("cnt_r_check"))) * 100);
//            hudStats.put(HUDStat.RIVER_FOLD_TO_BET, (rs.getDouble("cnt_r_bet_def_action_fold") / rs.getDouble("cnt_r_bet_def_opp")) * 100);
//            hudStats.put(HUDStat.RIVER_2BET, (rs.getDouble("cnt_r_bet_def_action_raise") / rs.getDouble("cnt_r_bet_def_opp")) * 100);
//            hudStats.put(HUDStat.RIVER_FOLD_TO_2BET, (rs.getDouble("cnt_r_2bet_def_action_fold") / rs.getDouble("cnt_r_3bet_opp")) * 100);
                hudStats.put(HUDStat.RIVER_CBET, (rs.getDouble("cnt_r_cbet") / rs.getDouble("cnt_r_cbet_opp")) * 100);
                hudStats.put(HUDStat.RIVER_FOLD_TO_CBET, (rs.getDouble("cnt_r_cbet_def_action_fold") / rs.getDouble("cnt_r_cbet_def_opp")) * 100);
//            hudStats.put(HUDStat.RIVER_DONK_BET, (rs.getDouble("cnt_r_donk") / rs.getDouble("cnt_r_donk_opp")) * 100);
//            hudStats.put(HUDStat.RIVER_FLOAT_BET, (rs.getDouble("cnt_r_float") / rs.getDouble("cnt_r_float_opp")) * 100);
                ///////////////////////

//            printResultSet(rs);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }
        return hudStats;
    }

    public Map<String, Object> getOtherVillainsPF(int handId, int winningPlayerId) {
        Connection connection = null;
        PreparedStatement ps = null;

        String sql = "SELECT (cash_hand_player_statistics.id_hand) as \"id_hand\", "
                + "(cash_hand_player_statistics.id_player) as \"id_player\", "
                + "(cash_hand_player_statistics.flg_f_saw) as \"flg_f_saw\", "
                + "(cash_hand_player_statistics.holecard_1) as id_holecard1, (cash_hand_player_statistics.holecard_2) as id_holecard2, "
                + "(cash_hand_player_statistics.id_action_p) as \"id_action_p\", "
                + "(cash_hand_player_statistics.id_action_f) as \"id_action_f\", "
                + "(cash_hand_player_statistics.id_action_t) as \"id_action_t\", "
                + "(cash_hand_player_statistics.id_action_r) as \"id_action_r\", "
                + "((case when( lookup_positions.flg_sb) then  0 else  (case when( lookup_positions.flg_bb) then  1 else  (case when( lookup_positions.flg_ep) "
                + "then  2 else  (case when( lookup_positions.flg_mp) then  3 else  (case when( lookup_positions.flg_co) then  4 else  5 end) end) end) end) end)) "
                + "as \"val_position_type\" FROM lookup_positions, cash_hand_player_statistics \n"
                + "WHERE\n"
                + " cash_hand_player_statistics.id_hand = " + handId + " AND\n"
                + " cash_hand_player_statistics.id_player != " + winningPlayerId + " AND\n"
                + " (lookup_positions.\"position\"=cash_hand_player_statistics.\"position\"  "
                + "AND lookup_positions.cnt_players=cash_hand_player_statistics.cnt_players_lookup_position) limit 5;";
        Map<String, Object> map = new HashMap<>();

        try {
            connection = getConnection();
            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Boolean sawFlop = rs.getBoolean("flg_f_saw");
                if (sawFlop) {
                    map.put("id_player", rs.getInt("id_player"));
                    map.put("val_position_type", rs.getInt("val_position_type"));
                    map.put("id_holecard1", rs.getInt("id_holecard1"));
                    map.put("id_holecard2", rs.getInt("id_holecard2"));

                    /*map.put("id_action_p_p2", rs.getInt("id_action_p"));
                    map.put("id_action_f_p2", rs.getInt("id_action_f"));
                    map.put("id_action_t_p2", rs.getInt("id_action_t"));
                    map.put("id_action_r_p2", rs.getInt("id_action_r"));*/
                }
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }

        return map;
    }

    public List<Map<String, Object>> getOtherVillains(int handId, int winningPlayerId) {
        Connection connection = null;
        PreparedStatement ps = null;

        String sql = "SELECT (cash_hand_player_statistics.id_hand) as \"id_hand\", "
                + "(cash_hand_player_statistics.id_player) as \"id_player\", "
                + "(cash_hand_player_statistics.holecard_1) as id_holecard1, (cash_hand_player_statistics.holecard_2) as id_holecard2, "
                + "(cash_hand_player_statistics.flg_f_saw) as \"flg_f_saw\", "
                + "(cash_hand_player_statistics.id_action_p) as \"id_action_p\", "
                + "(cash_hand_player_statistics.id_action_f) as \"id_action_f\", "
                + "(cash_hand_player_statistics.id_action_t) as \"id_action_t\", "
                + "(cash_hand_player_statistics.id_action_r) as \"id_action_r\", "
                + "(cash_hand_player_statistics.val_p_raise_made_pct) as val_p_raise_made_pct, "
                + "(cash_hand_player_statistics.val_p_raise_made_2_pct) as val_p_raise_made_2_pct,"
                + "(cash_hand_player_statistics.val_f_bet_made_pct) as val_f_bet_made_pct, "
                + "(cash_hand_player_statistics.val_f_raise_made_pct) as val_f_raise_made_pct, "
                + "(cash_hand_player_statistics.val_f_raise_made_2_pct) as val_f_raise_made_2_pct, "
                + "(cash_hand_player_statistics.val_t_bet_made_pct) as val_t_bet_made_pct, "
                + "(cash_hand_player_statistics.val_t_raise_made_pct) as val_t_raise_made_pct, "
                + "(cash_hand_player_statistics.val_t_raise_made_2_pct) as val_t_raise_made_2_pct, "
                + "(cash_hand_player_statistics.val_r_bet_made_pct) as val_r_bet_made_pct, "
                + "(cash_hand_player_statistics.val_r_raise_made_pct) as val_r_raise_made_pct, "
                + "(cash_hand_player_statistics.val_r_raise_made_2_pct) as val_r_raise_made_2_pct, "
                + "((case when( lookup_positions.flg_sb) then  0 else  (case when( lookup_positions.flg_bb) then  1 else  (case when( lookup_positions.flg_ep) "
                + "then  2 else  (case when( lookup_positions.flg_mp) then  3 else  (case when( lookup_positions.flg_co) then  4 else  5 end) end) end) end) end)) "
                + "as \"val_position_type\" FROM lookup_positions, cash_hand_player_statistics \n"
                + "WHERE\n"
                + " cash_hand_player_statistics.id_hand = " + handId + " AND\n"
                + " cash_hand_player_statistics.id_player != " + winningPlayerId + " AND\n"
                + " (lookup_positions.\"position\"=cash_hand_player_statistics.\"position\"  "
                + "AND lookup_positions.cnt_players=cash_hand_player_statistics.cnt_players_lookup_position) limit 5;";

        List<Map<String, Object>> list = new ArrayList<>();

        try {
            connection = getConnection();
            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new HashMap<>();
                String pos = PTUtil.getPTPositionString(rs.getInt("val_position_type"));
                map.put("val_position_type", rs.getInt("val_position_type"));
                map.put("position", pos);
                map.put("flg_f_saw", rs.getBoolean("flg_f_saw"));
                map.put("id_holecard1", rs.getInt("id_holecard1"));
                map.put("id_holecard2", rs.getInt("id_holecard2"));
                map.put("id_player", rs.getInt("id_player"));
                map.put("id_action_p", rs.getInt("id_action_p"));
                map.put("id_action_f", rs.getInt("id_action_f"));
                map.put("id_action_t", rs.getInt("id_action_t"));
                map.put("id_action_r", rs.getInt("id_action_r"));

                map.put("val_p_raise_made_pct", rs.getDouble("val_p_raise_made_pct"));
                map.put("val_p_raise_made_2_pct", rs.getDouble("val_p_raise_made_2_pct"));
                map.put("val_f_bet_made_pct", rs.getDouble("val_f_bet_made_pct"));
                map.put("val_f_raise_made_pct", rs.getDouble("val_f_raise_made_pct"));
                map.put("val_f_raise_made_2_pct", rs.getDouble("val_f_raise_made_2_pct"));
                map.put("val_t_bet_made_pct", rs.getDouble("val_t_bet_made_pct"));
                map.put("val_t_raise_made_pct", rs.getDouble("val_t_raise_made_pct"));
                map.put("val_t_raise_made_2_pct", rs.getDouble("val_t_raise_made_2_pct"));
                map.put("val_r_bet_made_pct", rs.getDouble("val_r_bet_made_pct"));
                map.put("val_r_raise_made_pct", rs.getDouble("val_r_raise_made_pct"));
                map.put("val_r_raise_made_2_pct", rs.getDouble("val_r_raise_made_2_pct"));

                list.add(map);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }

        return list;
    }

    private double getDoubleValue(Object object) {
        if (object != null) {
            return ((BigDecimal) object).doubleValue();
        } else {
            return 0.0;
        }
    }

    public Double getBB100StatValue(String playerId) {
        Connection connection = null;
        PreparedStatement ps = null;

        Map<HUDStat, Double> hudStats = new HashMap<>();
        String sql = "SELECT ic.id_player,player.id_site,player.player_name,sum(ic.amt_bb_won) as amt_bb_won,sum(ic.cnt_hands) as cnt_hands FROM (SELECT cash_cache.id_player,sum(amt_bb_won) AS amt_bb_won,sum(cnt_hands) AS cnt_hands  "
                + "FROM  cash_cache WHERE  cash_cache.id_player = (SELECT id_player FROM player WHERE player.id_player = " + playerId + "  AND id_site='100')  AND ((cash_cache.id_gametype = 1)"
                + "AND (cash_cache.id_gametype<>1 OR (cash_cache.id_limit IN (SELECT hlrl.id_limit FROM cash_limit hlrl WHERE (hlrl.flg_nlpl=false and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15) "
                + "ELSE (CASE WHEN hlrl.limit_currency='INR' THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC' THEN 0.0 ELSE (CASE WHEN hlrl.limit_currency='PLY' "
                + "THEN 0.0 ELSE hlrl.amt_bb END) END) END) END)<=1.01) or (hlrl.flg_nlpl=true and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15) ELSE (CASE WHEN hlrl.limit_currency='INR' "
                + "THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC' THEN 0.0 ELSE (CASE WHEN hlrl.limit_currency='PLY' THEN 0.0 ELSE hlrl.amt_bb END) END) END) END)<=0.51)))))  "
                + "GROUP BY cash_cache.id_player) ic,player WHERE player.id_player=ic.id_player  GROUP BY ic.id_player,player.id_site,player.player_name";
//        String sql = "SELECT (sum(cash_hand_player_statistics.amt_won / cash_limit.amt_bb) / ((sum((case when(cash_hand_player_statistics.id_hand > 0) then  1 else  0 end))) / 100)) as \"bb_100\", \n"
//                + "\n"
//                + "FROM cash_hand_player_statistics , cash_hand_summary, lookup_actions lookup_actions_p, cash_limit, player, player player_real, lookup_actions lookup_actions_f, lookup_actions lookup_actions_t, \n"
//                + "lookup_actions lookup_actions_r WHERE  (cash_hand_summary.id_hand = cash_hand_player_statistics.id_hand  AND cash_hand_summary.id_limit = cash_hand_player_statistics.id_limit)  \n"
//                + "AND (lookup_actions_p.id_action=cash_hand_player_statistics.id_action_p)  AND (cash_limit.id_limit = cash_hand_player_statistics.id_limit)  AND (player.id_player = cash_hand_player_statistics.id_player)  \n"
//                + "AND (player_real.id_player = cash_hand_player_statistics.id_player_real)  AND (lookup_actions_f.id_action=cash_hand_player_statistics.id_action_f)  AND (lookup_actions_t.id_action=cash_hand_player_statistics.id_action_t)  \n"
//                + "AND (lookup_actions_r.id_action=cash_hand_player_statistics.id_action_r)  AND (cash_limit.id_limit = cash_hand_summary.id_limit)   \n"
//                + "AND (cash_hand_player_statistics.id_player = (SELECT id_player FROM player WHERE player.id_player = " + playerId + "  AND id_site='100'))   AND ((cash_hand_player_statistics.id_gametype = 1)\n"
//                + "AND (cash_hand_player_statistics.id_gametype<>1 OR (cash_hand_player_statistics.id_gametype=1 AND (cash_hand_player_statistics.id_limit in (SELECT hlrl.id_limit FROM cash_limit hlrl \n"
//                + "WHERE (hlrl.flg_nlpl=false and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15) ELSE (CASE WHEN hlrl.limit_currency='INR' THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC' \n"
//                + "THEN 0.0 ELSE (CASE WHEN hlrl.limit_currency='PLY' THEN 0.0 ELSE hlrl.amt_bb END) END) END) END)<=1.01) or (hlrl.flg_nlpl=true and (CASE WHEN hlrl.limit_currency='SEK' THEN (hlrl.amt_bb*0.15) \n"
//                + "ELSE (CASE WHEN hlrl.limit_currency='INR' THEN (hlrl.amt_bb*0.020) ELSE (CASE WHEN hlrl.limit_currency='XSC' THEN 0.0 ELSE (CASE WHEN hlrl.limit_currency='PLY' THEN 0.0 ELSE hlrl.amt_bb END) END) END) END)<=0.51))))))  \n"
//                + "GROUP BY (cash_hand_player_statistics.id_player), (player_real.id_site), (player.player_name)";
        Double bb_100 = null;

        try {
            connection = getConnection();
            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                // bb/100 = amt_bb_won / (cnt_hands / 100)
                bb_100 = rs.getDouble("amt_bb_won") / (rs.getDouble("cnt_hands") / 100);

                printResultSet(rs);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {

        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
            }
        }
        return bb_100;
    }
}
