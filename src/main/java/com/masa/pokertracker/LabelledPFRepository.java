package com.masa.pokertracker;

import com.masa.pokertracker.model.LabelledPFModel;
import com.masa.repository.AbstractRepository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author compuuter
 */
public class LabelledPFRepository extends AbstractRepository {

    public List<LabelledPFModel> fetchMoves(int id_rangecombo, int val_position_type) {
        Connection connection = null;
        PreparedStatement ps = null;
        //ResultSet rs = null;

        List<LabelledPFModel> labelledModels = new ArrayList<>();

        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT * FROM labelled_pf WHERE id_rangecombo = ").append(id_rangecombo)
                    .append(" AND val_position_type = ").append(val_position_type);
            String sql = sb.toString();
            System.out.println(sql);

            connection = getConnection();

            ps = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//            ps.setQueryTimeout(QUERY_TIMEOUT);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                if (rs.getInt("id_hand") == 5385) {
                    System.out.println("DEBUG");
                }
                LabelledPFModel model = new LabelledPFModel();
                model.id_hand = rs.getInt("id_hand");
                model.hand_no = rs.getString("hand_no");
                model.p1_won = rs.getBoolean("p1_won");
                model.p1_won_bb = rs.getDouble("p1_won_bb");
                model.id_action_p = rs.getInt("id_action_p");
                model.id_action_p_ep = rs.getInt("id_action_p_ep");
                model.id_action_p_mp = rs.getInt("id_action_p_mp");
                model.id_action_p_co = rs.getInt("id_action_p_co");
                model.id_action_p_btn = rs.getInt("id_action_p_btn");
                model.id_action_p_sb = rs.getInt("id_action_p_sb");
                model.id_action_p_bb = rs.getInt("id_action_p_bb");
                model.id_holecard1 = rs.getInt("id_holecard1");
                model.id_holecard2 = rs.getInt("id_holecard2");
                model.id_rangecombo = rs.getInt("id_rangecombo");

                model.vpip_ep = rs.getInt("vpip_ep");
                model.pfr_ep = rs.getInt("pfr_ep");
                model.att_to_steal_ep = rs.getInt("att_to_steal_ep");
                model.fold_to_steal_ep = rs.getInt("fold_to_steal_ep");
                model.threebet_pf_ep = rs.getInt("threebet_pf_ep");
                model.fold_to_threebet_pf_ep = rs.getInt("fold_to_threebet_pf_ep");
                model.fourbet_pf_ep = rs.getInt("fourbet_pf_ep");
                model.fold_to_fourbet_ep = rs.getInt("fold_to_fourbet_ep");
                model.cbet_f_ep = rs.getInt("cbet_f_ep");
                model.fold_to_cbet_f_ep = rs.getInt("fold_to_cbet_f_ep");
                model.cbet_t_ep = rs.getInt("cbet_t_ep");
                model.fold_to_cbet_t_ep = rs.getInt("fold_to_cbet_t_ep");

                model.vpip_mp = rs.getInt("vpip_mp");
                model.pfr_mp = rs.getInt("pfr_mp");
                model.att_to_steal_mp = rs.getInt("att_to_steal_mp");
                model.fold_to_steal_mp = rs.getInt("fold_to_steal_mp");
                model.threebet_pf_mp = rs.getInt("threebet_pf_mp");
                model.fold_to_threebet_pf_mp = rs.getInt("fold_to_threebet_pf_mp");
                model.fourbet_pf_mp = rs.getInt("fourbet_pf_mp");
                model.fold_to_fourbet_mp = rs.getInt("fold_to_fourbet_mp");
                model.cbet_f_mp = rs.getInt("cbet_f_mp");
                model.fold_to_cbet_f_mp = rs.getInt("fold_to_cbet_f_mp");
                model.cbet_t_mp = rs.getInt("cbet_t_mp");
                model.fold_to_cbet_t_mp = rs.getInt("fold_to_cbet_t_mp");

                model.vpip_co = rs.getInt("vpip_co");
                model.pfr_co = rs.getInt("pfr_co");
                model.att_to_steal_co = rs.getInt("att_to_steal_co");
                model.fold_to_steal_co = rs.getInt("fold_to_steal_co");
                model.threebet_pf_co = rs.getInt("threebet_pf_co");
                model.fold_to_threebet_pf_co = rs.getInt("fold_to_threebet_pf_co");
                model.fourbet_pf_co = rs.getInt("fourbet_pf_co");
                model.fold_to_fourbet_co = rs.getInt("fold_to_fourbet_co");
                model.cbet_f_co = rs.getInt("cbet_f_co");
                model.fold_to_cbet_f_co = rs.getInt("fold_to_cbet_f_co");
                model.cbet_t_co = rs.getInt("cbet_t_co");
                model.fold_to_cbet_t_co = rs.getInt("fold_to_cbet_t_co");

                model.vpip_btn = rs.getInt("vpip_btn");
                model.pfr_btn = rs.getInt("pfr_btn");
                model.att_to_steal_btn = rs.getInt("att_to_steal_btn");
                model.fold_to_steal_btn = rs.getInt("fold_to_steal_btn");
                model.threebet_pf_btn = rs.getInt("threebet_pf_btn");
                model.fold_to_threebet_pf_btn = rs.getInt("fold_to_threebet_pf_btn");
                model.fourbet_pf_btn = rs.getInt("fourbet_pf_btn");
                model.fold_to_fourbet_btn = rs.getInt("fold_to_fourbet_btn");
                model.cbet_f_btn = rs.getInt("cbet_f_btn");
                model.fold_to_cbet_f_btn = rs.getInt("fold_to_cbet_f_btn");
                model.cbet_t_btn = rs.getInt("cbet_t_btn");
                model.fold_to_cbet_t_btn = rs.getInt("fold_to_cbet_t_btn");

                model.vpip_sb = rs.getInt("vpip_sb");
                model.pfr_sb = rs.getInt("pfr_sb");
                model.att_to_steal_sb = rs.getInt("att_to_steal_sb");
                model.fold_to_steal_sb = rs.getInt("fold_to_steal_sb");
                model.threebet_pf_sb = rs.getInt("threebet_pf_sb");
                model.fold_to_threebet_pf_sb = rs.getInt("fold_to_threebet_pf_sb");
                model.fourbet_pf_sb = rs.getInt("fourbet_pf_sb");
                model.fold_to_fourbet_sb = rs.getInt("fold_to_fourbet_sb");
                model.cbet_f_sb = rs.getInt("cbet_f_sb");
                model.fold_to_cbet_f_sb = rs.getInt("fold_to_cbet_f_sb");
                model.cbet_t_sb = rs.getInt("cbet_t_sb");
                model.fold_to_cbet_t_sb = rs.getInt("fold_to_cbet_t_sb");

                model.vpip_bb = rs.getInt("vpip_bb");
                model.pfr_bb = rs.getInt("pfr_bb");
                model.att_to_steal_bb = rs.getInt("att_to_steal_bb");
                model.fold_to_steal_bb = rs.getInt("fold_to_steal_bb");
                model.threebet_pf_bb = rs.getInt("threebet_pf_bb");
                model.fold_to_threebet_pf_bb = rs.getInt("fold_to_threebet_pf_bb");
                model.fourbet_pf_bb = rs.getInt("fourbet_pf_bb");
                model.fold_to_fourbet_bb = rs.getInt("fold_to_fourbet_bb");
                model.cbet_f_bb = rs.getInt("cbet_f_bb");
                model.fold_to_cbet_f_bb = rs.getInt("fold_to_cbet_f_bb");
                model.cbet_t_bb = rs.getInt("cbet_t_bb");
                model.fold_to_cbet_t_bb = rs.getInt("fold_to_cbet_t_bb");

                labelledModels.add(model);
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException ex) {
            System.out.println(ex);
            Logger.getLogger(LabelledPFRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }
        return labelledModels;
    }
}
