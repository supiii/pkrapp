package com.masa.pokertracker;

import com.masa.pokertracker.analyzer.HandCategoryAnalyzer;
import com.masa.pokertracker.analyzer.PTHandStrengthAnalyzer;
import com.masa.pokertracker.model.HandCategory;
import com.masa.type.PositionPreFlop;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author Matti
 */
public class PTUtil {

    public final static Map<Integer, String> actionsLookUpMap;
    public final static Set<Integer> idGroupStrengthList;

    static {
        idGroupStrengthList = new HashSet<>();
        // Straight Flush (1), Four of a Kind (2), Full House (3), Flush (4), Straight (5), Three of a Kind (6), Two Pair (7), One Pair (8) and High Card (9).
        idGroupStrengthList.add(10);
        idGroupStrengthList.add(11);
        idGroupStrengthList.add(12);
        idGroupStrengthList.add(13);
        idGroupStrengthList.add(14);
        idGroupStrengthList.add(15);

        // Four of a kind
        idGroupStrengthList.add(20);
        idGroupStrengthList.add(21);
        idGroupStrengthList.add(22);
        idGroupStrengthList.add(23);

        // Full house
        idGroupStrengthList.add(30);
        idGroupStrengthList.add(31);
        idGroupStrengthList.add(32);
        idGroupStrengthList.add(33);
        idGroupStrengthList.add(34);
        idGroupStrengthList.add(35);
        idGroupStrengthList.add(36);
        idGroupStrengthList.add(37);
        idGroupStrengthList.add(38);
        idGroupStrengthList.add(39);

        // Flush
        idGroupStrengthList.add(410);
        idGroupStrengthList.add(49);
        idGroupStrengthList.add(48);
        idGroupStrengthList.add(47);

        // Straight
        idGroupStrengthList.add(50);
        idGroupStrengthList.add(51);
        idGroupStrengthList.add(52);
        idGroupStrengthList.add(53);
        idGroupStrengthList.add(54);
        idGroupStrengthList.add(55);

        // Three of a kind
        idGroupStrengthList.add(60);
        idGroupStrengthList.add(61);
        idGroupStrengthList.add(62);
        idGroupStrengthList.add(63);
        idGroupStrengthList.add(64);
        idGroupStrengthList.add(65);
        idGroupStrengthList.add(66);

        // Two pair
        idGroupStrengthList.add(70);
        idGroupStrengthList.add(71);
        idGroupStrengthList.add(72);
        idGroupStrengthList.add(73);
        idGroupStrengthList.add(74);
        idGroupStrengthList.add(75);
        idGroupStrengthList.add(76);
        idGroupStrengthList.add(77);
        idGroupStrengthList.add(78);
        idGroupStrengthList.add(79);

        // One pair
        idGroupStrengthList.add(80);
        idGroupStrengthList.add(81);
        idGroupStrengthList.add(82);
        idGroupStrengthList.add(83);
        idGroupStrengthList.add(84);
        idGroupStrengthList.add(85);
        idGroupStrengthList.add(86);

        // High card
        idGroupStrengthList.add(90);
        idGroupStrengthList.add(91);
        idGroupStrengthList.add(92);

        actionsLookUpMap = new HashMap<>();
        actionsLookUpMap.put(0, "");

        BufferedReader br = null;
        try {

            br = new BufferedReader(new FileReader("src/main/resources/lookup_actions.csv"));
            String line = null;

            try {
                while ((line = br.readLine()) != null) {
                    String str[] = line.split(",");
                    for (int i = 1; i < str.length; i++) {
//                        String arr[] = str[i].split(":");
                        actionsLookUpMap.put(Integer.parseInt(str[0]), str[1]);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(PTUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
//        actionsLookUpMap.put(0, "");
//        actionsLookUpMap.put(1, "CC");
//        actionsLookUpMap.put(2, "");
//        actionsLookUpMap.put(3, "");
//        actionsLookUpMap.put(4, "");
//        actionsLookUpMap.put(5, "");
//        actionsLookUpMap.put(6, "");
//        actionsLookUpMap.put(7, "");
//        actionsLookUpMap.put(8, "");
//        actionsLookUpMap.put(9, "");
//        actionsLookUpMap.put(10, "");
//        actionsLookUpMap.put(11, "");
//        actionsLookUpMap.put(12, "");
//        actionsLookUpMap.put(13, "");
//        actionsLookUpMap.put(14, "");
//        actionsLookUpMap.put(15, "");
//        actionsLookUpMap.put(16, "");
//        actionsLookUpMap.put(17, "");
//        actionsLookUpMap.put(18, "");
//        actionsLookUpMap.put(19, "");
//        actionsLookUpMap.put(20, "");
//        actionsLookUpMap.put(21, "");
//        actionsLookUpMap.put(22, "");
//        actionsLookUpMap.put(23, "");
//        actionsLookUpMap.put(24, "");
//        actionsLookUpMap.put(25, "");
//        actionsLookUpMap.put(26, "");
//        actionsLookUpMap.put(27, "");
//        actionsLookUpMap.put(28, "");
//        actionsLookUpMap.put(29, "");
//        actionsLookUpMap.put(30, "");
//        actionsLookUpMap.put(31, "");
//        actionsLookUpMap.put(32, "");
//        actionsLookUpMap.put(33, "");
//        actionsLookUpMap.put(34, "");
//        actionsLookUpMap.put(35, "");
//        actionsLookUpMap.put(36, "");
//        actionsLookUpMap.put(37, "");
//        actionsLookUpMap.put(38, "");
//        actionsLookUpMap.put(39, "");
//        actionsLookUpMap.put(40, "");
//        actionsLookUpMap.put(41, "");
//        actionsLookUpMap.put(42, "");
//        actionsLookUpMap.put(43, "");
//        actionsLookUpMap.put(44, "");
//        actionsLookUpMap.put(45, "");
//        actionsLookUpMap.put(46, "");
//        actionsLookUpMap.put(47, "");
//        actionsLookUpMap.put(48, "");
//        actionsLookUpMap.put(49, "");
//        actionsLookUpMap.put(50, "");
//        actionsLookUpMap.put(51, "");
//        actionsLookUpMap.put(52, "");
//        actionsLookUpMap.put(53, "");
//        actionsLookUpMap.put(54, "");
//        actionsLookUpMap.put(55, "");
//        actionsLookUpMap.put(56, "");
//        actionsLookUpMap.put(57, "");
//        actionsLookUpMap.put(58, "");
//        actionsLookUpMap.put(59, "");
//        actionsLookUpMap.put(60, "");
//        actionsLookUpMap.put(61, "");
//        actionsLookUpMap.put(62, "");
//        actionsLookUpMap.put(63, "");
//        actionsLookUpMap.put(64, "");
//        actionsLookUpMap.put(65, "");
//        actionsLookUpMap.put(66, "");
//        actionsLookUpMap.put(67, "");
//        actionsLookUpMap.put(68, "");
//        actionsLookUpMap.put(69, "");
//        actionsLookUpMap.put(70, "");
//        actionsLookUpMap.put(71, "");
//        actionsLookUpMap.put(72, "");
//        actionsLookUpMap.put(73, "");
//        actionsLookUpMap.put(74, "");
//        actionsLookUpMap.put(75, "RRRRC");
//        actionsLookUpMap.put(76, "BRRF");
//        actionsLookUpMap.put(77, "BCR");
//        actionsLookUpMap.put(78, "XCRC");
//        actionsLookUpMap.put(0, "");
//        actionsLookUpMap.put(1, "CC");
//        actionsLookUpMap.put(2, "");
//        actionsLookUpMap.put(3, "");
//        actionsLookUpMap.put(4, "");
//        actionsLookUpMap.put(5, "");
//        actionsLookUpMap.put(6, "");
//        actionsLookUpMap.put(7, "");
//        actionsLookUpMap.put(8, "");
//        actionsLookUpMap.put(9, "");
//        actionsLookUpMap.put(10, "");
//        actionsLookUpMap.put(11, "");
//        actionsLookUpMap.put(12, "");
//        actionsLookUpMap.put(13, "");
//        actionsLookUpMap.put(14, "");
//        actionsLookUpMap.put(15, "");
//        actionsLookUpMap.put(16, "");
//        actionsLookUpMap.put(17, "");
//        actionsLookUpMap.put(18, "");
//        actionsLookUpMap.put(19, "");
//        actionsLookUpMap.put(20, "");
//        actionsLookUpMap.put(21, "");
//        actionsLookUpMap.put(22, "");
//        actionsLookUpMap.put(23, "");
//        actionsLookUpMap.put(24, "");
//        actionsLookUpMap.put(25, "");
//        actionsLookUpMap.put(26, "");
//        actionsLookUpMap.put(27, "");
//        actionsLookUpMap.put(28, "");
//        actionsLookUpMap.put(29, "");
//        actionsLookUpMap.put(30, "");
//        actionsLookUpMap.put(31, "");
//        actionsLookUpMap.put(32, "");
//        actionsLookUpMap.put(33, "");
//        actionsLookUpMap.put(34, "");
//        actionsLookUpMap.put(35, "");
//        actionsLookUpMap.put(36, "");
//        actionsLookUpMap.put(37, "");
//        actionsLookUpMap.put(38, "");
//        actionsLookUpMap.put(39, "");
//        actionsLookUpMap.put(40, "");
//        actionsLookUpMap.put(41, "");
//        actionsLookUpMap.put(42, "");
//        actionsLookUpMap.put(43, "");
//        actionsLookUpMap.put(44, "");
//        actionsLookUpMap.put(45, "");
//        actionsLookUpMap.put(46, "");
//        actionsLookUpMap.put(47, "");
//        actionsLookUpMap.put(48, "");
//        actionsLookUpMap.put(49, "");
//        actionsLookUpMap.put(50, "");
//        actionsLookUpMap.put(51, "");
//        actionsLookUpMap.put(52, "");
//        actionsLookUpMap.put(53, "");
//        actionsLookUpMap.put(54, "");
//        actionsLookUpMap.put(55, "");
//        actionsLookUpMap.put(56, "");
//        actionsLookUpMap.put(57, "");
//        actionsLookUpMap.put(58, "");
//        actionsLookUpMap.put(59, "");
//        actionsLookUpMap.put(60, "");
//        actionsLookUpMap.put(61, "");
//        actionsLookUpMap.put(62, "");
//        actionsLookUpMap.put(63, "");
//        actionsLookUpMap.put(64, "");
//        actionsLookUpMap.put(65, "");
//        actionsLookUpMap.put(66, "");
//        actionsLookUpMap.put(67, "");
//        actionsLookUpMap.put(68, "");
//        actionsLookUpMap.put(69, "");
//        actionsLookUpMap.put(70, "");
//        actionsLookUpMap.put(71, "");
//        actionsLookUpMap.put(72, "");
//        actionsLookUpMap.put(73, "");
//        actionsLookUpMap.put(74, "");
//        actionsLookUpMap.put(75, "RRRRC");
//        actionsLookUpMap.put(76, "BRRF");
//        actionsLookUpMap.put(77, "BCR");
//        actionsLookUpMap.put(78, "XCRC");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PTUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(PTUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private final static String[] cards = {
        "2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "Tc", "Jc", "Qc", "Kc", "Ac",
        "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "Td", "Jd", "Qd", "Kd", "Ad",
        "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "Th", "Jh", "Qh", "Kh", "Ah",
        "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "Ts", "Js", "Qs", "Ks", "As"
    }; // Js2h - J2o

    private final static String[] rangeCombos = {
        "A2o", "A3o", "A4o", "A5o", "A6o", "A7o", "A8o", "A9o", "ATo", "AJo",
        "AQo", "AKo", "K2o", "K3o", "K4o", "K5o", "K6o", "K7o", "K8o", "K9o",
        "KTo", "KJo", "KQo", "Q2o", "Q3o", "Q4o", "Q5o", "Q6o", "Q7o", "Q8o",
        "Q9o", "QTo", "QJo", "J2o", "J3o", "J4o", "J5o", "J6o", "J7o", "J8o",
        "J9o", "JTo", "T2o", "T3o", "T4o", "T5o", "T6o", "T7o", "T8o", "T9o",
        "TTo", "92o", "93o", "94o", "95o", "96o", "97o", "98o", "82o", "83o",
        "84o", "85o", "86o", "87o", "72o", "73o", "74o", "75o", "76o", "62o",
        "A6s", "A7s", "A8s", "A9s", "ATs", "AJs", "AQs", "AKs", "K2s", "K3s",
        "K4s", "K5s", "K6s", "K7s", "K8s", "K9s", "KTs", "KJs", "KQs", "Q2s",
        "Q3s", "Q4s", "Q5s", "Q6s", "Q7s", "Q8s", "Q9s", "QTs", "QJs", "J2s",
        "J3s", "J4s", "J5s", "J6s", "J7s", "J8s", "J9s", "JTs", "T2s", "T3s",
        "T4s", "T5s", "T6s", "T7s", "T8s", "T9s", "TTs", "92s", "93s", "94s",
        "95s", "96s", "97s", "98s", "82s", "83s", "84s", "85s", "86s", "87s",
        "72s", "73s", "74s", "75s", "76s", "62s", "63s", "64s", "65s", "52s",
        "53s", "54s", "42s", "43s", "32s", "22", "33", "44", "55", "66", "77",
        "88", "99", "TT", "JJ", "QQ", "KK", "AA",};

    public static String getRangeCombo(int rangeComboInt) {
        return rangeCombos[rangeComboInt - 1];
    }

    public static int getRangeComboInt(String rangeCombo) {
        for (int i = 0; i < rangeCombos.length; i++) {
            if (rangeCombos[i].equals(rangeCombo)) {
                //System.out.println("rangeCombos[i] " + rangeCombos[i]);
                return i + 1;
            }
        }
        return 0;
    }

    /*
     * ex. KsJd -> KJo
     *     KSKd -> KK
     *     KsJs -> KJs
     */
    public static String convertToRangeCombo(String s) {
        String card1Rank = s.substring(0, 1);
        String card1Suit = s.substring(1, 2);
        String card2Rank = s.substring(2, 3);
        String card2Suit = s.substring(3, 4);

        if (card1Rank.equals(card2Rank)) {
            return card1Rank + card2Rank;
        }
        Card c1 = new Card(card1Rank + card1Suit);
        Card c2 = new Card(card2Rank + card2Suit);

        if (card1Suit.equals(card2Suit)) {
            if (c1.rankOf().pipValue() > c2.rankOf().pipValue()) {
                return card1Rank + card2Rank + "s";
            }
            return card2Rank + card1Rank + "s";
        }
        if (c1.rankOf().pipValue() > c2.rankOf().pipValue()) {
            return card1Rank + card2Rank + "o";
        }
        return card2Rank + card1Rank + "o";
    }

    public static String getPTCard(String card) {
        for (int i = 0; i < cards.length; i++) {
            if (cards[i].equals(card)) {
                return Integer.toString(i + 1);
            }
        }
        return null;
    }

    public static int getPTCardInt(String card) {
        for (int i = 0; i < cards.length; i++) {
            if (cards[i].equals(card)) {
                return i + 1;
            }
        }
        return 0;
    }

    public static Card getCard(int ptCardInt) {
        if (ptCardInt == 0) {
            return null;
        }
        return new Card(cards[ptCardInt - 1]);
    }

    public static Set<String> getPossibleVillainPositions(String heroPosition, String villainPosition) {
        Set<String> villainPositions = new HashSet<>();

        if (villainPosition == null) {
            return villainPositions;
        }

        villainPositions.add(villainPosition);

        // Hero NOT SB/BB - villain sb/bb
        if (!heroPosition.equals("SB") && !heroPosition.equals("BB")) {
            if (villainPosition.equals("SB")) {
                villainPositions.add("SB");
                villainPositions.add("BB");
                return villainPositions;
            } else if (villainPosition.equals("BB")) {
                villainPositions.add("BB");
                villainPositions.add("SB");
                return villainPositions;
            }
        }

        // Hero EP - villain MP/CO
        if (heroPosition.equals("EP")) {
            if (villainPosition.equals("MP")) {
                villainPositions.add("MP");
                villainPositions.add("CO");
                villainPositions.add("BTN");
                return villainPositions;
            } else if (villainPosition.equals("CO")) {
                villainPositions.add("CO");
                villainPositions.add("MP");
                villainPositions.add("BTN");
                return villainPositions;
            }
        }

        // Hero MP - villain CO/BTN OR SB/BB
        if (heroPosition.equals("MP")) {
            if (villainPosition.equals("BTN")) {
                villainPositions.add("BTN");
                villainPositions.add("CO");
                return villainPositions;
            } else if (villainPosition.equals("CO")) {
                villainPositions.add("CO");
                villainPositions.add("BTN");
                return villainPositions;
            }
        }

        if (heroPosition.equals("CO")) {
            if (villainPosition.equals("EP")) {
                villainPositions.add("EP");
                villainPositions.add("MP");
                return villainPositions;
            } else if (villainPosition.equals("MP")) {
                villainPositions.add("MP");
                villainPositions.add("EP");
                return villainPositions;
            }
        }

        // checked
        if (heroPosition.equals("BTN")) {
            if (villainPosition.equals("EP")) {
                villainPositions.add("EP");
                villainPositions.add("MP");
                villainPositions.add("CO");
                return villainPositions;
            } else if (villainPosition.equals("MP")) {
                villainPositions.add("MP");
                villainPositions.add("EP");
                villainPositions.add("CO");
                return villainPositions;
            } else if (villainPosition.equals("CO")) {
                villainPositions.add("CO");
                villainPositions.add("MP");
                return villainPositions;
            }
        }

        if (heroPosition.equals("SB") || heroPosition.equals("BB")) {
            if (villainPosition.equals("EP")) {
                villainPositions.add("EP");
                villainPositions.add("MP");
                villainPositions.add("CO");
                return villainPositions;
            }
            if (villainPosition.equals("MP")) {
                villainPositions.add("MP");
                villainPositions.add("EP");
                villainPositions.add("CO");
                return villainPositions;
            }

            if (villainPosition.equals("CO")) {
                villainPositions.add("CO");
                villainPositions.add("MP");
                villainPositions.add("BTN");
                return villainPositions;
            }
            if (villainPosition.equals("BTN")) {
                villainPositions.add("BTN");
                villainPositions.add("CO");
                return villainPositions;
            }
        }

        return villainPositions;
    }

    public static String getPossibleVillainPosition(String heroPosition, String villainPosition) {
        // Hero NOT SB/BB - villain sb/bb
        if (!heroPosition.equals("SB") && !heroPosition.equals("BB")) {
            if (villainPosition.equals("SB")) {
                return "BB";
            } else if (villainPosition.equals("BB")) {
                return "SB";
            }
        }

        // Hero EP - villain MP/CO
        if (heroPosition.equals("EP")) {
            if (villainPosition.equals("MP")) {
                return "CO";
            } else if (villainPosition.equals("CO")) {
                return "MP";
            }
        }

        // Hero MP - villain CO/BTN OR SB/BB
        if (heroPosition.equals("MP")) {
            if (villainPosition.equals("BTN")) {
                return "CO";
            } else if (villainPosition.equals("CO")) {
                return "BTN";
            }
        }

        if (heroPosition.equals("CO")) {
            if (villainPosition.equals("EP")) {
                return "MP";
            } else if (villainPosition.equals("MP")) {
                return "EP";
            }
        }

        if (heroPosition.equals("BTN")) {
            if (villainPosition.equals("EP")) {
                return "MP";
            } else if (villainPosition.equals("MP")) {
                return "EP";
            } else if (villainPosition.equals("CO")) {
                return "MP";
            }
        }

        if (heroPosition.equals("SB") || heroPosition.equals("BB")) {
            if (villainPosition.equals("EP")) {
                return "MP";
            }
            if (villainPosition.equals("MP")) {
                return "EP";
            }

            if (villainPosition.equals("CO")) {
                return "MP";
            }
            if (villainPosition.equals("MP")) {
                return "CO";
            }

            if (villainPosition.equals("CO")) {
                return "BTN";
            } else if (villainPosition.equals("BTN")) {
                return "CO";
            }
        }

        return null;
    }

    public static int getCategoryIdStrengthId(CardSet holeCards, CardSet board) {
        int strengthId = PTHandStrengthAnalyzer.getPTHandStrengthIDValue(holeCards, board);
        HandCategory handCategory = HandCategoryAnalyzer.handCategory(holeCards, board);
        int categoryId = handCategory.ordinal() + 1;
        return Integer.parseInt(String.valueOf(categoryId) + String.valueOf(strengthId));
    }

    public static int getCategoryIdStrengthId(int categoryId, int strengthId) {
        return Integer.parseInt(String.valueOf(categoryId) + String.valueOf(strengthId));
    }

    public static CardSet getHoleCardSet(int idHoleCardId1, int idHoleCardId2) {
        CardSet holeCards = new CardSet();

        holeCards.add(PTUtil.getCard(idHoleCardId1));
        holeCards.add(PTUtil.getCard(idHoleCardId2));

        return holeCards;
    }

    public static CardSet getBoardCardSet(int id_flop1, int id_flop2, int id_flop3, int id_turn, int id_river) {
        CardSet board = new CardSet();

        if (id_flop1 > 0) {
            board.add(PTUtil.getCard(id_flop1));
            board.add(PTUtil.getCard(id_flop2));
            board.add(PTUtil.getCard(id_flop3));
        }
        if (id_turn > 0) {
            board.add(PTUtil.getCard(id_turn));
        }
        if (id_river > 0) {
            board.add(PTUtil.getCard(id_river));
        }

        return board;
    }

    public static String getPTPositionString(PositionPreFlop positionPreflop) {
        if (positionPreflop == PositionPreFlop.UTG) {
            return "EP";
        }
        if (positionPreflop == PositionPreFlop.UTGplus1) {
            return "MP";
        }
        if (positionPreflop == PositionPreFlop.CO) {
            return "CO";
        }
        if (positionPreflop == PositionPreFlop.BTN) {
            return "BTN";
        }
        if (positionPreflop == PositionPreFlop.SB) {
            return "SB";
        }
        if (positionPreflop == PositionPreFlop.BB) {
            return "BB";
        }
        return null;
    }

    public static String getPTPositionString(int posInt) {
        if (posInt == 2) {
            return "EP";
        }
        if (posInt == 3) {
            return "MP";
        }
        if (posInt == 4) {
            return "CO";
        }
        if (posInt == 5) {
            return "BTN";
        }
        if (posInt == 0) {
            return "SB";
        }
        return "BB";
    }

    public static int getPositionInt(String pos) {
        if (pos.equalsIgnoreCase("EP")) {
            return 2;
        }
        if (pos.equalsIgnoreCase("MP")) {
            return 3;
        }
        if (pos.equalsIgnoreCase("CO")) {
            return 4;
        }
        if (pos.equalsIgnoreCase("BTN")) {
            return 5;
        }
        if (pos.equalsIgnoreCase("SB")) {
            return 0;
        }
        return 1;
    }

//    public static String getPosTextDrawFlop(int heroPosInt, int villainPosInt, CardSet flopSet) {
//        boolean tex_any_pair_f = PTBoardTextureAnalyzer.isTex_any_pair_f(flopSet);
//        boolean tex_two_cards_suited_f = PTBoardTextureAnalyzer.isTex_two_cards_suited_f(flopSet);
//        boolean tex_three_of_a_kind_f = PTBoardTextureAnalyzer.isTex_three_of_a_kind_f(flopSet);
//        boolean tex_three_cards_connected_f = PTBoardTextureAnalyzer.isTex_three_cards_connected_f(flopSet);
//        boolean tex_three_cards_suited_f = PTBoardTextureAnalyzer.isTex_three_cards_suited_f(flopSet);
//
//        return ""
//                + heroPosInt
//                + villainPosInt
//                + bs(tex_any_pair_f)
//                + bs(tex_two_cards_suited_f)
//                + bs(tex_three_of_a_kind_f)
//                + bs(tex_three_cards_connected_f)
//                + bs(tex_three_cards_suited_f);
//    }
//    public static String getPosTextDrawTurn(int heroPosInt, int villainPosInt, CardSet flopSet, CardSet turnSet) {
//        boolean tex_paired_three_of_a_kind_t = PTBoardTextureAnalyzer.isTex_paired_three_of_a_kind_t(flopSet, turnSet);
//        boolean tex_rainbow_t = PTBoardTextureAnalyzer.isTex_rainbow_t(turnSet);
//        boolean tex_t_card_is_third_card_of_suit_t = PTBoardTextureAnalyzer.isTex_t_card_is_third_card_of_suit_t(flopSet, turnSet);
//        boolean tex_all_cards_one_suit_t = PTBoardTextureAnalyzer.isTex_all_cards_one_suit_t(turnSet);
//        boolean tex_four_cards_connected_t = PTBoardTextureAnalyzer.isTex_four_cards_connected_t(turnSet);
//        boolean tex_paired_double_pair_t = PTBoardTextureAnalyzer.isTex_paired_double_pair_t(flopSet, turnSet);
//
//        return ""
//                + heroPosInt
//                + villainPosInt
//                + bs(tex_paired_three_of_a_kind_t)
//                + bs(tex_rainbow_t)
//                + bs(tex_t_card_is_third_card_of_suit_t)
//                + bs(tex_all_cards_one_suit_t)
//                + bs(tex_four_cards_connected_t)
//                + bs(tex_paired_double_pair_t);
//    }
//    public static String getPosTextDrawRiver(int heroPosInt, int villainPosInt, CardSet turnSet, CardSet riverSet) {
//        boolean tex_paired_double_paired_r = PTBoardTextureAnalyzer.isTex_paired_double_paired_r(riverSet);
//        boolean tex_paired_fullhouse_r = PTBoardTextureAnalyzer.isTex_paired_fullhouse_r(riverSet);
//        boolean tex_paired_three_of_a_kind_r = PTBoardTextureAnalyzer.isTex_paired_three_of_a_kind_r(turnSet, riverSet);
//        boolean tex_four_cards_suited_r = PTBoardTextureAnalyzer.isTex_four_cards_suited_r(riverSet);
//        boolean tex_less_than_three_cards_suited_r = PTBoardTextureAnalyzer.isTex_less_than_three_cards_suited_r(riverSet);
//        boolean tex_three_cards_suited_r = PTBoardTextureAnalyzer.isTex_three_cards_suited_r(riverSet);
//        boolean tex_five_cards_connected_r = PTBoardTextureAnalyzer.isTex_five_cards_connected_r(riverSet);
//        boolean tex_five_cards_suited_r = PTBoardTextureAnalyzer.isTex_five_cards_suited_r(riverSet);
//        boolean tex_four_cards_connected_r = PTBoardTextureAnalyzer.isTex_four_cards_connected_r(riverSet);
//
//        return ""
//                + heroPosInt
//                + villainPosInt
//                + bs(tex_paired_double_paired_r)
//                + bs(tex_paired_fullhouse_r)
//                + bs(tex_paired_three_of_a_kind_r)
//                + bs(tex_four_cards_suited_r)
//                + bs(tex_less_than_three_cards_suited_r)
//                + bs(tex_three_cards_suited_r)
//                + bs(tex_five_cards_connected_r)
//                + bs(tex_five_cards_suited_r)
//                + bs(tex_four_cards_connected_r);
//    }
    private static String bs(boolean b) {
        return b == true ? "1" : "0";
    }

    public static String getActionsString(int actionsId) {
        for (Map.Entry<Integer, String> entry : actionsLookUpMap.entrySet()) {
            String value = (String) entry.getValue();
            Integer key = entry.getKey();

            if (actionsId == 0) {
                return "-";
            }

            if (key == actionsId) {
                return value;
            }
        }

        return "-";
    }

    public static int getActionsID(String actions) {
        for (Map.Entry<Integer, String> entry : actionsLookUpMap.entrySet()) {
            String value = (String) entry.getValue();
            Integer key = entry.getKey();

            if (value.equalsIgnoreCase(actions)) {
                return key;
            }
        }

        return 0;
    }

    public static List<Integer> getActionsIDsStartingOrEqualsWith(String actions) {
        List<Integer> actionIDs = new ArrayList<>();

        for (Map.Entry<Integer, String> entry : actionsLookUpMap.entrySet()) {
            String value = (String) entry.getValue();
            Integer key = entry.getKey();

            if (value.startsWith(actions) || actions.isEmpty()) {
                actionIDs.add(key);
            }
        }

        return actionIDs;
    }

    public static Double getBB(int id_limit) {
        switch (id_limit) {
            case 1:
                return 0.1;
            case 2:
                return 0.1;
            case 3:
                return 0.1;
            case 4:
                return 0.1;
            case 5:
                return 0.1;
            case 6:
                return 0.1;
            case 7:
                return 0.0;
            case 8:
                return 0.02;
            case 9:
                return 0.16;
            case 10:
                return 0.05;
            case 11:
                return 0.05;
            case 12:
                return 0.25;
            case 13:
                return 0.5;
            default:
                break;
        }

        return null;
    }
}
