package com.masa.action;

import com.masa.type.Action;

/**
 * Resolves action for specific hand combination in a given situation
 *
 * @author Matti
 */
public class PreflopActionResolver {

    private AbstractActionResolver actionResolver;

    public Action resolve(String aggressors, String heroPosition, String villainPosition, String holeCards) {

        if (isOpen(aggressors)) {
            actionResolver = new PreflopActionOpenResolver(heroPosition, holeCards);
        } else if (isVsOpen(aggressors)) {
            actionResolver = new PreflopActionVsOpenResolver(heroPosition, villainPosition, holeCards);
        } else if (isVs3Bet(aggressors)) {
            actionResolver = new PreflopActionVs3BetResolver(heroPosition, villainPosition, holeCards);
        }
        /*else if (isVs4Bet(aggressors)) {
            actionResolver = new PreflopComboActionVs4BetResolver(hand, holeCards);
        }*/

        return actionResolver.resolve();
    }

    private boolean isOpen(String actions) {
        return actions.isEmpty();
    }

    private boolean isVsOpen(String actions) {
        return actions.length() == 1;
    }

    private boolean isVs3Bet(String actions) {
        return actions.length() == 2;
    }

    private boolean isVs4Bet(String actions) {
        return actions.length() == 3;
    }
}
