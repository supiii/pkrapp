package com.masa.action;

import com.masa.type.Action;

/**
 * http://www.zenithpoker.com/preflop/vs-3bb-open/
 *
 * @author Matti
 */
public class PreflopActionVsOpenResolver extends AbstractActionResolver {

    public PreflopActionVsOpenResolver(String heroPosition, String villainPosition, String holeCards) {
        super(heroPosition, villainPosition, holeCards);
    }

    @Override
    public Action resolve() {
        switch (heroPosition) {
            case "MP":
                return whenMP();
            case "CO":
                return whenCO();
            case "BTN":
                return whenBTN();
            case "SB":
                return whenSB();
            case "BB":
                return whenBB();
        }
        return null;
    }

    private Action whenMP() {
        switch (villainPosition) {
            case "EP":
                return whenMPvsEP();
        }
        return null;
    }

    private Action whenCO() {
        switch (villainPosition) {
            case "EP":
                return whenCOvsEP();
            case "MP":
                return whenCOvsMP();
        }
        return null;
    }

    private Action whenBTN() {
        switch (villainPosition) {
            case "EP":
                return whenBTNvsEP();
            case "MP":
                return whenBTNvsMP();
            case "CO":
                return whenBTNvsCO();
        }
        return null;
    }

    private Action whenSB() {
        switch (villainPosition) {
            case "EP":
                return whenSBvsEP();
            case "MP":
                return whenSBvsMP();
            case "CO":
                return whenSBvsCO();
            case "BTN":
                return whenSBvsBTN();
        }
        return null;
    }

    private Action whenBB() {
        switch (villainPosition) {
            case "EP":
                return whenBBvsEP();
            case "MP":
                return whenBBvsMP();
            case "CO":
                return whenBBvsCO();
            case "BTN":
                return whenBBvsBTN();
            case "SB":
                return whenBBvsSB();
        }
        return null;
    }

    private Action whenMPvsEP() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, SAK, SAQ, SAJ, SA5, SA4, SKQ, SKJ, SQJ, OAK, OAQ);
        addRaiseOrFold(SAT, SA9, SKT, P99, S76, S65, S54);

        return checkListsAndReturnAction();
    }

    private Action whenCOvsEP() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, P99, SA4, SA5, SA9, SAT, SAJ, SAQ, SAK, SKT, SKJ, SKQ, SQJ, OAK, OAQ);
        addRaiseOrFold(SQT, OKQ, P88, S76, S65, S54);

        return checkListsAndReturnAction();
    }

    private Action whenCOvsMP() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, P99, P88, SAT, SAJ, SAQ, SAK, SKT, SKJ, SKQ, SQJ, SQT, SJT, OKQ, OAK, OAQ);
        addRaiseOrFold(OAJ, S76, S65, S54);

        return checkListsAndReturnAction();
    }

    private Action whenBTNvsEP() {
        addRaise(PAA, PKK, PQQ, SAK, OAK, SQJ, SQT, SA5, SA4);
        addRaiseOrFold(SK9, OAJ);
        addCall(SAJ, SAT, SKQ, SKJ, SJT, P99, P88, P77, P66);
        addRaiseOrCall(SA3, SA8, SA9, SAQ, SKT, SAQ, OKQ, OAQ, PJJ, ST9);
        addRaiseCallOrFold(S98, S87, S76, S65, S54);

        return checkListsAndReturnAction();
    }

    private Action whenBTNvsMP() {
        addRaise(PAA, PKK, PQQ, SAK, OAK, SQJ, SQT, SA4, SA5);
        addRaiseOrCall(SA3, SA8, SA9, SAQ, SKT, OAQ, OAK, PJJ, PTT, ST9);
        addCall(SAJ, SAT, SKJ, SKQ, SJT, P99, P88, P77, P66);
        addCallOrFold(P55, P44, P33, P22);
        addRaiseOrFold(OAJ, SK9);

        return checkListsAndReturnAction();
    }

    private Action whenBTNvsCO() {
        addRaise(PAA, PKK, PQQ, SAQ, SAK, SQT, SQJ, OAK);
        addRaiseOrCall(SA3, SA4, SA5, SA6, SA7, SA8, SA9, SAT, SAJ, SKT, SKJ, SKQ, SQ9,
                SJ9, SJT, ST9, OKQ, OAQ, OAJ, PJJ, PTT, P99, P88, P77, P66, P55);
        addRaiseCallOrFold(SA2, SK9, S98, S87, S76, S65, S54);
        addCallOrFold(P44, P33, P22);

        return checkListsAndReturnAction();
    }

    private Action whenSBvsEP() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, P99, SA5, SAT, SAJ, SAQ, SAK, SKT, SKJ, SKQ, SQT,
                SQJ, SJT, OAK, OAQ);
        addRaiseOrFold(SA4, P88);
        return checkListsAndReturnAction();
    }

    private Action whenSBvsMP() {
        addRaise(SA5, SAT, SAJ, SAQ, SAK, SKT, SKJ, SKQ, SQJ, OAK, PAA, PKK, PQQ, PJJ, PTT, P99);
        addRaiseOrFold(SQT, SJT, OAQ);

        return checkListsAndReturnAction();
    }

    private Action whenSBvsCO() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, P99, SA5, SAT, SAJ, SAQ, SAK, SKT, SKJ, SKQ, SQT,
                SQJ, SJT, OKQ, OAQ, OAK);
        addRaiseOrFold(SA4, SA9, SK9, SJ9, ST9, OAJ, P88, P77);

        return checkListsAndReturnAction();
    }

    private Action whenSBvsBTN() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, P99, P88, P77, SA4, SA5, SA8, SA9, SAT, SAJ, SAQ, SAK,
                SK9, SKT, SKJ, SKQ, SQT, SQJ, SJ9, SJT, ST9, OAJ, OAQ, OAK, OKQ);
        addRaiseOrFold(SA7, SQ9, OKJ, OAT, P66, P55);

        return checkListsAndReturnAction();
    }

    private Action whenBBvsEP() {
        addRaise(PAA, PKK, PQQ, SAK, OAK);
        addRaiseOrCall(SA2, SA3, SA4, SA5, SA8, SA9, SAT, SAJ, SAQ, SK6, SK7, SKT, SKJ, SKQ,
                SQ9, SQT, SQJ, SJT, S87, S76, S65, S54, PJJ, PTT);
        addCall(SA6, SA7, SK2, SK3, SK4, SK5, SK8, SK9, SQ6, SQ7, SQ8, SJ8, SJ9, ST7, ST8, ST9, S96, S97,
                S98, S86, S75, S64, S53, S43, OKQ, OAQ, OQJ, OKJ, OAJ, OAT, P99, P88, P77,
                P66, P55, P44, P33, P22);
        addCallOrFold(SQ3, SQ4, SQ5, SJ7, S85, S74, S63, S52, S42, OJT, OQT, OKT, OA9);
        return checkListsAndReturnAction();
    }

    private Action whenBBvsMP() {
        addRaise(PAA, PKK, PQQ, SAK, OAK);
        addRaiseOrCall(SA2, SA3, SA4, SA5, SA8, SA8, SA9, SAT, SAJ, SAQ, SK6, SK7, SKT, SKJ, SKQ, SQ9,
                SQT, SQJ, SJT, PJJ, S87, S76, S65, S54);
        addCall(SA6, SA7, SK2, SK3, SK4, SK5, SK8, SK9, SQ6, SQ7, SQ8, SJ8, SJ9, ST7, ST8, ST9, S98, S96,
                S97, S86, S75, S64, S53, S43, OKQ, OAQ, OKJ, OAJ, OAT, PTT, P99, P88, P77, P66, P55,
                P44, P33, P22);
        addCallOrFold(SQ3, SQ4, SQ5, SJ7, OQJ, S52, S42);

        return checkListsAndReturnAction();
    }

    private Action whenBBvsCO() {
        addRaise(PAA, PKK, PQQ, PJJ, SAQ, SAK, OAK);
        addRaiseOrCall(SA2, SA3, SA4, SA5, SAT, SAJ, SK6, SK7, SKT, SKJ, SKQ, SQ4, SQ5,
                SQ9, SQT, SQJ, SJ9, SJT, ST9, S87, S76, S65, S54, PTT, OKQ, OAQ);
        addCall(SA6, SA7, SA8, SA9, SK2, SK3, SK4, SK5, SK8, SK9, SQ6, SQ7, SQ8, SJ7, SJ8,
                ST7, ST8, S96, S97, S98, S86, S75, S64, S53, S43, OQJ, OKJ, OAJ,
                OJT, OQT, OKT, OAT, OA9, P99, P88, P77, P66, P55, P44, P33, P22);
        addCallOrFold(SQ3, SJ6, S85, S74, S63, S52, S42, OA8, OA5);

        return checkListsAndReturnAction();
    }

    private Action whenBBvsBTN() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, SAQ, SAK, SJ9, SJT, ST9, OAK);
        addRaiseOrCall(SA2, SA3, SA4, SA5, SAT, SAJ, SK6, SK7, SK9, SKT, SKJ, SKQ,
                SQ8, SQ9, SQT, SQJ, OKQ, OAQ, SJ7, SJ8, OKJ, OAJ, ST6, ST7, ST8,
                OAT, S97, S98, P99, S87, P88, S76, S65, S54);
        addCall(SA6, SA7, SA8, SA9, SK2, SK3, SK4, SK5, SK8, SQ2, SQ3, SQ4,
                SQ5, SQ6, SQ7, SJ4, SJ5, SJ6, OQJ, OJT, OQT, OKT, S96,
                OT9, OK9, OA9, S85, S86, OA8, S74, S75, P77, OA7, S64, S53, S43,
                P66, P55, P44, P33, P22, OA5);
        addCallOrFold(S63, S52, S42, OQ9, O98, OT8, OK8, O87, O76, OA6, O65, OA4);

        return checkListsAndReturnAction();
    }

    private Action whenBBvsSB() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, SA4, SA5, SAQ, SAK, OAQ, OAK);
        addRaiseOrCall(P99, P88, SA2, SA3, SAT, SAJ, SKT, SKJ, SKQ, SQ9, SQT, SQJ,
                SJ8, SJ9, SJT, ST8, ST9, S98, S87, S76, S65, S54, OKQ,
                OKJ, OAJ, OK6, OA4, OA3, OA2);
        addCall(P77, P66, P55, P44, P33, P22, SA6, SA7, SA8, SA9, SK2, SK3, SK4,
                SK5, SK6, SK7, SK8, SK9, SQ2, SQ3, SQ4, SQ5, SQ6, SQ7, SQ8,
                SJ2, SJ3, SJ4, SJ5, SJ6, SJ7, ST2, ST3, ST4, ST5, ST6, ST7, S95, S96,
                S97, S85, S86, S74, S75, S63, S64, S52, S53, S42, S43, S32,
                OQJ, OJT, OQT, OKT, OAT, OT9, OJ9, OQ9, OK9, OA9, OJ8, OQ8, OK8, OA8,
                OK7, OA7, OA6, OA5);
        addCallOrFold(S84, S73, O98, OT8, O87, O76, O65);

        return checkListsAndReturnAction();
    }

}
