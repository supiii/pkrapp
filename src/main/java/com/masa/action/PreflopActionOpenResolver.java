package com.masa.action;

import com.masa.type.Action;
import java.util.Arrays;

/**
 *
 * @author Matti
 */
public class PreflopActionOpenResolver extends AbstractActionResolver {

    public PreflopActionOpenResolver(String heroPosition, String holeCards) {
        super(heroPosition, null, holeCards);
    }

    @Override
    public Action resolve() {
        switch (heroPosition) {
            case "EP":
                return whenEP();
            case "MP":
                return whenMP();
            case "CO":
                return whenCO();
            case "BTN":
                return whenBTN();
            case "SB":
                return whenSB();
        }
        return Action.FOLD;
    }

    private Action whenEP() {
        raise.addAll(getRestOfRangeCombos("A2s"));
        raise.addAll(getRestOfRangeCombos("K9s"));
        raise.addAll(getRestOfRangeCombos("Q9s"));
        raise.addAll(getRestOfRangeCombos("J9s"));
        raise.addAll(getRestOfRangeCombos("ATo"));
        raise.addAll(getRestOfRangeCombos("KJo"));
        raise.addAll(getRestOfRangeCombos("77"));

        raiseOrFold.add("K8s");
        raiseOrFold.add("98s");
        raiseOrFold.add("87s");
        raiseOrFold.add("76s");
        raiseOrFold.add("66");
        raiseOrFold.add("65s");
        raiseOrFold.add("55");

        return checkListsAndReturnAction();
    }

    private Action whenMP() {
        raise.addAll(getRestOfRangeCombos("A2s"));
        raise.addAll(getRestOfRangeCombos("K9s"));
        raise.addAll(getRestOfRangeCombos("Q9s"));
        raise.addAll(getRestOfRangeCombos("J9s"));
        raise.addAll(getRestOfRangeCombos("T9s"));
        raise.addAll(getRestOfRangeCombos("ATo"));
        raise.addAll(getRestOfRangeCombos("KJo"));
        raise.addAll(getRestOfRangeCombos("55"));
        raise.add("98s");
        raise.add("87s");
        raise.add("76s");
        raise.add("65s");
        raise.add("54s");

        raiseOrFold.add("K7s");
        raiseOrFold.add("K8s");
        raiseOrFold.add("T8s");
        raiseOrFold.add("QJo");
        raiseOrFold.add("44");
        raiseOrFold.add("33");

        return checkListsAndReturnAction();
    }

    private Action whenCO() {
        raise.addAll(getRestOfRangeCombos("A2s"));
        raise.addAll(getRestOfRangeCombos("K6s"));
        raise.addAll(getRestOfRangeCombos("Q8s"));
        raise.addAll(getRestOfRangeCombos("J9s"));
        raise.addAll(getRestOfRangeCombos("A9o"));
        raise.addAll(getRestOfRangeCombos("KJo"));
        raise.addAll(getRestOfRangeCombos("22"));
        raise.add("T9s");
        raise.add("98s");
        raise.add("87s");
        raise.add("76s");
        raise.add("65s");
        raise.add("54s");
        raise.add("QJo");

        raiseOrFold.add("K3s");
        raiseOrFold.add("K4s");
        raiseOrFold.add("K5s");
        raiseOrFold.add("Q6s");
        raiseOrFold.add("Q7s");
        raiseOrFold.add("J7s");
        raiseOrFold.add("J8s");
        raiseOrFold.add("T7s");
        raiseOrFold.add("T8s");
        raiseOrFold.add("97s");
        raiseOrFold.add("86s");
        raiseOrFold.add("JTo");
        raiseOrFold.add("QTo");
        raiseOrFold.add("KTo");
        raiseOrFold.add("A8o");

        return checkListsAndReturnAction();
    }

    private Action whenBTN() {
        raise.addAll(getRestOfRangeCombos("A2s"));
        raise.addAll(getRestOfRangeCombos("K2s"));
        raise.addAll(getRestOfRangeCombos("Q4s"));
        raise.addAll(getRestOfRangeCombos("J6s"));
        raise.addAll(getRestOfRangeCombos("T7s"));
        raise.addAll(getRestOfRangeCombos("97s"));
        raise.addAll(getRestOfRangeCombos("A5o"));
        raise.addAll(getRestOfRangeCombos("K8o"));
        raise.addAll(getRestOfRangeCombos("Q9o"));
        raise.addAll(getRestOfRangeCombos("J9o"));
        raise.addAll(getRestOfRangeCombos("T9o"));
        raise.addAll(getRestOfRangeCombos("22"));
        raise.addAll(Arrays.asList("87s", "76s", "65s", "54s"));

        raiseOrFold.add("Q2s");
        raiseOrFold.add("Q3s");
        raiseOrFold.add("J4s");
        raiseOrFold.add("J5s");
        raiseOrFold.add("T6s");
        raiseOrFold.add("96s");
        raiseOrFold.add("86s");
        raiseOrFold.add("98o");
        raiseOrFold.add("T8o");
        raiseOrFold.add("J8o");
        raiseOrFold.add("75s");
        raiseOrFold.add("A4o");
        raiseOrFold.add("A3o");
        raiseOrFold.add("A2o");

        return checkListsAndReturnAction();
    }

    private Action whenSB() {

        raise.addAll(getRestOfRangeCombos("A2s"));
        raise.addAll(getRestOfRangeCombos("K2s"));
        raise.addAll(getRestOfRangeCombos("Q4s"));
        raise.addAll(getRestOfRangeCombos("J6s"));
        raise.addAll(getRestOfRangeCombos("T7s"));
        raise.addAll(getRestOfRangeCombos("97s"));
        raise.addAll(getRestOfRangeCombos("A5o"));
        raise.addAll(getRestOfRangeCombos("K8o"));
        raise.addAll(getRestOfRangeCombos("Q9o"));
        raise.addAll(getRestOfRangeCombos("J9o"));
        raise.addAll(Arrays.asList("87s", "76s", "65s", "54s", "T9o"));

        raiseOrFold.addAll(Arrays.asList("Q2s", "Q3s", "J4s", "J5s", "T6s", "96s",
                "86s", "75s", "98o", "T8o", "J8o", "Q8o", "K7o", "A2o", "A3o", "A4o"));

        return checkListsAndReturnAction();
    }
}
