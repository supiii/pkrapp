package com.masa.pkrapp;

import com.masa.action.PreflopActionResolver;
import static com.masa.component.InfoPanel.createInfoPanel;
import static com.masa.component.TablePanel.createTablePanel;
import com.masa.pokertracker.PTNextPreflopMoveService;
import com.masa.range.raise.PreflopRangeResolver;
import com.masa.service.NextPostFlopMovesService;
import com.masa.type.ComboCheckBox;
import com.masa.type.Draw;
import com.masa.type.MadeHandSpecific;
import com.masa.type.Ranges;
import com.masa.type.Seat;
import static com.masa.util.Util.cleanUp;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.TreeMap;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 * JavaFX App
 */
public class App extends Application {

    //public static Stage stage;
    public static final int TABLE_NRO = 1;

    public static final DecimalFormat df = new DecimalFormat("0.00");

    public static final PTNextPreflopMoveService nextPreflopMoveService = new PTNextPreflopMoveService();
    public static final NextPostFlopMovesService nextPostFlopMovesService = new NextPostFlopMovesService();
    //public static final NextMovesAnalyzer nextMovesAnalyzer = new NextMovesAnalyzer();
    public static PreflopActionResolver preflopResolver = new PreflopActionResolver();
    public static PreflopRangeResolver preflopRangesResolver = new PreflopRangeResolver();
    public static final Image backImg = new Image(App.class.getResourceAsStream("back.png"));

    public static final char[] SUITS = {'s', 'd', 'h', 'c'};
    public static final char[] RANKS = {'A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'};

    public static Ranges ranges;

    public static String holeCardLeft;
    public static String holeCardRight;
    public static String firstFlopCard;
    public static String secondFlopCard;
    public static String thirdFlopCard;
    public static String turnCard;
    public static String riverCard;
    public static Seat dealerSeat;
    public static Seat villainSeat;
    public static String villainPos;
    public static String heroPos;
    public static boolean heroActingFirstPF = false;
    public static String actionsHeroPreFlop = "";
    public static String actionsHeroFlop = "";
    public static String actionsHeroTurn = "";
    public static String actionsHeroRiver = "";
    public static String actionsVillainPreFlop = "";
    public static String actionsVillainFlop = "";
    public static String actionsVillainTurn = "";
    public static String actionsVillainRiver = "";
    public static String actionsEPPF = "";
    public static String actionsMPPF = "";
    public static String actionsCOPF = "";
    public static String actionsBTNPF = "";
    public static String actionsSBPF = "";
    public static String actionsBBPF = "";
    public static String preflopActions = "";
    public static String preflopActors = "";
    public static String flopActors = "";
    public static String turnActors = "";
    public static String riverActors = "";
    public static String preflopAggressors = "";

    public static ToggleGroup dealerGroup = new ToggleGroup();
    public static Label resolvedActionLabel = new Label("-");
    public static Label rawEquityLabelFlop = new Label("-");
    public static Label rawEquityLabelTurn = new Label("-");
    public static Label rawEquityLabelRiver = new Label("-");
    public static Label rangeEquityLabelFlop = new Label("-");
    public static Label rangeEquityLabelTurn = new Label("-");
    public static Label rangeEquityLabelRiver = new Label("-");
    public static Label nutAdvantageLabel = new Label("-");
    public static Label villainRangeConnectivityLabel = new Label("-");
    public static Label villainNutAdvantageLabel = new Label("-");
    public static Label vulnerabilityLabel = new Label("-");
    public static Label equityVsContRangeFlop = new Label("-");
    public static Label equityVsContRangeTurn = new Label("-");
    public static Label equityVsContRangeRiver = new Label("-");
    public static ImageView leftHoleCardImgView = new ImageView(backImg);
    public static ImageView rightHoleCardImgView = new ImageView(backImg);

    public static Slider villainContinueRangeSlider;

    public static ImageView firstFlopCardImgView = new ImageView(backImg);
    public static ImageView secondFlopCardImgView = new ImageView(backImg);
    public static ImageView thirdFlopCardImgView = new ImageView(backImg);
    public static ImageView turnCardImgView = new ImageView(backImg);
    public static ImageView riverCardImgView = new ImageView(backImg);

    public static int villainVPIP = 24;
    public static int villainPFR = 17;
    public static int villain3Bet = 6;

    public static CardSet board = new CardSet();
    public static CardSet holeCards = new CardSet();

    public static VBox cardsRows = new VBox();

    public static Map<MadeHandSpecific, ComboCheckBox> comboCheckBoxValues = new TreeMap<>();
    public static Map<Draw, ComboCheckBox> drawCheckBoxValues = new TreeMap<>();

    public static VBox infoPanel;

    public static double cardImgWidth = 28.00;
    public static double cardImgHeight = 45.00;

    public static void main(String[] args) {
        leftHoleCardImgView.setFitWidth(46.0);
        leftHoleCardImgView.setFitHeight(65.0);
        rightHoleCardImgView.setFitWidth(46.0);
        rightHoleCardImgView.setFitHeight(65.0);
        firstFlopCardImgView.setFitWidth(cardImgWidth);
        firstFlopCardImgView.setFitHeight(cardImgHeight);
        secondFlopCardImgView.setFitWidth(cardImgWidth);
        secondFlopCardImgView.setFitHeight(cardImgHeight);
        thirdFlopCardImgView.setFitWidth(cardImgWidth);
        thirdFlopCardImgView.setFitHeight(cardImgHeight);
        turnCardImgView.setFitWidth(cardImgWidth);
        turnCardImgView.setFitHeight(cardImgHeight);
        riverCardImgView.setFitWidth(cardImgWidth);
        riverCardImgView.setFitHeight(cardImgHeight);
        actionsHeroPreFlop = "";
        actionsEPPF = "";
        actionsMPPF = "";
        actionsCOPF = "";
        actionsBTNPF = "";
        actionsSBPF = "";
        actionsBBPF = "";
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        //stage = primaryStage;
        startNew(primaryStage);
    }

    public static void startNew(Stage stage) {
        stage.setTitle("Acer NitroSense & CoolBoost");
        //ranges = new Ranges();
        infoPanel = createInfoPanel();
        HBox hBoxLayout = new HBox(createTablePanel(stage), infoPanel);
        hBoxLayout.setPadding(new Insets(10, 10, 10, 10));
        stage.setScene(createScene(hBoxLayout));
        stage.show();
    }

    public static void restart(Stage stage) {
        cleanUp();
        startNew(stage);
    }

    private static Scene createScene(HBox hBoxLayout) {
        Scene scene = new Scene(hBoxLayout, 1080, 400);

        scene.getStylesheets().add(App.class.getResource("style.css").toExternalForm());

        scene.setOnScroll((ScrollEvent event) -> {
            //System.out.println(event.getDeltaX());
            //System.out.println(event.getDeltaY());
            if (null == dealerSeat) {
                dealerSeat = Seat.FOUR;
            } else {
                switch (dealerSeat) {
                    case ONE:
                        dealerSeat = Seat.TWO;
                        dealerGroup.getToggles().get(1).setSelected(true);
                        break;
                    case TWO:
                        dealerSeat = Seat.THREE;
                        dealerGroup.getToggles().get(2).setSelected(true);
                        break;
                    case THREE:
                        dealerSeat = Seat.FOUR;
                        dealerGroup.getToggles().get(3).setSelected(true);
                        break;
                    case FOUR:
                        dealerSeat = Seat.FIVE;
                        dealerGroup.getToggles().get(4).setSelected(true);
                        break;
                    case FIVE:
                        dealerSeat = Seat.SIX;
                        dealerGroup.getToggles().get(5).setSelected(true);
                        break;
                    case SIX:
                        dealerSeat = Seat.ONE;
                        dealerGroup.getToggles().get(0).setSelected(true);
                        break;
                    default:
                        break;
                }
            }
        });

        return scene;
    }

}
