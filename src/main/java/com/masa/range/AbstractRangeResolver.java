package com.masa.range;

import com.masa.type.Action;
import com.masa.type.WeightedCombination;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author Matti
 */
public abstract class AbstractRangeResolver {

    protected final String OA2 = "A2o";
    protected final String OA3 = "A3o";
    protected final String OA4 = "A4o";
    protected final String OA5 = "A5o";
    protected final String OA6 = "A6o";
    protected final String OA7 = "A7o";
    protected final String OA8 = "A8o";
    protected final String OA9 = "A9o";
    protected final String OAT = "ATo";
    protected final String OAJ = "AJo";
    protected final String OAQ = "AQo";
    protected final String OAK = "AKo";
    protected final String OK2 = "K2o";
    protected final String OK3 = "K3o";
    protected final String OK4 = "K4o";
    protected final String OK5 = "K5o";
    protected final String OK6 = "K6o";
    protected final String OK7 = "K7o";
    protected final String OK8 = "K8o";
    protected final String OK9 = "K9o";
    protected final String OKT = "KTo";
    protected final String OKJ = "KJo";
    protected final String OKQ = "KQo";
    protected final String OQ2 = "Q2o";
    protected final String OQ3 = "Q3o";
    protected final String OQ4 = "Q4o";
    protected final String OQ5 = "Q5o";
    protected final String OQ6 = "Q6o";
    protected final String OQ7 = "Q7o";
    protected final String OQ8 = "Q8o";
    protected final String OQ9 = "Q9o";
    protected final String OQT = "QTo";
    protected final String OQJ = "QJo";
    protected final String OJ2 = "J2o";
    protected final String OJ3 = "J3o";
    protected final String OJ4 = "J4o";
    protected final String OJ5 = "J5o";
    protected final String OJ6 = "J6o";
    protected final String OJ7 = "J7o";
    protected final String OJ8 = "J8o";
    protected final String OJ9 = "J9o";
    protected final String OJT = "JTo";
    protected final String OT2 = "T2o";
    protected final String OT3 = "T3o";
    protected final String OT4 = "T4o";
    protected final String OT5 = "T5o";
    protected final String OT6 = "T6o";
    protected final String OT7 = "T7o";
    protected final String OT8 = "T8o";
    protected final String OT9 = "T9o";
    protected final String OTT = "TTo";
    protected final String O92 = "92o";
    protected final String O93 = "93o";
    protected final String O94 = "94o";
    protected final String O95 = "95o";
    protected final String O96 = "96o";
    protected final String O97 = "97o";
    protected final String O98 = "98o";
    protected final String O82 = "82o";
    protected final String O83 = "83o";
    protected final String O84 = "84o";
    protected final String O85 = "85o";
    protected final String O86 = "86o";
    protected final String O87 = "87o";
    protected final String O72 = "72o";
    protected final String O73 = "73o";
    protected final String O74 = "74o";
    protected final String O75 = "75o";
    protected final String O76 = "76o";
    protected final String O62 = "62o";
    protected final String O63 = "63o";
    protected final String O64 = "64o";
    protected final String O65 = "65o";
    protected final String O52 = "52o";
    protected final String O53 = "53o";
    protected final String O54 = "54o";
    protected final String O42 = "42o";
    protected final String O43 = "43o";
    protected final String O32 = "32o";

    protected final String SA2 = "A2s";
    protected final String SA3 = "A3s";
    protected final String SA4 = "A4s";
    protected final String SA5 = "A5s";
    protected final String SA6 = "A6s";
    protected final String SA7 = "A7s";
    protected final String SA8 = "A8s";
    protected final String SA9 = "A9s";
    protected final String SAT = "ATs";
    protected final String SAJ = "AJs";
    protected final String SAQ = "AQs";
    protected final String SAK = "AKs";
    protected final String SK2 = "K2s";
    protected final String SK3 = "K3s";
    protected final String SK4 = "K4s";
    protected final String SK5 = "K5s";
    protected final String SK6 = "K6s";
    protected final String SK7 = "K7s";
    protected final String SK8 = "K8s";
    protected final String SK9 = "K9s";
    protected final String SKT = "KTs";
    protected final String SKJ = "KJs";
    protected final String SKQ = "KQs";
    protected final String SQ2 = "Q2s";
    protected final String SQ3 = "Q3s";
    protected final String SQ4 = "Q4s";
    protected final String SQ5 = "Q5s";
    protected final String SQ6 = "Q6s";
    protected final String SQ7 = "Q7s";
    protected final String SQ8 = "Q8s";
    protected final String SQ9 = "Q9s";
    protected final String SQT = "QTs";
    protected final String SQJ = "QJs";
    protected final String SJ2 = "J2s";
    protected final String SJ3 = "J3s";
    protected final String SJ4 = "J4s";
    protected final String SJ5 = "J5s";
    protected final String SJ6 = "J6s";
    protected final String SJ7 = "J7s";
    protected final String SJ8 = "J8s";
    protected final String SJ9 = "J9s";
    protected final String SJT = "JTs";
    protected final String ST2 = "T2s";
    protected final String ST3 = "T3s";
    protected final String ST4 = "T4s";
    protected final String ST5 = "T5s";
    protected final String ST6 = "T6s";
    protected final String ST7 = "T7s";
    protected final String ST8 = "T8s";
    protected final String ST9 = "T9s";
    protected final String STT = "TTs";
    protected final String S92 = "92s";
    protected final String S93 = "93s";
    protected final String S94 = "94s";
    protected final String S95 = "95s";
    protected final String S96 = "96s";
    protected final String S97 = "97s";
    protected final String S98 = "98s";
    protected final String S82 = "82s";
    protected final String S83 = "83s";
    protected final String S84 = "84s";
    protected final String S85 = "85s";
    protected final String S86 = "86s";
    protected final String S87 = "87s";
    protected final String S72 = "72s";
    protected final String S73 = "73s";
    protected final String S74 = "74s";
    protected final String S75 = "75s";
    protected final String S76 = "76s";
    protected final String S62 = "62s";
    protected final String S63 = "63s";
    protected final String S64 = "64s";
    protected final String S65 = "65s";
    protected final String S52 = "52s";
    protected final String S53 = "53s";
    protected final String S54 = "54s";
    protected final String S42 = "42s";
    protected final String S43 = "43s";
    protected final String S32 = "32s";

    protected final String P22 = "22";
    protected final String P33 = "33";
    protected final String P44 = "44";
    protected final String P55 = "55";
    protected final String P66 = "66";
    protected final String P77 = "77";
    protected final String P88 = "88";
    protected final String P99 = "99";
    protected final String PTT = "TT";
    protected final String PJJ = "JJ";
    protected final String PQQ = "QQ";
    protected final String PKK = "KK";
    protected final String PAA = "AA";

    protected List<String> allPairsList = new ArrayList<>();
    protected Action rangeForAction;
    protected String heroPosition;
    protected String villainPosition;
    protected String holeCardsRangeComboString;
    protected List<String> raise = new ArrayList<>();
    protected List<String> raiseOrFold = new ArrayList<>();
    protected List<String> raiseOrCall = new ArrayList<>();
    protected List<String> raiseCallOrFold = new ArrayList<>();
    protected List<String> call = new ArrayList<>();
    protected List<String> callOrFold = new ArrayList<>();

    protected List<WeightedCombination> weightedCombinations = new ArrayList<>();
    protected String combinations = "";

    public AbstractRangeResolver(String heroPosition, String villainPosition, Action rangeForAction) {
        //this.holeCards = holdeCards;
        this.rangeForAction = rangeForAction;
        this.heroPosition = heroPosition;
        this.villainPosition = villainPosition;
        this.allPairsList.addAll(Arrays.asList("AA", "KK", "QQ", "JJ", "TT"));
    }

    protected abstract void whenEP();

    protected abstract void whenMP();

    protected abstract void whenCO();

    protected abstract void whenBTN();

    protected abstract void whenSB();

    protected abstract void whenBB();

    private void addWithStats() {
        if (rangeForAction == Action.RAISE) {
            add(raise);
            add(raiseOrFold, 0.5);
            add(raiseOrCall, 0.5);
            add(raiseCallOrFold, 0.33);
        } else {
            add(raiseOrCall, 0.5);
            add(raiseCallOrFold, 0.33);
            add(call);
            add(callOrFold, 0.5);
        }
    }

    private void addWithStatsString() {
        if (rangeForAction == Action.RAISE) {
            addToString(raise);
            addToString(raiseOrFold);
            addToString(raiseOrCall);
            addToString(raiseCallOrFold);
        } else {
            addToString(raiseOrCall);
            addToString(raiseCallOrFold);
            addToString(call);
            addToString(callOrFold);
        }
    }

    private void addToString(List<String> combinationsToAdd) {
        for (String combination : combinationsToAdd) {
            if (combinations.isEmpty()) {
                combinations = combinations + combination;
            } else {
                combinations = combinations + "|" + combination;
            }
        }

    }

    public List<WeightedCombination> resolve() {
        switch (heroPosition) {
            case "EP":
                whenEP();
                break;
            case "MP":
                whenMP();
                break;
            case "CO":
                whenCO();
                break;
            case "BTN":
                whenBTN();
                break;
            case "SB":
                whenSB();
                break;
            case "BB":
                whenBB();
        }

        addWithStats();

        return weightedCombinations;
    }

    public String resolveString() {
        switch (heroPosition) {
            case "EP":
                whenEP();
                break;
            case "MP":
                whenMP();
                break;
            case "CO":
                whenCO();
                break;
            case "BTN":
                whenBTN();
                break;
            case "SB":
                whenSB();
                break;
            case "BB":
                whenBB();
        }

        addWithStatsString();

        return combinations;
    }

    protected void add(String combination, double weigth) {
        weightedCombinations.add(new WeightedCombination(combination, weigth));
    }

    protected void add(List<String> combinations, double weigth) {
        for (String combination : combinations) {
            weightedCombinations.add(new WeightedCombination(combination, weigth));
        }
    }

    protected void add(String combination) {
        weightedCombinations.add(new WeightedCombination(combination));
    }

    protected void add(List<String> combinations) {
        for (String combination : combinations) {
            weightedCombinations.add(new WeightedCombination(combination));
        }
    }

    protected void addRaise(String... cards) {
        raise.addAll(Arrays.asList(cards));
    }

    protected void addRaiseOrFold(String... cards) {
        raiseOrFold.addAll(Arrays.asList(cards));
    }

    protected void addRaiseOrCall(String... cards) {
        raiseOrCall.addAll(Arrays.asList(cards));
    }

    protected void addCall(String... cards) {
        call.addAll(Arrays.asList(cards));
    }

    protected void addRaiseCallOrFold(String... cards) {
        raiseCallOrFold.addAll(Arrays.asList(cards));
    }

    protected void addCallOrFold(String... cards) {
        callOrFold.addAll(Arrays.asList(cards));
    }

    protected void addRaise(List<String> cards) {
        raise.addAll(cards);
    }

    protected void addRaiseOrFold(List<String> cards) {
        raiseOrFold.addAll(cards);
    }

    protected void addRaiseOrCall(List<String> cards) {
        raiseOrCall.addAll(cards);
    }

    protected void addCall(List<String> cards) {
        call.addAll(cards);
    }

    protected void addRaiseCallOrFold(List<String> cards) {
        raiseCallOrFold.addAll(cards);
    }

    protected void addCallOrFold(List<String> cards) {
        callOrFold.addAll(cards);
    }

    /*
     * K9o+ -> KQo,KJo,KTo,K9o
     * K9s+ -> KQs,KJs,KTs,K9s
     * TT+ -> AA,KK,QQ,JJ,TT
     */
    public List<String> rest(String s) {
        List<String> returnList = new ArrayList<>();
        String card1Rank = s.substring(0, 1);
        String card2Rank = s.substring(1, 2);

        Rank r1 = Rank.fromChar(s.charAt(0));
        Rank r2 = Rank.fromChar(s.charAt(1));

        if (card1Rank.equals(card2Rank)) {
            returnList.add(card1Rank + card2Rank);
            for (int i = 1; i <= 14; i++) {
                int nextOrdinal = r1.ordinal() + i;
                if (nextOrdinal > 12) {
                    break;
                }

                Rank nextRank = Rank.values()[nextOrdinal];
                String next = "" + nextRank.toChar() + nextRank.toChar();
                returnList.add(next);
            }
        } else {
            String oOrs = s.substring(2, 3);
            Rank hi = null;
            Rank low = null;
            if (r1.pipValue() > r2.pipValue()) {
                hi = r1;
                low = r2;
            } else {
                hi = r2;
                low = r1;
            }

            returnList.add(card1Rank + card2Rank + oOrs);
            for (int i = 1; i <= 14; i++) {
                int nextOrdinal = low.ordinal() + i;
                if (nextOrdinal > 12 || nextOrdinal >= hi.ordinal()) {
                    break;
                }

                Rank nextRank = Rank.values()[nextOrdinal];
                String next = "" + hi.toChar() + nextRank.toChar() + oOrs;
                returnList.add(next);
            }
        }

        return returnList;
    }
}
