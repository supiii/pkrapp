package com.masa.range.raise;

import com.masa.range.AbstractRangeResolver;
import com.masa.type.Action;
import com.masa.type.Ranges;

/**
 * Resolves action for specific hand combination in a given situation
 *
 * @author Matti
 */
public class PreflopRangeResolver {

    private AbstractRangeResolver heroRangeResolver;
    private AbstractRangeResolver villainRangeResolver;

    public Ranges resolveRanges(Ranges ranges, String actions, String aggressors, String heroPosition, String villainPosition) {
        String lastAggressor = lastAggressor(aggressors);
        String lastAction = actions.substring(actions.length() - 1);

        // Preflop raising range
        if (lastAction.equals("R")) {
            switch (aggressors.length()) {
                case 1:
                    if (lastAggressor.equals("4")) {
                        heroRangeResolver = new PreflopRangeOpenResolver(heroPosition, Action.RAISE);
                        //ranges.heroRangeString = heroRangeResolver.resolveString();
                        ranges.heroRange = heroRangeResolver.resolve();

                    } else {
                        villainRangeResolver = new PreflopRangeOpenResolver(villainPosition, Action.RAISE);
                        //ranges.villainRangeString = villainRangeResolver.resolveString();
                        ranges.villainRange = villainRangeResolver.resolve();
                    }
                    break;
                case 2:
                    if (lastAggressor.equals("4")) {
                        heroRangeResolver = new PreflopRangeVsOpenResolver(heroPosition, villainPosition, Action.RAISE);
                        //ranges.heroRangeString = heroRangeResolver.resolveString();
                        ranges.heroRange = heroRangeResolver.resolve();
                    } else {
                        villainRangeResolver = new PreflopRangeVsOpenResolver(villainPosition, heroPosition, Action.RAISE);
                        //ranges.villainRangeString = villainRangeResolver.resolveString();
                        ranges.villainRange = villainRangeResolver.resolve();
                    }
                    break;
                case 3:
                    if (lastAggressor.equals("4")) {
                        heroRangeResolver = new PreflopRangeVs3BetResolver(heroPosition, villainPosition, Action.RAISE);
                        //ranges.heroRangeString = heroRangeResolver.resolveString();
                        ranges.heroRange = heroRangeResolver.resolve();
                    } else {
                        villainRangeResolver = new PreflopRangeVs3BetResolver(villainPosition, heroPosition, Action.RAISE);
                        //ranges.villainRangeString = villainRangeResolver.resolveString();
                        ranges.villainRange = villainRangeResolver.resolve();
                    }
                    break;
                default:
                    break;
            }
        } else {
            switch (aggressors.length()) {
                case 1:
                    if (!lastAggressor.equals("4")) {
                        heroRangeResolver = new PreflopRangeVsOpenResolver(heroPosition, villainPosition, Action.CALL);
                        //ranges.heroRangeString = heroRangeResolver.resolveString();
                        ranges.heroRange = heroRangeResolver.resolve();
                    } else {
                        villainRangeResolver = new PreflopRangeVsOpenResolver(villainPosition, heroPosition, Action.CALL);
                        //ranges.villainRangeString = villainRangeResolver.resolveString();
                        ranges.villainRange = villainRangeResolver.resolve();
                    }
                    break;
                case 2:
                    if (!lastAggressor.equals("4")) {
                        heroRangeResolver = new PreflopRangeVs3BetResolver(heroPosition, villainPosition, Action.CALL);
                        //ranges.heroRangeString = heroRangeResolver.resolveString();
                        ranges.heroRange = heroRangeResolver.resolve();
                    } else {
                        villainRangeResolver = new PreflopRangeVs3BetResolver(villainPosition, heroPosition, Action.CALL);
                        //ranges.villainRangeString = villainRangeResolver.resolveString();
                        ranges.villainRange = villainRangeResolver.resolve();
                    }
                    break;
                default:
                    break;
            }
        }

        return ranges;
    }

    private String lastAggressor(String aggressors) {
        return aggressors.substring(aggressors.length() - 1);
    }

    private boolean isOpen(String actions) {
        return actions.length() == 1;
    }

    private boolean isVsOpen(String actions) {
        return actions.length() == 2;
    }

    private boolean isVs3Bet(String actions) {
        return actions.length() == 3;
    }

    private boolean isVs4Bet(String actions) {
        return actions.length() == 4;
    }
}
