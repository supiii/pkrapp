package com.masa.range.raise;

import com.masa.range.AbstractRangeResolver;
import com.masa.type.Action;
import java.util.Arrays;

/**
 *
 * @author Matti
 */
public class PreflopRangeOpenResolver extends AbstractRangeResolver {

    public PreflopRangeOpenResolver(String heroPosition, Action rangeForAction) {
        super(heroPosition, null, rangeForAction);
    }

    @Override
    protected void whenEP() {
        addRaise(rest(SA2));
        addRaise(rest(SK9));
        addRaise(rest(SQ9));
        addRaise(rest(SJ9));
        addRaise(rest(OAT));
        addRaise(rest(OKJ));
        addRaise(rest(P77));

        addRaiseOrFold(Arrays.asList(SK8, S98, S87, S76, P66, S65, P55));
    }

    @Override
    protected void whenMP() {
        addRaise(rest(SA2));
        addRaise(rest(SK9));
        addRaise(rest(SQ9));
        addRaise(rest(SJ9));
        addRaise(rest(ST9));
        addRaise(rest(OAT));
        addRaise(rest(OKJ));
        addRaise(rest(P55));
        addRaise(Arrays.asList(S98, S87, S76, S65, S54));
        addRaiseOrFold(Arrays.asList(SK7, SK8, ST8, OQJ, P44, P33));
    }

    @Override
    protected void whenCO() {
        addRaise(rest(SA2));
        addRaise(rest(SK6));
        addRaise(rest(SQ8));
        addRaise(rest(SJ9));
        addRaise(rest(OA9));
        addRaise(rest(OKJ));
        addRaise(rest(P22));
        addRaise(Arrays.asList(ST9, S98, S87, S76, S65, S54, OQJ));
        addRaiseOrFold(Arrays.asList(SK3, SK4, SK5, SQ6, SQ7, SJ7, SJ8, ST7, ST8, S97, S86, OJT, OQT, OKT, OA8));
    }

    @Override
    protected void whenBTN() {
        addRaise(rest(SA2));
        addRaise(rest(SK2));
        addRaise(rest(SQ4));
        addRaise(rest(SJ6));
        addRaise(rest(ST7));
        addRaise(rest(S97));
        addRaise(rest(OA5));
        addRaise(rest(OK8));
        addRaise(rest(OQ9));
        addRaise(rest(OJ9));
        addRaise(rest(OT9));
        addRaise(rest(P22));
        addRaise(Arrays.asList(S87, S76, S65, S54));

        addRaiseOrFold(Arrays.asList(SQ2, SQ3, SJ4, SJ5, ST6, S96, S86, O98, OT8, OJ8, S75, OA4, OA3, OA2));
    }

    @Override
    protected void whenSB() {
        addRaise(rest(SA2));
        addRaise(rest(SK2));
        addRaise(rest(SQ4));
        addRaise(rest(SJ6));
        addRaise(rest(ST7));
        addRaise(rest(S97));
        addRaise(rest(OA5));
        addRaise(rest(OK8));
        addRaise(rest(OQ9));
        addRaise(rest(OJ9));
        addRaise(Arrays.asList(S87, S76, S65, S54, OT9));

        addRaiseOrFold(Arrays.asList(SQ2, SQ3, SJ4, SJ5, ST6, S96,
                S86, S75, O98, OT8, OJ8, OQ8, OK7, OA2, OA3, OA4));
    }

    @Override
    protected void whenBB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
