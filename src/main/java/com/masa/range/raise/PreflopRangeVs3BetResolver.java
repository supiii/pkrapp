package com.masa.range.raise;

import com.masa.range.AbstractRangeResolver;
import com.masa.type.Action;

public class PreflopRangeVs3BetResolver extends AbstractRangeResolver {

    public PreflopRangeVs3BetResolver(String heroPosition, String villainPosition, Action rangeForAction) {
        super(heroPosition, villainPosition, rangeForAction);
    }

    @Override
    protected void whenEP() {
        switch (villainPosition) {
            case "MP":
                whenEPvsMP();
            case "CO":
                whenEPvsCO();
            case "BTN":
                whenEPvsBTN();
            case "SB":
                whenEPvsSB();
            case "BB":
                whenEPvsBB();
        }
    }

    @Override
    protected void whenMP() {
        switch (villainPosition) {
            case "CO":
                whenMPvsCO();
            case "BTN":
                whenMPvsBTN();
            case "SB":
                whenMPvsSB();
            case "BB":
                whenMPvsBB();
        }
    }

    @Override
    protected void whenCO() {
        switch (villainPosition) {
            case "BTN":
                whenCOvsBTN();
            case "SB":
                whenCOvsSB();
            case "BB":
                whenCOvsBB();
        }
    }

    @Override
    protected void whenBTN() {
        switch (villainPosition) {
            case "SB":
                whenBTNvsSB();
            case "BB":
                whenBTNvsBB();
        }
    }

    @Override
    protected void whenSB() {
        switch (villainPosition) {
            case "BB":
                whenSBvsBB();
        }
    }

    protected void whenSBvsBB() {
        addRaise(PAA, PKK, PQQ, PJJ, PTT, SA2, OAK, OAJ, OAT);
        addRaiseOrCall(SA3, SA4, OKQ, OAQ, OKJ);
        addCall(P99, P88, P77, P66, P55, SA5, SA6, SA7, SA8, SA9, SAT, SAJ,
                SK8, SK9, SKT, SKJ, SKQ, SQ9, SQT, SQJ, SJ9, SJT, ST8, ST9);
        addCallOrFold(P44, P33, P22, SK7, SQ8, SJ8, S98, S87, S76, S65, S54);
        addRaiseCallOrFold(SK6, OQJ);

        add(raise);
        add(raiseOrFold, 0.5);
        add(raiseOrCall, 0.5);
        add(raiseCallOrFold, 0.33);
    }

    protected void whenBTNvsSB() {
        addRaise(SAK, PQQ, PKK, PAA);
        addRaiseOrCall(PJJ, PTT, SA4, SA8, SA9, SK9, SJ9, S54, OAK, OKQ, OAJ);
        addCall(P99, P88, P77, P66, P55, P44, P33, P22, SA5, SAT, SAJ, SAQ,
                SKT, SKJ, SKQ, SQT, SQJ, SJT, ST9, S87, S76, S65, OAQ);
        addCallOrFold(SK6, SK7, SK8, ST8, S97, S98);
        addRaiseCallOrFold(SA3, SA7, SQ9);
        addRaiseOrFold(SA6, SK5, OKJ, OAT);
    }

    protected void whenBTNvsBB() {
        addRaise(PAA, PKK, PQQ);
        addRaiseOrCall(PJJ, SA4, SA7, SA8, ST8, OKQ, OAJ);
        addCall(PTT, P99, P88, P77, P66, SA5, SA9, SAT, SAJ, SAQ, SK9, SKT, SKJ, SKQ,
                SQT, SQJ, SJ9, SJT, ST9, S98, S87, OAQ);
        addCallOrFold(P55, P44, P33, P22, SK6, SK7, SK8, S76, S65, S54);
        addRaiseCallOrFold(SA3, SA6, SQ9, OKJ);
        addRaiseOrFold(SA2, SK5, OAT);
    }

    protected void whenCOvsBTN() {
        addRaise(PAA, PKK, PQQ, PJJ, SAK, OAK);
        addRaiseOrCall(PTT, SAT, SAJ, SKT, SKJ, SQJ, OAQ);
        addRaiseOrFold(OAJ);
        addRaiseCallOrFold(SA4, SA5, SK9, SJT, OKQ);
        addCall(P99, P88, SAQ, SKQ, S87, S76, S65, S54);
        addCallOrFold(P77, P66, P55, P44, P33, P22, SA9, SQT, ST9, S98);
    }

    protected void whenCOvsSB() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(PQQ, PJJ, SA5, SKT, OAK, OAQ);
        addRaiseOrFold(SA8, OAJ);
        addRaiseCallOrFold(SA3, SA4, SA9, SK9, OKQ);
        addCall(PTT, P99, P88, SAT, SAJ, SAQ, SKJ, SKQ, SQJ, SJT, S87, S76);
        addCallOrFold(P77, P66, P55, P44, P33, P22, SQT, ST9, S98, S65, S54);
    }

    protected void whenCOvsBB() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(PQQ, SA5, SA9, OAK, OAQ);
        addRaiseOrFold(OAJ);
        addRaiseCallOrFold(SA3, SA4, SA8, SK9, OKQ);
        addCall(PJJ, PTT, P99, SAT, SAJ, SAQ, SKT, SKJ, SKQ, SQT, SQJ, SJT);
        addCallOrFold(P88, P77, P66, P55, P44, P33, P22, ST9, S87,
                S76, S65, S54);
    }

    protected void whenMPvsCO() {
        addRaise(PAA, PKK, PQQ, SAK, SKJ, OAK);
        addRaiseOrCall(PJJ, PTT, SAJ, SKQ, OAQ);
        addRaiseOrFold(SKT);
        addRaiseCallOrFold(SA5, SAT);
        addCall(SAQ);
        addCallOrFold(P99, P88, P77, P66, P55, P44, P33, SQJ, SJT, ST9, S98, S87, S76, S65, S54);
    }

    protected void whenMPvsBTN() {
        addRaise(PAA, PKK, PQQ, SAK, SKT, SKJ, OAK);
        addRaiseOrCall(PJJ, PTT, SAJ, SKQ, OAQ);
        addRaiseOrFold(SA4);
        addRaiseCallOrFold(SA5, SAT);
        addCall(SAQ);
        addCallOrFold(P99, P88, P77, P66, P55, P44, P33, SQJ, SJT, ST9, S98, S87, S76, S65, S54);
    }

    protected void whenMPvsSB() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(PQQ, PJJ, SAJ, SKT, SKJ, SKQ, OAK, OAQ);
        addRaiseOrFold(OAJ);
        addRaiseCallOrFold(SA4, SA5);
        addCall(PTT, SAQ, P99);
        addCallOrFold(P88, P77, P66, P55, P44, P33, SAT, SQT, SQJ, SJT, ST9, S98, S87, S76, S65, S54);
    }

    protected void whenMPvsBB() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(SA5, SAT, SKT, SKJ, OAK);
        addRaiseOrFold(SA9, SK9, OAQ);
        addRaiseCallOrFold(SA4);
        addCall(PQQ, PJJ, PTT, SAJ, SAQ, SKQ);
        addCallOrFold(P99, P88, P77, P66, P55, P44, P33, SQT, SQJ, SJT, ST9,
                S87, S76, S65, S54);
    }

    protected void whenEPvsMP() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(PQQ, PJJ, SAJ, SKQ, OAK);
        addRaiseOrFold(OAQ);
        addRaiseCallOrFold(SA5, SAT, SKT, SKJ);
        addCall(PTT, SAQ);
        addCallOrFold(P99, P88, P77, P66, P55, SQT, SQJ, SJT, ST9, S98,
                S87, S76, S65);
    }

    protected void whenEPvsCO() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(PQQ, PJJ, SAJ, SKQ, OAK);
        addRaiseOrFold(OAQ);
        addRaiseCallOrFold(SA5, SAT, SKT, SKJ);
        addCall(PTT, SAQ);
        addCallOrFold(P99, P88, P77, P66, P55, SQT, SQJ, SJT, ST9, S98,
                S87, S76, S65);
    }

    protected void whenEPvsBTN() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(PQQ, PJJ, SAJ, SKJ, SKQ, OAK);
        addRaiseOrFold(OAQ);
        addRaiseCallOrFold(SA5, SAT, SKT);
        addCall(PTT, SAQ);
        addCallOrFold(P99, P88, P77, P66, P55, SQT, SQJ, SJT, ST9, S98,
                S87, S76, S65);
    }

    protected void whenEPvsSB() {
        addRaise(PAA, PKK, SAK);
        addRaiseOrCall(PQQ, PJJ, SAJ, SKJ, SKQ, OAK);
        addRaiseOrFold(SA9, OAQ);
        addRaiseCallOrFold(SA5, SKT);
        addCall(PTT, SAQ);
        addCallOrFold(P99, P88, P77, P66, P55, SA4, SAT, SQT, SQJ, SJT,
                ST9, S98, S87, S76, S65);
    }

    protected void whenEPvsBB() {
        addRaise(PAA);
        addRaiseOrCall(PKK, SAT, SAJ, SAK, SKJ, OAK);
        addRaiseOrFold(SA3, SA9);
        addRaiseCallOrFold(SA4, SA5, SKT);
        addCall(PQQ, PJJ, PTT, SAQ, SKQ);
        addCallOrFold(P99, P88, P77, P66, P55, SQJ, SJT,
                ST9, S87, S76, S65);
    }

    @Override
    protected void whenBB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
