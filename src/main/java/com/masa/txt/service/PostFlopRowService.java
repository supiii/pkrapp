package com.masa.txt.service;

import com.masa.pkrapp.App;
import com.masa.pokertracker.PTUtil;
import com.masa.type.Draw;
import com.masa.type.FileCategory;
import com.masa.type.MadeHandSpecific;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import com.masa.util.DrawAnalyzer;
import com.masa.util.LineFilter;
import com.masa.util.MadeHandAnalyzer;
import com.masa.util.Util;
import com.masa.util.VillainFoldedHandsRemover;
import com.masa.util.WonLossRowsService;
import com.masa.util.WrongLinesRemover2;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class PostFlopRowService {

    private static final String FOLDER = ".\\data\\files\\4\\";
    private static final String MANIPULATED_FOLDER = ".\\data\\files_manipulated\\";
    // TODO: change to correvt folder after testing PostFlopServiceTest
    //private static final String FOLDER = ".\\files_test\\";
    private static int countAll = 0;

    private Map<FileCategory, List<PostFlopRow>> mapRows(List<PostFlopRow> rows) {
        Map<FileCategory, List<PostFlopRow>> linesMap = new HashMap<>();

        for (PostFlopRow row : rows) {
            FileCategory fileCategory = new FileCategory(row.boardTextureFlop, row.position, row.villainPosition);

            if (linesMap.containsKey(fileCategory)) {
                linesMap.get(fileCategory).add(row);
            } else {
                List<PostFlopRow> postFlopRows = new ArrayList<>();
                postFlopRows.add(row);
                linesMap.put(fileCategory, postFlopRows);
            }
        }

        int count = 0;

        for (Map.Entry<FileCategory, List<PostFlopRow>> entry : linesMap.entrySet()) {
            List<PostFlopRow> value = entry.getValue();
            count = count + value.size();
        }

        return linesMap;
    }

    public void write(List<PostFlopRow> rows, boolean postflop) {
        Map<FileCategory, List<PostFlopRow>> mapRows = mapRows(rows);

        for (Map.Entry<FileCategory, List<PostFlopRow>> entry2 : mapRows.entrySet()) {
            List<PostFlopRow> mappedByTexture = entry2.getValue();
            FileCategory key = entry2.getKey();
            String filename = null;
            //String position = mappedByTexture.get(0).position;

            //List<List<PostFlopRow>> chopIntoParts = CollectionUtil.chopIntoParts(mappedByTexture, 4);
            if (postflop) {
                /*for (List<PostFlopRow> chopIntoPart : chopIntoParts) {
                    int index = chopIntoParts.indexOf(chopIntoPart);
                    filename = FOLDER + key.toString() + "_" + index + ".txt";
                    writeIntoFile(filename, chopIntoPart);
                }*/
                //int rnd = getRandomNumber(0, 4);
                //filename = FOLDER + key.toString() + "_" + rnd + ".txt";
                filename = FOLDER + key.toString() + ".txt";
                //System.out.println("FILENAME: " + filename);
                writeIntoFile(filename, mappedByTexture);
            } else {
                filename = FOLDER + "postfloprows_preflop" + ".txt";
            }

        }
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public void writeIntoFile(String filename, List<PostFlopRow> mappedByTexture) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))) {
            for (PostFlopRow row : mappedByTexture) {
                if (row.actionsPreflop.equalsIgnoreCase("F")) {
                    continue;
                }
                if (row.playersPreflop > 2) {
                    continue;
                }
                if (row.round(row.wonBB, 2) == null) {
                    continue;
                }
                out.println(Util.createLine(row));
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    /*
    Filter with positions, actions. flopBucket, flopSuitness
     */
    public Map<String, List<PostFlopRow>> readAndFilterWithBasicInfo(PostFlopRow current, Street currentStreet) throws FileNotFoundException, IOException {
        boolean postflop = currentStreet != Street.PREFLOP;
        List<PostFlopRow> rows = new ArrayList<>();
        BufferedReader objReader = null;

        try {
            if (postflop) {
                Set<String> possibleVillainPositions = PTUtil.getPossibleVillainPositions(App.heroPos, App.villainPos);

                for (String possibleVillainPosition : possibleVillainPositions) {
                    FileCategory fileCategory = new FileCategory(current.boardTextureFlop, current.position, possibleVillainPosition);
                    String filename = MANIPULATED_FOLDER + fileCategory.toString() + ".txt";
                    System.out.println("FILENAME: " + filename);
                    objReader = new BufferedReader(new FileReader(filename));
                    rows.addAll(filter(objReader, currentStreet, current));
                    objReader = null;
                }

            } else {
                //inputStream = new FileInputStream(".\\files\\postfloprows_preflop" + ".txt");
            }

        } finally {
            if (objReader != null) {
                objReader.close();
            }
        }

        VillainFoldedHandsRemover.removeFoldedHands(rows, currentStreet);
        List<PostFlopRow> allFilteredSDWithoutVillainHand = LineFilter.filterSDHandsWithoutVillainCards(rows);
        WonLossRowsService.calculateWonLossWithCurrentHand(allFilteredSDWithoutVillainHand);

        // FOR TESTINGG
        /*List<PostFlopRow> remove = new ArrayList<>();
        for (PostFlopRow row : rows) {
            if (row.showdown && row.villainHoleCard1Id == 0 || row.villainHoleCard2Id == 0) {
                remove.add(row);
            }
        }

        rows.removeAll(remove);*/
        //System.out.println("ROWS: " + rows.size() + " REMOVED: " + remove.size());
        //List<PostFlopRow> bestLinesForShowdonwRows = LineFilter.getAllBestLineRows(rows, current, currentStreet);
        List<PostFlopRow> simpleValueBetRows = LineFilter.getSimpleBetCallRows(allFilteredSDWithoutVillainHand, currentStreet);
        List<PostFlopRow> aggressionRows = LineFilter.getAggressionRows(rows, currentStreet);
        List<PostFlopRow> aggressionCallCheckRows = LineFilter.getAggressionCallCheckRows(allFilteredSDWithoutVillainHand, currentStreet);
        List<PostFlopRow> passiveRows = LineFilter.getPassiveRows(allFilteredSDWithoutVillainHand, currentStreet);
        List<PostFlopRow> semiBluffRows = LineFilter.getSemiBluffRows(allFilteredSDWithoutVillainHand, currentStreet, current);
        List<PostFlopRow> semiBluffRowsWithFiltering = LineFilter.getSemiBluffRowsWithFiltering(allFilteredSDWithoutVillainHand, currentStreet, current);

        //List<PostFlopRow> filteredAllRows = LineFilter.filterVillainFoldHands(rows, currentStreet);
        List<PostFlopRow> allRows = WrongLinesRemover2.removeLosingLines(allFilteredSDWithoutVillainHand, current, currentStreet, Util.isHeroIP(), App.holeCards);
        Map<String, List<PostFlopRow>> rowsMap = new HashMap<>();

        /*for (PostFlopRow simpleValueBetRow : simpleValueBetRows) {
            simpleValueBetRow.printActionsAndCards();
        }*/
        //rowsMap.put("Best lines", bestLinesForShowdonwRows);
        rowsMap.put("All", allRows);
        rowsMap.put("Simple valuebet", simpleValueBetRows);
        rowsMap.put("Aggression", aggressionRows);
        rowsMap.put("Aggression - Call/Check to villain aggression", aggressionCallCheckRows);
        rowsMap.put("Passive", passiveRows);
        rowsMap.put("Semibluff", semiBluffRows);
        rowsMap.put("SemiBluffRowsWithFiltering", semiBluffRowsWithFiltering);

        return rowsMap;
    }

    private List<PostFlopRow> getSimpleValueRows(List<PostFlopRow> rows) {
        List<PostFlopRow> simpleValueRows = new ArrayList<>();

        for (PostFlopRow row : rows) {
            if (row.showdown
                    && row.getActions(Street.FLOP).equals("B")
                    && row.getActions(Street.TURN).equals("B")
                    && row.getActions(Street.RIVER).equals("B")) {
                simpleValueRows.add(row);
            }
        }

        return simpleValueRows;
    }

    private List<PostFlopRow> getBestLinesForShowdonwRows(List<PostFlopRow> rows, PostFlopRow current, Street currentStreet) {
        List<PostFlopRow> valueRows = new ArrayList<>();

        for (PostFlopRow row : rows) {
            if (row.showdown) {
                valueRows.add(row);
            }
        }

        return WrongLinesRemover2.removeLosingLines(valueRows, current, currentStreet, Util.isHeroIP(), App.holeCards);
    }

    private List<PostFlopRow> getPassiveRows(List<PostFlopRow> rows) {
        List<PostFlopRow> simpleValueRows = new ArrayList<>();

        for (PostFlopRow row : rows) {
            if (row.showdown
                    && row.getActions(Street.FLOP).equals("X")
                    && row.getActions(Street.TURN).equals("X")
                    && row.getActions(Street.RIVER).equals("X")) {
                simpleValueRows.add(row);
            }
        }

        return simpleValueRows;
    }

    private List<PostFlopRow> getBluffRows(List<PostFlopRow> rows, PostFlopRow current, Street currentStreet) {
        List<PostFlopRow> bluffRows = new ArrayList<>();

        for (PostFlopRow row : rows) {
            if (row.showdown) {
                continue;
            }
            if (row.getActions(Street.FLOP).endsWith("X")
                    || row.getActions(Street.FLOP).endsWith("C")
                    || row.getActions(Street.FLOP).equals("F")) {
                continue;
            }
            if (row.getActions(Street.TURN).endsWith("X")
                    || row.getActions(Street.TURN).endsWith("C")
                    || row.getActions(Street.TURN).equals("F")) {
                continue;
            }
            if (row.getActions(Street.RIVER).endsWith("X")
                    || row.getActions(Street.RIVER).endsWith("C")
                    || row.getActions(Street.RIVER).equals("F")) {
                continue;
            }
            bluffRows.add(row);
        }

        return WrongLinesRemover2.removeLosingLines(bluffRows, current, currentStreet, Util.isHeroIP(), App.holeCards);
    }

    private List<PostFlopRow> filter(BufferedReader objReader, Street currentStreet, PostFlopRow current) throws IOException {
        List<PostFlopRow> rows = new ArrayList<>();
        String line;
        int count = 0;

        while ((line = objReader.readLine()) != null) {
            count++;
            Set<String> possibleVillainPositions = PTUtil.getPossibleVillainPositions(App.heroPos, App.villainPos);
            String[] splitted = line.split("\\s+");
            boolean pass = LineFilter.filterBasic(splitted, currentStreet, possibleVillainPositions);

            if (!pass) {
                continue;
            }

            PostFlopRow row = new PostFlopRow();
            row.setId(count);
            Util.createPostFlopRow(line, row);

            //if (!row.getVillainActions(Street.FLOP).endsWith("F") || !row.getVillainActions(Street.FLOP).endsWith("F") || !row.getVillainActions(Street.RIVER).endsWith("F") ||)
            /*if (row.getActions(Street.FLOP).endsWith("B") || row.getActions(Street.FLOP).endsWith("B") || row.getActions(Street.RIVER).endsWith("B")) {

            } else {
                continue;
            }

            if (!row.showdown) {
                continue;
            }*/
            CardSet board = row.getBoardByStreet(currentStreet);

            /*if (currentStreet == Street.PREFLOP) {
                if (checkPreflopActions(row, current)) {
                    continue;
                }
            } else if (currentStreet == Street.FLOP) {
                if (checkFlopActions(current, row)) {
                    continue;
                }
            } else if (currentStreet == Street.TURN) {
                if (checkTurnActions(current, row)) {
                    if (board.containsRank(Rank.JACK) && board.containsRank(Rank.TEN) && board.containsRank(Rank.NINE)) {
                        if (row.actionsPreflop.startsWith("R") && row.actionsFlop.startsWith("B") && row.actionsTurn.startsWith("X")) {
                            System.out.println("DEBUG ZIKI WIKI hero PRE: " + row.actionsPreflop + " FLOP: " + row.actionsFlop + " TURN: " + row.actionsTurn);
                            System.out.println("DEBUG ZIKI WIKI villain " + row.actionsVillainPreflop + row.actionsVillainFlop + row.actionsVillainTurn);
                        }
                    }
                    continue;
                }
            } else if (currentStreet == Street.RIVER) {
                if (checkRiverActions(current, row)) {
                    continue;
                }
            }*/
            // villain stats > <
            /*if (row.villainVPIP < 23 && row.villainPFR < 20 && row.villainPFR > 14) {

            } else {
                continue;
            }*/
 /*if (row.villainVPIP > 28 && row.villainPFR < 18) {

            } else {
            continue;
            }*/
            //List<String> possibleVillainPositions = PTUtil.getPossibleVillainPositions(current.position, current.villainPosition);
            // Position
            /*if (!current.position.equalsIgnoreCase(row.position)) {
                continue;
            }

            if (current.villainPosition != null && !possibleVillainPositions.contains(row.villainPosition)) {
                continue;
            }*/
            // 2 players max
            // TODO: CHECK THIS
            /*if (!postflop && row.playersPreflop > 2) {
                System.out.println("Too many players preflop");
                continue;
            }
            if (postflop && row.playersOnFlop > 2) {
                System.out.println("Too many players flop");
                continue;
            }*/
            Set<Draw> draws = DrawAnalyzer.getDraws(board, App.holeCards, null);
            // dont need when files are categorized in files?
            if (currentStreet == Street.FLOP) {
                if (!equalSets(draws, current.drawsFlop)) {
                    continue;
                }
                if (!row.boardTextureFlop.equals(current.boardTextureFlop)) {
                    continue;
                }
                MadeHandSpecific madehandRow = MadeHandAnalyzer.getMadeHandSpecific(row.getBoardByStreet(currentStreet), App.holeCards);
                if (madehandRow == null || !madehandRow.equals(current.madeHandSpecificFlop)) {
                    continue;
                }
            } else if (currentStreet == Street.TURN) {
                if (!equalSets(draws, current.drawsTurn)) {
                    continue;
                }
                /*if (!row.boardTextureTurn.equals(current.boardTextureTurn)) {
                continue;
                }*/
                MadeHandSpecific madehandRow = MadeHandAnalyzer.getMadeHandSpecific(row.getBoardByStreet(currentStreet), App.holeCards);
                if (madehandRow == null || !madehandRow.equals(current.madeHandSpecificTurn)) {
                    //System.out.println("Could not determine madehand on turn. " + madehandRow + " " + board.toStringWithoutCommas());
                    continue;
                }
                //System.out.println("Mmadehand on turn. " + madehandRow + " " + turn.toStringWithoutCommas());
            } else if (currentStreet == Street.RIVER) {
                if (!row.boardTextureRiver.equals(current.boardTextureRiver)) {
                    System.out.println("BOARD TEXTURE RIVER DOES NOT MATCH!!!!!");
                    System.out.println(row.boardTextureRiver + " " + current.boardTextureRiver);
                    row.printBoard();
                    continue;
                }
                MadeHandSpecific madehandRow = MadeHandAnalyzer.getMadeHandSpecific(row.getBoardByStreet(currentStreet), App.holeCards);
                if (madehandRow == null || !madehandRow.equals(current.madeHandSpecificRiver)) {
                    System.out.println("MADEHAND ON RIVER DOES NOT MATCH!!!!!!!!!!!!!!!!!");
                    System.out.println(madehandRow + " " + current.madeHandSpecificRiver);
                    row.printBoard();
                    continue;
                }
            }

            rows.add(row);
        }
        System.out.println("COUNT ALL FROM TXT: " + count + " rows after basic filtering " + rows.size());

        return rows;
    }

    public int readAndCountPostFlopRows() throws FileNotFoundException, IOException {
        int count = 0;

        File folder = new File(FOLDER);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            String fileName = FOLDER + file.getName();
            count += rfsss(fileName);
        }

        return count;
    }

    private int rfsss(String filename) throws IOException {
        FileInputStream inputStream = null;
        Scanner sc = null;
        int count = 0;
        try {
            inputStream = new FileInputStream(filename);
            sc = new Scanner(inputStream, "UTF-8");

            while (sc.hasNextLine()) {
                count++;
                sc.nextLine();
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }
        return count;
    }

    /*private boolean checkPreflopActions(PostFlopRow row, PostFlopRow current) {
        if (!row.actionsPreflop.startsWith(current.actionsPreflop)) {
            return true;
        }
        if (current.villainPosition != null && !row.actionsVillainPreflop.startsWith(current.actionsPreflop)) {
            return true;
        }
        return false;
    }

    private boolean checkRiverActions(PostFlopRow current, PostFlopRow row) {
        if (!current.actionsPreflop.equals(row.actionsPreflop)) {
            return true;
        }
        if (!current.actionsVillainPreflop.equals(row.actionsVillainPreflop)) {
            return true;
        }
        if (!row.actionsFlop.equals(current.actionsFlop)) {
            return true;
        }
        if (!row.actionsVillainFlop.equals(current.actionsVillainFlop)) {
            return true;
        }
        if (!row.actionsTurn.equals(current.actionsTurn)) {
            return true;
        }
        if (!row.actionsVillainTurn.equals(current.actionsVillainTurn)) {
            return true;
        }
        if (!row.actionsRiver.startsWith(current.actionsRiver)) {
            return true;
        }
        if (!row.actionsVillainRiver.startsWith(current.actionsVillainRiver)) {
            return true;
        }
        return false;
    }

    private boolean checkTurnActions(PostFlopRow current, PostFlopRow row) {
        if (!current.actionsPreflop.equals(row.actionsPreflop)) {
            return true;
        }
        if (!current.actionsVillainPreflop.equals(row.actionsVillainPreflop)) {
            return true;
        }
        if (!row.actionsFlop.equals(current.actionsFlop)) {
            return true;
        }
        if (!row.actionsVillainFlop.equals(current.actionsVillainFlop)) {
            return true;
        }
        if (!row.actionsTurn.startsWith(current.actionsTurn)) {
            return true;
        }
        if (!row.actionsVillainTurn.startsWith(current.actionsVillainTurn)) {
            return true;
        }
        return false;
    }

    private boolean checkFlopActions(PostFlopRow current, PostFlopRow row) {
        if (!current.actionsPreflop.equals(row.actionsPreflop)) {
            return true;
        }
        if (!current.actionsVillainPreflop.equals(row.actionsVillainPreflop)) {
            return true;
        }
        if (!row.actionsFlop.startsWith(current.actionsFlop)) {
            return true;
        }
        if (!row.actionsVillainFlop.startsWith(current.actionsVillainFlop)) {
            return true;
        }
        return false;
    }*/
    private static boolean equalSets(Set<Draw> one, Set<Draw> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        return one.equals(two);
    }
}
