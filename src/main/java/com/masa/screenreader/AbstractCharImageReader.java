package com.masa.screenreader;

import com.masa.type.Globals;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.Map;

/**
 *
 * @author Matti
 */
public class AbstractCharImageReader extends AbstractImageReader<BigDecimal, String> {

    @Override
    protected BigDecimal readValue(Map<String, BufferedImage> imgs) {
        StringBuilder sb = new StringBuilder();

        int decimals = 0;
        boolean dotFound = false;
        while (decimals < 2) {
            String c = readImages(imgs);

            if (c == null) {
                if (!dotFound) {
                    sb.append(Globals.DOT).append(Globals.ZERO).append(Globals.ZERO);
                }
                break;
            }

            if (dotFound) {
                decimals++;
            }

            if (c.equals(Globals.DOT)) {
                dotFound = true;
            }

            sb.append(c);
        }

        return new BigDecimal(sb.toString()).setScale(2);
    }

}
