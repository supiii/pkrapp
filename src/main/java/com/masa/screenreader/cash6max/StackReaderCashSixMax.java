package com.masa.screenreader.cash6max;

import com.masa.screenreader.*;
import com.masa.type.Globals;
import com.masa.type.TableType;

/**
 *
 * @author Matti
 */
public class StackReaderCashSixMax extends AbstractStackReader {
    // 100, 128, 95
    // 125, 152, 119

    private final int dollarLookingPointXSeat1 = 275;
    private final int dollarLookingPointYSeat1 = 98;
    private final int dollarLookingPointYSeat1Zoom = 90;

    private final int dollarLookingPointXSeat2 = 542;
    private final int dollarLookingPointYSeat2 = 150;
    private final int dollarLookingPointYSeat2Zoom = 142;

    private final int dollarLookingPointXSeat3 = 542;
    private final int dollarLookingPointYSeat3 = 286;
    private final int dollarLookingPointYSeat3Zoom = 277;

    private final int dollarLookingPointXSeat4 = 310;
    private final int dollarLookingPointYSeat4 = 362;
    private final int dollarLookingPointYSeat4Zoom = 353;

    private final int dollarLookingPointXSeat5 = 45;
    private final int dollarLookingPointYSeat5 = 286;
    private final int dollarLookingPointYSeat5Zoom = 277;

    private final int dollarLookingPointXSeat6 = 45;
    // 150 - 8 jossain vaiheessa????
    private final int dollarLookingPointYSeat6 = 150;
    private final int dollarLookingPointYSeat6Zoom = 142;

    public StackReaderCashSixMax() {

    }

    @Override
    public int getDollarLookingPointXSeat1() {
        return dollarLookingPointXSeat1;
    }

    @Override
    public int getDollarLookingPointYSeat1() {
        if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
            return dollarLookingPointYSeat1Zoom;
        }
        return dollarLookingPointYSeat1;
    }

    @Override
    public int getDollarLookingPointXSeat2() {
        return dollarLookingPointXSeat2;
    }

    @Override
    public int getDollarLookingPointYSeat2() {
        if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
            return dollarLookingPointYSeat2Zoom;
        }
        return dollarLookingPointYSeat2;
    }

    @Override
    public int getDollarLookingPointXSeat3() {
        return dollarLookingPointXSeat3;
    }

    @Override
    public int getDollarLookingPointYSeat3() {
        if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
            return dollarLookingPointYSeat3Zoom;
        }
        return dollarLookingPointYSeat3;
    }

    @Override
    public int getDollarLookingPointXSeat4() {
        return dollarLookingPointXSeat4;
    }

    @Override
    public int getDollarLookingPointYSeat4() {
        if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
            return dollarLookingPointYSeat4Zoom;
        }
        return dollarLookingPointYSeat4;
    }

    @Override
    public int getDollarLookingPointXSeat5() {
        return dollarLookingPointXSeat5;
    }

    @Override
    public int getDollarLookingPointYSeat5() {
        if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
            return dollarLookingPointYSeat5Zoom;
        }
        return dollarLookingPointYSeat5;
    }

    @Override
    protected int getDollarLookingPointXSeat6() {
        return dollarLookingPointXSeat6;
    }

    @Override
    protected int getDollarLookingPointYSeat6() {
        if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
            return dollarLookingPointYSeat6Zoom;
        }
        return dollarLookingPointYSeat6;
    }

}
