package com.masa.screenreader.foldtext;

import com.masa.screenreader.AbstractImageReader;
import com.masa.type.Globals;
import com.masa.type.Seat;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matti
 */
public class FoldTextReader extends AbstractImageReader<Boolean, Boolean> {

    protected Map<Boolean, BufferedImage> images = new HashMap<>();

    public FoldTextReader() {
        images.put(true, Globals.FOLDED_TEXT_IMAGE);
    }

    public Boolean readFolded(BufferedImage tableImage, Seat seat) {
        if (null != seat) {
            switch (seat) {
                case ONE:
                    setStartingPoints(296, 79);
                    break;
                case TWO:
                    setStartingPoints(542, 121);
                    break;
                case THREE:
                    setStartingPoints(562, 253);
                    break;
                case FOUR:
                    setStartingPoints(304, 347);
                    break;
                case FIVE:
                    setStartingPoints(60, 253);
                    break;
                case SIX:
                    setStartingPoints(79, 121);
                    break;
                default:
                    break;
            }
        }

        return readValue(tableImage, images);
    }

    @Override
    protected Boolean readValue(Map<Boolean, BufferedImage> imgs) {
        return readImages(imgs);
    }

    @Override
    protected int getStartingPointX() {
        return startingPointX;
    }
;
}
