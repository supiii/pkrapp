package com.masa.screenreader.stack;

import com.masa.screenreader.AbstractCharImageReader;
import com.masa.type.Globals;
import com.masa.type.Seat;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matti
 */
public class StackReader extends AbstractCharImageReader {

    protected Map<String, BufferedImage> heroImages = new HashMap<>();
    protected Map<String, BufferedImage> villainImages = new HashMap<>();
    protected Map<String, BufferedImage> foldedImages = new HashMap<>();

    public StackReader() {
        startingImage = Globals.STACK_DOLLAR_IMAGE;

        villainImages.put(Globals.DOT, Globals.STACK_DOT_IMAGE);
        villainImages.put(Globals.ZERO, Globals.STACK_ZERO_IMAGE);
        villainImages.put(Globals.ONE, Globals.STACK_ONE_IMAGE);
        villainImages.put(Globals.TWO, Globals.STACK_TWO_IMAGE);
        villainImages.put(Globals.THREE, Globals.STACK_THREE_IMAGE);
        villainImages.put(Globals.FOUR, Globals.STACK_FOUR_IMAGE);
        villainImages.put(Globals.FIVE, Globals.STACK_FIVE_IMAGE);
        villainImages.put(Globals.SIX, Globals.STACK_SIX_IMAGE);
        villainImages.put(Globals.SEVEN, Globals.STACK_SEVEN_IMAGE);
        villainImages.put(Globals.EIGHT, Globals.STACK_EIGHT_IMAGE);
        villainImages.put(Globals.NINE, Globals.STACK_NINE_IMAGE);

        heroImages.put(Globals.DOT, Globals.STACK_HERO_DOT_IMAGE);
        heroImages.put(Globals.ZERO, Globals.STACK_HERO_ZERO_IMAGE);
        heroImages.put(Globals.ONE, Globals.STACK_HERO_ONE_IMAGE);
        heroImages.put(Globals.TWO, Globals.STACK_HERO_TWO_IMAGE);
        heroImages.put(Globals.THREE, Globals.STACK_HERO_THREE_IMAGE);
        heroImages.put(Globals.FOUR, Globals.STACK_HERO_FOUR_IMAGE);
        heroImages.put(Globals.FIVE, Globals.STACK_HERO_FIVE_IMAGE);
        heroImages.put(Globals.SIX, Globals.STACK_HERO_SIX_IMAGE);
        heroImages.put(Globals.SEVEN, Globals.STACK_HERO_SEVEN_IMAGE);
        heroImages.put(Globals.EIGHT, Globals.STACK_HERO_EIGHT_IMAGE);
        heroImages.put(Globals.NINE, Globals.STACK_HERO_NINE_IMAGE);

        foldedImages.put(Globals.DOT, Globals.STACK_FOLDED_DOT_IMAGE);
        foldedImages.put(Globals.ZERO, Globals.STACK_FOLDED_ZERO_IMAGE);
        foldedImages.put(Globals.ONE, Globals.STACK_FOLDED_ONE_IMAGE);
        foldedImages.put(Globals.TWO, Globals.STACK_FOLDED_TWO_IMAGE);
        foldedImages.put(Globals.THREE, Globals.STACK_FOLDED_THREE_IMAGE);
        foldedImages.put(Globals.FOUR, Globals.STACK_FOLDED_FOUR_IMAGE);
        foldedImages.put(Globals.FIVE, Globals.STACK_FOLDED_FIVE_IMAGE);
        foldedImages.put(Globals.SIX, Globals.STACK_FOLDED_SIX_IMAGE);
        foldedImages.put(Globals.SEVEN, Globals.STACK_FOLDED_SEVEN_IMAGE);
        foldedImages.put(Globals.EIGHT, Globals.STACK_FOLDED_EIGHT_IMAGE);
        foldedImages.put(Globals.NINE, Globals.STACK_FOLDED_NINE_IMAGE);
    }

    public BigDecimal readStack(BufferedImage tableImage, Seat seat) {
        return readValue(tableImage, seat, false);
    }

    public BigDecimal readFoldedStack(BufferedImage tableImage, Seat seat) {
        return readValue(tableImage, seat, true);
    }

    private BigDecimal readValue(BufferedImage tableImage, Seat seat, boolean folded) {
        if (null != seat) {
            switch (seat) {
                case ONE:
                    setStartingPoints(274, 94);
                    break;
                case TWO:
                    setStartingPoints(520, 135);
                    break;
                case THREE:
                    setStartingPoints(537, 268);
                    break;
                case FOUR:
                    setStartingPoints(304, 347);
                    break;
                case FIVE:
                    setStartingPoints(38, 268);
                    break;
                case SIX:
                    setStartingPoints(58, 135);
                    break;
                default:
                    break;
            }
        }

        if (folded) {
            startingImage = Globals.STACK_FOLDED_DOLLAR_IMAGE;
            return readValue(tableImage, foldedImages);
        }
        if (seat == Seat.FOUR) {
            startingImage = Globals.STACK_HERO_DOLLAR_IMAGE;
            readValue(tableImage, heroImages);
            return readValue(tableImage, heroImages);
        }
        startingImage = Globals.STACK_DOLLAR_IMAGE;
        return readValue(tableImage, villainImages);
    }

    public Map<String, BufferedImage> getHeroImages() {
        return heroImages;
    }

    public Map<String, BufferedImage> getVillainImages() {
        return villainImages;
    }

    public Map<String, BufferedImage> getFoldedImages() {
        return foldedImages;
    }

}
