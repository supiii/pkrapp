package com.masa.screenreader;

/**
 *
 * @author Matti
 */
public class Rgb {

    public int red;
    public int green;
    public int blue;

    public Rgb(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.red;
        hash = 67 * hash + this.green;
        hash = 67 * hash + this.blue;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rgb other = (Rgb) obj;
        if (this.red != other.red) {
            return false;
        }
        if (this.green != other.green) {
            return false;
        }
        if (this.blue != other.blue) {
            return false;
        }
        return true;
    }
}
