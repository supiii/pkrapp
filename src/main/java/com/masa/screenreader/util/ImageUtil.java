package com.masa.screenreader.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Matti
 */
public class ImageUtil {

    public static BufferedImage getImageResource(String filename) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(filename));
        } catch (IOException e) {
        }
        if (img == null) {
            System.out.println("filename " + filename + " is NULL");
        }
        return img;

//        System.out.println("******* " + filename);
//        BufferedImage bufferedImage = null;
//        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
//        InputStream is = classloader.getResourceAsStream(filename);
//        
//        try {
//            bufferedImage = ImageIO.read(is);
//        } catch (IOException ex) {
//            Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        return bufferedImage;
    }

}
