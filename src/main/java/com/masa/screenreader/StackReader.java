package com.masa.screenreader;


/**
 *
 * @author Matti
 */
public class StackReader extends AbstractStackReader {
       // 100, 128, 95
    // 125, 152, 119
    private final int dollarLookingPointXSeat1 = 275;
    private final int dollarLookingPointYSeat1 = 98;
    
    private final int dollarLookingPointXSeat2 = 542;
    private final int dollarLookingPointYSeat2 = 150;
    
    private final int dollarLookingPointXSeat3 = 542;
    private final int dollarLookingPointYSeat3 = 286;
    
    private final int dollarLookingPointXSeat4 = 310;
    private final int dollarLookingPointYSeat4 = 362;
    
    private final int dollarLookingPointXSeat5 = 45;
    private final int dollarLookingPointYSeat5 = 286;
    
    private final int dollarLookingPointXSeat6 = 45;
    private final int dollarLookingPointYSeat6 = 150;

    public StackReader() {

    }

    @Override
    public int getDollarLookingPointXSeat1() {
        return dollarLookingPointXSeat1;
    }

    @Override
    public int getDollarLookingPointYSeat1() {
        return dollarLookingPointYSeat1;
    }

    @Override
    public int getDollarLookingPointXSeat2() {
        return dollarLookingPointXSeat2;
    }

    @Override
    public int getDollarLookingPointYSeat2() {
        return dollarLookingPointYSeat2;
    }

    @Override
    public int getDollarLookingPointXSeat3() {
        return dollarLookingPointXSeat3;
    }

    @Override
    public int getDollarLookingPointYSeat3() {
        return dollarLookingPointYSeat3;
    }

    @Override
    public int getDollarLookingPointXSeat4() {
        return dollarLookingPointXSeat4;
    }

    @Override
    public int getDollarLookingPointYSeat4() {
        return dollarLookingPointYSeat4;
    }

    @Override
    public int getDollarLookingPointXSeat5() {
        return dollarLookingPointXSeat5;
    }

    @Override
    public int getDollarLookingPointYSeat5() {
        return dollarLookingPointYSeat5;
    }
    
    @Override
    protected int getDollarLookingPointXSeat6() {
        return dollarLookingPointXSeat6;
    }

    @Override
    protected int getDollarLookingPointYSeat6() {
        return dollarLookingPointYSeat6;
    }

    

}
