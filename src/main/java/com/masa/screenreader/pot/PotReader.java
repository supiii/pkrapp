package com.masa.screenreader.pot;

import com.masa.screenreader.AbstractCharImageReader;
import com.masa.type.Globals;
import com.masa.type.TableType;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matti
 */
public class PotReader extends AbstractCharImageReader {

    protected Map<String, BufferedImage> images = new HashMap<>();

    public PotReader() {
        startingImage = Globals.POT_DOLLAR_IMAGE;

        images.put(Globals.DOT, Globals.POT_DOT_IMAGE);
        images.put(Globals.ZERO, Globals.POT_ZERO_IMAGE);
        images.put(Globals.ONE, Globals.POT_ONE_IMAGE);
        images.put(Globals.TWO, Globals.POT_TWO_IMAGE);
        images.put(Globals.THREE, Globals.POT_THREE_IMAGE);
        images.put(Globals.FOUR, Globals.POT_FOUR_IMAGE);
        images.put(Globals.FIVE, Globals.POT_FIVE_IMAGE);
        images.put(Globals.SIX, Globals.POT_SIX_IMAGE);
        images.put(Globals.SEVEN, Globals.POT_SEVEN_IMAGE);
        images.put(Globals.EIGHT, Globals.POT_EIGHT_IMAGE);
        images.put(Globals.NINE, Globals.POT_NINE_IMAGE);

        if (Globals.TABLE_TYPE == TableType.CASH_6_MAX) {
            startingPointY = 157;
        } // ZOOM
        else {
            startingPointY = 157;
        }
        startingPointX = 310;
    }

    public BigDecimal readValue(BufferedImage tableImage) {
        return readValue(tableImage, images);
    }

    public Map<String, BufferedImage> getImages() {
        return images;
    }
}
