package com.masa.screenreader.pot;

import com.masa.screenreader.Pixel;
import com.masa.screenreader.PixelModel;
import java.util.List;

/**
 *
 * @author Matti
 */
public class PotCharacterPixelModel implements PixelModel<String> {
    private final String value;
    private final List<Pixel> pixels;

    public PotCharacterPixelModel(String value, List<Pixel> pixels) {
        this.value = value;
        this.pixels = pixels;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public List<Pixel> getPixels() {
        return pixels;
    }

}
