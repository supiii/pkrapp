package com.masa.screenreader.pot;

import com.masa.screenreader.Pixel;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Matti
 */
public class PotCharacterPixelModels {
    public List<PotCharacterPixelModel> pixelModels = new ArrayList<>();
    int lowRed = 1;
    int lowGreen = 50;
    int lowBlue = 15;
    
    int hiRed = 25;
    int hiGreen = 80;
    int hiBlue = 35;
    
    public Color lowTableColor = null;
    public Color hiTableColor = null;
    
    public PotCharacterPixelModels() {
        lowTableColor = new Color(lowRed, lowGreen, lowBlue);
        hiTableColor = new Color(hiRed, hiGreen, hiBlue);
//        createModel(".", Arrays.asList(
//                pixel(0, 0, true),
//                pixel(1, 0, false),
//                pixel(2, 0, false),
//                pixel(3, 0, false),
//                pixel(4, 0, false),
//                pixel(5, 0, true)));
        createModel("0", Arrays.asList(
                pixel(0, 0, true),
                pixel(1, 0, false),
                pixel(2, 0, false),
                pixel(3, 0, false),
                pixel(4, 0, false),
                pixel(5, 0, true)));
    }
    
    private Pixel pixel(int x, int y) {
        return new Pixel(x, y, lowTableColor, hiTableColor);
    }
    
    private Pixel pixel(int x, int y, boolean matches) {
        return new Pixel(x, y, lowTableColor, hiTableColor, matches);
    }
    
    private void createModel(String value, List<Pixel> pixels) {
        pixelModels.add(new PotCharacterPixelModel(value, pixels));
    }
    
}
