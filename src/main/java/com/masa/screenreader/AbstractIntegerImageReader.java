package com.masa.screenreader;

import com.masa.type.Globals;
import java.awt.image.BufferedImage;
import java.util.Map;

/**
 *
 * @author Matti
 */
public class AbstractIntegerImageReader extends AbstractImageReader<Integer, String> {

    @Override
    protected Integer readValue(Map<String, BufferedImage> imgs) {
        StringBuilder sb = new StringBuilder();

        int decimals = 0;
        boolean dotFound = false;
        while (decimals < 2) {
            String c = readImages(imgs);

            if (c == null) {
                if (!dotFound) {
                    sb.append(Globals.DOT).append(Globals.ZERO).append(Globals.ZERO);
                }
                break;
            }

            if (dotFound) {
                decimals++;
            }

            if (c.equals(Globals.DOT)) {
                dotFound = true;
            }

            sb.append(c);
        }

        String integerString = readImages(imgs) + readImages(imgs);

        return Integer.parseInt(integerString);
    }

}
