package com.masa.screenreader;

import java.util.List;

/**
 *
 * @author Matti
 * @param <T>
 */
public interface PixelModel<T> {
    T getValue();
    List<Pixel> getPixels();
}




