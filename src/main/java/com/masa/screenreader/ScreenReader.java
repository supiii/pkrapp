package com.masa.screenreader;

import com.masa.type.Globals;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matti
 */
public class ScreenReader {

    private Robot robot;
    private Rectangle fullScreenRectangle;
    private BufferedImage fullScreenImage;

    public ScreenReader() {

    }

    /**
     * Scans full screen
     *
     * @return
     */
    public void readFullScreenImage() {
        this.fullScreenImage = getRobot().createScreenCapture(getFullscreenRectangle());
    }

    /**
     * x,y is top-left corner of the table-window
     *
     * @return
     */
    public BufferedImage readTableImage(int tableNro) {

        if (tableNro == 1) {
            // C:\Users\Matti\Documents\NetBeansProjects\MasaMaven\src\test\resources\tablereadertest\10sAs
            return fullScreenImage.getSubimage(Globals.TABLE_1_POSITION_X, Globals.TABLE_1_POSITION_Y, Globals.TABLE_WIDTH, Globals.TABLE_HEIGHT);
        } else if (tableNro == 2) {
            return fullScreenImage.getSubimage(Globals.TABLE_2_POSITION_X, Globals.TABLE_2_POSITION_Y, Globals.TABLE_WIDTH, Globals.TABLE_HEIGHT);
        } else if (tableNro == 3) {
            return fullScreenImage.getSubimage(Globals.TABLE_3_POSITION_X, Globals.TABLE_3_POSITION_Y, Globals.TABLE_WIDTH, Globals.TABLE_HEIGHT);
        } else if (tableNro == 4) {
            return fullScreenImage.getSubimage(Globals.TABLE_4_POSITION_X, Globals.TABLE_4_POSITION_Y, Globals.TABLE_WIDTH, Globals.TABLE_HEIGHT);
        } else if (tableNro == 5) {
            return fullScreenImage.getSubimage(Globals.TABLE_5_POSITION_X, Globals.TABLE_5_POSITION_Y, Globals.TABLE_WIDTH, Globals.TABLE_HEIGHT);
        } else {
            return fullScreenImage.getSubimage(Globals.TABLE_6_POSITION_X, Globals.TABLE_6_POSITION_Y, Globals.TABLE_WIDTH, Globals.TABLE_HEIGHT);
        }
    }

    public boolean tablePlacementOk(int tableNro) {
        int addX = 0;
        int addY = 0;
        if (tableNro == 2) {
            addX = Globals.TABLE_2_POSITION_X;
        }
        if (tableNro == 3) {
            addX = Globals.TABLE_3_POSITION_X;
            addY = Globals.TABLE_3_POSITION_Y;
        }
        if (tableNro == 4) {
            addX = Globals.TABLE_4_POSITION_X;
            addY = Globals.TABLE_4_POSITION_Y;
        }
        if (tableNro == 5) {
            addX = Globals.TABLE_5_POSITION_X;
            addY = Globals.TABLE_5_POSITION_Y;
        }
        if (tableNro == 6) {
            addX = Globals.TABLE_6_POSITION_X;
            addY = Globals.TABLE_6_POSITION_Y;
        }

        if (!ColorUtil.similar(new Color(fullScreenImage.getRGB(addX + 605, addY + 6)), new Color(255, 255, 255))) {
            return false;
        }
        if (!ColorUtil.similar(new Color(fullScreenImage.getRGB(addX + 613, addY + 6)), new Color(255, 255, 255))) {
            return false;
        }
        if (!ColorUtil.similar(new Color(fullScreenImage.getRGB(addX + 605, addY + 13)), new Color(218, 218, 218))) {
            return false;
        }
        if (!ColorUtil.similar(new Color(fullScreenImage.getRGB(addX + 613, addY + 13)), new Color(218, 218, 218))) {
            return false;
        }
        if (!ColorUtil.similar(new Color(fullScreenImage.getRGB(addX + 604, addY + 6)), new Color(83, 86, 102))) {
            return false;
        }

        return true;
    }

    public boolean tableNeedsReading(int tableNro) {
        // old 355,427
        int x = 430;
        int y = 430;
        Color color = null;

        if (tableNro == 1) {
            color = new Color(fullScreenImage.getRGB(x, y));
        } else if (tableNro == 2) {
            color = new Color(fullScreenImage.getRGB(Globals.TABLE_2_POSITION_X + x, Globals.TABLE_2_POSITION_Y + y));
        } else if (tableNro == 3) {
            color = new Color(fullScreenImage.getRGB(Globals.TABLE_3_POSITION_X + x, Globals.TABLE_3_POSITION_Y + y));
        } else if (tableNro == 4) {
            color = new Color(fullScreenImage.getRGB(Globals.TABLE_4_POSITION_X + x, Globals.TABLE_4_POSITION_Y + y));
        } else if (tableNro == 5) {
            color = new Color(fullScreenImage.getRGB(Globals.TABLE_5_POSITION_X + x, Globals.TABLE_5_POSITION_Y + y));
        } else {
            color = new Color(fullScreenImage.getRGB(Globals.TABLE_6_POSITION_X + x, Globals.TABLE_6_POSITION_Y + y));
        }

        boolean needsReading = color.getRed() > 70;
        if (needsReading) {
//            System.out.println("***Table needs reading!***");
        } else {
//            System.out.println("Table does not need reading.");
        }

        return needsReading;
    }

    public Robot getRobot() {
        if (robot == null) {
            try {
                robot = new Robot();
            } catch (AWTException ex) {
                Logger.getLogger(ScreenReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return robot;
    }

    public Rectangle getFullscreenRectangle() {
        if (fullScreenRectangle == null) {
            fullScreenRectangle = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        }
        return fullScreenRectangle;
    }

    public void setFullScreenImage(BufferedImage fullScreenImage) {
        this.fullScreenImage = fullScreenImage;
    }

    public BufferedImage getFullScreenImage() {
        return fullScreenImage;
    }

}
