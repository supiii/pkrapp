package com.masa.screenreader;

import com.masa.type.Globals;
import java.awt.image.BufferedImage;
import java.util.Map;

/**
 *
 * @author Matti
 * @param <T> Object to be returned
 */
public abstract class AbstractImageReader<T, V> {

    protected BufferedImage tableImage = null;
    protected BufferedImage startingImage = null;
    //protected Map<String, BufferedImage> images = new HashMap<>();
    protected int startingPointX = 0;
    protected int startingPointY = 0;
    protected int currentX = 0;
    protected int currentY = 0;

    public AbstractImageReader() {

    }

    protected T readValue(BufferedImage tableImage, Map<V, BufferedImage> imgs) {
        this.tableImage = tableImage;
        currentX = getStartingPointX();
        if (currentX == 0) {
            return null;
        }
        currentY = startingPointY;

        return this.readValue(imgs);
    }

    protected int getStartingPointX() {
        for (int i = 0; i < 100; i++) {
            if ((startingPointX + i) > Globals.TABLE_WIDTH - 50) {
                return 0;
            }
            boolean startingImgFound = readStartingImage(startingPointX + i, startingPointY);
            if (startingImgFound) {
                return startingPointX + i + startingImage.getWidth();
            }

        }
        return 0;
    }

    private boolean readStartingImage(int x, int y) {
        return ColorUtil.match(tableImage, startingImage, x, y);
    }

    protected abstract T readValue(Map<V, BufferedImage> imgs);

    public V readImages(Map<V, BufferedImage> imgs) {
        for (Map.Entry<V, BufferedImage> entry : imgs.entrySet()) {
            V value = entry.getKey();
            BufferedImage img = entry.getValue();

            if (img == null) {
                System.out.println("img is NULL : " + value);
            }

            if (ColorUtil.match(tableImage, img, currentX, currentY)) {
                currentX += img.getWidth();
                //System.out.println("...readCharFromImage returnung " + value);
                return value;
            }

        }
        return null;
    }

    public void setCurrentX(int currentX) {
        this.currentX = currentX;
    }

    public void setCurrentY(int currentY) {
        this.currentY = currentY;
    }

    public BufferedImage getTableImage() {
        return tableImage;
    }

    public void setTableImage(BufferedImage tableImage) {
        this.tableImage = tableImage;
    }

    protected void setStartingPoints(int x, int y) {
        startingPointX = x;
        startingPointY = y;
    }
}
