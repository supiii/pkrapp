package com.masa.screenreader;

import com.masa.screenreader.card.CardReader;
import com.masa.screenreader.foldtext.FoldTextReader;
import com.masa.screenreader.pot.PotReader;
import com.masa.screenreader.stack.StackReader;
import com.masa.screenreader.util.ImageUtil;
import com.masa.type.Globals;
import static com.masa.type.Globals.CARD_REFERENCE_FOLDER;
import static com.masa.type.Globals.PNG;
import com.masa.type.Point;
import com.masa.type.Seat;
import com.masa.type.Street;
import com.masa.type.TableType;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author Matti
 */
public class TableReader {

//    private static final int CARDIMAGE_WIDTH = 39;
//    private static final int CARDIMAGE_HEIGHT = 34;
    private BufferedImage tableImage;
    private final CardReader cardReader;
    private final PotReader potReader;
    private final StackReader stackReader;
    private final FoldTextReader foldTextReader;
    //private ImageSaver imageSaver;

    BufferedImage twoRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "2" + PNG);
    BufferedImage threeRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "3" + PNG);
    BufferedImage fourRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "4" + PNG);
    BufferedImage fiveRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "5" + PNG);
    BufferedImage sixRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "6" + PNG);
    BufferedImage sevenRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "7" + PNG);
    BufferedImage eightRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "8" + PNG);
    BufferedImage nineRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "9" + PNG);
    BufferedImage tenRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "T" + PNG);
    BufferedImage jackRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "J" + PNG);
    BufferedImage queenRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "Q" + PNG);
    BufferedImage kingRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "K" + PNG);
    BufferedImage aceRefenceImg = ImageUtil.getImageResource(CARD_REFERENCE_FOLDER + "A" + PNG);
    public final Map<Rank, List<Point>> cardReferncePoints = new HashMap<>();

    private int leftHoleCardStartingPointX = 0;
    private int leftHoleCardStartingPointY = 0;
    private int firstFlopCardStartingPointX = 0;
    private int firstFlopCardStartingPointY = 0;

    private final Rgb diamondColor = new Rgb(2, 2, 237);
    private final Rgb heartColor = new Rgb(201, 7, 7);
    //private final Rgb spadeColor = new Rgb(2,2,237);
    private final Rgb clubColor = new Rgb(16, 110, 1);

    private final Rgb lowWhiteColor = new Rgb(250, 250, 250);
    private final Rgb hiWhiteColor = new Rgb(255, 255, 255);
    private final Rgb lowHeartColor = new Rgb(200, 0, 0);
    private final Rgb hiHeartColor = new Rgb(255, 255, 160);
    private final Rgb lowClubColor = new Rgb(35, 130, 50);
    private final Rgb hiClubColor = new Rgb(210, 255, 210);
    private final Rgb lowDiamondColor = new Rgb(0, 90, 210);
    private final Rgb hiDiamondColor = new Rgb(255, 255, 160);
    private final Rgb lowSpadeColor = new Rgb(0, 0, 0);
    private final Rgb hiSpadeColor = new Rgb(100, 100, 100);

    private final List<Point> buttonPoints = new ArrayList<>(
            Arrays.asList(
                    p(0, 0, new Rgb(174, 27, 32)),
                    p(1, 0, new Rgb(176, 62, 66)),
                    p(2, 0, new Rgb(174, 38, 43))));

    private final List<Point> firstFlopCardTopLeftPoints = new ArrayList<>(
            Arrays.asList(
                    p(0, 0, new Rgb(7, 102, 35)),
                    p(1, 0, new Rgb(15, 84, 35)),
                    p(2, 0, new Rgb(88, 107, 94)),
                    p(1, 1, new Rgb(133, 134, 133)),
                    p(5, 1, new Rgb(255, 255, 255))));

    private final List<Point> leftHoleCardTopLeftPoints = new ArrayList<>(
            Arrays.asList(
                    p(0, 0, new Rgb(6, 82, 28)),
                    p(1, 0, new Rgb(14, 69, 30)),
                    p(2, 0, new Rgb(87, 103, 92)),
                    p(1, 1, new Rgb(133, 134, 133)),
                    p(5, 1, new Rgb(255, 255, 255))));

    private final int CARD_WIDTH = 35;

    //private final StackReaderFactory stackReaderFactory = new StackReaderFactory();
    public TableReader() {
        stackReader = new StackReader();
        cardReader = new CardReader();
        potReader = new PotReader();
        foldTextReader = new FoldTextReader();
        //imageSaver = new ImageSaver();

        cardReferncePoints.put(Rank.TWO, getCardReferencePoints(twoRefenceImg));
        cardReferncePoints.put(Rank.THREE, getCardReferencePoints(threeRefenceImg));
        cardReferncePoints.put(Rank.FOUR, getCardReferencePoints(fourRefenceImg));
        cardReferncePoints.put(Rank.FIVE, getCardReferencePoints(fiveRefenceImg));
        cardReferncePoints.put(Rank.SIX, getCardReferencePoints(sixRefenceImg));
        cardReferncePoints.put(Rank.SEVEN, getCardReferencePoints(sevenRefenceImg));
        cardReferncePoints.put(Rank.EIGHT, getCardReferencePoints(eightRefenceImg));
        cardReferncePoints.put(Rank.NINE, getCardReferencePoints(nineRefenceImg));
        cardReferncePoints.put(Rank.TEN, getCardReferencePoints(tenRefenceImg));
        cardReferncePoints.put(Rank.JACK, getCardReferencePoints(jackRefenceImg));
        cardReferncePoints.put(Rank.QUEEN, getCardReferencePoints(queenRefenceImg));
        cardReferncePoints.put(Rank.KING, getCardReferencePoints(kingRefenceImg));
        cardReferncePoints.put(Rank.ACE, getCardReferencePoints(aceRefenceImg));
    }

    public Seat readButtonSeat(BufferedImage screenImg) {
        // x: leftHoleCardStartingPointX + (477-299)
        // y: leftHoleCardStartingPointY - (320 - 290)
        //

        // leftholecardX 299
        // leftHoleCardY 320
        // seat1 345, 152
        int seat1X = leftHoleCardStartingPointX + 51;
        int seat1Y = leftHoleCardStartingPointY - 168;

        int seat2X = leftHoleCardStartingPointX + 198;
        int seat2Y = leftHoleCardStartingPointY - 155;

        int seat3X = leftHoleCardStartingPointX + 175;
        int seat3Y = leftHoleCardStartingPointY - 31;

        // 142, 245
        int seat5X = leftHoleCardStartingPointX - 157;
        int seat5Y = leftHoleCardStartingPointY - 75;

        // 152, 191
        int seat6X = leftHoleCardStartingPointX - 147;
        int seat6Y = leftHoleCardStartingPointY - 129;

        System.out.println("seat: " + seat6X + ", " + seat3Y);

        if (checkButton(screenImg, seat1X, seat1Y)) {
            return Seat.ONE;
        }
        if (checkButton(screenImg, seat2X, seat2Y)) {
            return Seat.TWO;
        }
        if (checkButton(screenImg, seat3X, seat3Y)) {
            return Seat.THREE;
        }
        if (checkButton(screenImg, seat5X, seat5Y)) {
            return Seat.FIVE;
        }
        if (checkButton(screenImg, seat6X, seat6Y)) {
            return Seat.SIX;
        }

        return Seat.FOUR;
    }

    public boolean checkButton(BufferedImage screenImg, int startX, int startY) {
        for (int y = startY; y < startY + 60; y++) {
            for (int x = startX; x < startX + 60; x++) {
                boolean match = true;
                for (Point point : buttonPoints) {
                    int rgbInt = screenImg.getRGB(x + point.x, y + point.y);
                    Rgb rgb = ColorUtil.getRbg(rgbInt);
                    if (!ColorUtil.matchWithRange(rgb, point.rgb, 25)) {
                        match = false;
                    }
                }
                if (match) {
                    return true;
                }
            }
        }
        return false;
    }

    public CardSet readHoleCardset(BufferedImage screenImg) {
        // Hole cards
        CardSet holeCardset = new CardSet();
        readLeftHoleCardStartingPoint(screenImg);
        Suit leftHoleCardSuit = readCardSuit(screenImg, leftHoleCardStartingPointX, leftHoleCardStartingPointY, 0);
        //System.out.println(leftHoleCardSuit);
        Suit rightHoleCardSuit = readCardSuit(screenImg, leftHoleCardStartingPointX, leftHoleCardStartingPointY, CARD_WIDTH);
        //System.out.println(rightHoleCardSuit);
        Rank leftHoleCardRank = readCardRank(screenImg, leftHoleCardStartingPointX, leftHoleCardStartingPointY, 0);
        //System.out.println(leftHoleCardRank);
        Rank rightHoleCardRank = readCardRank(screenImg, leftHoleCardStartingPointX, leftHoleCardStartingPointY, CARD_WIDTH);

        if (leftHoleCardRank == null) {
            System.out.println("Could not find card rank");
        } else {
            Card leftHoleCard = new Card(leftHoleCardRank, leftHoleCardSuit);
            Card rightHoleCard = new Card(rightHoleCardRank, rightHoleCardSuit);
            holeCardset.add(leftHoleCard);
            holeCardset.add(rightHoleCard);

            // Check if new hand
            /*if (holeCardLeft != null && holeCardRight != null
                    && !holeCardLeft.equals(leftHoleCard.toString()) && !holeCardRight.equals(rightHoleCard.toString())) {
                System.out.println("NEW HAND");
                restart();
            } else if (holeCardLeft != null && holeCardRight != null
                    && holeCardLeft.equals(leftHoleCard.toString()) && holeCardRight.equals(rightHoleCard.toString())) {
                return;
            }*/
        }
        return holeCardset;
    }

    public CardSet readFlopCardset(BufferedImage screenImg) {
        CardSet flopCardSet = new CardSet();

        Suit firstFlopCardSuit = null;
        Suit secondFlopCardSuit = null;
        Suit thirdFlopCardSuit = null;
        Rank firstFlopCardRank = null;
        Rank secondFlopCardRank = null;
        Rank thirdFlopCardRank = null;

        Card firstFlopCard = null;
        Card secondFlopCard = null;
        Card thirdFlopCard = null;

        readFirstFlopCardStartingPoint(screenImg);

        if (firstFlopCardStartingPointX != 0) {
            firstFlopCardSuit = readCardSuit(screenImg, firstFlopCardStartingPointX, firstFlopCardStartingPointY, 0);
            //System.out.println("First flop card suit: " + firstFlopCardSuit);
            secondFlopCardSuit = readCardSuit(screenImg, firstFlopCardStartingPointX, firstFlopCardStartingPointY, CARD_WIDTH + 3);
            //System.out.println("Second flop card suit: " + secondFlopCardSuit);
            thirdFlopCardSuit = readCardSuit(screenImg, firstFlopCardStartingPointX, firstFlopCardStartingPointY, CARD_WIDTH + 3 + CARD_WIDTH + 3);
            //System.out.println("Third flop card suit: " + thirdFlopCardSuit);

            firstFlopCardRank = readCardRank(screenImg, firstFlopCardStartingPointX, firstFlopCardStartingPointY, 0);
            //System.out.println("First flop card rank: " + firstFlopCardRank);
            secondFlopCardRank = readCardRank(screenImg, firstFlopCardStartingPointX, firstFlopCardStartingPointY, CARD_WIDTH + 3);
            //System.out.println("Second flop card rank: " + secondFlopCardRank);
            thirdFlopCardRank = readCardRank(screenImg, firstFlopCardStartingPointX, firstFlopCardStartingPointY, CARD_WIDTH + 3 + CARD_WIDTH + 3);
            //System.out.println("Third flop card rank: " + thirdFlopCardRank);

        }

        if (firstFlopCardRank == null || secondFlopCardRank == null || thirdFlopCardRank == null) {
            System.out.println("Could not find flop card ranks");
        } else {
            firstFlopCard = new Card(firstFlopCardRank, firstFlopCardSuit);
            secondFlopCard = new Card(secondFlopCardRank, secondFlopCardSuit);
            thirdFlopCard = new Card(thirdFlopCardRank, thirdFlopCardSuit);
            flopCardSet.add(firstFlopCard);
            flopCardSet.add(secondFlopCard);
            flopCardSet.add(thirdFlopCard);
            flopCardSet.toStringWithoutCommas();
        }

        return flopCardSet;
    }

    private List<Point> getCardReferencePoints(BufferedImage referenceImg) {
        int width = referenceImg.getWidth();
        int height = referenceImg.getHeight();

        List<Point> points = new ArrayList<>();
        for (int y = 0; y < width; y++) {
            for (int x = 0; x < height; x++) {
                int binaryRgb = referenceImg.getRGB(x, y);
                Rgb rgb = ColorUtil.getRbg(binaryRgb);
                if (rgb.equals(hiWhiteColor)) {
                    points.add(new Point(x, y));
                }
            }
        }
        return points;
    }

    private void readLeftHoleCardStartingPoint(BufferedImage screenImg) {
        for (int y = 300; y < 374; y++) {
            for (int x = 280; x < 342; x++) {
                boolean match = true;
                for (Point point : leftHoleCardTopLeftPoints) {
                    int rgbInt = screenImg.getRGB(x + point.x, y + point.y);
                    Rgb rgb = ColorUtil.getRbg(rgbInt);
                    if (!ColorUtil.matchWithRange(rgb, point.rgb, 3)) {
                        match = false;
                    }
                    /*if (!ColorUtil.matchWithRange(rgb, new Rgb(255, 255, 255), 1)) {
                        match = false;
                    }*/

                    //System.out.println(matches + " " + pointsSix.size());
                }
                if (match) {
                    leftHoleCardStartingPointX = x;
                    leftHoleCardStartingPointY = y;
                    //System.out.println("Starting point found x: " + startingPointX + " y: " + startingPointY);
                    ImageSaver.saveImage(
                            screenImg.getSubimage(x, y, 100, 100), "startingpoint" + System.currentTimeMillis());
                    return;
                }
            }
        }
        System.out.println("Starting point NOT found");
    }

    private void readFirstFlopCardStartingPoint(BufferedImage screenImg) {
        for (int y = 178; y < 252; y++) { // -122 than leftHoleCard
            for (int x = 221; x < 283; x++) { // -59 than leftHoleCrd
                boolean match = true;
                for (Point point : firstFlopCardTopLeftPoints) {
                    int rgbInt = screenImg.getRGB(x + point.x, y + point.y);
                    Rgb rgb = ColorUtil.getRbg(rgbInt);
                    if (!ColorUtil.matchWithRange(rgb, point.rgb, 3)) {
                        match = false;
                    }
                }
                if (match) {
                    firstFlopCardStartingPointX = x;
                    firstFlopCardStartingPointY = y;
                    //System.out.println("Starting point found x: " + startingPointX + " y: " + startingPointY);
                    ImageSaver.saveImage(
                            screenImg.getSubimage(x, y, 100, 100), "flopstartingpoint" + System.currentTimeMillis());
                    return;
                }
            }
        }
        System.out.println("Flop card starting point NOT found");
    }

    private Suit readHoleCardSuit(BufferedImage screenImg, int offsetX) {
        int maxX = leftHoleCardStartingPointX + 25 + offsetX;
        int maxY = leftHoleCardStartingPointY + 25;
        for (int y = leftHoleCardStartingPointY; y < maxY; y++) {
            for (int x = leftHoleCardStartingPointX + offsetX; x < maxX; x++) {
                // 307 345
                /*if (x == 307 && y == 345) {
                    System.out.println("x: " + x + " y: " + y);
                }*/

                int rgbInt = screenImg.getRGB(x, y);
                Rgb rgb = ColorUtil.getRbg(rgbInt);
                if (ColorUtil.matchWithRange(rgb, clubColor, 4)) {
                    return Suit.CLUB;
                } else if (ColorUtil.matchWithRange(rgb, diamondColor, 4)) {
                    return Suit.DIAMOND;
                } else if (ColorUtil.matchWithRange(rgb, heartColor, 0)) {
                    return Suit.HEART;
                }
            }
        }

        return Suit.SPADE;
    }

    private Suit readCardSuit(BufferedImage screenImg, int startX, int startY, int offsetX) {
        int maxX = startX + 25 + offsetX;
        int maxY = startY + 25;
        for (int y = startY; y < maxY; y++) {
            for (int x = startX + offsetX; x < maxX; x++) {
                // 307 345
                /*if (x == 307 && y == 345) {
                    System.out.println("x: " + x + " y: " + y);
                }*/

                int rgbInt = screenImg.getRGB(x, y);
                Rgb rgb = ColorUtil.getRbg(rgbInt);
                if (ColorUtil.matchWithRange(rgb, clubColor, 4)) {
                    return Suit.CLUB;
                } else if (ColorUtil.matchWithRange(rgb, diamondColor, 4)) {
                    return Suit.DIAMOND;
                } else if (ColorUtil.matchWithRange(rgb, heartColor, 0)) {
                    return Suit.HEART;
                }
            }
        }

        return Suit.SPADE;
    }

    private Rank readCardRank(BufferedImage screenImg, int startX, int startY, int offsetX) {
        for (int i = 0; i < 2; i++) {
            for (Map.Entry<Rank, List<Point>> entry : cardReferncePoints.entrySet()) {
                boolean match = true;
                Rank rank = entry.getKey();
                List<Point> points = entry.getValue();

                if (rank == Rank.SIX) {
                    //System.out.println("CHECKING SIX");
                    int rgbInt = screenImg.getRGB(startX + 4 + i, startY + 10);
                    Rgb rgb = ColorUtil.getRbg(rgbInt);
                    //System.out.println("SIX RGB " + rgb.red + ", " + rgb.green + ", " + rgb.blue);
                    if (rgb.equals(hiWhiteColor)) {
                        match = false;
                    }
                }

                for (Point point : points) {
                    int x = startX + point.x + offsetX + i;
                    int y = startY + point.y;
                    int rgbInt = screenImg.getRGB(x, y);
                    Rgb rgb = ColorUtil.getRbg(rgbInt);
                    if (!rgb.equals(hiWhiteColor)) {
                        match = false;
                    }
                }
                if (match) {
                    return rank;
                }
            }
        }

        return null;
    }

    public Card readImages(Map<Card, BufferedImage> imgs, BufferedImage fullscreenImg, int x, int y) {
        for (Map.Entry<Card, BufferedImage> entry : imgs.entrySet()) {
            Card value = entry.getKey();
            BufferedImage img = entry.getValue();

            if (ColorUtil.match(fullscreenImg, img, x, y)) {
                return value;
            }

        }
        return null;
    }

    public Card readLeftHoleCard() throws NullPointerException {
        //System.out.println("Reading left hole card...");
//        ImageSaver saver = new ImageSaver();
//        saver.saveImage(getLeftHoleCardImage(), "testing_9.2.2020");
        Card card = cardReader.readLeftHoleCard(tableImage);
//        Card card = new Card("4d");
        if (card.rankOf() == null || card.suitOf() == null) {
//            throw new HandDataMissingException("Could not read LEFT HOLE CARD!!!");
//            System.out.println("Could not read LEFT HOLE CARD");
        }
//        System.out.println(card);

        return card;
    }

    public Card readRightHoleCard() {
        //System.out.println("Reading right hole card...");
        Card card = cardReader.readRightHoleCard(tableImage);
//        Card card = new Card("4s");
        if (card.rankOf() == null || card.suitOf() == null) {
//            throw new HandDataMissingException("Could not read RIGHT HOLE CARD!!!");
//            System.out.println("Could not read RIGHT HOLE CARD");
        }
//        System.out.println(card);
        return card;
//        return new Card("2d");
    }

    private enum PositionWhenSix {

        SB,
        BB,
        UTG,
        UTGplus1,
        CO,
        BTN
    }

    private enum PositionWhenFive {

        SB,
        BB,
        UTGplus1,
        CO,
        BTN
    }

    private enum PositionWhenFour {

        SB,
        BB,
        CO,
        BTN
    }

    private enum PositionWhenThree {

        SB,
        BB,
        BTN
    }

    private boolean isSittingOutOrEmpty(Seat seat, List<Seat> seatsSittingOutOrEmpty) {
        for (Seat out : seatsSittingOutOrEmpty) {
            if (out == seat) {
                return true;
            }
        }
        return false;
    }

    public Seat readDealerPosition() {

        if (!colorIsTableColor(tableImage.getRGB(Globals.DEALER_BUTTON_X_SEAT_2, Globals.DEALER_BUTTON_Y_SEAT_2))) {
            return Seat.TWO;
        } else if (!colorIsTableColor(tableImage.getRGB(Globals.DEALER_BUTTON_X_SEAT_3, Globals.DEALER_BUTTON_Y_SEAT_3))) {
            return Seat.THREE;
        } else if (!colorIsTableColor(tableImage.getRGB(Globals.DEALER_BUTTON_X_SEAT_4, Globals.DEALER_BUTTON_Y_SEAT_4))) {
            return Seat.FOUR;
        } else if (!colorIsTableColor(tableImage.getRGB(Globals.DEALER_BUTTON_X_SEAT_5, Globals.DEALER_BUTTON_Y_SEAT_5))) {
            return Seat.FIVE;
        } else if (!colorIsTableColor(tableImage.getRGB(Globals.DEALER_BUTTON_X_SEAT_6, Globals.DEALER_BUTTON_Y_SEAT_6))) {
            return Seat.SIX;
        } else {
            return Seat.ONE;
        }

        /*if (ColorUtil.inRange(tableImage.getRGB(GlobalValues.DEALER_BUTTON_X_SEAT_3, GlobalValues.DEALER_BUTTON_Y_SEAT_3), GlobalValues.DEALER_BUTTON_COLOR_RANGE_1, GlobalValues.DEALER_BUTTON_COLOR_RANGE_2)) {
            return Seat.THREE;
        } else if (ColorUtil.inRange(tableImage.getRGB(GlobalValues.DEALER_BUTTON_X_SEAT_2, GlobalValues.DEALER_BUTTON_Y_SEAT_2), GlobalValues.DEALER_BUTTON_COLOR_RANGE_1, GlobalValues.DEALER_BUTTON_COLOR_RANGE_2)) {
            return Seat.TWO;
        } else if (ColorUtil.inRange(tableImage.getRGB(GlobalValues.DEALER_BUTTON_X_SEAT_6, GlobalValues.DEALER_BUTTON_Y_SEAT_6), GlobalValues.DEALER_BUTTON_COLOR_RANGE_1, GlobalValues.DEALER_BUTTON_COLOR_RANGE_2)) {
            return Seat.SIX;
        } else if (ColorUtil.inRange(tableImage.getRGB(GlobalValues.DEALER_BUTTON_X_SEAT_5, GlobalValues.DEALER_BUTTON_Y_SEAT_5), GlobalValues.DEALER_BUTTON_COLOR_RANGE_1, GlobalValues.DEALER_BUTTON_COLOR_RANGE_2)) {
            return Seat.FIVE;
        } else if (ColorUtil.inRange(tableImage.getRGB(GlobalValues.DEALER_BUTTON_X_SEAT_4, GlobalValues.DEALER_BUTTON_Y_SEAT_4), GlobalValues.DEALER_BUTTON_COLOR_RANGE_1, GlobalValues.DEALER_BUTTON_COLOR_RANGE_2)) {
            return Seat.FOUR;
        } else {
            return Seat.ONE;
        }*/
    }

    public Card readFirstFlopCard() {
//        System.out.println("Reading first flop card...");
        Card card = cardReader.readFirstFlopCard(tableImage);
        if (card.rankOf() == null || card.suitOf() == null) {
//            throw new HandDataMissingException("Could not read RIGHT HOLE CARD!!!");
            //System.out.println("Could not read FIRST FLOP CARD");
        }
        return card;
    }

    public Card readSecondFlopCard() {
//        System.out.println("Reading second flop card...");
        Card card = cardReader.readSecondFlopCard(tableImage);
        return card;
    }

    public Card readThirdFlopCard() {
//        System.out.println("Reading third flop card...");
        Card card = cardReader.readThirdFlopCard(tableImage);
        return card;
    }

    public Card readTurnCard() {
//        System.out.println("Reading turn card...");
        Card card = cardReader.readTurnCard(tableImage);
        return card;
    }

    public Card readRiverCard() {
//        System.out.println("Reading river card...");
//        imageSaver.saveImage(tableImage.getSubimage(389, 183, CARDIMAGE_WIDTH, CARDIMAGE_HEIGHT), "River_card" + System.currentTimeMillis());
//        Card card = cardReader.readCard(tableImage.getSubimage(289, 178, CARDIMAGE_WIDTH, CARDIMAGE_HEIGHT));
        Card card = cardReader.readRiverCard(tableImage);
        return card;
    }

    public boolean readSeatHasPutMoneyIn(Seat seat) {
        List<Integer> colors = new ArrayList<>();

        if (null != seat) {
            switch (seat) {
                case ONE: {
                    int y = 0;
                    if (Globals.TABLE_TYPE == TableType.CASH_6_MAX) {
                        y = 123;
                    } else if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
                        y = 123;
                    }
                    colors.add(tableImage.getRGB(301, y));
                    colors.add(tableImage.getRGB(315, y));
                    colors.add(tableImage.getRGB(324, y));
                    colors.add(tableImage.getRGB(331, y));
                    colors.add(tableImage.getRGB(340, y));
                    colors.add(tableImage.getRGB(345, y));
                    colors.add(tableImage.getRGB(350, y));
                    colors.add(tableImage.getRGB(355, y));
                    colors.add(tableImage.getRGB(360, y));
                    colors.add(tableImage.getRGB(370, y));

                    return !allColorsAreTableColor(colors);

                }
                case TWO: {
                    int y = 0;
                    if (Globals.TABLE_TYPE == TableType.CASH_6_MAX) {
                        y = 147;
                    } else if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
                        y = 147;
                    }
                    colors.add(tableImage.getRGB(407, y));
                    colors.add(tableImage.getRGB(420, y));
                    colors.add(tableImage.getRGB(430, y));
                    colors.add(tableImage.getRGB(440, y));
                    colors.add(tableImage.getRGB(445, y));
                    colors.add(tableImage.getRGB(450, y));
                    colors.add(tableImage.getRGB(455, y));
                    colors.add(tableImage.getRGB(460, y));
                    colors.add(tableImage.getRGB(471, y));
//            colors.add(tableImage.getRGB(494, y));

                    return !allColorsAreTableColor(colors);
                }
                case THREE: {
                    int y = 0;
                    if (Globals.TABLE_TYPE == TableType.CASH_6_MAX) {
                        y = 255;
                    } else if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
                        y = 255;
                    }
                    colors.add(tableImage.getRGB(420, y));
                    colors.add(tableImage.getRGB(430, y));
                    colors.add(tableImage.getRGB(440, y));
                    colors.add(tableImage.getRGB(445, y));
                    colors.add(tableImage.getRGB(450, y));
                    colors.add(tableImage.getRGB(454, y));
                    colors.add(tableImage.getRGB(464, y));
                    colors.add(tableImage.getRGB(485, y));

                    return !allColorsAreTableColor(colors);
                }
                case FOUR: {
                    int y = 0;
                    if (Globals.TABLE_TYPE == TableType.CASH_6_MAX) {
                        y = 279;
                    } else if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
                        y = 279;
                    }
                    colors.add(tableImage.getRGB(266, y));
                    colors.add(tableImage.getRGB(285, y));
                    colors.add(tableImage.getRGB(305, y));
                    colors.add(tableImage.getRGB(310, y));
                    colors.add(tableImage.getRGB(314, y));
                    colors.add(tableImage.getRGB(320, y));
                    colors.add(tableImage.getRGB(326, y));
                    colors.add(tableImage.getRGB(330, y));
                    colors.add(tableImage.getRGB(337, y));

                    return !allColorsAreTableColor(colors);
                }
                case FIVE: {
                    int y = 0;
                    if (Globals.TABLE_TYPE == TableType.CASH_6_MAX) {
                        y = 256;
                    } else if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
                        y = 256;
                    }
                    colors.add(tableImage.getRGB(151, y));
                    colors.add(tableImage.getRGB(158, y));
                    colors.add(tableImage.getRGB(163, y));
                    colors.add(tableImage.getRGB(168, y));
                    colors.add(tableImage.getRGB(174, y));
                    colors.add(tableImage.getRGB(180, y));
                    colors.add(tableImage.getRGB(186, y));
                    colors.add(tableImage.getRGB(194, y));

                    return !allColorsAreTableColor(colors);
                }
                case SIX: {
                    int y = 0;
                    if (Globals.TABLE_TYPE == TableType.CASH_6_MAX) {
                        y = 163;
                    } else if (Globals.TABLE_TYPE == TableType.CASH_6_MAX_ZOOM) {
                        y = 163;
                    }
                    colors.add(tableImage.getRGB(152, y));
                    colors.add(tableImage.getRGB(163, y));
                    colors.add(tableImage.getRGB(168, y));
                    colors.add(tableImage.getRGB(174, y));
                    colors.add(tableImage.getRGB(179, y));
                    colors.add(tableImage.getRGB(182, y));
                    colors.add(tableImage.getRGB(186, y));
                    colors.add(tableImage.getRGB(190, y));
                    colors.add(tableImage.getRGB(198, y));
                    colors.add(tableImage.getRGB(212, y));

                    return !allColorsAreTableColor(colors);
                }
                default:
                    break;
            }
        }
        return false;
    }

    private boolean allColorsAreTableColor(List<Integer> colors) {
        boolean allColorsMatch;

        for (Integer color : colors) {

            allColorsMatch = ColorUtil.inRange(color, Globals.TABLE_COLOR_RANGE_1, Globals.TABLE_COLOR_RANGE_2);
            if (allColorsMatch == false) {
                return false;
            }
        }

        return true;
    }

    private boolean colorIsTableColor(Integer color) {
        return ColorUtil.inRange(color, Globals.TABLE_COLOR_RANGE_1, Globals.TABLE_COLOR_RANGE_2);
    }

    public BufferedImage getTableImage() {
        return tableImage;
    }

    public void setTableImage(BufferedImage tableImage) {
        this.tableImage = tableImage;
    }

    public Street readStreet() {
        // Card card = readRiverCard();
        if (readFirstFlopCard().rankOf() == null) {
            return Street.PREFLOP;
        } else if (readRiverCard().rankOf() != null) {

            return Street.RIVER;
        } else if (readTurnCard().rankOf() != null) {
            return Street.TURN;
        } else {
            return Street.FLOP;
        }
    }

    public BigDecimal readPot() {
        return potReader.readValue(tableImage);
    }

    public BigDecimal readStack(Seat seat) {
        if (readFoldedText(seat)) {
            return null;
        }
        return stackReader.readStack(tableImage, seat);
    }

    public BigDecimal readFoldedStack(Seat seat) {
        if (readFoldedText(seat)) {
            return new BigDecimal(BigInteger.ONE);
        }
        return stackReader.readFoldedStack(tableImage, seat);
    }

    private boolean readFoldedText(Seat seat) {
        Boolean result = foldTextReader.readFolded(tableImage, seat);
        if (result == null || !result) {
            return false;
        }
        return result;
    }

    private Point p(int x, int y) {
        return new Point(x, y);
    }

    private Point p(int x, int y, boolean matches) {
        return new Point(x, y, matches);
    }

    private Point p(int x, int y, Rgb rgb) {
        return new Point(x, y, rgb);
    }
}
