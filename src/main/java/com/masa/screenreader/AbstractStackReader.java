package com.masa.screenreader;

import com.masa.type.Globals;
import com.masa.type.Seat;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;

/**
 *
 * @author Matti
 */
public abstract class AbstractStackReader {

    protected static final int WIDTH_NUMBERS = 7;
    protected static final int WIDTH_DOT = 3;
    // 100, 128, 95
    // 125, 152, 119
    protected BufferedImage tableImage;

    protected int currentX = 0;
    protected int currentY = 0;
    protected boolean foldedStackReading = false;

    protected abstract int getDollarLookingPointXSeat1();

    protected abstract int getDollarLookingPointYSeat1();

    protected abstract int getDollarLookingPointXSeat2();

    protected abstract int getDollarLookingPointYSeat2();

    protected abstract int getDollarLookingPointXSeat3();

    protected abstract int getDollarLookingPointYSeat3();

    protected abstract int getDollarLookingPointXSeat4();

    protected abstract int getDollarLookingPointYSeat4();

    protected abstract int getDollarLookingPointXSeat5();

    protected abstract int getDollarLookingPointYSeat5();

    protected abstract int getDollarLookingPointXSeat6();

    protected abstract int getDollarLookingPointYSeat6();

    public BigDecimal readStack(BufferedImage tableImage, Seat seat) {
        this.tableImage = tableImage;
        foldedStackReading = false;

        setCurrentXY(seat);
        if (currentX == 0) {
            return null;
        }
        return readStack();
    }

    public BigDecimal readFolded(BufferedImage tableImage, Seat seat) {
        this.tableImage = tableImage;
        foldedStackReading = true;

        setCurrentXY(seat);
        if (currentX == 0) {
            return null;
        }
        return readFoldedStack();
    }

    private void setCurrentXY(Seat seat) {
        if (seat == Seat.ONE) {
            int startingpointX = getStartingPointX(getDollarLookingPointXSeat1(), getDollarLookingPointYSeat1());
            currentX = startingpointX;
            currentY = getDollarLookingPointYSeat1();
        } else if (seat == Seat.TWO) {
            int startingpointX = getStartingPointX(getDollarLookingPointXSeat2(), getDollarLookingPointYSeat2());
            currentX = startingpointX;
            currentY = getDollarLookingPointYSeat2();
        } else if (seat == Seat.THREE) {
            int startingpointX = getStartingPointX(getDollarLookingPointXSeat3(), getDollarLookingPointYSeat3());
            currentX = startingpointX;
            currentY = getDollarLookingPointYSeat3();
        } else if (seat == Seat.FOUR) {
            int startingpointX = getStartingPointX(getDollarLookingPointXSeat4(), getDollarLookingPointYSeat4());
            currentX = startingpointX;
            currentY = getDollarLookingPointYSeat4();
        } else if (seat == Seat.FIVE) {
            int startingpointX = getStartingPointX(getDollarLookingPointXSeat5(), getDollarLookingPointYSeat5());
            currentX = startingpointX;
            currentY = getDollarLookingPointYSeat5();
        } else if (seat == Seat.SIX) {
            int startingpointX = getStartingPointX(getDollarLookingPointXSeat6(), getDollarLookingPointYSeat6());
            currentX = startingpointX;
            currentY = getDollarLookingPointYSeat6();
        }
    }

    private BigDecimal readFoldedStack() {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        while (readChar().length() > 0) {
            String c = readChar();
            sb.append(c);
            updateXY(c);
            i++;
        }
        String stack = sb.toString();
        if (stack.isEmpty()) {
            return null;
        }
        return new BigDecimal(sb.toString()).setScale(2);
    }

    private BigDecimal readStack() {
        StringBuilder sb = new StringBuilder();

        int i = 0;
//        System.out.println("Reading stack, before while..");
        while (readChar().length() > 0) {
//            System.out.println("Reading stack, inside while loop..");
            String c = readChar();
//            System.out.println("c: " + c);
            sb.append(c);
            updateXY(c);
            i++;
//            System.out.println("stack is: " + sb.toString());
        }
//        System.out.println("FINAL stack is: " + sb.toString());
        String stack = sb.toString();
        if (stack.isEmpty()) {
            return null;
        }
        return new BigDecimal(sb.toString()).setScale(2);
    }

    private boolean readFolded() {
        boolean folded = false;
        int i = 0;
        while (!folded || currentX < currentX + 100) {
            folded = readFoldedPixel();
            currentX += 1;
            i++;
        }

        return folded;
    }

    private boolean readFoldedPixel() {
        int color = getRBG(currentX, currentY);

        int red = (color & 0xff0000) >> 16;
        int green = (color & 0xff00) >> 8;
        int blue = color & 0xff;

        if (red > 99 && red < 110
                && green > 128 && green < 137
                && blue > 94 && blue < 104) {
            return true;
        } else {
            return false;
        }
        // font 192, 248, 181
    }

    private String readChar() {
        if (isDot()) {
            return ".";
        } else if (isZero()) {
            return "0";
        } else if (isOne()) {
            return "1";
        } else if (isTwo()) {
            return "2";
        } else if (isThree()) {
            return "3";
        } else if (isFour()) {
            return "4";
        } else if (isFive()) {
            return "5";
        } else if (isSix()) {
            return "6";
        } else if (isSeven()) {
            return "7";
        } else if (isEight()) {
            return "8";
        } else if (isNine()) {
            return "9";
        } else {
            return "";
        }

    }

    private boolean isDot() {
        return green(0, 6)
                && green(0, 7)
                && !green(0, 1);
    }

    private boolean isSeven() {
//        System.out.println("isSeven()");
        return green(0, 0) && green(5, 0);
    }

    private boolean isZero() {
//        System.out.println("isZero()");
        return !green(5, 0)
                && !green(2, 2)
                && !green(2, 3)
                && !green(2, 4)
                && green(4, 4);
    }

    // 6,9 2,9
    private boolean isOne() {
//        System.out.println("isOne()");
        return green(3, 1)
                && !green(0, 4);
    }

    private boolean isTwo() {
//           System.out.println("isTwo()");
        return green(0, 7)
                && green(0, 1);
    }

    private boolean isThree() {
//        System.out.println("isThree()");
        return green(5, 1)
                && green(5, 2)
                && !green(5, 3)
                && green(5, 4)
                && !green(0, 4);
    }

    private boolean isFour() {
//        System.out.println("isFour()");
        return green(0, 4)
                && !green(1, 6)
                && !green(5, 6);
    }

    private boolean isFive() {
//        System.out.println("isFive()");
        return !green(5, 1) && green(5, 0)
                && !green(2, 4)
                && !green(5, 2)
                && !green(0, 2);
    }

    private boolean isSix() {
//        System.out.println("isSix()");
        return green(0, 5)
                && green(0, 6)
                && !green(5, 2);
    }

    private boolean isEight() {
//        System.out.println("isEight()");
        return green(5, 2)
                && !green(5, 3)
                && green(0, 5);
    }

    private boolean isNine() {
        return green(5, 3)
                && green(5, 2)
                && !green(0, 5);
    }

    private boolean green(int x, int y) {
//        System.out.println("x: " + x + " y: " + y);
        return isAllowedColor(getRBG(x, y));
    }

    private int getRBG(int x, int y) {
//        System.out.println("getRBG... currentX + x; " + currentX + x + " currentY + y: " + currentY + y);
//        System.out.println("currentX; " + currentX + " x: " + x);
        int xx = currentX + x;
        int yy = currentY + y;
//        System.out.println("xx; " + xx + " yy: " + yy);
        return tableImage.getRGB(xx, yy);
    }

    private boolean isAllowedColor(int i) {
//        System.out.println("Väri on: " + new Color(i));
        if (foldedStackReading == false) {
            Color color = new Color(i);

            return Globals.STACK_FONT_COLOR.equals(color);
        }
        return ColorUtil.inRange(i, Globals.FOLDED_STACK_COLOR_RANGE_1, Globals.FOLDED_STACK_COLOR_RANGE_2);
    }

    private int getStartingPointX(int x, int y) {
        for (int i = 0; i < 100; i++) {
            if ((x + i) > Globals.TABLE_WIDTH - 20) {
                return 0;
            }
            if (isAllowedColor(tableImage.getRGB(x + i, y - 1))) {
                return x + i + 4;
            }
        }
        return 0;

    }

    private void updateXY(String c) {
        if (c.equals(".")) {
            currentX += WIDTH_DOT;
        } else {
            currentX += WIDTH_NUMBERS;
        }
//        System.out.println("Updated: currentX: " + currentX + " currentY: " + currentY);
    }

    public int getCurrentX() {
        return currentX;
    }

    public void setCurrentX(int currentX) {
        this.currentX = currentX;
    }

    public int getCurrentY() {
        return currentY;
    }

    public void setCurrentY(int currentY) {
        this.currentY = currentY;
    }

    public BufferedImage getTableImage() {
        return tableImage;
    }

    public void setTableImage(BufferedImage tableImage) {
        this.tableImage = tableImage;
    }
}
