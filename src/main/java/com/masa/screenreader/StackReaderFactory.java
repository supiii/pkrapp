package com.masa.screenreader;

import com.masa.screenreader.cash6max.StackReaderCashSixMax;
import com.masa.type.TableType;

/**
 *
 * @author Matti
 */
public class StackReaderFactory {

    public AbstractStackReader getStackReader(TableType tableType) {
        if (tableType == null) {
            return null;
        }
        if (tableType == TableType.CASH_6_MAX || tableType == TableType.CASH_6_MAX_ZOOM) {
            return new StackReaderCashSixMax();
        }

        return null;
    }
}
