package com.masa.screenreader;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author Matti
 */
public class ColorUtil {

//    private static final int RANGE_STEP = 35;
    private static final int RANGE_STEP = 70;

    public static boolean match(BufferedImage tableImg, BufferedImage img, int x, int y) {
        BufferedImage imgToSave = tableImg.getSubimage(x, y, 12, 12);
        ImageSaver.saveImage(imgToSave, "hhhhhhh" + x + y);
        int firstRow = 1;
        int secondRow = 4;
        int thirdRow = 7;
        int width = img.getWidth();

        for (int i = 0; i < width; i++) {
            int imgRgb1 = img.getRGB(i, firstRow);
            int imgRgb2 = img.getRGB(i, secondRow);
            int imgRgb3 = img.getRGB(i, thirdRow);
            int tableImgRgb1 = tableImg.getRGB(x + i, y + firstRow);
            int tableImgRgb2 = tableImg.getRGB(x + i, y + secondRow);
            int tableImgRgb3 = tableImg.getRGB(x + i, y + thirdRow);

            if (!matchWithRange(imgRgb1, tableImgRgb1)
                    || !matchWithRange(imgRgb2, tableImgRgb2)
                    || !matchWithRange(imgRgb3, tableImgRgb3)) {
                return false;
            }
        }
        return true;
    }

    public static boolean matchWithRange(int binaryRgb1, int binaryRgb2) {
        Rgb rgb1 = ColorUtil.getRbg(binaryRgb1);
        Rgb rgb2 = ColorUtil.getRbg(binaryRgb2);

        Rgb lowRange = new Rgb(rgb2.red - RANGE_STEP, rgb2.green - RANGE_STEP, rgb2.blue - RANGE_STEP);
        Rgb hiRange = new Rgb(rgb2.red + RANGE_STEP, rgb2.green + RANGE_STEP, rgb2.blue + RANGE_STEP);

        return ColorUtil.inRange(rgb1, lowRange, hiRange);
    }

    public static boolean matchWithRange(int binaryRgb1, int binaryRgb2, int stepStep) {
        Rgb rgb1 = ColorUtil.getRbg(binaryRgb1);
        Rgb rgb2 = ColorUtil.getRbg(binaryRgb2);

        Rgb lowRange = new Rgb(rgb2.red - stepStep, rgb2.green - stepStep, rgb2.blue - stepStep);
        Rgb hiRange = new Rgb(rgb2.red + stepStep, rgb2.green + stepStep, rgb2.blue + stepStep);

        return ColorUtil.inRange(rgb1, lowRange, hiRange);
    }

    public static boolean matchWithRange(Rgb rgb1, Rgb rgb2, int stepStep) {
        Rgb lowRange = new Rgb(rgb2.red - stepStep, rgb2.green - stepStep, rgb2.blue - stepStep);
        Rgb hiRange = new Rgb(rgb2.red + stepStep, rgb2.green + stepStep, rgb2.blue + stepStep);

        return ColorUtil.inRange(rgb1, lowRange, hiRange);
    }

    public static Rgb getRbg(int rgb) {
        int red = (rgb & 0xff0000) >> 16;
        int green = (rgb & 0xff00) >> 8;
        int blue = rgb & 0xff;

        return new Rgb(red, green, blue);
    }

    public static boolean inRange(Rgb toBeTested, Rgb range1, Rgb range2) {
        return toBeTested.red >= range1.red && toBeTested.red <= range2.red
                && toBeTested.green >= range1.green && toBeTested.green <= range2.green
                && toBeTested.blue >= range1.blue && toBeTested.blue <= range2.blue;
    }

    public static boolean inRange(int i, Color range1, Color range2) {
        int red = (i & 0xff0000) >> 16;
        int green = (i & 0xff00) >> 8;
        int blue = i & 0xff;

        return red >= range1.getRed() && red <= range2.getRed()
                && green >= range1.getGreen() && green <= range2.getGreen()
                && blue >= range1.getBlue() && blue <= range2.getBlue();
    }

    public static boolean similar(Color color1, Color color2) {
        return color1.getRed() == color2.getRed() && color1.getGreen() == color2.getGreen() && color1.getBlue() == color2.getBlue();
    }

}
