package com.masa.screenreader.card;

import com.masa.type.Globals;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import mi.poker.common.model.testbed.klaatu.Card;

/**
 *
 * @author Matti
 */
public class CardReader extends AbstractCardImageReader {

    public final Map<Card, BufferedImage> images = new HashMap<>();
    private final int HOLECARDS_Y = 294;
    private final int BOARDCARDS_Y = 172;

    public CardReader() {
        images.put(Globals.TWO_SPADE, Globals.TWO_SPADE_IMAGE);
        images.put(Globals.THREE_SPADE, Globals.THREE_SPADE_IMAGE);
        images.put(Globals.FOUR_SPADE, Globals.FOUR_SPADE_IMAGE);
        images.put(Globals.FIVE_SPADE, Globals.FIVE_SPADE_IMAGE);
        images.put(Globals.SIX_SPADE, Globals.SIX_SPADE_IMAGE);
        images.put(Globals.SEVEN_SPADE, Globals.SEVEN_SPADE_IMAGE);
        images.put(Globals.EIGHT_SPADE, Globals.EIGHT_SPADE_IMAGE);
        images.put(Globals.NINE_SPADE, Globals.NINE_SPADE_IMAGE);
        images.put(Globals.TEN_SPADE, Globals.TEN_SPADE_IMAGE);
        images.put(Globals.JACK_SPADE, Globals.JACK_SPADE_IMAGE);
        images.put(Globals.QUEEN_SPADE, Globals.QUEEN_SPADE_IMAGE);
        images.put(Globals.KING_SPADE, Globals.KING_SPADE_IMAGE);
        images.put(Globals.ACE_SPADE, Globals.ACE_SPADE_IMAGE);

        images.put(Globals.TWO_HEART, Globals.TWO_HEART_IMAGE);
        images.put(Globals.THREE_HEART, Globals.THREE_HEART_IMAGE);
        images.put(Globals.FOUR_HEART, Globals.FOUR_HEART_IMAGE);
        images.put(Globals.FIVE_HEART, Globals.FIVE_HEART_IMAGE);
        images.put(Globals.SIX_HEART, Globals.SIX_HEART_IMAGE);
        images.put(Globals.SEVEN_HEART, Globals.SEVEN_HEART_IMAGE);
        images.put(Globals.EIGHT_HEART, Globals.EIGHT_HEART_IMAGE);
        images.put(Globals.NINE_HEART, Globals.NINE_HEART_IMAGE);
        images.put(Globals.TEN_HEART, Globals.TEN_HEART_IMAGE);
        images.put(Globals.JACK_HEART, Globals.JACK_HEART_IMAGE);
        images.put(Globals.QUEEN_HEART, Globals.QUEEN_HEART_IMAGE);
        images.put(Globals.KING_HEART, Globals.KING_HEART_IMAGE);
        images.put(Globals.ACE_HEART, Globals.ACE_HEART_IMAGE);

        images.put(Globals.TWO_CLUB, Globals.TWO_CLUB_IMAGE);
        images.put(Globals.THREE_CLUB, Globals.THREE_CLUB_IMAGE);
        images.put(Globals.FOUR_CLUB, Globals.FOUR_CLUB_IMAGE);
        images.put(Globals.FIVE_CLUB, Globals.FIVE_CLUB_IMAGE);
        images.put(Globals.SIX_CLUB, Globals.SIX_CLUB_IMAGE);
        images.put(Globals.SEVEN_CLUB, Globals.SEVEN_CLUB_IMAGE);
        images.put(Globals.EIGHT_CLUB, Globals.EIGHT_CLUB_IMAGE);
        images.put(Globals.NINE_CLUB, Globals.NINE_CLUB_IMAGE);
        images.put(Globals.TEN_CLUB, Globals.TEN_CLUB_IMAGE);
        images.put(Globals.JACK_CLUB, Globals.JACK_CLUB_IMAGE);
        images.put(Globals.QUEEN_CLUB, Globals.QUEEN_CLUB_IMAGE);
        images.put(Globals.KING_CLUB, Globals.KING_CLUB_IMAGE);
        images.put(Globals.ACE_CLUB, Globals.ACE_CLUB_IMAGE);

        images.put(Globals.TWO_DIAMOND, Globals.TWO_DIAMOND_IMAGE);
        images.put(Globals.THREE_DIAMOND, Globals.THREE_DIAMOND_IMAGE);
        images.put(Globals.FOUR_DIAMOND, Globals.FOUR_DIAMOND_IMAGE);
        images.put(Globals.FIVE_DIAMOND, Globals.FIVE_DIAMOND_IMAGE);
        images.put(Globals.SIX_DIAMOND, Globals.SIX_DIAMOND_IMAGE);
        images.put(Globals.SEVEN_DIAMOND, Globals.SEVEN_DIAMOND_IMAGE);
        images.put(Globals.EIGHT_DIAMOND, Globals.EIGHT_DIAMOND_IMAGE);
        images.put(Globals.NINE_DIAMOND, Globals.NINE_DIAMOND_IMAGE);
        images.put(Globals.TEN_DIAMOND, Globals.TEN_DIAMOND_IMAGE);
        images.put(Globals.JACK_DIAMOND, Globals.JACK_DIAMOND_IMAGE);
        images.put(Globals.QUEEN_DIAMOND, Globals.QUEEN_DIAMOND_IMAGE);
        images.put(Globals.KING_DIAMOND, Globals.KING_DIAMOND_IMAGE);
        images.put(Globals.ACE_DIAMOND, Globals.ACE_DIAMOND_IMAGE);
    }

    public Card readLeftHoleCard(BufferedImage tableImage) {
        setStartingPoints(284, 294);
        return readValue(tableImage, images);
    }

    public Card readRightHoleCard(BufferedImage tableImage) {
        setStartingPoints(320, 294);
        return readValue(tableImage, images);
    }

    public Card readFirstFlopCard(BufferedImage tableImage) {
        setStartingPoints(225, 172);
        return readValue(tableImage, images);
    }

    public Card readSecondFlopCard(BufferedImage tableImage) {
        setStartingPoints(264, 172);
        return readValue(tableImage, images);
    }

    public Card readThirdFlopCard(BufferedImage tableImage) {
        setStartingPoints(302, 172);
        return readValue(tableImage, images);
    }

    public Card readTurnCard(BufferedImage tableImage) {
        setStartingPoints(340, 172);
        return readValue(tableImage, images);
    }

    public Card readRiverCard(BufferedImage tableImage) {
        setStartingPoints(379, 172);
        return readValue(tableImage, images);
    }

    public void readLeftHoleCardPosition(BufferedImage image) {

    }

}
