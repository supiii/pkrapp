package com.masa.screenreader.card;

import com.masa.screenreader.AbstractImageReader;
import java.awt.image.BufferedImage;
import java.util.Map;
import mi.poker.common.model.testbed.klaatu.Card;

/**
 *
 * @author Matti
 */
public class AbstractCardImageReader extends AbstractImageReader<Card, Card> {
    
    @Override
    protected Card readValue(Map<Card, BufferedImage> imgs) {
        Card card = readImages(imgs); 
        if (card == null) {
            card = new Card(null, null);
        }
        return card;
    }
    
    @Override
    protected int getStartingPointX() {
        return startingPointX;
    };

}
