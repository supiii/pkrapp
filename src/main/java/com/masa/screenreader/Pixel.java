package com.masa.screenreader;
import java.awt.Color;

/**
 *
 * @author Matti
 */
public class Pixel {
    public int x;
    public int y;
    public Color lowColor;
    public Color hiColor;
    public boolean matches;
    
    public Pixel(int x, int y, Color lowColor, Color hiColor) {
        this.x = x;
        this.y = y;
        this.lowColor = lowColor;
        this.hiColor = hiColor;
        this.matches = true;
    }
    
    public Pixel(int x, int y, Color lowColor, Color hiColor, boolean matches) {
        this.x = x;
        this.y = y;
        this.lowColor = lowColor;
        this.hiColor = hiColor;
        this.matches = matches;
    }
}
