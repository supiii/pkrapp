package com.masa.screenreader;

import static com.masa.type.Globals.CARD_REFERENCE_FOLDER;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Matti
 */
public class ImageSaver {

    public ImageSaver() {
    }

    public static void saveImage(BufferedImage image, String filename) {
        // C:\Users\compuuter\sources\Pkrapp\src\test\resources\PS_new_skin\card_screenshots
        try {
            ImageIO.write(image, "png",
                    new File(CARD_REFERENCE_FOLDER + filename + ".png"));
        } catch (IOException ex) {
            Logger.getLogger(ImageSaver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
