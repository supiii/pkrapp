package com.masa.util;

import com.masa.type.BetSizeRatio;
import com.masa.type.BoardTextureRiver;
import com.masa.type.BoardTextureTurn;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author compuuter
 */
public class EVUtil {

    public static double bet(List<PostFlopRow> allRows, PostFlopRow current, Street currentStreet, Street street, BetSizeRatio betSizeRatio) {
        double ev = 0.0;

        if (currentStreet == street) {
            ev = betForNode(allRows, current, currentStreet, street, betSizeRatio);
        } else {
            // divide by street texture
            Map<Line, List<PostFlopRow>> mappedRows = mapRows(allRows, street);

            // iterate by street texture and calc ev
            for (Map.Entry<Line, List<PostFlopRow>> entry : mappedRows.entrySet()) {
                List<PostFlopRow> value = entry.getValue();
                ev = ev + betForNode(value, current, currentStreet, street, betSizeRatio);
            }
        }

        return ev;
    }

    /*
    * Heros first action on the street
     */
    public static double betForNode(List<PostFlopRow> allRows, PostFlopRow current, Street currentStreet, Street street, BetSizeRatio betSizeRatio) {
        double potCount = 0.0;
        double startPot = 0.0;
        double betSize = calculateBetSize(startPot, betSizeRatio);

        int wonWhenCalled = 0;
        int lostWhenCalled = 0;
        int villainFolded = 0;
        int villainCalled = 0;
        int villainRaised = 0;

        for (PostFlopRow row : allRows) {
            String villainCurrentActions = current.getVillainActions(currentStreet);
            String villainActions = row.getVillainActions(currentStreet);
            potCount = potCount + row.getPotByStreet(currentStreet);

            // Fold to cbet
            if (villainActions.equals(villainCurrentActions + "F")) {
                villainFolded++;
            } // Call cbet
            else if (villainActions.equals(villainCurrentActions + "C")) {
                villainCalled++;
                if (row.won) {
                    wonWhenCalled++;
                } else {
                    lostWhenCalled++;
                }
            }
        }

        startPot = potCount / allRows.size();
        double villainFoldedPercentage = villainFolded / (villainFolded + villainCalled + villainRaised);
        double villainCalledPercentage = villainCalled / (villainFolded + villainCalled + villainRaised);
        double equityWhenCalled = wonWhenCalled / (wonWhenCalled + lostWhenCalled);

        //EV = (%W * $187) – (%L * $125)
        double evWhenVillainCalls;
        if (currentStreet == street) {
            evWhenVillainCalls = (equityWhenCalled * (startPot + betSize)) - ((1 - equityWhenCalled) * betSize);
        } else {
            evWhenVillainCalls = (equityWhenCalled * (betSize)) - ((1 - equityWhenCalled) * betSize);
        }

        double evWhenVillainFolds = villainFoldedPercentage * startPot;

        double ev = evWhenVillainFolds + (villainCalledPercentage * evWhenVillainCalls);

        return ev;
    }

    public static double betCall(List<PostFlopRow> allRows, PostFlopRow current, Street currentStreet, Street street, BetSizeRatio betSizeRatio) {
        double potCount = 0.0;
        double startPot = 0.0;
        double betSize = calculateBetSize(startPot, betSizeRatio);

        double totalVillainRaise = 0.0;
        int villainRaisesCount = 0;
        for (PostFlopRow row : allRows) {
            if (row.getVillainActions(currentStreet).equals("R") || row.getVillainActions(currentStreet).equals("XR")) {
                totalVillainRaise = totalVillainRaise + row.getVillainRaiseMadeByStreet(currentStreet);
                villainRaisesCount++;
            }
        }

        double villainRaiseSize = totalVillainRaise / villainRaisesCount;
        int wonWhenCalled = 0;
        int lostWhenCalled = 0;
        int wonWhenRaised = 0;
        int lostWhenRaised = 0;
        int villainFolded = 0;
        int villainCalled = 0;
        int villainRaised = 0;

        for (PostFlopRow row : allRows) {
            String villainCurrentActions = current.getVillainActions(currentStreet);
            String villainActions = row.getVillainActions(currentStreet);
            potCount = potCount + row.getPotByStreet(currentStreet);

            // Fold to cbet
            if (villainActions.equals(villainCurrentActions + "F")) {
                villainFolded++;
            } // Call cbet
            else if (villainActions.equals(villainCurrentActions + "C")) {
                villainCalled++;
                if (row.won) {
                    wonWhenCalled++;
                } else {
                    lostWhenCalled++;
                }
            } // Raise cbet
            else if (villainActions.startsWith(villainCurrentActions + "R")) {
                villainRaised++;
                if (row.won) {
                    wonWhenRaised++;
                } else {
                    lostWhenRaised++;
                }
            }
        }

        double villainFoldedPercentage = villainFolded / (villainFolded + villainCalled + villainRaised);
        double villainCalledPercentage = villainCalled / (villainFolded + villainCalled + villainRaised);
        double villainRaisedPercentage = villainRaised / (villainFolded + villainCalled + villainRaised);

        double equityWhenCalled = wonWhenCalled / (wonWhenCalled + lostWhenCalled);
        double equityWhenRaised = wonWhenRaised / (wonWhenRaised + lostWhenRaised);

        //EV = (%W * $187) – (%L * $125)
        double evWhenVillainCalls;
        if (currentStreet == street) {
            evWhenVillainCalls = (equityWhenCalled * (startPot + betSize)) - ((1 - equityWhenCalled) * betSize);
        } else {
            evWhenVillainCalls = (equityWhenCalled * (betSize)) - ((1 - equityWhenCalled) * betSize);
        }
        double evWhenVillainFolds = villainFoldedPercentage * startPot;
        //double evWhenVillainRaises = (equityWhenRaised * (startPot + villainRaiseSize)) - ((1 - equityWhenRaised) * betSize);
        double evWhenVillainRaises;
        if (currentStreet == street) {
            evWhenVillainRaises = (equityWhenRaised * (startPot + villainRaiseSize)) - ((1 - equityWhenRaised) * betSize);
        } else {
            evWhenVillainRaises = (equityWhenRaised * (villainRaiseSize)) - ((1 - equityWhenRaised) * betSize);
        }

        double ev = (villainFoldedPercentage * evWhenVillainFolds) + (villainCalledPercentage * evWhenVillainCalls) + (villainRaisedPercentage + evWhenVillainRaises);

        return ev;
    }

    /*
    * Heros first action on the street
    TODO
     */
    public static double call(List<PostFlopRow> allRows, PostFlopRow current, Street currentStreet, BetSizeRatio betSizeRatio) {
        double potCount = 0.0;
        double startPot = 0.0;
        double betSize = calculateBetSize(startPot, betSizeRatio);

        int wonWhenCalled = 0;
        int lostWhenCalled = 0;
        int villainFolded = 0;
        int villainCalled = 0;
        int villainRaised = 0;

        for (PostFlopRow row : allRows) {
            String villainCurrentActions = current.getVillainActions(currentStreet);
            String villainActions = row.getVillainActions(currentStreet);
            potCount = potCount + row.getPotByStreet(currentStreet);

            // Fold to cbet
            if (villainActions.equals(villainCurrentActions + "F")) {
                villainFolded++;
            } // Call cbet
            else if (villainActions.equals(villainCurrentActions + "C")) {
                villainCalled++;
                if (row.won) {
                    wonWhenCalled++;
                } else {
                    lostWhenCalled++;
                }
            }
        }

        startPot = potCount / allRows.size();
        double villainFoldedPercentage = villainFolded / (villainFolded + villainCalled + villainRaised);
        double villainCalledPercentage = villainCalled / (villainFolded + villainCalled + villainRaised);
        double equityWhenCalled = wonWhenCalled / (wonWhenCalled + lostWhenCalled);

        //EV = (%W * $187) – (%L * $125)
        double evWhenVillainCalls = (equityWhenCalled * (startPot + betSize)) - ((1 - equityWhenCalled) * betSize);
        double evWhenVillainFolds = villainFoldedPercentage * startPot;

        double ev = (villainFoldedPercentage * evWhenVillainFolds) + (villainCalledPercentage * evWhenVillainCalls);

        return ev;
    }

    private static double calculateBetSize(double pot, BetSizeRatio betSizeRatio) {
        if (betSizeRatio == BetSizeRatio.ONE_THIRD) {
            return pot * 0.33;
        } else if (betSizeRatio == BetSizeRatio.TWO_THIRDS) {
            return pot * 0.66;
        } else if (betSizeRatio == BetSizeRatio.POT) {
            return pot;
        }

        return pot;
    }

    private static Map<Line, List<PostFlopRow>> mapRows(List<PostFlopRow> rows, Street street) {
        Map<Line, List<PostFlopRow>> linesMap = new HashMap<>();

        for (PostFlopRow row : rows) {

            Line line = null;
            if (street == Street.TURN) {
                line = new Line(row.boardTextureTurn, null);
            } else if (street == Street.RIVER) {
                line = new Line(row.boardTextureTurn, row.boardTextureRiver);
            }

            if (linesMap.containsKey(line)) {
                linesMap.get(line).add(row);
            } else {
                List<PostFlopRow> postFlopRows = new ArrayList<>();
                postFlopRows.add(row);
                linesMap.put(line, postFlopRows);
            }
        }

        return linesMap;
    }

    public static class Line {

        BoardTextureTurn turnTexture;
        BoardTextureRiver riverTexture;

        public Line(
                BoardTextureTurn turnTexture,
                BoardTextureRiver riverTexture
        ) {
            this.turnTexture = turnTexture;
            this.riverTexture = riverTexture;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 37 * hash + Objects.hashCode(this.turnTexture);
            hash = 37 * hash + Objects.hashCode(this.riverTexture);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Line other = (Line) obj;
            if (!Objects.equals(this.turnTexture, other.turnTexture)) {
                return false;
            }
            if (!Objects.equals(this.riverTexture, other.riverTexture)) {
                return false;
            }
            return true;
        }

    }

    public static double getBetOrCallAmount(PostFlopRow row, String actions, Street street) {
        if (actions.equals("B") || actions.equals("XB")) {
            return row.getBetMadeByStreet(street);
        } else if (actions.equals("R") || actions.equals("XR") || actions.equals("BR")) {
            return row.getRaiseMadeByStreet(street);
        } else if (actions.equals("C") || actions.equals("XC")) {
            return row.getVillainBetMadeByStreet(street);
        } else if (actions.equals("RR") || actions.equals("XRR") || actions.equals("BRR")) {
            return row.getRaise2MadeByStreet(street);
        } else if (actions.equals("BC") || actions.equals("RC") || actions.equals("XRC")) {
            return row.getVillainRaiseMadeByStreet(street);
        } else if (actions.equals("BRC")) {
            return row.getVillainRaise2MadeByStreet(street);
        }

        return 0.0;
    }

    public static double getNextBet(PostFlopRow row, Street currentStreet) {
        String action = "B";
        if (currentStreet == Street.FLOP) {
            if (row.getVillainActions(Street.FLOP).contains(action)) {
                return row.getVillainBetMadeByStreet(Street.FLOP);
            }
            if (row.getActions(Street.TURN).contains(action)) {
                return row.getBetMadeByStreet(Street.TURN);
            }
            if (row.getVillainActions(Street.TURN).contains(action)) {
                return row.getVillainBetMadeByStreet(Street.TURN);
            }
            if (row.getActions(Street.RIVER).contains(action)) {
                return row.getBetMadeByStreet(Street.RIVER);
            }
            if (row.getVillainActions(Street.RIVER).contains(action)) {
                return row.getVillainBetMadeByStreet(Street.RIVER);
            }
        } else if (currentStreet == Street.TURN) {
            if (row.getVillainActions(Street.TURN).contains(action)) {
                return row.getVillainBetMadeByStreet(Street.TURN);
            }
            if (row.getActions(Street.RIVER).contains(action)) {
                return row.getBetMadeByStreet(Street.RIVER);
            }
            if (row.getVillainActions(Street.RIVER).contains(action)) {
                return row.getVillainBetMadeByStreet(Street.RIVER);
            }
        } else if (currentStreet == Street.RIVER) {
            if (row.getVillainActions(Street.RIVER).contains(action)) {
                return row.getVillainBetMadeByStreet(Street.RIVER);
            }
        }

        return 0.0;
    }

    public static double getEVForBet(PostFlopRow row, String actions, Street street) {
        return row.wonBB + (row.getPotByStreet(street) * (EVUtil.getBetOrCallAmount(row, actions, street) / 100));
    }

    public static double getEVForCheck(List<PostFlopRow> allRows, PostFlopRow row, String actions, Street street) {
        // if only checks until SD
        if (LineFilter.allStreetsEquals(row, "X", street, true)) {
            int won = 0;
            int lost = 0;
            for (PostFlopRow allRow : allRows) {
                if (LineFilter.allStreetsEquals(allRow, "X", street, true)) {
                    if (allRow.won) {
                        won++;
                    } else {
                        lost++;
                    }
                }
            }
            double equity = won / (won + lost);

            return equity * row.getPotByStreet(street);
        }
        return row.wonBB + (getNextBet(row, street) / 100);
    }

    public static double getEVForCall(PostFlopRow row, String actions, Street street) {
        return row.wonBB + (EVUtil.getBetOrCallAmount(row, actions, street) / 100);
    }
}
