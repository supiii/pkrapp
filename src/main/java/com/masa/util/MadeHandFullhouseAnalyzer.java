package com.masa.util;

import com.masa.type.MadeHand;
import com.masa.type.MadeHandSpecific;
import static com.masa.util.HandStrengthAnalyzer.similarRanks;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class MadeHandFullhouseAnalyzer {

    public static MadeHandSpecific getSpecificFlullhouse(CardSet board, CardSet bestHand, CardSet holeCards) {
        //int nroOfHoleCardsOnBestHand = MadeHandUtil.countNroOfHandsInBestHand(bestHand, holeCards);
        CardSet allCards = new CardSet(board);
        allCards.addAll(holeCards);

        // fullhouse on board
        if (board.size() == 5 && MadeHandAnalyzer.getMadeHand(board) == MadeHand.FULL_HOUSE) {
            return whenFullhouseOnBoard(board, bestHand, holeCards);
        }
        // 3 of a kind on board
        if (MadeHandUtil.threeOfAKindOnBoard(board)) {
            return whenThreeofAKindOnBoard(board, bestHand, holeCards);
        }
        // two pair on board
        if (MadeHandUtil.twoPairOnBoard(board)) {
            return whenTwoPairsOnBoard(board, bestHand, holeCards);
        }
        // pair on board
        if (MadeHandUtil.pairOnBoard(board)) {
            return MadeHandSpecific.FULL_HOUSE;
        }
        return null;
    }

    private static MadeHandSpecific whenTwoPairsOnBoard(CardSet board, CardSet bestHand, CardSet holeCards) {
        boolean pp = MadeHandUtil.isPocketPair(holeCards);
        Rank holeCardsRank1 = holeCards.get(0).rankOf();
        Rank holeCardsRank2 = holeCards.get(1).rankOf();
        Rank highestRankOnBoard = MadeHandUtil.highestRank(board);
        Rank secondHighestRankOnBoard = MadeHandUtil.secondHighestRank(board);
        Rank thirdHighestRankOnBoard = MadeHandUtil.thirdHighestRank(board);

        if (board.size() == 4) {
            Rank highesConnectedHoleCardRank = null;
            if (similarRanks(holeCardsRank1, bestHand) == 3 && similarRanks(holeCardsRank2, bestHand) == 3) {
                highesConnectedHoleCardRank = holeCardsRank1.pipValue() > holeCardsRank2.pipValue() ? holeCardsRank1 : holeCardsRank2;
            } else {
                highesConnectedHoleCardRank = MadeHandUtil.getConnectedHoleCard(board, holeCards).rankOf();
            }
            if (highestRankOnBoard.pipValue() == highesConnectedHoleCardRank.pipValue()) {
                return MadeHandSpecific.FULL_HOUSE_TOP;
            } else if (secondHighestRankOnBoard.pipValue() == highesConnectedHoleCardRank.pipValue()) {
                return MadeHandSpecific.FULL_HOUSE_BOTTOM;
            }
        } else {
            Rank connectedHoleCardRank = MadeHandUtil.getConnectedHoleCard(board, holeCards).rankOf();
            Rank pairlessRankOnBoard = null;

            for (Card card : board) {
                if (similarRanks(card.rankOf(), board) == 1) {
                    pairlessRankOnBoard = card.rankOf();
                }
            }
            if (pp) {
                // FULL_HOUSE_STRONG
                // PP trips with highest rank
                if (pairlessRankOnBoard.pipValue() == highestRankOnBoard.pipValue()
                        && highestRankOnBoard.pipValue() == holeCardsRank1.pipValue()) {
                    return MadeHandSpecific.FULL_HOUSE_STRONG;
                }
                // FULL_HOUSE_OTHER
                if (pairlessRankOnBoard.pipValue() == secondHighestRankOnBoard.pipValue()
                        && secondHighestRankOnBoard.pipValue() == holeCardsRank1.pipValue()) {
                    return MadeHandSpecific.FULL_HOUSE_OTHER;
                }
                if (thirdHighestRankOnBoard != null
                        && pairlessRankOnBoard.pipValue() == thirdHighestRankOnBoard.pipValue()
                        && thirdHighestRankOnBoard.pipValue() == holeCardsRank1.pipValue()) {
                    return MadeHandSpecific.FULL_HOUSE_OTHER;
                }
            } else {
                // river and !pp
                Rank highesConnectedHoleCardRank = null;
                Rank higherBoardPairRank = null;
                Rank lowerBoardPairRank = null;

                for (Card card : board) {
                    if (similarRanks(card.rankOf(), board) == 2) {
                        if (higherBoardPairRank == null) {
                            higherBoardPairRank = card.rankOf();
                        } else {
                            if (card.rankOf().pipValue() > higherBoardPairRank.pipValue()) {
                                higherBoardPairRank = card.rankOf();
                            } else {
                                lowerBoardPairRank = card.rankOf();
                            }
                        }
                    }
                }

                if (higherBoardPairRank.pipValue() == holeCardsRank1.pipValue()
                        || higherBoardPairRank.pipValue() == holeCardsRank2.pipValue()) {
                    return MadeHandSpecific.FULL_HOUSE_TOP;
                }

                if (lowerBoardPairRank.pipValue() == holeCardsRank1.pipValue()
                        || lowerBoardPairRank.pipValue() == holeCardsRank2.pipValue()) {
                    return MadeHandSpecific.FULL_HOUSE_BOTTOM;
                }
            }

        }

        return null;
    }

    private static MadeHandSpecific whenThreeofAKindOnBoard(CardSet board, CardSet bestHand, CardSet holeCards) {
        boolean pp = MadeHandUtil.isPocketPair(holeCards);

        CardSet all = new CardSet(board);
        all.addAll(holeCards);
        Rank holeCardsRank1 = holeCards.get(0).rankOf();
        Rank holeCardsRank2 = holeCards.get(1).rankOf();
        Rank highestRankOnBoard = MadeHandUtil.highestRank(board);
        Rank highestRankOnAll = MadeHandUtil.highestRank(all);
        Rank highestRankOnBestHand = MadeHandUtil.highestRank(bestHand);
        Rank secondHighestRankOnBoard = MadeHandUtil.secondHighestRank(board);
        Rank thirdHighestRankOnBoard = MadeHandUtil.thirdHighestRank(board);
        Rank secondHighestRankOnAll = MadeHandUtil.secondHighestRank(all);
        Rank secondHighestRankOnBestHand = MadeHandUtil.secondHighestRank(bestHand);
        Rank tripsRank = null;
        for (Card card : board) {
            if (HandStrengthAnalyzer.similarRanks(card.rankOf(), board) == 3) {
                tripsRank = card.rankOf();
            }
        }

        if (pp) {
            if (board.size() == 3) {
                if (holeCardsRank1.equals(Rank.ACE)) {
                    return MadeHandSpecific.FULL_HOUSE_AA;
                } else if (holeCardsRank1.equals(Rank.KING)) {
                    return MadeHandSpecific.FULL_HOUSE_KK;
                } else if (holeCardsRank1.equals(Rank.QUEEN)) {
                    return MadeHandSpecific.FULL_HOUSE_QQ;
                } else if (holeCardsRank1.equals(Rank.JACK)) {
                    return MadeHandSpecific.FULL_HOUSE_JJ;
                } else if (holeCardsRank1.equals(Rank.TEN)) {
                    return MadeHandSpecific.FULL_HOUSE_TT;
                } else if (holeCardsRank1.pipValue() >= Rank.SIX.pipValue()
                        && holeCardsRank1.pipValue() <= Rank.NINE.pipValue()) {
                    return MadeHandSpecific.FULL_HOUSE_66_99;
                } else if (holeCardsRank1.pipValue() >= Rank.TWO.pipValue()
                        && holeCardsRank1.pipValue() <= Rank.FIVE.pipValue()) {
                    return MadeHandSpecific.FULL_HOUSE_22_55;
                }
            } else {

                // trips on board is highest rank
                if (highestRankOnBoard.pipValue() == tripsRank.pipValue()) {
                    // Overpair
                    if (holeCardsRank1.pipValue() > tripsRank.pipValue()) {
                        return MadeHandSpecific.FULL_HOUSE_OVERPAIR;
                    }
                    // PP below trips
                    if (secondHighestRankOnBestHand.pipValue() == holeCardsRank1.pipValue()) {
                        return MadeHandSpecific.FULL_HOUSE_PP_BELOW_TRIPS;
                    }
                    return MadeHandSpecific.FULL_HOUSE_WEAK_PAIR;
                } else {
                    // trips on board is NOT the highest rank on board
                    // Set
                    for (Card card : holeCards) {
                        if (HandStrengthAnalyzer.similarRanks(card.rankOf(), bestHand) == 3) {
                            if (card.rankOf().pipValue() > tripsRank.pipValue()) {
                                return MadeHandSpecific.FULL_HOUSE_SET;
                            } else {
                                return MadeHandSpecific.FULL_HOUSE_WEAK_PAIR;
                            }
                        }
                    }
                    // Overpair KK JJJQ9
                    if (highestRankOnAll.pipValue() == holeCardsRank1.pipValue()) {
                        return MadeHandSpecific.FULL_HOUSE_OVERPAIR;
                    }
                    // PP below TP QQ JJJK9
                    if (secondHighestRankOnAll.pipValue() == holeCardsRank1.pipValue()) {
                        return MadeHandSpecific.FULL_HOUSE_PP_BELOW_TP;
                    }
                    return MadeHandSpecific.FULL_HOUSE_WEAK_PAIR;
                }

            }
        } else {
            Rank highesConnectedHoleCardRank = null;
            if (similarRanks(holeCardsRank1, bestHand) == 2 && similarRanks(holeCardsRank2, bestHand) == 2) {
                highesConnectedHoleCardRank = holeCardsRank1.pipValue() > holeCardsRank2.pipValue() ? holeCardsRank1 : holeCardsRank2;
            } else {
                highesConnectedHoleCardRank = MadeHandUtil.getConnectedHoleCard(board, holeCards).rankOf();
            }
            // connected holeCard is highest rank on board
            //return FULL_HOUSE_TP
            if (highestRankOnBoard.pipValue() == highesConnectedHoleCardRank.pipValue()) {
                return MadeHandSpecific.FULL_HOUSE_TP;
            }

            // connected holeCard is second highest rank on board
            //return FULL_HOUSE_MIDDLE_PAIR
            if (secondHighestRankOnBoard == highesConnectedHoleCardRank) {
                return MadeHandSpecific.FULL_HOUSE_MIDDLE_PAIR;
            }

            // connected holeCard is third highest rank on board
            //return FULL_HOUSE_WEAK_PAIR
            if (thirdHighestRankOnBoard != null && thirdHighestRankOnBoard == highesConnectedHoleCardRank) {
                return MadeHandSpecific.FULL_HOUSE_WEAK_PAIR;
            }
        }

        return null;
    }

    private static MadeHandSpecific whenFullhouseOnBoard(CardSet board, CardSet bestHand, CardSet holeCards) {
        Rank tripsRank = null;
        Rank pairRank = null;

        if (!MadeHandUtil.isPocketPair(holeCards)) {
            return MadeHandSpecific.NO_MADE_HAND;
        }

        for (Card card : board) {
            if (HandStrengthAnalyzer.similarRanks(card.rankOf(), board) == 3) {
                tripsRank = card.rankOf();
            }
            if (HandStrengthAnalyzer.similarRanks(card.rankOf(), board) == 2) {
                pairRank = card.rankOf();
            }
        }

        Rank holeCardsRank = holeCards.get(0).rankOf();

        // Trips on board higher rank than pair
        if (tripsRank.pipValue() > pairRank.pipValue()) {
            // Overpair
            if (holeCardsRank.pipValue() > tripsRank.pipValue()) {
                return MadeHandSpecific.FULL_HOUSE_OVERPAIR;
            }
            // Underpair
            if (holeCardsRank.pipValue() > pairRank.pipValue()) {
                return MadeHandSpecific.FULL_HOUSE_UNDERPAIR;
            }
        }
        // Pair on board higher rank than trips
        return MadeHandSpecific.NO_MADE_HAND;
    }
}
