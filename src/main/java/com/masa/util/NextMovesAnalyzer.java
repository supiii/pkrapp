package com.masa.util;

import com.masa.pkrapp.App;
import com.masa.pokertracker.model.PTActionType;
import com.masa.service.NextPostFlopMovesService;
import com.masa.txt.service.PostFlopRowService;
import com.masa.type.BoardTextureFlop;
import com.masa.type.BoardTextureRiver;
import com.masa.type.BoardTextureTurn;
import com.masa.type.NextPostFlopMoves;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

public class NextMovesAnalyzer {

    public static Map<String, NextPostFlopMoves> getNextPostFlopMoves() {
        PostFlopRow current = createCurrentPostFlopRow();
        long startWonLoss = System.currentTimeMillis();

        Map<String, List<PostFlopRow>> rowsMap = readPostFlopRowsTextFile(current);

        /*if (rows == null || rows.isEmpty()) {
            return nextMoves;
        }*/
        long endWonLoss = System.currentTimeMillis();
        System.out.println("readPostFlopRowsTextFile Time: " + (endWonLoss - startWonLoss));

        Map<String, NextPostFlopMoves> nextMovesMap = new HashMap<>();

        for (Map.Entry<String, List<PostFlopRow>> entry : rowsMap.entrySet()) {
            NextPostFlopMoves nextMoves = new NextPostFlopMoves();
            String key = entry.getKey();
            List<PostFlopRow> value = entry.getValue();
            divideToNextMoves(current, value, nextMoves);
            nextMovesMap.put(key, nextMoves);
        }

        return nextMovesMap;
    }

    private static Map<String, List<PostFlopRow>> readPostFlopRowsTextFile(PostFlopRow current) {
        PostFlopRowService postFlopService = new PostFlopRowService();
        try {
            return postFlopService.readAndFilterWithBasicInfo(current, Util.getStreet());
        } catch (IOException ex) {
            Logger.getLogger(NextPostFlopMovesService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void divideToNextMoves(PostFlopRow current, List<PostFlopRow> rows, NextPostFlopMoves nextMoves) {
        Street currentStreet = Util.getStreet();
        String foldAction = current.getActions(currentStreet) + PTActionType.FOLD.value();
        String checkAction = current.getActions(currentStreet) + PTActionType.CHECK.value();
        String callAction = current.getActions(currentStreet) + PTActionType.CALL.value();
        String betAction = current.getActions(currentStreet) + PTActionType.BET.value();
        String raiseAction = current.getActions(currentStreet) + PTActionType.RAISE.value();

        for (PostFlopRow row : rows) {

            String rowActions = row.getActions(currentStreet);
            //System.out.println("Divide actions, current street: " + rowActions + " " + row.wonBB);
            /*if (rowActions.startsWith(foldAction)) {
                nextMoves.getFoldMoves().add(row.wonBB);
            } else */
            if (rowActions.startsWith(checkAction)) {
                double EV = EVUtil.getEVForCheck(rows, row, callAction, currentStreet);
                nextMoves.getCheckMoves().add(EV);
            } else if (rowActions.startsWith(callAction)) {
                double EV = EVUtil.getEVForCall(row, callAction, currentStreet);
                nextMoves.getCallMoves().add(EV);
            } else if (rowActions.startsWith(betAction)) {
                double EV = EVUtil.getEVForBet(row, betAction, currentStreet);
                nextMoves.getRaiseMoves().add(EV);
            } else if (rowActions.startsWith(raiseAction)) {
                double EV = EVUtil.getEVForBet(row, raiseAction, currentStreet);
                nextMoves.getRaiseMoves().add(EV);
            }
        }
    }

    private static PostFlopRow createCurrentPostFlopRow() {
        PostFlopRow current = new PostFlopRow();

        // basic
        current.position = App.heroPos;
        current.villainPosition = App.villainPos;
        current.actionsPreflop = App.actionsHeroPreFlop;
        current.actionsFlop = App.actionsHeroFlop;
        current.actionsTurn = App.actionsHeroTurn;
        current.actionsRiver = App.actionsHeroRiver;
        current.actionsVillainPreflop = App.actionsVillainPreFlop;
        current.actionsVillainFlop = App.actionsVillainFlop;
        current.actionsVillainTurn = App.actionsVillainTurn;
        current.actionsVillainRiver = App.actionsVillainRiver;

        if (Util.isFlop()) {
            currentFlop(current);
            System.out.println("");
        } else if (Util.isTurn()) {
            currentFlop(current);
            currentTurn(current);
            System.out.println("");
        } else if (Util.isRiver()) {
            currentFlop(current);
            currentTurn(current);
            currentRiver(current);
        }

        return current;
    }

    private static void currentRiver(PostFlopRow current) {
        CardSet boardRiver = new CardSet(
                new Card(App.firstFlopCard),
                new Card(App.secondFlopCard),
                new Card(App.thirdFlopCard),
                new Card(App.turnCard),
                new Card(App.riverCard));
        current.madeHandSpecificRiver = MadeHandAnalyzer.getMadeHandSpecific(boardRiver, App.holeCards);
        //current.drawsTurn = DrawAnalyzer.getDraws(boardRiver, App.holeCards, null);
        //Util.mapDraws(current, current.drawsTurn, Street.TURN);
        BoardTextureAnalyzer.analyzeRiver(current, boardRiver, new Card(App.riverCard));
        current.boardTextureRiver = new BoardTextureRiver(current);
    }

    private static void currentTurn(PostFlopRow current) {
        CardSet boardTurn = new CardSet(new Card(App.firstFlopCard), new Card(App.secondFlopCard), new Card(App.thirdFlopCard), new Card(App.turnCard));
        current.madeHandSpecificTurn = MadeHandAnalyzer.getMadeHandSpecific(boardTurn, App.holeCards);
        current.drawsTurn = DrawAnalyzer.getDraws(boardTurn, App.holeCards, null);
        Util.mapDraws(current, current.drawsTurn, Street.TURN);
        BoardTextureAnalyzer.analyzeTurn(current, boardTurn, new Card(App.turnCard));
        current.boardTextureTurn = new BoardTextureTurn(current);
    }

    private static void currentFlop(PostFlopRow current) {
        CardSet boardFlop = new CardSet(new Card(App.firstFlopCard), new Card(App.secondFlopCard), new Card(App.thirdFlopCard));
        //current.flopBucket = FlopBucketAnalyzer.getFlopBucket(App.board);
        //current.flopSuitness = FlopSuitnessAnalyzer.getFlopSuitness(App.board);
        current.madeHandSpecificFlop = MadeHandAnalyzer.getMadeHandSpecific(App.board, App.holeCards);
        current.drawsFlop = DrawAnalyzer.getDraws(boardFlop, App.holeCards, null);
        Util.mapDraws(current, current.drawsFlop, Street.FLOP);
        BoardTextureAnalyzer.analyzeFlop(current, boardFlop);
        current.boardTextureFlop = new BoardTextureFlop(current);
    }
}
