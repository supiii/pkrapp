package com.masa.util;

import com.masa.type.BetSizeRatio;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.List;

/**
 *
 * @author compuuter
 */
public class EVService {

    public static double ev(List<PostFlopRow> allRows, Street currentStreet, PostFlopRow current) {
        double finalEV = 0.0;
        // TODO
        boolean heroIsAggressor = true;

        if (heroIsAggressor) {
            finalEV = evWhenHeroAggressor(allRows, currentStreet, current);
        } else {

        }

        return finalEV;
    }

    public static double evWhenHeroAggressor(List<PostFlopRow> allRows, Street currentStreet, PostFlopRow current) {
        double finalEV = 0.0;

        double evAggressive = 0.0;
        double evSemiAggressive = 0.0;
        double evPassive = 0.0;

        double evBetBetBet = 0.0;

        if (currentStreet == Street.FLOP) {
            evBetBetBet = EVUtil.bet(allRows, current, currentStreet, Street.FLOP, BetSizeRatio.ONE_THIRD);
            evBetBetBet = evBetBetBet + EVUtil.bet(allRows, current, currentStreet, Street.TURN, BetSizeRatio.ONE_THIRD);
            evBetBetBet = evBetBetBet + EVUtil.bet(allRows, current, currentStreet, Street.RIVER, BetSizeRatio.ONE_THIRD);
        } else if (currentStreet == Street.TURN) {
            evBetBetBet = evBetBetBet + EVUtil.bet(allRows, current, currentStreet, Street.TURN, BetSizeRatio.ONE_THIRD);
            evBetBetBet = evBetBetBet + EVUtil.bet(allRows, current, currentStreet, Street.RIVER, BetSizeRatio.ONE_THIRD);
        }
        if (currentStreet == Street.RIVER) {
            evBetBetBet = evBetBetBet + EVUtil.bet(allRows, current, currentStreet, Street.RIVER, BetSizeRatio.ONE_THIRD);
        }

        System.out.println("evBetBetBet: " + evBetBetBet);

        return finalEV;
    }

    private static double betBetBet(List<PostFlopRow> rows, Street currentStreet, PostFlopRow current, boolean ip) {
        double ev = 0.0;

        // filter rows that match B|B|B XC|XC|XC or B|-|- XF|-|- or B|B|- XC|XF|- or B|B|B XC|XC|XF
        // IP
        // FLOP
        double evBetBetBet = EVUtil.bet(rows, current, currentStreet, Street.FLOP, BetSizeRatio.ONE_THIRD);

        return ev;
    }

    private static double betMadeAvg(List<PostFlopRow> allRows, Street currentStreet) {
        double betSize = 0.0;
        for (PostFlopRow allRow : allRows) {
            betSize = betSize + allRow.getBetMadeByStreet(currentStreet);
        }
        return betSize / allRows.size();
    }
}
