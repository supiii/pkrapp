package com.masa.util;

import com.masa.type.FlopBucket;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class FlopBucketAnalyzer {

    public static int JACK = Rank.JACK.pipValue();
    public static int TEN = Rank.TEN.pipValue();
    public static int NINE = Rank.NINE.pipValue();
    public static int EIGHT = Rank.EIGHT.pipValue();
    public static int SEVEN = Rank.SEVEN.pipValue();
    public static int SIX = Rank.SIX.pipValue();
    public static int FIVE = Rank.FIVE.pipValue();

    public static FlopBucket getFlopBucket(CardSet boardCardSet) {
        FlopBucket flopBucket = null;
        boardCardSet.sortHighestToLowest();

        int r1 = boardCardSet.get(0).rankOf().pipValue();
        int r2 = boardCardSet.get(1).rankOf().pipValue();
        int r3 = boardCardSet.get(2).rankOf().pipValue();

        if (r1 == 9 && r2 == 6 && r3 == 3) {
            //System.out.println("DEBUG");
        }

        boolean paired = !everyRankDiffers(r1, r2, r3) && !triples(r1, r2, r3);
        boolean triples = triples(r1, r2, r3);

        // > = <   >=  <=
        if (!paired && !triples) {
            if (r3 > NINE) {
                flopBucket = FlopBucket.HI_CONNECTED;
            } else if (r1 >= TEN && r2 >= TEN && r3 <= NINE
                    && !(r1 == JACK && r2 == TEN && (r3 == NINE || r3 == EIGHT))) {
                flopBucket = FlopBucket.TWO_HI_CARDS;
            } else if (r1 >= JACK && r2 <= NINE && r2 >= SEVEN && connected(r2, r3)) {
                flopBucket = FlopBucket.ONE_HI_TWO_MIDDLE_CONNECTED;
            } else if (r1 >= JACK && r2 <= SIX && connected(r2, r3)) {
                flopBucket = FlopBucket.ONE_HI_TWO_LOW_CONNECTED;
            } else if (r1 >= JACK && !connectedThree(r1, r2, r3) && !connected(r1, r2) && !connected(r2, r3)) {
                flopBucket = FlopBucket.ONE_HI_NOT_CONNECTED;
            } else if (r1 <= JACK && r2 >= SIX && connected(r1, r2) && r3 <= SIX) {
                flopBucket = FlopBucket.TWO_MID_ONE_LOW;
            } else if (r1 <= JACK && r1 >= EIGHT && r2 <= FIVE && connected(r2, r3)) {
                flopBucket = FlopBucket.ONE_MID_TWO_LOW_CONNECTED;
            } else if (r1 <= JACK && r1 >= SEVEN && connectedThree(r1, r2, r3)) {
                flopBucket = FlopBucket.MID_CONNECTED;
            } else if (r1 <= SEVEN && r1 >= SIX && connectedThree(r1, r2, r3)) {
                flopBucket = FlopBucket.MID_LOW_CONNECTED;
            } else if (r1 <= FIVE) {
                flopBucket = FlopBucket.LOW_CONNECTED;
            } else if (r1 <= TEN && !connectedThree(r1, r2, r3)) {
                flopBucket = FlopBucket.MID_LOW_NOT_CONNECTED;
            }
        } else if (paired) {
            flopBucket = FlopBucket.PAIRED;
        } else if (triples) {
            flopBucket = FlopBucket.TRIPLES;
        }

        if (flopBucket == null) {
            System.out.println("flopBucket is null " + boardCardSet);
        }

        return flopBucket;
    }

    private static boolean connected(int r1, int r2) {
        int r2OrAce = r2;

        if (r2 == 14) {
            r2OrAce = 1;
        }

        int diff = r1 - r2OrAce;

        return diff <= 2;
    }

    private static boolean connectedThree(int r1, int r2, int r3) {
        int r3OrAce = r3;

        if (r1 == 11 && r2 == 10 && r3 == 8) {
            System.out.println("DEBUG");
        }

        if (r3 == 14) {
            r3OrAce = 1;
        }

        int diff = (r1 - r2) + (r2 - r3OrAce);

        int max = 4;

        if (r1 - r2 == 1 || r2 - r3OrAce == 1) {
            max = 3;
        }

        return diff <= max;
    }

    private static boolean everyRankDiffers(int r1, int r2, int r3) {
        return r1 != r2 && r1 != r3 && r2 != r3;
    }

    private static boolean triples(int r1, int r2, int r3) {
        return r1 == r2 && r1 == r3 && r2 == r3;
    }
}
