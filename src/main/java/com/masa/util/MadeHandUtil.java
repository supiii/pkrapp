package com.masa.util;

import static com.masa.util.HandStrengthAnalyzer.similarRanks;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author compuuter
 */
public class MadeHandUtil {

    public static Rank highestRank(CardSet cards) {
        List<Card> sortedList = new ArrayList<>(cards);
        Collections.sort(sortedList);
        return sortedList.get(sortedList.size() - 1).rankOf();
    }

    public static Rank highestRankWithinSuit(CardSet cards, Suit suit) {
        List<Card> sortedList = new ArrayList<>();
        for (Card card : cards) {
            if (card.suitOf() == suit) {
                sortedList.add(card);
            }
        }
        Collections.sort(sortedList);
        return sortedList.get(sortedList.size() - 1).rankOf();
    }

    public static Rank secondHighestRank(CardSet cards) {
        Set<Rank> sortedSet = new HashSet<>();
        for (Card card : cards) {
            sortedSet.add(card.rankOf());
        }
        List<Rank> sortedList = new ArrayList<>(sortedSet);
        Collections.sort(sortedList);
        if (sortedList.size() < 2) {
            return null;
        }
        return sortedList.get(sortedList.size() - 2);
    }

    public static Rank thirdHighestRank(CardSet cards) {
        Set<Rank> sortedSet = new HashSet<>();
        for (Card card : cards) {
            sortedSet.add(card.rankOf());
        }
        List<Rank> sortedList = new ArrayList<>(sortedSet);
        Collections.sort(sortedList);
        if (sortedList.size() < 3) {
            return null;
        }
        return sortedList.get(sortedList.size() - 3);
    }

    public static boolean hasRank(Rank rank, CardSet cards) {
        for (Card card : cards) {
            if (card.rankOf().pipValue() == rank.pipValue()) {
                return true;
            }
        }
        return false;
    }

    public static boolean twoPairOnBoard(CardSet board) {
        int nroOfPairs = 0;
        //Rank checkedRank = null;
        List<Rank> checked = new ArrayList<>();
        for (Card card : board) {
            if (similarRanks(card.rankOf(), board) == 2 && !checked.contains(card.rankOf())) {
                //checkedRank = card.rankOf();
                checked.add(card.rankOf());
                nroOfPairs++;
            }
        }
        return nroOfPairs == 2;
    }

    public static boolean pairOnBoard(CardSet board) {
        int nroOfPairs = 0;
        for (Card card : board) {
            int nroOfRanks = similarRanks(card.rankOf(), board);
            if (nroOfRanks > 2) {
                return false;
            }
            if (nroOfRanks == 2) {
                nroOfPairs++;
            }
        }
        return nroOfPairs == 2;
    }

    public static boolean threeOfAKindOnBoard(CardSet board) {
        for (Card card : board) {
            if (similarRanks(card.rankOf(), board) == 3) {
                return true;
            }
        }
        return false;
    }

    public static boolean fourOfAKindOnBoard(CardSet board) {
        for (Card card : board) {
            if (similarRanks(card.rankOf(), board) == 4) {
                return true;
            }
        }
        return false;
    }

    public static int countRankConnections(CardSet cards, CardSet holeCards) {
        CardSet allCards = new CardSet(cards);
        allCards.addAll(holeCards);
        int connections = 0;
        for (Card holeCard : holeCards) {
            if (similarRanks(holeCard.rankOf(), allCards) > 1) {
                connections++;
            }
        }
        return connections;
    }

    public static boolean isPocketPair(Rank leftCardRank, Rank rightCardRank) {
        return leftCardRank.equals(rightCardRank);
    }

    public static boolean isPocketPair(CardSet holeCards) {
        return holeCards.get(0).rankOf().equals(holeCards.get(1).rankOf());
    }

    public static Card getConnectedHoleCard(CardSet fiveBestCards, CardSet holeCards) {
        return similarRanks(holeCards.get(0).rankOf(), fiveBestCards) > 0 ? holeCards.get(0) : holeCards.get(1);
    }

    public static Card getNotConnectedHoleCard(Card connectedHoleCard, CardSet holeCards) {
        return holeCards.get(0).equals(connectedHoleCard) ? holeCards.get(1) : holeCards.get(0);
    }

    public static int countNroOfHandsInBestHand(CardSet bestHand, CardSet holeCards) {
        int count = 0;
        for (Card holeCard : holeCards) {
            if (bestHand.contains(holeCard)) {
                count++;
            }
        }
        return count;
    }

    public static boolean allCardsDifferentRanks(CardSet cards) {
        for (Rank rank : Rank.values()) {
            if (similarRanks(rank, cards) > 1) {
                return true;
            }
        }
        return false;
    }
}
