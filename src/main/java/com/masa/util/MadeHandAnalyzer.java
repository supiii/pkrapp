package com.masa.util;

import com.masa.type.MadeHand;
import com.masa.type.MadeHandSpecific;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class MadeHandAnalyzer {

    //NO_PAIR, PAIR, TWO_PAIR, THREE_OF_A_KIND, STRAIGHT,
    //FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH;
    private static final int PAIR_LOW = 16908302; //83886279
    private static final int PAIR_HI = 17698304;
    private static final int TWO_PAIR_LOW = 36831236;
    private static final int TWO_PAIR_HI = 49087488;
    private static final int THREE_OF_A_KIND_LOW = 50462726;
    private static final int THREE_OF_A_KIND_HI = 51252224;
    private static final int STRAIGHT_LOW = 67436544;
    private static final int STRAIGHT_HI = 68026368;
    private static final int FLUSH_LOW = 83890199; // 83886279
    private static final int FLUSH_HI = 83893888;
    private static final int FULL_HOUSE_LOW = 100794370;
    private static final int FULL_HOUSE_HI = 101582848;
    private static final int FOUR_OF_A_KIND_LOW = 117571586;
    private static final int FOUR_OF_A_KIND_HI = 118360064;
    private static final int STRAIGHT_FLUSH_LOW = 134610944;
    private static final int STRAIGHT_FLUSH_HI = 135135232;

    public static MadeHand getMadeHand(CardSet cardSet) {
        return getMadeHand(HandEval.handEval(cardSet));
    }

    private static MadeHand getMadeHand(int handValue) {
        if (handValue < PAIR_LOW) {
            return MadeHand.NO_MADE_HAND;
        } else if (handValue <= PAIR_HI) {
            return MadeHand.PAIR;
        } else if (handValue <= TWO_PAIR_HI) {
            return MadeHand.TWO_PAIR;
        } else if (handValue <= THREE_OF_A_KIND_HI) {
            return MadeHand.THREE_OF_A_KIND;
        } else if (handValue <= STRAIGHT_HI) {
            return MadeHand.STRAIGHT;
        } else if (handValue <= FLUSH_HI) {
            return MadeHand.FLUSH;
        } else if (handValue <= FULL_HOUSE_HI) {
            return MadeHand.FULL_HOUSE;
        } else if (handValue <= FOUR_OF_A_KIND_HI) {
            return MadeHand.FOUR_OF_A_KIND;
        } else if (handValue <= STRAIGHT_FLUSH_HI) {
            return MadeHand.STRAIGHT_FLUSH;
        }

        return null;
    }

    public static CardSet getFiveBestCards(CardSet board, CardSet holeCards) {
        CardSet all = new CardSet(board);
        all.addAll(holeCards);
        if (holeCards.get(0) == null) {
            System.out.println("DEBUG YYYYYYYYYYYYYYYYY");
        }
        int highestValue = HandEval.handEval(all);
        // Flop
        if (board.size() == 3) {
            return all;
        }

        // Turn
        if (board.size() == 4 && board.get(3) != null) {
            for (int i = 0; i < 6; i++) {
                /*System.out.println("-----");
                System.out.println("holecards " + holeCards);
                System.out.println("board " + board);
                System.out.println("all " + all);*/

                CardSet turn = new CardSet(all);
                turn.remove(all.get(i));
                //System.out.println("all after " + all);
                //System.out.println("turn agter " + turn);
                if (HandEval.handEval(turn) == highestValue) {
                    return turn;
                }
            }
        }
        // River
        if (board.size() == 5 && board.get(4) != null) {
            CardSet turn = null;
            for (int i = 0; i < 7; i++) {
                for (int j = 0; j < 7; j++) {
                    if (i == j) {
                        continue;
                    }
                    turn = new CardSet(all);
                    //System.out.println(board);
                    //System.out.println(turn);
                    turn.remove(all.get(i));
                    turn.remove(all.get(j));
                    int handValue = HandEval.handEval(turn);
                    if (handValue == highestValue) {
                        // if straight, check if board makes the best straight and return it
                        if (getMadeHand(handValue) == MadeHand.STRAIGHT && HandEval.handEval(board) == highestValue) {
                            return board;
                        }
                        return turn;
                    }

                }
            }
        }
        return null;
    }

    public static MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        CardSet all = new CardSet(board);
        all.addAll(holeCards);

        // 8d7d
        /* CardSet cs = new CardSet(new Card("8d"), new Card("7d"));
        if (holeCards.containsAll(cs)) {
            System.out.println("DEBUG jhdgdgdfsdadsdpdj");
        }*/
        CardSet fiveBestCards = getFiveBestCards(board, holeCards);

        MadeHand madeHand = getMadeHand(all);

        switch (madeHand) {
            case NO_MADE_HAND:
                return getSpecificNoMadeHand(holeCards);
            case PAIR:
                return MadeHandPairAnalyzer.getSpecificPair(board, fiveBestCards, holeCards);
            case TWO_PAIR:
                return MadeHandTwoPairAnalyzer.getSpecificTwoPair(board, fiveBestCards, holeCards);
            case THREE_OF_A_KIND:
                return MadeHandSetAnalyzer.getSpecificSet(board, fiveBestCards, holeCards);
            case STRAIGHT:
                return MadeHandStraightAnalyzer.getSpecificStraight(board, fiveBestCards, holeCards);
            case FLUSH:
                return MadeHandFlushAnalyzer.getSpecificFlush(board, fiveBestCards, holeCards);
            case FULL_HOUSE:
                return MadeHandFullhouseAnalyzer.getSpecificFlullhouse(board, fiveBestCards, holeCards);
            case FOUR_OF_A_KIND:
                return MadeHandQuadsAnalyzer.getSpecificQuads(board, fiveBestCards, holeCards);
            case STRAIGHT_FLUSH:
                return MadeHandStraightFlushAnalyzer.getSpecificStraightFlush(board, fiveBestCards, holeCards);
            default:
                break;
        }

        return null;
    }

    private static MadeHandSpecific getSpecificNoMadeHand(CardSet holeCards) {
        if (holeCards.containsRank(Rank.ACE)) {
            return MadeHandSpecific.ACE_HIGH;
        }
        if (holeCards.containsRank(Rank.KING)) {
            return MadeHandSpecific.KING_HIGH;
        }
        if (holeCards.containsRank(Rank.QUEEN)) {
            return MadeHandSpecific.QUEEN_HIGH;
        }
        if (holeCards.containsRank(Rank.JACK)) {
            return MadeHandSpecific.JACK_HIGH;
        }
        if (holeCards.containsRank(Rank.TEN) || holeCards.containsRank(Rank.NINE) || holeCards.containsRank(Rank.EIGHT) || holeCards.containsRank(Rank.SEVEN)) {
            return MadeHandSpecific.MEDIUM_HIGH;
        }
        return MadeHandSpecific.LOW_HIGH;
    }
}
