package com.masa.util;

import com.masa.type.Draw;
import com.masa.type.MadeHand;
import java.util.HashSet;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author compuuter
 */
public class DrawAnalyzer {

    public static Set<Draw> getDraws(CardSet board, CardSet holeCards, CardSet deadCards) {
        Set<Draw> draws = new HashSet<>();
        if (board.size() > 4) {
            return draws;
        }
        CardSet flopAndHoleCards = new CardSet(board);
        flopAndHoleCards.addAll(holeCards);

        CardSet deck = CardSet.freshDeck();
        deck.removeAll(flopAndHoleCards);
        if (deadCards != null && !deadCards.isEmpty()) {
            deck.removeAll(deadCards);
        }

        int straightOuts = 0;

        for (Card card : deck) {
            CardSet allCards = new CardSet(flopAndHoleCards);

            MadeHand madeHandOnFlop = MadeHandAnalyzer.getMadeHand(flopAndHoleCards);
            allCards.add(card);
            MadeHand madeHand = MadeHandAnalyzer.getMadeHand(allCards);
            /*if (madeHandOnFlop != MadeHand.FLUSH && madeHand == MadeHand.FLUSH) {
                draws.add(Draw.FLUSH_DRAW);
            }*/
            if (madeHandOnFlop != MadeHand.STRAIGHT && madeHand == MadeHand.STRAIGHT) {
                straightOuts++;
            }
        }

        // Removed 26.7.2023 to try to get better results
        /*if (straightOuts > 0 && straightOuts < 5) {
            draws.add(Draw.Gutshot);
        }*/
        if (straightOuts > 4) {
            draws.add(Draw.OESD);
        }

        checkFlushDraws(board, holeCards, draws);

        return draws;
    }

    private static void checkFlushDraws(CardSet board, CardSet holeCards, Set<Draw> draws) {
        boolean holeCardsSuited = holeCards.get(0).suitOf() == holeCards.get(1).suitOf();
        Suit holeCardSuit1 = holeCards.get(0).suitOf();
        Suit holeCardSuit2 = holeCards.get(1).suitOf();

        // Overcards
        if (!MadeHandUtil.isPocketPair(holeCards)) {
            Rank highestRankOnBoard = MadeHandUtil.highestRank(board);
            if (holeCards.get(0).rankOf().pipValue() > highestRankOnBoard.pipValue()
                    && holeCards.get(1).rankOf().pipValue() > highestRankOnBoard.pipValue()) {
                draws.add(Draw.OVERCARDS);
            }
        }

        if (holeCardsSuited) {
            boolean twoSameSuitsOnBoard = false;
            for (Card card : board) {
                if (card.suitOf() == holeCardSuit1 && HandStrengthAnalyzer.similarSuits(card.suitOf(), board) == 2) {
                    twoSameSuitsOnBoard = true;
                    break;
                }
            }
            if (twoSameSuitsOnBoard) {
                draws.add(Draw.FLUSH_DRAW);
                return;
            }
            // FLUSH_BACKDOOR
            /*if (board.size() == 3) {
                boolean oneSameSuitsOnBoard = false;
                for (Card card : board) {
                    if (HandStrengthAnalyzer.similarSuits(card.suitOf(), board) == 1) {
                        oneSameSuitsOnBoard = true;
                        break;
                    }
                }
                if (oneSameSuitsOnBoard) {
                    draws.add(Draw.FLUSH_DRAW_BACKDOOR);
                }
            }*/
        } else {
            Card holeCardWithSameSuit = null;
            boolean threeSameSuitsOnBoard = false;
            Suit suit = null;
            for (Card card : board) {
                if ((card.suitOf() == holeCardSuit1 || card.suitOf() == holeCardSuit2)
                        && HandStrengthAnalyzer.similarSuits(card.suitOf(), board) == 3) {
                    threeSameSuitsOnBoard = true;
                    suit = card.suitOf();
                    if (holeCardSuit1 == suit) {
                        holeCardWithSameSuit = holeCards.get(0);
                    }
                    if (holeCardSuit2 == suit) {
                        holeCardWithSameSuit = holeCards.get(1);
                    }
                    break;
                }
            }
            if (threeSameSuitsOnBoard && holeCardWithSameSuit != null) {
                CardSet suitedCards = CardSet.freshDeck(suit);
                suitedCards.removeAll(board.getAllWithSuit(suit));

                if (MadeHandUtil.highestRank(suitedCards).pipValue() == holeCardWithSameSuit.rankOf().pipValue()) {
                    draws.add(Draw.FLUSH_DRAW_NUT);
                    return;
                }
                /*if (MadeHandUtil.secondHighestRank(suitedCards).pipValue() == holeCardWithSameSuit.rankOf().pipValue()) {
                    draws.add(Draw.FLUSH_DRAW_2ND_NUT);
                    return;
                }
                if (MadeHandUtil.thirdHighestRank(suitedCards).pipValue() == holeCardWithSameSuit.rankOf().pipValue()) {
                    draws.add(Draw.FLUSH_DRAW_3RD_NUT);
                    return;
                }*/
                draws.add(Draw.FLUSH_DRAW_WEAK);
            }
        }
    }

}
