package com.masa.util;

import com.masa.type.BoardTextureFlop;
import com.masa.type.BoardTextureRiver;
import com.masa.type.BoardTextureTurn;
import com.masa.type.MadeHand;
import com.masa.type.PostFlopRow;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author compuuter
 */
public class BoardTextureAnalyzer {

    public static void analyzeFlop(PostFlopRow row, CardSet boardCardSet) {
        boardCardSet.sortHighestToLowest();

        int r1 = boardCardSet.get(0).rankOf().pipValue();
        int r2 = boardCardSet.get(1).rankOf().pipValue();
        int r3 = boardCardSet.get(2).rankOf().pipValue();

        boolean paired = !everyRankDiffers(r1, r2, r3) && !triples(r1, r2, r3);
        boolean triples = triples(r1, r2, r3);

        int nroOfSameRanksFlop = 1;
        if (paired) {
            nroOfSameRanksFlop = 2;
        } else if (triples) {
            nroOfSameRanksFlop = 3;
        }
        row.aceFlop = boardCardSet.containsRank(Rank.ACE);
        row.kingFlop = boardCardSet.containsRank(Rank.KING);
        row.queenFlop = boardCardSet.containsRank(Rank.QUEEN);
        row.jackFlop = boardCardSet.containsRank(Rank.JACK);
        row.nroOfSameRanksFlop = nroOfSameRanksFlop;
        row.nroOfSameSuitsFlop = highestNumberOfSameSuits(boardCardSet);
        row.straightPossibleFlop = straightPossible(boardCardSet);
        row.ranksAddedValueCategoryId = getRanksAddedValueCategoryId(r1 + r2 + r3);

        if (everyRankDiffers(r1, r2, r3)) {
            int between = (r1 - r2 - r3);
            row.noRanksOrOneRankBetweenFlop = between == 2 || between == 3;
            row.twoOrThreeRanksBetweenFlop = between == 4 || between == 5;
        } else {
            int between = (r1 - r3);
            row.noRanksOrOneRankBetweenFlop = between == 1 || between == 2;
            row.twoOrThreeRanksBetweenFlop = between == 3 || between == 4;
        }

        row.boardTextureFlop = new BoardTextureFlop(row);
    }

    private static int getRanksAddedValueCategoryId(int addedValue) {
        // 6-42
        if (addedValue < 18) {
            return 1;
        } else if (addedValue < 30) {
            return 2;
        } else {
            return 3;
        }
    }

    public static void analyzeTurn(PostFlopRow row, CardSet turnCardSet, Card turnCard) {
        CardSet boardCardSet = new CardSet(turnCardSet);
        boardCardSet.sortHighestToLowest();

        boolean straightPossible = straightPossible(boardCardSet);
        boolean straightPossibleWithOneCard = straightPossible && straightPossibleWithOneCardTurn(boardCardSet);
        boolean straightPossibleWithTwoCards = straightPossible && straightPossibleWithTwoCardsTurn(boardCardSet);

        Rank secondHighestRank = secondHighestRank(boardCardSet);
        Rank thirdHighestRank = thirdHighestRank(boardCardSet);

        row.aceTurn = turnCard.rankOf() == Rank.ACE;
        row.kingTurn = turnCard.rankOf() == Rank.KING;
        row.turnCardIsHighest = highestRank(boardCardSet) == turnCard.rankOf();
        row.turnCardIsSecondHighest = secondHighestRank != null && secondHighestRank == turnCard.rankOf();
        row.turnCardIsThirdHighest = thirdHighestRank != null && thirdHighestRank == turnCard.rankOf();
        row.boardHasPairTurn = countPairs(boardCardSet) == 1;
        row.boardHasTwoPairsTurn = countPairs(boardCardSet) == 2;
        row.boardHasTriplesTurn = countTriples(boardCardSet) == 1;
        row.nroOfSameSuitsTurn = highestNumberOfSameSuits(boardCardSet);
        row.straightPossibleWithOneCardTurn = straightPossibleWithOneCard;
        row.straightPossibleWithTwoCardsTurn = straightPossibleWithTwoCards;

        row.boardTextureTurn = new BoardTextureTurn(row);
    }

    public static void analyzeRiver(PostFlopRow row, CardSet riverCardSet, Card riverCard) {
        CardSet boardCardSet = new CardSet(riverCardSet);
        boardCardSet.sortHighestToLowest();

        boolean straightPossible = straightPossible(boardCardSet);
        boolean straightPossibleWithOneCard = straightPossible && straightPossibleWithOneCardTurn(boardCardSet);
        boolean straightPossibleWithTwoCards = straightPossible && straightPossibleWithTwoCardsTurn(boardCardSet);

        MadeHand madeHand = MadeHandAnalyzer.getMadeHand(boardCardSet);
        Rank secondHighestRank = secondHighestRank(boardCardSet);
        Rank thirdHighestRank = thirdHighestRank(boardCardSet);

        row.aceRiver = riverCard.rankOf() == Rank.ACE;
        row.kingRiver = riverCard.rankOf() == Rank.KING;
        row.riverCardIsHighest = highestRank(boardCardSet) == riverCard.rankOf();
        row.riverCardIsSecondHighest = secondHighestRank != null && secondHighestRank == riverCard.rankOf();
        row.riverCardIsThirdeHighest = thirdHighestRank != null && thirdHighestRank == riverCard.rankOf();
        row.nroOfSameSuitsRiver = highestNumberOfSameSuits(boardCardSet);
        row.boardHasPairRiver = madeHand == MadeHand.PAIR;
        row.boardHasTwoPairsRiver = madeHand == MadeHand.TWO_PAIR;
        row.boardHasTriplesRiver = madeHand == MadeHand.THREE_OF_A_KIND;
        row.boardHasFullhouseRiver = madeHand == MadeHand.FULL_HOUSE;
        row.straightPossibleWithOneCardRiver = straightPossibleWithOneCard;
        row.straightPossibleWithTwoCardsRiver = straightPossibleWithTwoCards;
        row.straightOnBoardRiver = madeHand == MadeHand.STRAIGHT;

        row.boardTextureRiver = new BoardTextureRiver(row);
    }

    private static int countPairs(CardSet boardCardSet) {
        Set<Rank> pairRanks = new HashSet<>();
        for (Card card : boardCardSet) {
            Rank rank = card.rankOf();
            int count = countRanks(rank, boardCardSet);
            if (count == 2) {
                pairRanks.add(rank);
            }
        }
        return pairRanks.size();
    }

    private static int countTriples(CardSet boardCardSet) {
        Set<Rank> tripleRanks = new HashSet<>();
        for (Card card : boardCardSet) {
            Rank rank = card.rankOf();
            int count = countRanks(rank, boardCardSet);
            if (count == 3) {
                tripleRanks.add(rank);
            }
        }
        return tripleRanks.size();
    }

    private static int highestNumberOfSameRanks(CardSet boardCardSet) {
        int highestNumberOfSameRanks = 1;
        for (Card card : boardCardSet) {
            Rank rank = card.rankOf();
            int count = countRanks(rank, boardCardSet);
            if (count > highestNumberOfSameRanks) {
                highestNumberOfSameRanks = count;
            }
        }
        return highestNumberOfSameRanks;
    }

    public static boolean straightPossible(CardSet board) {
        Set<CardSet> possibleHands = CardUtil.getPossibleHandsAsCardSets();

        for (CardSet possibleHand : possibleHands) {
            CardSet boardAndHoleCards = new CardSet(board);
            boardAndHoleCards.addAll(possibleHand);

            MadeHand madeHandOnFlop = MadeHandAnalyzer.getMadeHand(boardAndHoleCards);

            if (madeHandOnFlop == MadeHand.STRAIGHT) {
                return true;
            }

        }

        return false;
    }

    public static boolean straightPossibleWithOneCardTurn(CardSet board) {
        Set<CardSet> possibleHands = CardUtil.getPossibleHandsAsCardSets();

        for (CardSet possibleHand : possibleHands) {
            CardSet boardAndHoleCard1 = new CardSet(board);
            boardAndHoleCard1.add(possibleHand.get(0));
            MadeHand madeHandOnFlop = MadeHandAnalyzer.getMadeHand(boardAndHoleCard1);
            if (madeHandOnFlop == MadeHand.STRAIGHT) {
                return true;
            }

            CardSet boardAndHoleCard2 = new CardSet(board);
            boardAndHoleCard2.add(possibleHand.get(1));
            MadeHand madeHandOnFlop2 = MadeHandAnalyzer.getMadeHand(boardAndHoleCard2);
            if (madeHandOnFlop2 == MadeHand.STRAIGHT) {
                return true;
            }
        }

        return false;
    }

    public static boolean straightPossibleWithTwoCardsTurn(CardSet board) {
        Set<CardSet> possibleHands = CardUtil.getPossibleHandsAsCardSets();
        Set<CardSet> boards = new HashSet<>();
        // 4th missing
        boards.add(new CardSet(board.get(0), board.get(1), board.get(2)));
        // 3rd missing
        boards.add(new CardSet(board.get(0), board.get(1), board.get(3)));
        // 2nd missing
        boards.add(new CardSet(board.get(0), board.get(2), board.get(3)));
        // 1st missing
        boards.add(new CardSet(board.get(1), board.get(2), board.get(3)));

        for (CardSet boardVariant : boards) {
            for (CardSet possibleHand : possibleHands) {
                CardSet boardAndHoleCards = new CardSet(boardVariant);
                boardAndHoleCards.addAll(possibleHand);

                MadeHand madeHandOnFlop = MadeHandAnalyzer.getMadeHand(boardAndHoleCards);

                if (madeHandOnFlop == MadeHand.STRAIGHT) {
                    return true;
                }
            }
        }

        return false;
    }

    public static Rank highestRank(CardSet cardSet) {
        Rank highestRank = null;

        for (Card card : cardSet) {
            if (highestRank == null) {
                highestRank = card.rankOf();
                continue;
            }
            if (card.rankOf().pipValue() > highestRank.pipValue()) {
                highestRank = card.rankOf();
            }
        }

        return highestRank;
    }

    public static Rank highestSingleRank(CardSet cardSet) {
        CardSet cardSetSingles = removePairsAndAbove(cardSet);
        Rank highestRank = null;

        for (Card card : cardSetSingles) {
            if (highestRank == null) {
                highestRank = card.rankOf();
                continue;
            }
            if (card.rankOf().pipValue() > highestRank.pipValue()) {
                highestRank = card.rankOf();
            }
        }

        return highestRank;
    }

    public static Rank lowestRank(CardSet cardSet) {
        CardSet cardSetSingles = leaveSingleRanks(cardSet);
        Rank lowestRank = null;

        for (Card card : cardSetSingles) {
            if (lowestRank == null) {
                lowestRank = card.rankOf();
                continue;
            }
            if (card.rankOf().pipValue() < lowestRank.pipValue()) {
                lowestRank = card.rankOf();
            }
        }

        return lowestRank;
    }

    public static Rank lowestSingleRank(CardSet cardSet) {
        CardSet cardSetSingles = removePairsAndAbove(cardSet);
        Rank lowestRank = null;

        for (Card card : cardSetSingles) {
            if (countRanks(card.rankOf(), cardSet) > 1) {
                continue;
            }
            if (lowestRank == null) {
                lowestRank = card.rankOf();
                break;
            }
            if (card.rankOf().pipValue() < lowestRank.pipValue()) {
                lowestRank = card.rankOf();
            }
        }

        return lowestRank;
    }

    public static Rank secondHighestRank(CardSet cardSet) {
        CardSet cardSetSingles = leaveSingleRanks(cardSet);
        List<Card> cardsSorted = cardSetSingles.stream().collect(Collectors.toList());
        Collections.sort(cardsSorted, (c1, c2) -> c1.rankOf().compareTo(c2.rankOf()));

//        System.out.println(cardsSorted);
        if (cardsSorted.size() < 2) {
            return null;
        }
        return cardsSorted.get(cardsSorted.size() - 2).rankOf();
    }

    public static Rank secondHighestSingleRank(CardSet cardSet) {
        CardSet cardSetSingles = removePairsAndAbove(cardSet);

        List<Card> cardsSorted = cardSetSingles.stream().collect(Collectors.toList());
        Collections.sort(cardsSorted, (c1, c2) -> c1.rankOf().compareTo(c2.rankOf()));

        if (cardsSorted.size() < 2) {
            return null;
        }

//        System.out.println(cardsSorted);
        return cardsSorted.get(cardsSorted.size() - 2).rankOf();
    }

    public static Rank thirdHighestRank(CardSet cardSet) {
        CardSet cardSetSingles = leaveSingleRanks(cardSet);
        List<Card> cardsSorted = cardSetSingles.stream().collect(Collectors.toList());
        Collections.sort(cardsSorted, (c1, c2) -> c1.rankOf().compareTo(c2.rankOf()));

        if (cardsSorted.size() < 3) {
            return null;
        }

        return cardsSorted.get(cardsSorted.size() - 3).rankOf();
    }

    private static CardSet leaveSingleRanks(CardSet cardSet) {
        CardSet cardSetSingles = new CardSet();
        for (Card card : cardSet) {
            if (cardSetSingles.containsRank(card.rankOf()) == false) {
                cardSetSingles.add(card);
            }
        }
        return cardSetSingles;
    }

    private static CardSet removePairsAndAbove(CardSet cardSet) {
        CardSet cardSetSingles = new CardSet();
        for (Card card : cardSet) {
            if (countRanks(card.rankOf(), cardSet) < 2) {
                cardSetSingles.add(card);
            }
        }
        return cardSetSingles;
    }

    private static int highestNumberOfSameSuits(CardSet boardCardSet) {
        int highestNro = 0;
        for (Suit suit : Suit.values()) {
            int similarSuits = similarSuits(suit, boardCardSet);
            if (similarSuits > highestNro) {
                highestNro = similarSuits;
            }
        }
        return highestNro;
    }

    private static int similarSuits(Suit suit, Collection<Card> cards) {
        int times = 0;
        for (Card card : cards) {
            if (suit == card.suitOf()) {
                times++;
            }
        }
        return times;
    }

    private static boolean everyRankDiffers(int r1, int r2, int r3) {
        return r1 != r2 && r1 != r3 && r2 != r3;
    }

    private static boolean triples(int r1, int r2, int r3) {
        return r1 == r2 && r1 == r3 && r2 == r3;
    }

    public static int countRanks(Rank rank, CardSet cards) {
        int times = 0;
        for (Card card : cards) {
            if (rank.equals(card.rankOf())) {
                times++;
            }
        }
        return times;
    }
}
