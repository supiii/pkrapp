package com.masa.util;

import com.masa.type.MadeHandSpecific;
import static com.masa.util.HandStrengthAnalyzer.similarRanks;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class MadeHandPairAnalyzer {

    public static MadeHandSpecific getSpecificPair(CardSet board, CardSet fiveBestHand, CardSet holeCards) {
        int nroOfHoleCardsInBestHand = MadeHandUtil.countNroOfHandsInBestHand(fiveBestHand, holeCards);

        if (nroOfHoleCardsInBestHand == 1) {
            return getSpecificPairOneCardOnBestHand(board, fiveBestHand, holeCards);
        }
        if (nroOfHoleCardsInBestHand == 2) {
            return getSpecificPairTwoCardsOnBestHand(board, fiveBestHand, holeCards);
        }

        return MadeHandUtil.hasRank(Rank.ACE, holeCards) ? MadeHandSpecific.ACE_HIGH : MadeHandSpecific.NO_MADE_HAND;
    }

    public static MadeHandSpecific getSpecificPairOneCardOnBestHand(CardSet board, CardSet fiveBestHand, CardSet holeCards) {
        Card connectedHoleCard = MadeHandUtil.getConnectedHoleCard(board, holeCards);
        int nroOfHoleCardsConnected = MadeHandUtil.countRankConnections(board, holeCards);

        if (nroOfHoleCardsConnected == 0) {
            return connectedHoleCard.rankOf() == Rank.ACE ? MadeHandSpecific.ACE_HIGH : MadeHandSpecific.NO_MADE_HAND;
        } else {
            if (isTopPairOneCardOnBestHand(board, connectedHoleCard)) {
                return MadeHandSpecific.TOP_PAIR;
            }
            if (isMiddlePairOneCardOnBestHand(board, connectedHoleCard)) {
                return MadeHandSpecific.MIDDLE_PAIR;
            }
            return MadeHandSpecific.WEAK_PAIR;
        }
    }

    private static MadeHandSpecific getSpecificPairTwoCardsOnBestHand(CardSet board, CardSet fiveBestHand, CardSet holeCards) {
        Card connectedHoleCard = MadeHandUtil.getConnectedHoleCard(board, holeCards);
        Rank connectedHoleCardRank = connectedHoleCard.rankOf();
        int nroOfHoleCardsConnected = MadeHandUtil.countRankConnections(board, holeCards);

        if (nroOfHoleCardsConnected == 0 && !MadeHandUtil.isPocketPair(holeCards)) {
            return MadeHandUtil.hasRank(Rank.ACE, holeCards) ? MadeHandSpecific.ACE_HIGH : MadeHandSpecific.NO_MADE_HAND;
        }
        if (nroOfHoleCardsConnected == 1) {
            if (isTopPairOneCardOnBestHand(board, connectedHoleCard)) {

                if (connectedHoleCardRank == Rank.ACE) {
                    return MadeHandSpecific.ACE_HIGH;
                }
                if (connectedHoleCardRank == Rank.KING) {
                    return MadeHandSpecific.KING_HIGH;
                }
                if (connectedHoleCardRank == Rank.QUEEN) {
                    return MadeHandSpecific.QUEEN_HIGH;
                }
                if (connectedHoleCardRank == Rank.JACK) {
                    return MadeHandSpecific.JACK_HIGH;
                }
                if (connectedHoleCardRank == Rank.TEN || connectedHoleCardRank == Rank.NINE || connectedHoleCardRank == Rank.EIGHT || connectedHoleCardRank == Rank.SEVEN) {
                    return MadeHandSpecific.MEDIUM_HIGH;
                }
                return MadeHandSpecific.LOW_HIGH;
            }
            if (isMiddlePairOneCardOnBestHand(board, connectedHoleCard)) {
                return MadeHandSpecific.MIDDLE_PAIR;
            }
            return MadeHandSpecific.WEAK_PAIR;
        }
        if (MadeHandUtil.isPocketPair(holeCards)) {
            Rank leftRank = holeCards.get(0).rankOf();
            Rank rightRank = holeCards.get(1).rankOf();
            if (isOverPair(board, leftRank, rightRank)) {
                return MadeHandSpecific.OVERPAIR;
            }
            if (isPPBelowTP(board, leftRank, rightRank)) {
                return MadeHandSpecific.PP_BELOW_TP;
            }
            return MadeHandSpecific.WEAK_PAIR;
        }

        System.out.println("DEBUG kjh7777555");

        return null;
    }

    private static boolean isOverPair(CardSet cards, Rank leftCardRank, Rank rightCardRank) {
        if (leftCardRank.equals(rightCardRank)) {
            for (Card card : cards) {
                if (leftCardRank.pipValue() < card.rankOf().pipValue()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static boolean isTopPairOneCardOnBestHand(CardSet cards, Card holeCard) {
        if (similarRanks(holeCard.rankOf(), cards) > 0) {
            if (MadeHandUtil.highestRank(cards).equals(holeCard.rankOf())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMiddlePairOneCardOnBestHand(CardSet cards, Card holeCard) {
        if (similarRanks(holeCard.rankOf(), cards) > 0) {
            if (MadeHandUtil.secondHighestRank(cards).equals(holeCard.rankOf())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isPPBelowTP(CardSet cards, Rank leftCardRank, Rank rightCardRank) {
        if (leftCardRank.pipValue() > MadeHandUtil.secondHighestRank(cards).pipValue()) {
            return true;
        }

        return false;
    }

}
