package com.masa.util;

//import com.masa.type.StreetData;
import com.masa.type.HUDStat;
import com.masa.type.NLLimit;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matti
 */
public class HUDStatUtil {

    //
    /**
     * VPIP 20, 2000 hands AVG VPIP 57 (2000*20+50*57) / 2050 (2000*5+10000*-8)
     * / 12000
     *
     * @param hand
     * @param seat
     * @param hudStat
     * @return
     */
    /*public static Double getHUDStatFrequency(Hand hand, Seat seat, HUDStat hudStat) {
        double average = hand.getDefaultHudStats().get(hudStat);
        Map<HUDStat, Double> hudStatsBySeat = hand.getHudStatsBySeat(seat);

        if (hudStatsBySeat == null) {
            return average;
        }

        double playerValue = hudStatsBySeat.get(hudStat);
        Double hands = hand.getHudStatsBySeat(seat).get(HUDStat.HANDS);

        if (hands == null) {
            return average;
        }
        return compensateHudStatDouble(playerValue, average, hands);
        //return (hands * playerValue + 50 * average) / (hands + 50);
    }*/
    public static int getHUDStatCompensated(Map<HUDStat, Double> hudStats, Map<HUDStat, Double> defaultHUDStats, HUDStat hudStat, Double hands) {

        if (!hudStats.containsKey(hudStat)) {
            return 0;
        }
        return HUDStatUtil.compensateHudStat(hudStats.get(hudStat), defaultHUDStats.get(hudStat), hands);
    }

    public static int compensateHudStat(double value, double averageValue, double hands) {
        if (hands == 0) {
            return (int) averageValue;
        }

        return (int) ((hands * value + 50 * averageValue) / (hands + 50));
    }

    public static Double compensateHudStatDouble(double value, double averageValue, double hands) {
        if (hands == 0) {
            return averageValue;
        }

        return ((hands * value + 50 * averageValue) / (hands + 50));
    }

    public static boolean isPreflopHudStat(HUDStat hudStat) {
        return hudStat.ordinal() > 0 && hudStat.ordinal() <= HUDStat.PREFLOP_FOLD_TO_5BET.ordinal();
    }

    public static boolean isFlopHudStat(HUDStat hudStat) {
        return hudStat.ordinal() > HUDStat.FLOP_BET.ordinal() && hudStat.ordinal() <= HUDStat.FLOP_FOLD_TO_4BET.ordinal();
    }

    public static boolean isTurnHudStat(HUDStat hudStat) {
        return hudStat.ordinal() > HUDStat.TURN_BET.ordinal() && hudStat.ordinal() <= HUDStat.TURN_FOLD_FLOAT_BET.ordinal();
    }

    public static boolean isRiverHudStat(HUDStat hudStat) {
        return hudStat.ordinal() > HUDStat.RIVER_BET.ordinal() && hudStat.ordinal() <= HUDStat.RIVER_FLOAT_BET.ordinal();
    }

    public static double minHandsRequired(HUDStat hudStat) {
        if (isPreflopHudStat(hudStat)) {
            if (hudStat == HUDStat.PREFLOP_VPIP || hudStat == HUDStat.PREFLOP_PFR) {
                return 100.0;
            }
            return 500.0;
        }
        if (isFlopHudStat(hudStat)) {
            return 1200.0;
        }
        if (isTurnHudStat(hudStat)) {
            return 2400.0;
        }
        if (isRiverHudStat(hudStat)) {
            return 3600.0;
        }
        return 1.0;
    }

//    private static Seat getPreviousStreetLastAggressor(Hand hand) {
//        return hand.getPreviousStreetData().getAggressors().get(hand.getPreviousStreetData().getAggressors().size() - 1).getSeat();
//    }
    public static Map<HUDStat, Double> getDefaultHUDStats(NLLimit limit) {
        Map<HUDStat, Double> stats = new HashMap<>();

        if (limit == NLLimit.NL5 || limit == NLLimit.NL10) {

            stats.put(HUDStat.PREFLOP_VPIP, 24.00);
            stats.put(HUDStat.PREFLOP_PFR, 17.25);
            stats.put(HUDStat.PREFLOP_ATT_TO_STEAL, 32.58);
            stats.put(HUDStat.PREFLOP_ATT_TO_STEAL_LP, 30.82);
            stats.put(HUDStat.PREFLOP_ATT_TO_STEAL_UTG, 15.91);
            stats.put(HUDStat.PREFLOP_ATT_TO_STEAL_UTG1, 18.51);
            stats.put(HUDStat.PREFLOP_ATT_TO_STEAL_CO, 24.64);
            stats.put(HUDStat.PREFLOP_ATT_TO_STEAL_BTN, 39.06);
            stats.put(HUDStat.PREFLOP_ATT_TO_STEAL_SB, 39.84);
            stats.put(HUDStat.PREFLOP_FOLD_TO_STEAL, 73.35);
            stats.put(HUDStat.PREFLOP_3BET_STEAL, 8.70);
            stats.put(HUDStat.PREFLOP_3BET_VS_UTG_OR_UTG1_OPEN, 4.42);
            stats.put(HUDStat.PREFLOP_3BET_VS_LP_STEAL, 8.38);
            stats.put(HUDStat.PREFLOP_3BET_VS_SB_STEAL, 9.15);
            stats.put(HUDStat.PREFLOP_3BET, 6.34);
            stats.put(HUDStat.PREFLOP_FOLD_TO_3BET, 72.68);
            stats.put(HUDStat.PREFLOP_4BET, 5.44);
            stats.put(HUDStat.PREFLOP_FOLD_TO_4BET, 56.65);
            stats.put(HUDStat.PREFLOP_5BET, 13.6);
            stats.put(HUDStat.PREFLOP_FOLD_TO_5BET, 18.71);

            stats.put(HUDStat.FLOP_BET, 35.91);
            stats.put(HUDStat.FLOP_FOLD_TO_BET, 54.89);
            stats.put(HUDStat.FLOP_DONK_BET, 8.55);
            stats.put(HUDStat.FLOP_FOLD_TO_DONK_BET, 38.19);
            stats.put(HUDStat.FLOP_RAISE_DONK_BET, 19.27);
            stats.put(HUDStat.FLOP_CBET, 57.03);
            stats.put(HUDStat.FLOP_FOLD_TO_CBET, 51.55);
            stats.put(HUDStat.FLOP_CBET_IN_3BET_POT, 67.63);
            stats.put(HUDStat.FLOP_FOLD_TO_CBET_IN_3BET_POT, 46.36);
            stats.put(HUDStat.FLOP_RAISE_CBET, 8.21);
            stats.put(HUDStat.FLOP_2BET, 8.48);
            stats.put(HUDStat.FLOP_FOLD_TO_2BET, 51.38);
            stats.put(HUDStat.FLOP_3BET, 10.77);
            stats.put(HUDStat.FLOP_FOLD_TO_3BET, 29.77);
            stats.put(HUDStat.FLOP_4BET, 33.27);
            stats.put(HUDStat.FLOP_FOLD_TO_4BET, 12.42);

            stats.put(HUDStat.TURN_BET, 35.11);
            stats.put(HUDStat.TURN_FOLD_TO_BET, 52.25);
            stats.put(HUDStat.TURN_2BET, 8.02);
            stats.put(HUDStat.TURN_FOLD_TO_2BET, 48.37);
            stats.put(HUDStat.TURN_DONK_BET, 5.13);
            stats.put(HUDStat.TURN_CBET, 52.29);
            stats.put(HUDStat.TURN_FOLD_TO_CBET, 38.97);
            stats.put(HUDStat.TURN_RAISE_CBET, 11.41);
            stats.put(HUDStat.TURN_FOLD_TO_RAISE_CBET, 40.49);
            stats.put(HUDStat.TURN_3BET, 10.44);
            stats.put(HUDStat.TURN_4BET, 32.38);
            stats.put(HUDStat.TURN_FLOAT_BET, 51.25);
            stats.put(HUDStat.TURN_FOLD_FLOAT_BET, 55.62);

            stats.put(HUDStat.RIVER_BET, 29.37);
            stats.put(HUDStat.RIVER_FOLD_TO_BET, 60.56);
            stats.put(HUDStat.RIVER_2BET, 7.17);
            stats.put(HUDStat.RIVER_FOLD_TO_2BET, 51.22);
            stats.put(HUDStat.RIVER_CBET, 49.69);
            stats.put(HUDStat.RIVER_FOLD_TO_CBET, 46.69);
            stats.put(HUDStat.RIVER_DONK_BET, 10.37);
            stats.put(HUDStat.RIVER_FLOAT_BET, 43.20);

        }

        return stats;
    }

    public static Map<HUDStat, Double> getIdealHUDStatsSmallStakes() {
        Map<HUDStat, Double> stats = new HashMap<>();

        stats.put(HUDStat.PREFLOP_VPIP, 24.00);
        stats.put(HUDStat.PREFLOP_PFR, 18.00);
        stats.put(HUDStat.PREFLOP_ATT_TO_STEAL, 35.00);
        stats.put(HUDStat.PREFLOP_FOLD_TO_STEAL, 70.00);
        stats.put(HUDStat.PREFLOP_3BET, 8.00);
        stats.put(HUDStat.PREFLOP_FOLD_TO_3BET, 65.00);
        stats.put(HUDStat.PREFLOP_4BET, 5.44);
        stats.put(HUDStat.PREFLOP_FOLD_TO_4BET, 56.65);

        stats.put(HUDStat.FLOP_CBET, 64.00);
        stats.put(HUDStat.FLOP_FOLD_TO_CBET, 50.00);
        stats.put(HUDStat.FLOP_RAISE_CBET, 8.21);
        stats.put(HUDStat.FLOP_FOLD_TO_2BET, 51.38);

        stats.put(HUDStat.TURN_FOLD_TO_2BET, 48.37);
        stats.put(HUDStat.TURN_CBET, 50.00);
        stats.put(HUDStat.TURN_FOLD_TO_CBET, 40.00);
        stats.put(HUDStat.TURN_RAISE_CBET, 11.41);

        stats.put(HUDStat.RIVER_2BET, 7.17);
        stats.put(HUDStat.RIVER_FOLD_TO_2BET, 51.22);
        stats.put(HUDStat.RIVER_CBET, 50.00);
        stats.put(HUDStat.RIVER_FOLD_TO_CBET, 40.00);
        stats.put(HUDStat.RIVER_FLOAT_BET, 43.20);

        stats.put(HUDStat.WTSD, 30.00);

        return stats;
    }
}
