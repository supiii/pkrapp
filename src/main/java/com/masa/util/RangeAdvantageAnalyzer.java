package com.masa.util;

import static com.masa.pkrapp.App.firstFlopCard;
import static com.masa.pkrapp.App.holeCardLeft;
import static com.masa.pkrapp.App.holeCardRight;
import static com.masa.pkrapp.App.rangeEquityLabelFlop;
import static com.masa.pkrapp.App.rangeEquityLabelRiver;
import static com.masa.pkrapp.App.rangeEquityLabelTurn;
import static com.masa.pkrapp.App.ranges;
import static com.masa.pkrapp.App.rawEquityLabelFlop;
import static com.masa.pkrapp.App.rawEquityLabelRiver;
import static com.masa.pkrapp.App.rawEquityLabelTurn;
import static com.masa.pkrapp.App.riverCard;
import static com.masa.pkrapp.App.secondFlopCard;
import static com.masa.pkrapp.App.thirdFlopCard;
import static com.masa.pkrapp.App.turnCard;
import com.masa.type.FiveTypeValue;
import com.masa.type.Street;

/**
 *
 * @author compuuter
 */
public class RangeAdvantageAnalyzer {

    public static FiveTypeValue calculateRawEquity() {
        Street street = Util.getStreet();
        String board = firstFlopCard + secondFlopCard + thirdFlopCard;
        if (street == Street.TURN) {
            board = board + turnCard;
        } else if (street == Street.RIVER) {
            board = board + turnCard + riverCard;
        }

        double equityValue = EquityHandler.calculateEquityMonteCarlo(holeCardLeft + holeCardRight, ranges.villainRangeString, board);
        //double handEquity = EquityHandler.calculateEquityMonteCarlo(holeCardLeft + holeCardRight, ranges.villainRangeString, firstFlopCard + secondFlopCard + thirdFlopCard);

        switch (street) {
            case FLOP:
                rawEquityLabelFlop.setText("" + (int) (equityValue * 100.0));
                break;
            case TURN:
                rawEquityLabelTurn.setText("" + (int) (equityValue * 100.0));
                break;
            case RIVER:
                rawEquityLabelRiver.setText("" + (int) (equityValue * 100.0));
                break;
            default:
                break;
        }

        FiveTypeValue rangeEquity = null;

        if (equityValue < 0.42) {
            rangeEquity = FiveTypeValue.VERY_BAD;
        } else if (equityValue < 0.48) {
            rangeEquity = FiveTypeValue.BAD;
        } else if (equityValue < 0.52) {
            rangeEquity = FiveTypeValue.MEDIUM;
        } else if (equityValue < 0.56) {
            rangeEquity = FiveTypeValue.GOOD;
        } else {
            rangeEquity = FiveTypeValue.VERY_GOOD;
        }

        System.out.println("RangeEquity: " + equityValue + " " + rangeEquity);

        return rangeEquity;
    }

    public static FiveTypeValue calculateRangeEquityAndSetLabels() {
        Street street = Util.getStreet();
        String board = firstFlopCard + secondFlopCard + thirdFlopCard;

        if (street == Street.TURN) {
            board = board + turnCard;
        } else if (street == Street.RIVER) {
            board = board + turnCard + riverCard;
        }
        double rangeEquityValue = EquityHandler.calculateEquityMonteCarlo(ranges.heroRangeString, ranges.villainRangeString, board);
        //double handEquity = EquityHandler.calculateEquityMonteCarlo(holeCardLeft + holeCardRight, ranges.villainRangeString, firstFlopCard + secondFlopCard + thirdFlopCard);
        switch (street) {
            case FLOP:
                rangeEquityLabelFlop.setText("" + (int) (rangeEquityValue * 100.0));
                break;
            case TURN:
                rangeEquityLabelTurn.setText("" + (int) (rangeEquityValue * 100.0));
                break;
            case RIVER:
                rangeEquityLabelRiver.setText("" + (int) (rangeEquityValue * 100.0));
                break;
            default:
                break;
        }

        FiveTypeValue rangeEquity = null;

        if (rangeEquityValue < 0.42) {
            rangeEquity = FiveTypeValue.VERY_BAD;
        } else if (rangeEquityValue < 0.48) {
            rangeEquity = FiveTypeValue.BAD;
        } else if (rangeEquityValue < 0.52) {
            rangeEquity = FiveTypeValue.MEDIUM;
        } else if (rangeEquityValue < 0.56) {
            rangeEquity = FiveTypeValue.GOOD;
        } else {
            rangeEquity = FiveTypeValue.VERY_GOOD;
        }

        System.out.println("RangeEquity: " + rangeEquityValue + " " + rangeEquity);

        return rangeEquity;
    }
}
