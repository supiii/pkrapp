package com.masa.util;

import com.masa.type.MadeHandSpecific;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author Matti
 */
public class HandStrengthAnalyzer {

    private static Rank leftCardRank;
    private static Rank rightCardRank;
    private static List<Card> boardCards;

    public HandStrengthAnalyzer() {

    }

    public static boolean isTopPairOrBetterWithHoleCardsOnFlop(CardSet holeCards, CardSet flopCards) {
        CardSet cardSet = new CardSet();
        cardSet.add(holeCards.get(0));
        cardSet.add(holeCards.get(1));

        cardSet.add(flopCards.get(0));
        cardSet.add(flopCards.get(1));
        cardSet.add(flopCards.get(2));

        List<Card> board = new ArrayList<>();
        board.add(flopCards.get(0));
        board.add(flopCards.get(1));
        board.add(flopCards.get(2));

        // 35651594
        if (HandEval.hand5Eval(cardSet) >= 35651594) {
            return true;
        }
        for (Rank rank : Rank.values()) {
            if (similarRanks(rank, board) > 1) {
                return false;
            }
        }
        for (Suit suit : Suit.values()) {
            if (similarSuits(suit, board) > 2) {
                return false;
            }
        }

        Rank highestRank = highestRank(board);

        Card left = holeCards.get(0);
        Card right = holeCards.get(1);

        // is holecards overpair
        if (left.rankOf() == right.rankOf() && left.rankOf().pipValue() > highestRank.pipValue()) {
            return true;
        }

        // is top pair
        return left.rankOf() == highestRank || right.rankOf() == highestRank;
    }

//    public Rank highestRank(List<Card> cards) {
//        Rank hi = null;
//
//        for (Card card : cards) {
//            if (hi == null) {
//                hi = card.rankOf();
//            }
//            if (hi.compareTo(card.rankOf()) > 0) {
//                hi = card.rankOf();
//            }
//        }
//        return hi;
//    }
    /**
     * Checks if there is a good draw and highest madehand is Ace high on the
     * flop or turn
     *
     *
     * @param holeCards
     * @param flopCards
     * @param turnCard
     * @param riverCard
     * @return
     */
    public static boolean isPairOrBetter(Card leftHoleCard, Card rightHoleCard, Card[] flopCards, Card turnCard, Card riverCard) {
        CardSet cardSet = new CardSet();
        cardSet.add(leftHoleCard);
        cardSet.add(rightHoleCard);

        cardSet.add(flopCards[0]);
        cardSet.add(flopCards[1]);
        cardSet.add(flopCards[2]);

        int rank = 0;
//        List<Card> board = new ArrayList<>();
//        board.add(flopCards[0]);
//        board.add(flopCards[1]);
//        board.add(flopCards[2]);

        if (turnCard == null && riverCard == null) {
            rank = HandEval.hand5Eval(cardSet);
        } else if (turnCard != null && riverCard == null) {
            cardSet.add(turnCard);
            rank = HandEval.hand6Eval(cardSet);
        } else if (turnCard != null && riverCard != null) {
            cardSet.add(riverCard);
            rank = HandEval.hand7Eval(cardSet);
        }

//        if (rank <= 7808) {
//            for (Suit suit : Suit.values()) {
//                int suits = similarSuits(suit, board);
//                if (suits == 3) {
//                    if (holeCards[0].suitOf() == suit && holeCards[0].rankOf() == Rank.ACE || holeCards[0].rankOf() == Rank.KING) {
//                        return true;
//                    }
//                    if (holeCards[1].suitOf() == suit && holeCards[1].rankOf() == Rank.ACE || holeCards[1].rankOf() == Rank.KING) {
//                        return true;
//                    }
//                }
//            }
//        }
        return rank > 7808;
    }

    public static MadeHandSpecific getHandValueOnFlop(List<Card> allCards) {
//        Collections.sort(allCards);

        leftCardRank = allCards.get(0).rankOf();
        rightCardRank = allCards.get(1).rankOf();
        boardCards = new ArrayList<>();
        boardCards.add(allCards.get(2));
        boardCards.add(allCards.get(3));
        boardCards.add(allCards.get(4));

        if (isStraightFlush(allCards)) {
            return MadeHandSpecific.STRAIGHT_FLUSH;
        } else if (isQuads(allCards)) {
            return MadeHandSpecific.QUADS;
        } else if (isFullHouse(allCards)) {
            return MadeHandSpecific.FULL_HOUSE;
        } else if (isFlush(allCards)) {
            return MadeHandSpecific.FLUSH;
        } else if (isStraight(allCards)) {
            return MadeHandSpecific.STRAIGHT;
        } else if (isThreeOfAKind(allCards)) {
            return MadeHandSpecific.THREE_OF_A_KIND;
        } else if (isTwoPair(allCards)) {
            return MadeHandSpecific.TWO_PAIR;
        } else if (isPair(allCards)) {
            if (isOverPair(allCards)) {
                return MadeHandSpecific.OVERPAIR;
            } else if (isTopPair(allCards)) {
                return MadeHandSpecific.TOP_PAIR;
            } else if (isPPBelowTP(allCards)) {
                return MadeHandSpecific.PP_BELOW_TP;
            } else if (isSecondPair(allCards)) {
                return MadeHandSpecific.MIDDLE_PAIR;
            } else if (isWeakPair(allCards)) {
                return MadeHandSpecific.WEAK_PAIR;
            }
        } else if (isAceHigh(allCards)) {
            return MadeHandSpecific.ACE_HIGH;
        }

        return MadeHandSpecific.NO_MADE_HAND;
    }

    private static boolean isThreeOfAKind(List<Card> cards) {
        if (similarRanks(leftCardRank, cards) == 3 || similarRanks(rightCardRank, cards) == 3) {
            return true;
        }
        return false;
    }

    private static boolean isOverPair(List<Card> cards) {
        if (leftCardRank.equals(rightCardRank)) {
            for (Card card : cards) {
                if (leftCardRank.pipValue() < card.rankOf().pipValue()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static boolean isTopPair(List<Card> cards) {
        if (similarRanks(leftCardRank, cards) == 2) {
            if (highestRank(cards).equals(leftCardRank)) {
                return true;
            }
        } else if (similarRanks(rightCardRank, cards) == 2) {
            if (highestRank(cards).equals(rightCardRank)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isPPBelowTP(List<Card> cards) {
        if (isPocketPair()) {
            if (secondHighestRank(cards).equals(leftCardRank)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isSecondPair(List<Card> cards) {
        if (similarRanks(leftCardRank, cards) == 2) {
            if (secondHighestRank(cards).equals(leftCardRank)) {
                return true;
            }
        } else if (similarRanks(rightCardRank, cards) == 2) {
            if (secondHighestRank(cards).equals(rightCardRank)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isWeakPair(List<Card> cards) {
        if (similarRanks(leftCardRank, cards) == 2) {
            if (secondHighestRank(cards).pipValue() > leftCardRank.pipValue()) {
                return true;
            }
        } else if (similarRanks(rightCardRank, cards) == 2) {
            if (secondHighestRank(cards).pipValue() > rightCardRank.pipValue()) {
                return true;
            }
        }
        return false;
    }

    private static boolean isPair(List<Card> cards) {
        leftCardRank = cards.get(0).rankOf();
        rightCardRank = cards.get(1).rankOf();

        if (leftCardRank.equals(rightCardRank)) {
            return true;
        }

        if (similarRanks(leftCardRank, cards) == 2 || similarRanks(rightCardRank, cards) == 2) {
            return true;
        }
        return false;
    }

    private static boolean isTwoPair(List<Card> cards) {
        if (leftCardRank.equals(rightCardRank)) {
            return false;
        }

        if (similarRanks(leftCardRank, cards) == 2 && similarRanks(rightCardRank, cards) == 2) {
            for (int i = 2; i < cards.size(); i++) {
                Rank r = cards.get(i).rankOf();

                if (!r.equals(leftCardRank) && !r.equals(rightCardRank)) {

                    if (similarRanks(r, cards) > 1 && r.pipValue() > leftCardRank.pipValue()) {
                        System.out.println("999");
                        return false;
                    }
                }
            }
            return true;

        }
        return false;
    }

    private static boolean isStraightFlush(List<Card> cardsList) {
        List<Card> cards = new ArrayList<>(cardsList);
        Collections.sort(cards);
        Suit suitFlush = null;
        for (int i = 0; i < cards.size(); i++) {

            if (i == 0) {
                suitFlush = cards.get(i).suitOf();
            } else {
                if (!suitFlush.equals(cards.get(i).suitOf())) {
                    return false;
                }
            }
            Rank rank = cards.get(i).rankOf();
            Rank next = Rank.values()[rank.ordinal() + 1];

            if (i < cards.size() - 1 && !next.equals(cards.get(i + 1).rankOf())) {
                return false;
            }
        }
        return true;
    }

    private static boolean isQuads(List<Card> cards) {
        if (similarRanks(leftCardRank, cards) == 4) {
            return true;
        }

        if (similarRanks(rightCardRank, cards) == 4) {
            return true;
        }
        return false;
    }

    private static boolean isFullHouse(List<Card> cards) {
        Rank rank = cards.get(0).rankOf();
        Rank rank2 = null;

        for (Card card : cards) {
            if (!rank.equals(card.rankOf())) {
                rank2 = card.rankOf();
            }
        }

        if (similarRanks(rank, cards) == 3 && similarRanks(rank2, cards) == 2) {
            return true;
        } else if (similarRanks(rank2, cards) == 3 && similarRanks(rank, cards) == 2) {
            return true;
        }
        return false;
    }

    private static boolean isFlush(List<Card> cards) {
        Suit suit = null;
        for (int i = 0; i < cards.size(); i++) {
            if (i == 0) {
                suit = cards.get(i).suitOf();
            } else {
                if (!suit.equals(cards.get(i).suitOf())) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean isAceHigh(List<Card> cards) {
        if (leftCardRank.equals(Rank.ACE) || rightCardRank.equals(Rank.ACE)) {
            return true;
        }
        return false;
    }

    private static boolean isStraight(List<Card> cardsList) {
        if (isFlush(cardsList)) {
            return false;
        }

        List<Card> cards = new ArrayList<>(cardsList);
        Collections.sort(cards);

        for (int i = 0; i < cards.size(); i++) {
            Rank rank = cards.get(i).rankOf();
            if (rank == Rank.ACE) {
                return false;
            }
            Rank next = null;
            try {
                next = Rank.values()[rank.ordinal() + 1];
            } catch (Exception e) {
                System.out.println("ff");
            }

            if (i < cards.size() - 1 && !next.equals(cards.get(i + 1).rankOf())) {
                return false;
            }
        }
        return true;
    }

    public static int similarRanks(Rank rank, Collection<Card> cards) {
        int times = 0;
        for (Card card : cards) {
            if (rank.equals(card.rankOf())) {
                times++;
            }
        }
        return times;
    }

    public static int similarSuits(Suit suit, Collection<Card> cards) {
        int times = 0;
        for (Card card : cards) {
            if (suit == card.suitOf()) {
                times++;
            }
        }
        return times;
    }

    public static Rank highestRank(List<Card> cards) {
        List<Card> sortedList = new ArrayList<Card>(cards);
        Collections.sort(sortedList);
        return sortedList.get(sortedList.size() - 1).rankOf();
    }

    private static Rank secondHighestRank(List<Card> cards) {
        List<Card> sortedList = new ArrayList<Card>(cards);
        Collections.sort(sortedList);
        return sortedList.get(sortedList.size() - 2).rankOf();
    }

    private static boolean isPocketPair() {
        return leftCardRank.equals(rightCardRank);
    }

}
