package com.masa.util;

import com.masa.pkrapp.App;
import static com.masa.pkrapp.App.actionsBBPF;
import static com.masa.pkrapp.App.actionsBTNPF;
import static com.masa.pkrapp.App.actionsCOPF;
import static com.masa.pkrapp.App.actionsEPPF;
import static com.masa.pkrapp.App.actionsHeroFlop;
import static com.masa.pkrapp.App.actionsHeroPreFlop;
import static com.masa.pkrapp.App.actionsHeroRiver;
import static com.masa.pkrapp.App.actionsHeroTurn;
import static com.masa.pkrapp.App.actionsMPPF;
import static com.masa.pkrapp.App.actionsSBPF;
import static com.masa.pkrapp.App.actionsVillainFlop;
import static com.masa.pkrapp.App.actionsVillainPreFlop;
import static com.masa.pkrapp.App.actionsVillainRiver;
import static com.masa.pkrapp.App.actionsVillainTurn;
import static com.masa.pkrapp.App.backImg;
import static com.masa.pkrapp.App.dealerSeat;
import static com.masa.pkrapp.App.firstFlopCard;
import static com.masa.pkrapp.App.firstFlopCardImgView;
import static com.masa.pkrapp.App.heroActingFirstPF;
import static com.masa.pkrapp.App.heroPos;
import static com.masa.pkrapp.App.holeCardLeft;
import static com.masa.pkrapp.App.holeCardRight;
import static com.masa.pkrapp.App.leftHoleCardImgView;
import static com.masa.pkrapp.App.nutAdvantageLabel;
import static com.masa.pkrapp.App.preflopActions;
import static com.masa.pkrapp.App.preflopRangesResolver;
import static com.masa.pkrapp.App.rangeEquityLabelFlop;
import static com.masa.pkrapp.App.rangeEquityLabelRiver;
import static com.masa.pkrapp.App.rangeEquityLabelTurn;
import static com.masa.pkrapp.App.ranges;
import static com.masa.pkrapp.App.rawEquityLabelFlop;
import static com.masa.pkrapp.App.rawEquityLabelRiver;
import static com.masa.pkrapp.App.rawEquityLabelTurn;
import static com.masa.pkrapp.App.resolvedActionLabel;
import static com.masa.pkrapp.App.rightHoleCardImgView;
import static com.masa.pkrapp.App.riverCard;
import static com.masa.pkrapp.App.riverCardImgView;
import static com.masa.pkrapp.App.secondFlopCard;
import static com.masa.pkrapp.App.secondFlopCardImgView;
import static com.masa.pkrapp.App.thirdFlopCard;
import static com.masa.pkrapp.App.thirdFlopCardImgView;
import static com.masa.pkrapp.App.turnCard;
import static com.masa.pkrapp.App.turnCardImgView;
import static com.masa.pkrapp.App.villainNutAdvantageLabel;
import static com.masa.pkrapp.App.villainPos;
import static com.masa.pkrapp.App.villainRangeConnectivityLabel;
import static com.masa.pkrapp.App.vulnerabilityLabel;
import com.masa.pokertracker.PTUtil;
import com.masa.type.BoardTextureFlop;
import com.masa.type.BoardTextureRiver;
import com.masa.type.BoardTextureTurn;
import com.masa.type.Draw;
import com.masa.type.MadeHandSpecific;
import com.masa.type.NextPostFlopMoves;
import com.masa.type.PositionPostFlop;
import com.masa.type.PostFlopRow;
import com.masa.type.Ranges;
import com.masa.type.Seat;
import com.masa.type.Street;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Set;
import javafx.scene.control.Label;

/**
 *
 * @author compuuter
 */
public class Util {

    public static void cleanUp() {
        ranges = new Ranges();
        holeCardLeft = null;
        holeCardRight = null;
        firstFlopCard = null;
        secondFlopCard = null;
        thirdFlopCard = null;
        turnCard = null;
        riverCard = null;
        leftHoleCardImgView.setImage(backImg);
        rightHoleCardImgView.setImage(backImg);
        firstFlopCardImgView.setImage(backImg);
        secondFlopCardImgView.setImage(backImg);
        thirdFlopCardImgView.setImage(backImg);
        turnCardImgView.setImage(backImg);
        riverCardImgView.setImage(backImg);
        heroPos = null;
        App.villainPos = null;
        dealerSeat = null;
        heroActingFirstPF = false;
        App.preflopAggressors = "";
        resolvedActionLabel.setText("");
        rangeEquityLabelFlop = new Label("-");
        rangeEquityLabelTurn = new Label("-");
        rangeEquityLabelRiver = new Label("-");
        rawEquityLabelFlop = new Label("-");
        rawEquityLabelTurn = new Label("-");
        rawEquityLabelRiver = new Label("-");
        nutAdvantageLabel.setText("-");
        villainRangeConnectivityLabel.setText("-");
        villainNutAdvantageLabel.setText("-");
        vulnerabilityLabel.setText("-");
        actionsHeroPreFlop = "";
        actionsHeroFlop = "";
        actionsHeroTurn = "";
        actionsHeroRiver = "";
        actionsVillainPreFlop = "";
        actionsVillainFlop = "";
        actionsVillainTurn = "";
        actionsVillainRiver = "";
        actionsEPPF = "";
        actionsMPPF = "";
        actionsCOPF = "";
        actionsBTNPF = "";
        actionsSBPF = "";
        actionsBBPF = "";
        App.preflopActors = "";
        App.flopActors = "";
        App.turnActors = "";
        App.riverActors = "";
        App.villainVPIP = 24;
        App.villainPFR = 17;
        App.villain3Bet = 6;
    }

    public static boolean isPreflop() {
        return firstFlopCard == null;
    }

    public static boolean isFlop() {
        return firstFlopCard != null && turnCard == null;
    }

    public static boolean isTurn() {
        return firstFlopCard != null && turnCard != null && riverCard == null;
    }

    public static boolean isRiver() {
        return firstFlopCard != null && turnCard != null && riverCard != null;
    }

    public static Street getStreet() {
        if (isPreflop()) {
            return Street.PREFLOP;
        } else if (isFlop()) {
            return Street.FLOP;
        } else if (isTurn()) {
            return Street.TURN;
        } else if (isRiver()) {
            return Street.RIVER;
        }
        return null;
    }

    public static void calculate() {
        if (dealerSeat == null || holeCardLeft == null || holeCardRight == null || !isHerosTurn()) {
            System.out.println("Skipping calculate: parameters missing!");
            System.out.println("dealerSeat: " + dealerSeat);
            System.out.println("Heros pos_ " + App.heroPos);
            System.out.println("Villains pos_ " + App.villainPos);
            System.out.println("preflopAggressors: " + App.preflopAggressors);
            System.out.println("isHerosTurn: " + isHerosTurn());
            return;
        }

        // Disable preflop for now
        if (isPreflop()) {
            return;
        }

        Map<String, NextPostFlopMoves> nextPostFlopMovesMap = NextMovesAnalyzer.getNextPostFlopMoves();

        for (Map.Entry<String, NextPostFlopMoves> entry : nextPostFlopMovesMap.entrySet()) {
            String key = entry.getKey();
            NextPostFlopMoves nextPostFlopMoves = entry.getValue();

            int foldMovesCount = nextPostFlopMoves.getFoldMoves().count;
            double foldAmountBB = nextPostFlopMoves.getFoldMoves().bbAmountWonOrLoss;
            int checkMovesCount = nextPostFlopMoves.getCheckMoves().count;
            double checkAmountBB = nextPostFlopMoves.getCheckMoves().bbAmountWonOrLoss;
            int callMovesCount = nextPostFlopMoves.getCallMoves().count;
            double callAmountBB = nextPostFlopMoves.getCallMoves().bbAmountWonOrLoss;
            int raiseMovesCount = nextPostFlopMoves.getRaiseMoves().count;
            double raiseAmountBB = nextPostFlopMoves.getRaiseMoves().bbAmountWonOrLoss;

            System.out.println(" ");
            System.out.println("### " + key + " ###");
            if (foldMovesCount != 0) {
                System.out.println("Fold: (" + foldMovesCount + ") " + foldAmountBB + " " + (foldAmountBB / foldMovesCount));
            }
            if (checkMovesCount != 0) {
                System.out.println("Check: (" + checkMovesCount + ") " + checkAmountBB + " " + (checkAmountBB / checkMovesCount));
            }
            if (callMovesCount != 0) {
                System.out.println("Call: (" + callMovesCount + ") " + callAmountBB + " " + (callAmountBB / callMovesCount));
            }
            if (raiseMovesCount != 0) {
                System.out.println("Raise: (" + raiseMovesCount + ") " + raiseAmountBB + " " + (raiseAmountBB / raiseMovesCount));
            }

        }

        /*System.out.println("Fold madehand: (" + nextPostFlopMoves.getFoldMovesMadeHand().count + ") " + nextPostFlopMoves.getFoldMovesMadeHand().bbAmountWonOrLoss);
        System.out.println("Check madehand: (" + nextPostFlopMoves.getCheckMovesMadeHand().count + ") " + nextPostFlopMoves.getCheckMovesMadeHand().bbAmountWonOrLoss);
        System.out.println("Call madehand: (" + nextPostFlopMoves.getCallMovesMadeHand().count + ") " + nextPostFlopMoves.getCallMovesMadeHand().bbAmountWonOrLoss);
        System.out.println("Raise madehand: (" + nextPostFlopMoves.getRaiseMovesMadeHand().count + ") " + nextPostFlopMoves.getRaiseMovesMadeHand().bbAmountWonOrLoss);

        System.out.println("CheckWithoutSD: (" + nextPostFlopMoves.getCheckMovesWithoutSD().count + ") " + nextPostFlopMoves.getCheckMovesWithoutSD().bbAmountWonOrLoss);
        System.out.println("CallWithoutSD: (" + nextPostFlopMoves.getCallMovesWithoutSD().count + ") " + nextPostFlopMoves.getCallMovesWithoutSD().bbAmountWonOrLoss);
        System.out.println("RaisWithoutSDe: (" + nextPostFlopMoves.getRaiseMovesWithoutSD().count + ") " + nextPostFlopMoves.getRaiseMovesWithoutSD().bbAmountWonOrLoss);

        System.out.println("Average equity: " + nextPostFlopMoves.getAverageEquity() + "(" + nextPostFlopMoves.getCountAverageEquity() + ")");*/
        if (isPreflop()) {
            System.out.println("CALUCALTING PREFLOP");
            System.out.println("dealerSeat: " + dealerSeat);
            System.out.println("holeCardLeft: " + holeCardLeft);
            System.out.println("holeCardRight: " + holeCardRight);
            System.out.println("isHerosTurn: " + isHerosTurn());
            // Starting hand charts
            System.out.println("preflopAggressors: " + App.preflopAggressors);

            /*Action resolvedAction = preflopResolver.resolve(App.preflopAggressors, heroPos, villainPos, holeCardLeft + holeCardRight);
            System.out.println("resolvedAction: " + resolvedAction);
            System.out.println("Heros pos_ " + App.heroPos);
            System.out.println("Villains pos_ " + App.villainPos);
            if (resolvedAction == null) {
                resolvedActionLabel.setText("null");
            } else {
                resolvedActionLabel.setText(resolvedAction.value());
            }

            resolvedActionLabel.getStyleClass().add("resolved-action");*/
            // PT preflop table
            //nextPreflopMoveService.getNextMoves(holeCards, heroPos, actionsHeroPreFlop, actionsEPPF, actionsMPPF, actionsCOPF, actionsBTNPF, actionsSBPF, actionsBBPF);
        } else {

            // Todo: implement
            boolean IP = true;
            //calculateRangeEquity(Street.FLOP);
            //FiveTypeValue rawEquity = RangeAdvantageAnalyzer.calculateRawEquity(Street.FLOP);
            //FiveTypeValue rangeEquity = RangeAdvantageAnalyzer.calculateRangeEquityAndSetLabels();
            //FiveTypeValue nutAdvantage = NutAdvantageAnalyzer.calculateSimpleNutAdvantage();
            //ThreeTypeValue villainRangeConnectivity = RangeConnectivityAnalyzer.calculateConnectivity();
            //FiveTypeValue vulnerability = VulnerabilityAnalyzer.analyzeVulnerability(ranges.villainRangeCardSet, board, holeCards);

            // AggressivenessMeter
            //double aggressiveness = AggressivenessMeter.determineAggressiveness(rangeEquity, nutAdvantage, vulnerability, IP);
            //System.out.println("Aggressiveness: " + aggressiveness);
            //vulnerabilityLabel.setText(vulnerability.name());
            // PostFlopRows
        }
    }

    /*private static double calculateRangeEquity(Street street) {
        double rangeEquity = 0.0;
        if (null != street) {
            switch (street) {
                case FLOP:
                    rangeEquity = EquityHandler.calculateEquityMonteCarlo(ranges.heroRangeString, ranges.villainRangeString, firstFlopCard + secondFlopCard + thirdFlopCard);
                    rangeEquityLabelFlop.setText("" + (int) (rangeEquity * 100.0));
                    break;
                case TURN:
                    rangeEquity = EquityHandler.calculateEquityMonteCarlo(ranges.heroRangeString, ranges.villainRangeString, firstFlopCard + secondFlopCard + thirdFlopCard + turnCard);
                    rangeEquityLabelTurn.setText("" + (int) (rangeEquity * 100.0));
                    break;
                case RIVER:
                    rangeEquity = EquityHandler.calculateEquityMonteCarlo(ranges.heroRangeString, ranges.villainRangeString, firstFlopCard + secondFlopCard + thirdFlopCard + turnCard + riverCard);
                    rangeEquityLabelRiver.setText("" + (int) (rangeEquity * 100.0));
                    break;
                default:
                    break;
            }
        }
        return rangeEquity;
        //double handEquity = EquityHandler.calculateEquityMonteCarlo(holeCardLeft + holeCardRight, ranges.villainRangeString, firstFlopCard + secondFlopCard + thirdFlopCard);
    }*/
    public static void addAction(String action, Seat seat) {
        boolean isAggressiveAction = false;
        boolean hero = seat.equals(Seat.FOUR);
        String nextAction = action;

        if (action.equalsIgnoreCase("B") || action.equalsIgnoreCase("R")) {
            nextAction = isBetOrRaiseAction();
            isAggressiveAction = true;
        }

        if (isPreflop()) {
            if (hero) {
                if (App.heroPos == null || App.heroPos.isEmpty()) {
                    App.heroPos = determinePos(seat, dealerSeat);
                }
                App.actionsHeroPreFlop += nextAction;
                App.preflopActors += PTUtil.getPositionInt(App.heroPos);
                if (isAggressiveAction) {
                    App.preflopAggressors += PTUtil.getPositionInt(App.heroPos);
                }
            } else {
                if (App.villainPos == null || App.villainPos.isEmpty()) {
                    App.villainPos = determinePos(seat, App.dealerSeat);
                }
                if (isAggressiveAction) {
                    App.preflopAggressors += PTUtil.getPositionInt(App.villainPos);
                }
                App.preflopActors += PTUtil.getPositionInt(App.villainPos);
                System.out.println(App.actionsVillainPreFlop);
                App.actionsVillainPreFlop += nextAction;
                System.out.println("DEBUG");
            }

            App.preflopActions += nextAction;
        } else if (isFlop()) {
            if (hero) {
                App.flopActors += PTUtil.getPositionInt(App.heroPos);
                App.actionsHeroFlop += nextAction;
            } else {
                App.flopActors += PTUtil.getPositionInt(App.villainPos);
                App.actionsVillainFlop += nextAction;
            }
        } else if (isTurn()) {
            if (hero) {
                App.turnActors += PTUtil.getPositionInt(App.heroPos);
                App.actionsHeroTurn += nextAction;
            } else {
                App.turnActors += PTUtil.getPositionInt(App.villainPos);
                App.actionsVillainTurn += nextAction;
            }
        } else if (isRiver()) {
            if (hero) {
                App.riverActors += PTUtil.getPositionInt(App.heroPos);
                App.actionsHeroRiver += nextAction;
            } else {
                App.riverActors += PTUtil.getPositionInt(App.villainPos);
                App.actionsVillainRiver += nextAction;
            }
        }
    }

    public static void mapDraws(PostFlopRow row, Set<Draw> draws, Street street) {
        if (draws == null || draws.isEmpty()) {
            return;
        }
        if (street == Street.FLOP) {
            if (draws.contains(Draw.FLUSH_DRAW)) {
                row.flushDrawFlop = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_2ND_NUT)) {
                row.flushSecondNutDrawFlop = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_3RD_NUT)) {
                row.flushThirdNutDrawFlop = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_BACKDOOR)) {
                row.backdoorFlushDrawFlop = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_NUT)) {
                row.flushNutDrawFlop = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_WEAK)) {
                row.flushWeakDrawFlop = true;
            }
            if (draws.contains(Draw.Gutshot)) {
                row.gutshotDrawFlop = true;
            }
            if (draws.contains(Draw.OESD)) {
                row.openEndedStraightDrawFlop = true;
            }
            if (draws.contains(Draw.OVERCARDS)) {
                row.overCardsFlop = true;
            }
        } else if (street == Street.TURN) {
            if (draws.contains(Draw.FLUSH_DRAW)) {
                row.flushDrawTurn = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_2ND_NUT)) {
                row.flushSecondNutDrawTurn = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_3RD_NUT)) {
                row.flushThirdNutDrawTurn = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_BACKDOOR)) {
                row.backdoorFlushDrawTurn = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_NUT)) {
                row.flushNutDrawTurn = true;
            }
            if (draws.contains(Draw.FLUSH_DRAW_WEAK)) {
                row.flushWeakDrawTurn = true;
            }
            if (draws.contains(Draw.Gutshot)) {
                row.gutshotDrawTurn = true;
            }
            if (draws.contains(Draw.OESD)) {
                row.openEndedStraightDrawTurn = true;
            }
            if (draws.contains(Draw.OVERCARDS)) {
                row.overCardsTurn = true;
            }
        }
    }

    public static void getRanges() {
        preflopRangesResolver.resolveRanges(ranges, preflopActions, App.preflopAggressors, heroPos, villainPos);
    }

    public static boolean requiredParametersPreflop() {
        return holeCardLeft != null && holeCardRight != null && heroPos != null;
    }

    public static boolean requiredParametersFlop() {
        return holeCardLeft != null && holeCardRight != null && heroPos != null;
    }

    public static boolean isHerosTurn() {
        String actors = "";
        if (isPreflop()) {
            actors = App.preflopActors;
        } else if (isFlop()) {
            actors = App.flopActors;
        } else if (isTurn()) {
            actors = App.turnActors;
        } else if (isRiver()) {
            actors = App.riverActors;
        }

        System.out.println("actors: " + actors);
        //if (actors == null || actors.isEmpty()) {
        if (!isPreflop()) {
            if (isFlop() && firstFlopCard == null || secondFlopCard == null || thirdFlopCard == null) {
                return false;
            } else if (isTurn() && turnCard == null) {
                return false;
            } else if (isRiver() && riverCard == null) {
                return false;
            }

            //PositionPostFlop heroPositionPostFlop = PositionPostFlop.valueOf(heroPos.toUpperCase());
            //PositionPostFlop villainPositionPostFlop = PositionPostFlop.valueOf(villainPos.toUpperCase());
            boolean ip = Util.isHeroIP();
            int actorsLength = actors.length();
            if (ip) {
                return actorsLength % 2 != 0;
            } else {
                return actorsLength % 2 == 0;
            }

            //System.out.println("heroPositionPostFlop.ordinal() < villainPositionPostFlop.ordinal(): " + heroPositionPostFlop.ordinal() < villainPositionPostFlop.ordinal());
            //return heroPositionPostFlop.ordinal() < villainPositionPostFlop.ordinal();
        } else {
            System.out.println("Util.calculate: 3");
            return true;
        }

        //}
        //String lastActor = actors.substring(actors.length() - 1);
        //System.out.println("Util.calculate: 4");
        //return !lastActor.equals(PTUtil.getPositionInt(App.heroPos));
    }

    private static String isBetOrRaiseAction() {
        if (isPreflop()) {
            return "R";
        } else if (isFlop()) {
            if (isHerosTurn() && App.actionsVillainFlop.isEmpty()) {
                return "B";
            } else if (isHerosTurn() && App.actionsVillainFlop.endsWith("B") || App.actionsVillainFlop.endsWith("R")) {
                return "R";
            } else if (!isHerosTurn() && App.actionsHeroFlop.isEmpty()) {
                return "B";
            } else if (!isHerosTurn() && App.actionsHeroFlop.endsWith("B") || App.actionsHeroFlop.endsWith("R")) {
                return "R";
            }
        } else if (isTurn()) {
            if (isHerosTurn() && App.actionsVillainTurn.isEmpty()) {
                return "B";
            } else if (isHerosTurn() && App.actionsVillainTurn.endsWith("B") || App.actionsVillainTurn.endsWith("R")) {
                return "R";
            } else if (!isHerosTurn() && App.actionsHeroTurn.isEmpty()) {
                return "B";
            } else if (!isHerosTurn() && App.actionsHeroTurn.endsWith("B") || App.actionsHeroTurn.endsWith("R")) {
                return "R";
            }
        } else if (isRiver()) {
            if (isHerosTurn() && App.actionsVillainRiver.isEmpty()) {
                return "B";
            } else if (isHerosTurn() && App.actionsVillainRiver.endsWith("B") || App.actionsVillainRiver.endsWith("R")) {
                return "R";
            } else if (!isHerosTurn() && App.actionsHeroRiver.isEmpty()) {
                return "B";
            } else if (!isHerosTurn() && App.actionsHeroRiver.endsWith("B") || App.actionsHeroRiver.endsWith("R")) {
                return "R";
            }
        }

        return "B";
    }

    public static void addAction(String action, String position) {
        if (position.equalsIgnoreCase("EP")) {
            actionsEPPF = actionsEPPF + action;
        } else if (position.equalsIgnoreCase("MP")) {
            actionsMPPF = actionsMPPF + action;
        } else if (position.equalsIgnoreCase("CO")) {
            actionsCOPF = actionsCOPF + action;
        } else if (position.equalsIgnoreCase("BTN")) {
            actionsBTNPF = actionsBTNPF + action;
        } else if (position.equalsIgnoreCase("SB")) {
            actionsSBPF = actionsSBPF + action;
        } else if (position.equalsIgnoreCase("BB")) {
            actionsBBPF = actionsBBPF + action;
        }
    }

    public static boolean isHeroIP() {
        PositionPostFlop heroPositionPostFlop = getPositionPostFlop(App.heroPos);
        PositionPostFlop villainPositionPostFlop = getPositionPostFlop(App.villainPos);
        return heroPositionPostFlop.ordinal() > villainPositionPostFlop.ordinal();
    }

    private static PositionPostFlop getPositionPostFlop(String pos) {
        if ("SB".equals(pos)) {
            return PositionPostFlop.SB;
        }
        if ("BB".equals(pos)) {
            return PositionPostFlop.BB;
        }
        if ("EP".equals(pos)) {
            return PositionPostFlop.EP;
        }
        if ("MP".equals(pos)) {
            return PositionPostFlop.MP;
        }
        if ("CO".equals(pos)) {
            return PositionPostFlop.CO;
        }
        if ("BTN".equals(pos)) {
            return PositionPostFlop.BTN;
        }
        return null;
    }

    public static String determinePos(Seat seat, Seat dealerSeat) {
        if (dealerSeat.equals(Seat.ONE)) {
            if (seat.equals(Seat.ONE)) {
                return "BTN";
            }
            if (seat.equals(Seat.TWO)) {
                return "SB";
            }
            if (seat.equals(Seat.THREE)) {
                return "BB";
            }
            if (seat.equals(Seat.FOUR)) {
                return "EP";
            }
            if (seat.equals(Seat.FIVE)) {
                return "MP";
            }
            if (seat.equals(Seat.SIX)) {
                return "CO";
            }
        }
        if (dealerSeat.equals(Seat.TWO)) {
            if (seat.equals(Seat.ONE)) {
                return "CO";
            }
            if (seat.equals(Seat.TWO)) {
                return "BTN";
            }
            if (seat.equals(Seat.THREE)) {
                return "SB";
            }
            if (seat.equals(Seat.FOUR)) {
                return "BB";
            }
            if (seat.equals(Seat.FIVE)) {
                return "EP";
            }
            if (seat.equals(Seat.SIX)) {
                return "MP";
            }
        }
        if (dealerSeat.equals(Seat.THREE)) {
            if (seat.equals(Seat.ONE)) {
                return "MP";
            }
            if (seat.equals(Seat.TWO)) {
                return "CO";
            }
            if (seat.equals(Seat.THREE)) {
                return "BTN";
            }
            if (seat.equals(Seat.FOUR)) {
                return "SB";
            }
            if (seat.equals(Seat.FIVE)) {
                return "BB";
            }
            if (seat.equals(Seat.SIX)) {
                return "EP";
            }
        }
        if (dealerSeat.equals(Seat.FOUR)) {
            if (seat.equals(Seat.ONE)) {
                return "EP";
            }
            if (seat.equals(Seat.TWO)) {
                return "MP";
            }
            if (seat.equals(Seat.THREE)) {
                return "CO";
            }
            if (seat.equals(Seat.FOUR)) {
                return "BTN";
            }
            if (seat.equals(Seat.FIVE)) {
                return "SB";
            }
            if (seat.equals(Seat.SIX)) {
                return "BB";
            }
        }
        if (dealerSeat.equals(Seat.FIVE)) {
            if (seat.equals(Seat.ONE)) {
                return "BB";
            }
            if (seat.equals(Seat.TWO)) {
                return "EP";
            }
            if (seat.equals(Seat.THREE)) {
                return "MP";
            }
            if (seat.equals(Seat.FOUR)) {
                return "CO";
            }
            if (seat.equals(Seat.FIVE)) {
                return "BTN";
            }
            if (seat.equals(Seat.SIX)) {
                return "SB";
            }
        }
        if (dealerSeat.equals(Seat.SIX)) {
            if (seat.equals(Seat.ONE)) {
                return "SB";
            }
            if (seat.equals(Seat.TWO)) {
                return "BB";
            }
            if (seat.equals(Seat.THREE)) {
                return "EP";
            }
            if (seat.equals(Seat.FOUR)) {
                return "MP";
            }
            if (seat.equals(Seat.FIVE)) {
                return "CO";
            }
            if (seat.equals(Seat.SIX)) {
                return "BTN";
            }
        }
        return null;
    }

    public static PostFlopRow createPostFlopRow(String line, PostFlopRow row) {
        String[] splitted = line.split("\\s+");

        row.won = booleanValue(splitted[0]);
        row.wonBB = Double.parseDouble(splitted[1]);
        row.position = getPosition(splitted);
        row.villainPosition = getVillainPosition(splitted);
        if (splitted[4].equals("null")) {
            row.madeHandSpecificFlop = null;
        } else {
            row.madeHandSpecificFlop = MadeHandSpecific.values()[Integer.parseInt(splitted[7])];
        }
        if (splitted[5].equals("null")) {
            row.madeHandSpecificTurn = null;
        } else {
            row.madeHandSpecificTurn = MadeHandSpecific.values()[Integer.parseInt(splitted[8])];
        }
        if (splitted[6].equals("null")) {
            row.madeHandSpecificRiver = null;
        } else {
            row.madeHandSpecificRiver = MadeHandSpecific.values()[Integer.parseInt(splitted[9])];
        }
        row.actionsPreflop = getActionsPreflop(splitted);
        row.actionsFlop = getActionsFlop(splitted);
        row.actionsTurn = getActionsTurn(splitted);
        row.actionsRiver = getActionsRiver(splitted);
        row.actionsVillainPreflop = getVillainActionsPreflop(splitted);
        row.actionsVillainFlop = getVillainActionsFlop(splitted);
        row.actionsVillainTurn = getVillainActionsTurn(splitted);
        row.actionsVillainRiver = getVillainActionsRiver(splitted);
        row.overCardsFlop = booleanValue(splitted[15]);
        row.backdoorFlushDrawFlop = booleanValue(splitted[16]);
        row.gutshotDrawFlop = booleanValue(splitted[17]);
        row.openEndedStraightDrawFlop = booleanValue(splitted[18]);
        row.flushDrawFlop = booleanValue(splitted[19]);
        row.flushNutDrawFlop = booleanValue(splitted[20]);
        row.flushSecondNutDrawFlop = booleanValue(splitted[21]);
        row.flushThirdNutDrawFlop = booleanValue(splitted[22]);
        row.flushWeakDrawFlop = booleanValue(splitted[23]);
        row.overCardsTurn = booleanValue(splitted[24]);
        row.backdoorFlushDrawTurn = booleanValue(splitted[25]);
        row.gutshotDrawTurn = booleanValue(splitted[26]);
        row.openEndedStraightDrawTurn = booleanValue(splitted[27]);
        row.flushDrawTurn = booleanValue(splitted[28]);
        row.flushNutDrawTurn = booleanValue(splitted[29]);
        row.flushSecondNutDrawTurn = booleanValue(splitted[30]);
        row.flushThirdNutDrawTurn = booleanValue(splitted[31]);
        row.flushWeakDrawTurn = booleanValue(splitted[32]);
        row.showdown = booleanValue(splitted[33]);
        row.holeCard1Id = intValue(getHoleCard1(splitted)); //34
        row.holeCard2Id = intValue(getHoleCard2(splitted)); //35
        row.flopCard1Id = intValue(getFlopCard1(splitted));
        row.flopCard2Id = intValue(getFlopCard2(splitted));
        row.flopCard3Id = intValue(getFlopCard3(splitted));
        row.turnCardId = intValue(getTurnCard(splitted));
        row.riverCardId = intValue(getRiverCard(splitted)); //40
        row.villainHoleCard1Id = intValue(splitted[41]);
        row.villainHoleCard2Id = intValue(splitted[42]);
        row.villainRangeComboId = intValue(splitted[43]);
        row.playersPreflop = intValue(getPlayersPreflop(splitted)); //44
        row.playersOnFlop = intValue(getPlayersOnFlop(splitted)); //45
        row.aceFlop = booleanValue(splitted[46]);
        row.kingFlop = booleanValue(splitted[47]);
        row.queenFlop = booleanValue(splitted[48]);
        row.jackFlop = booleanValue(splitted[49]);
        row.nroOfSameSuitsFlop = intValue(splitted[50]);
        row.nroOfSameRanksFlop = intValue(splitted[51]);
        row.straightPossibleFlop = booleanValue(splitted[52]);
        row.ranksAddedValueFlop = intValue(splitted[53]);
        row.aceTurn = booleanValue(splitted[54]);
        row.kingTurn = booleanValue(splitted[55]);
        row.turnCardIsHighest = booleanValue(splitted[56]);
        row.turnCardIsSecondHighest = booleanValue(splitted[57]);
        row.turnCardIsThirdHighest = booleanValue(splitted[58]);
        row.nroOfSameSuitsTurn = intValue(splitted[59]);
        row.boardHasPairTurn = booleanValue(splitted[60]);
        row.boardHasTwoPairsTurn = booleanValue(splitted[61]);
        row.boardHasTriplesTurn = booleanValue(splitted[62]);
        row.straightPossibleWithOneCardTurn = booleanValue(splitted[63]);
        row.straightPossibleWithTwoCardsTurn = booleanValue(splitted[64]);
        row.aceRiver = booleanValue(splitted[65]); //65
        row.kingRiver = booleanValue(splitted[66]);
        row.riverCardIsHighest = booleanValue(splitted[67]);
        row.riverCardIsSecondHighest = booleanValue(splitted[68]);
        row.riverCardIsThirdeHighest = booleanValue(splitted[69]);
        row.nroOfSameSuitsRiver = intValue(splitted[70]);
        row.boardHasPairRiver = booleanValue(splitted[71]);
        row.boardHasTwoPairsRiver = booleanValue(splitted[72]);
        row.boardHasTriplesRiver = booleanValue(splitted[73]);
        row.boardHasFullhouseRiver = booleanValue(splitted[74]);
        row.straightPossibleWithOneCardRiver = booleanValue(splitted[75]);
        row.straightPossibleWithTwoCardsRiver = booleanValue(splitted[76]);
        row.straightOnBoardRiver = booleanValue(splitted[77]);
        row.noRanksOrOneRankBetweenFlop = booleanValue(splitted[78]);
        row.twoOrThreeRanksBetweenFlop = booleanValue(splitted[79]);
        row.noCardsBetweenTurn = booleanValue(splitted[80]);
        row.oneCardBetweenTurn = booleanValue(splitted[81]);
        row.noCardsBetweenRiver = booleanValue(splitted[82]);
        row.oneCardBetweenRiver = booleanValue(splitted[83]);
        row.villainVPIP = getVillainVPIP(splitted); //84
        row.villainPFR = getVillainPFR(splitted);
        row.villainAF = getVillainAF(splitted);
        row.villain3Bet = getVillain3Bet(splitted);
        row.villainFoldTo3Bet = getVillainFoldTo3Bet(splitted);
        row.villain4Bet = intValue(splitted[89]);
        row.villainFoldTo4Bet = intValue(splitted[90]);
        row.villainCbetFlop = getVillainCbetFlop(splitted);
        row.villainCbetTurn = getVillainCbetTurn(splitted);
        row.villainFoldToCbetFlop = getFoldToCbetFlop(splitted);
        row.villainFoldToCbetTurn = getFoldToCbetTurn(splitted);
        row.ranksAddedValueCategoryId = intValue(splitted[95]);

        row.potPreflopBB = Double.parseDouble(splitted[96]);
        row.potFlopBB = Double.parseDouble(splitted[97]);
        row.potTurnBB = Double.parseDouble(splitted[98]);
        row.potRiverBB = Double.parseDouble(splitted[99]);

        row.betMadeFlopPct = Double.parseDouble(splitted[100]);
        row.betMadeTurnPct = Double.parseDouble(splitted[101]);
        row.betMadeRiverPct = Double.parseDouble(splitted[102]);
        row.raiseMadePreFlopPct = Double.parseDouble(splitted[103]);
        row.raiseMadeFlopPct = Double.parseDouble(splitted[104]);
        row.raiseMadeTurnPct = Double.parseDouble(splitted[105]);
        row.raiseMadeRiverPct = Double.parseDouble(splitted[106]);
        row.raise2MadePreFlopPct = Double.parseDouble(splitted[107]);
        row.raise2MadeFlopPct = Double.parseDouble(splitted[108]);
        row.raise2MadeTurnPct = Double.parseDouble(splitted[109]);
        row.raise2MadeRiverPct = Double.parseDouble(splitted[110]);

        row.villainBetMadeFlopPct = Double.parseDouble(splitted[111]);
        row.villainBetMadeTurnPct = Double.parseDouble(splitted[112]);
        row.villainBetMadeRiverPct = Double.parseDouble(splitted[113]);
        row.villainRaiseMadePreFlopPct = Double.parseDouble(splitted[114]);
        row.villainRaiseMadeFlopPct = Double.parseDouble(splitted[115]);
        row.villainRaiseMadeTurnPct = Double.parseDouble(splitted[116]);
        row.villainRaiseMadeRiverPct = Double.parseDouble(splitted[117]);
        row.villainRaise2MadePreFlopPct = Double.parseDouble(splitted[118]);
        row.villainRaise2MadeFlopPct = Double.parseDouble(splitted[119]);
        row.villainRaise2MadeTurnPct = Double.parseDouble(splitted[120]);
        row.villainRaise2MadeRiverPct = Double.parseDouble(splitted[121]);

        row.boardTextureFlop = new BoardTextureFlop(row);
        row.boardTextureTurn = new BoardTextureTurn(row);
        row.boardTextureRiver = new BoardTextureRiver(row);

        return row;
    }

    public static String getPosition(String[] splitted) {
        return splitted[2];
    }

    public static String getVillainPosition(String[] splitted) {
        return splitted[3];
    }

    public static String getActionsPreflop(String[] splitted) {
        return splitted[7];
    }

    public static String getActionsFlop(String[] splitted) {
        return splitted[8];
    }

    public static String getActionsTurn(String[] splitted) {
        return splitted[9];
    }

    public static String getActionsRiver(String[] splitted) {
        return splitted[10];
    }

    public static String getVillainActionsPreflop(String[] splitted) {
        return splitted[11];
    }

    public static String getVillainActionsFlop(String[] splitted) {
        return splitted[12];
    }

    public static String getVillainActionsTurn(String[] splitted) {
        return splitted[13];
    }

    public static String getVillainActionsRiver(String[] splitted) {
        return splitted[14];
    }

    ///////////////
    public static String getHoleCard1(String[] splitted) {
        return splitted[34];
    }

    public static String getHoleCard2(String[] splitted) {
        return splitted[35];
    }

    public static String getFlopCard1(String[] splitted) {
        return splitted[36];
    }

    public static String getFlopCard2(String[] splitted) {
        return splitted[37];
    }

    public static String getFlopCard3(String[] splitted) {
        return splitted[38];
    }

    public static String getTurnCard(String[] splitted) {
        return splitted[39];
    }

    public static String getRiverCard(String[] splitted) {
        return splitted[40];
    }

    ////////////////
    public static String getPlayersPreflop(String[] splitted) {
        return splitted[44];
    }

    public static String getPlayersOnFlop(String[] splitted) {
        return splitted[45];
    }

    public static int getVillainVPIP(String[] splitted) {
        return intValue(splitted[84]);
    }

    public static int getVillainPFR(String[] splitted) {
        return intValue(splitted[85]);
    }

    public static int getVillainAF(String[] splitted) {
        return intValue(splitted[86]);
    }

    public static int getVillain3Bet(String[] splitted) {
        return intValue(splitted[87]);
    }

    public static int getVillainFoldTo3Bet(String[] splitted) {
        return intValue(splitted[88]);
    }

    public static int getVillainCbetFlop(String[] splitted) {
        return intValue(splitted[91]);
    }

    public static int getVillainCbetTurn(String[] splitted) {
        return intValue(splitted[92]);
    }

    public static int getFoldToCbetFlop(String[] splitted) {
        return intValue(splitted[93]);
    }

    public static int getFoldToCbetTurn(String[] splitted) {
        return intValue(splitted[94]);
    }

    public static String createLine(PostFlopRow row) {
        String line = "";

        // 0
        line = line + intValue(row.won) + " ";
        // 1
        line = line + round(row.wonBB, 2) + " ";
        // 2
        line = line + row.position + " ";
        // 3
        line = line + row.villainPosition + " ";
        // 4
        line = line + (row.madeHandSpecificFlop != null ? row.madeHandSpecificFlop.ordinal() : "null") + " ";
        // 5
        line = line + (row.madeHandSpecificTurn != null ? row.madeHandSpecificTurn.ordinal() : "null") + " ";
        // 6
        line = line + (row.madeHandSpecificRiver != null ? row.madeHandSpecificRiver.ordinal() : "null") + " ";
        // 7
        line = line + row.actionsPreflop + " ";
        // 8
        line = line + row.actionsFlop + " ";
        // 9
        line = line + row.actionsTurn + " ";
        // 10
        line = line + row.actionsRiver + " ";
        // 11
        line = line + row.actionsVillainPreflop + " ";
        // 12
        line = line + row.actionsVillainFlop + " ";
        // 13
        line = line + row.actionsVillainTurn + " ";
        // 14
        line = line + row.actionsVillainRiver + " ";
        // 15
        line = line + intValue(row.overCardsFlop) + " ";
        // 16
        line = line + intValue(row.backdoorFlushDrawFlop) + " ";
        // 17
        line = line + intValue(row.gutshotDrawFlop) + " ";
        // 18
        line = line + intValue(row.openEndedStraightDrawFlop) + " ";
        // 19
        line = line + intValue(row.flushDrawFlop) + " ";
        // 20
        line = line + intValue(row.flushNutDrawFlop) + " ";
        // 21
        line = line + intValue(row.flushSecondNutDrawFlop) + " ";
        // 22
        line = line + intValue(row.flushThirdNutDrawFlop) + " ";
        // 23
        line = line + intValue(row.flushWeakDrawFlop) + " ";
        // 24
        line = line + intValue(row.overCardsTurn) + " ";
        // 25
        line = line + intValue(row.backdoorFlushDrawTurn) + " ";
        // 26
        line = line + intValue(row.gutshotDrawTurn) + " ";
        // 27
        line = line + intValue(row.openEndedStraightDrawTurn) + " ";
        // 28
        line = line + intValue(row.flushDrawTurn) + " ";
        // 29
        line = line + intValue(row.flushNutDrawTurn) + " ";
        // 30
        line = line + intValue(row.flushSecondNutDrawTurn) + " ";
        // 31
        line = line + intValue(row.flushThirdNutDrawTurn) + " ";
        // 32
        line = line + intValue(row.flushWeakDrawTurn) + " ";
        // 33
        line = line + intValue(row.showdown) + " ";
        // 34
        line = line + row.holeCard1Id + " ";
        // 35
        line = line + row.holeCard2Id + " ";
        // 36
        line = line + row.flopCard1Id + " ";
        // 37
        line = line + row.flopCard2Id + " ";
        // 38
        line = line + row.flopCard3Id + " ";
        // 39
        line = line + row.turnCardId + " ";
        // 40
        line = line + row.riverCardId + " ";
        // 41
        line = line + row.villainHoleCard1Id + " ";
        // 42
        line = line + row.villainHoleCard2Id + " ";
        // 43
        line = line + row.villainRangeComboId + " ";
        // 44
        line = line + row.playersPreflop + " ";
        // 45
        line = line + row.playersOnFlop + " ";
        // 46
        line = line + intValue(row.aceFlop) + " ";
        // 47
        line = line + intValue(row.kingFlop) + " ";
        // 48
        line = line + intValue(row.queenFlop) + " ";
        // 49
        line = line + intValue(row.jackFlop) + " ";
        // 50
        line = line + row.nroOfSameSuitsFlop + " ";
        // 51
        line = line + row.nroOfSameRanksFlop + " ";
        // 52
        line = line + intValue(row.straightPossibleFlop) + " ";
        // 53
        line = line + row.ranksAddedValueFlop + " ";
        // 54
        line = line + intValue(row.aceTurn) + " ";
        // 55
        line = line + intValue(row.kingTurn) + " ";
        // 56
        line = line + intValue(row.turnCardIsHighest) + " ";
        // 57
        line = line + intValue(row.turnCardIsSecondHighest) + " ";
        // 58
        line = line + intValue(row.turnCardIsThirdHighest) + " ";
        // 59
        line = line + row.nroOfSameSuitsTurn + " ";
        // 60
        line = line + intValue(row.boardHasPairTurn) + " ";
        // 61
        line = line + intValue(row.boardHasTwoPairsTurn) + " ";
        // 62
        line = line + intValue(row.boardHasTriplesTurn) + " ";
        // 63
        line = line + intValue(row.straightPossibleWithOneCardTurn) + " ";
        // 64
        line = line + intValue(row.straightPossibleWithTwoCardsTurn) + " ";
        // 65
        line = line + intValue(row.aceRiver) + " ";
        // 66
        line = line + intValue(row.kingRiver) + " ";
        // 67
        line = line + intValue(row.riverCardIsHighest) + " ";
        // 68
        line = line + intValue(row.riverCardIsSecondHighest) + " ";
        // 69
        line = line + intValue(row.riverCardIsThirdeHighest) + " ";
        // 70
        line = line + row.nroOfSameSuitsRiver + " ";
        // 71
        line = line + intValue(row.boardHasPairRiver) + " ";
        // 72
        line = line + intValue(row.boardHasTwoPairsRiver) + " ";
        // 73
        line = line + intValue(row.boardHasTriplesRiver) + " ";
        // 74
        line = line + intValue(row.boardHasFullhouseRiver) + " ";
        // 75
        line = line + intValue(row.straightPossibleWithOneCardRiver) + " ";
        // 76
        line = line + intValue(row.straightPossibleWithTwoCardsRiver) + " ";
        // 77
        line = line + intValue(row.straightOnBoardRiver) + " ";
        // 78
        line = line + intValue(row.noRanksOrOneRankBetweenFlop) + " ";
        // 79
        line = line + intValue(row.twoOrThreeRanksBetweenFlop) + " ";
        // 80
        line = line + intValue(row.noCardsBetweenTurn) + " ";
        // 81
        line = line + intValue(row.oneCardBetweenTurn) + " ";
        // 82
        line = line + intValue(row.noCardsBetweenRiver) + " ";
        // 83
        line = line + intValue(row.oneCardBetweenRiver) + " ";
        // 84
        line = line + row.villainVPIP + " ";
        // 85
        line = line + row.villainPFR + " ";
        // 86
        line = line + row.villainAF + " ";
        // 87
        line = line + row.villain3Bet + " ";
        // 88
        line = line + row.villainFoldTo3Bet + " ";
        // 89
        line = line + row.villain4Bet + " ";
        // 90
        line = line + row.villainFoldTo4Bet + " ";
        // 91
        line = line + row.villainCbetFlop + " ";
        // 92
        line = line + row.villainCbetTurn + " ";
        // 93
        line = line + row.villainFoldToCbetFlop + " ";
        // 94
        line = line + row.villainFoldToCbetTurn + " ";
        // 95
        line = line + row.ranksAddedValueCategoryId + " ";
        line = line + round(row.potPreflopBB, 2) + " ";
        line = line + round(row.potFlopBB, 2) + " ";
        line = line + round(row.potTurnBB, 2) + " ";
        line = line + round(row.potRiverBB, 2) + " ";

        line = line + round(row.betMadeFlopPct, 2) + " ";
        line = line + round(row.betMadeTurnPct, 2) + " ";
        line = line + round(row.betMadeRiverPct, 2) + " ";
        line = line + round(row.raiseMadePreFlopPct, 2) + " ";
        line = line + round(row.raiseMadeFlopPct, 2) + " ";
        line = line + round(row.raiseMadeTurnPct, 2) + " ";
        line = line + round(row.raiseMadeRiverPct, 2) + " ";
        line = line + round(row.raise2MadePreFlopPct, 2) + " ";
        line = line + round(row.raise2MadeFlopPct, 2) + " ";
        line = line + round(row.raise2MadeTurnPct, 2) + " ";
        line = line + round(row.raise2MadeRiverPct, 2) + " ";

        line = line + round(row.villainBetMadeFlopPct, 2) + " ";
        line = line + round(row.villainBetMadeTurnPct, 2) + " ";
        line = line + round(row.villainBetMadeRiverPct, 2) + " ";
        line = line + round(row.villainRaiseMadePreFlopPct, 2) + " ";
        line = line + round(row.villainRaiseMadeFlopPct, 2) + " ";
        line = line + round(row.villainRaiseMadeTurnPct, 2) + " ";
        line = line + round(row.villainRaiseMadeRiverPct, 2) + " ";
        line = line + round(row.villainRaise2MadePreFlopPct, 2) + " ";
        line = line + round(row.villainRaise2MadeFlopPct, 2) + " ";
        line = line + round(row.villainRaise2MadeTurnPct, 2) + " ";
        line = line + round(row.villainRaise2MadeRiverPct, 2) + " ";

        return line;
    }

    private static int intValue(String s) {
        return Integer.parseInt(s);
    }

    private static int intValue(boolean b) {
        return b == true ? 1 : 0;
    }

    private static boolean booleanValue(String s) {
        return s.equals("1");
    }

    public static Double round(Double value, int places) {
        if (value == null) {
            return null;
        }
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd;

        try {
            bd = BigDecimal.valueOf(value);
        } catch (Exception e) {
            return null;
        }

        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
