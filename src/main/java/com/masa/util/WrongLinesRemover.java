package com.masa.util;

import com.masa.pokertracker.model.PTActionType;
import com.masa.type.BoardTextureFlop;
import com.masa.type.Draw;
import com.masa.type.MadeHandSpecific;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author compuuter
 */
public class WrongLinesRemover {

    public static List<PostFlopRow> removeLosingLines(List<PostFlopRow> rows, PostFlopRow current, Street currentStreet, boolean ip) {
        //System.out.println("WrongLinesRemover start...");
        //System.out.println("Rows before removing wrongly played lines: " + rows.size());
        long start = System.currentTimeMillis();
        List<PostFlopRow> riverRows = new ArrayList<>();
        List<PostFlopRow> turnRows = new ArrayList<>();
        List<PostFlopRow> winningRows = new ArrayList<>();

        if (currentStreet == Street.PREFLOP) {
            /*getWinningRowsForStreet(Street.RIVER, linesMap, current, winningRows, currentStreet, ip);
            getWinningRowsForStreet(Street.TURN, linesMap, current, winningRows, currentStreet, ip);
            getWinningRowsForStreet(Street.FLOP, linesMap, current, winningRows, currentStreet, ip);
            getWinningRowsForStreet(Street.PREFLOP, linesMap, current, winningRows, currentStreet, ip);*/
        } else if (currentStreet == Street.FLOP) {
            Map<Line, List<PostFlopRow>> linesMapRiver = mapRows(rows, Street.RIVER, currentStreet);
            riverRows.addAll(getWinningRowsForStreet(Street.RIVER, linesMapRiver, current, currentStreet, ip));
            //System.out.println("Mapped count river: " + countRows(linesMapRiver));

            Map<Line, List<PostFlopRow>> linesMapTurn = mapRows(riverRows, Street.TURN, currentStreet);
            turnRows.addAll(getWinningRowsForStreet(Street.TURN, linesMapTurn, current, currentStreet, ip));
            System.out.println("Mapped count turn: " + countRows(linesMapTurn));

            Map<Line, List<PostFlopRow>> linesMapFlop = mapRows(turnRows, Street.FLOP, currentStreet);
            winningRows.addAll(getWinningRowsForStreet(Street.FLOP, linesMapFlop, current, currentStreet, ip));
            System.out.println("Mapped count flop: " + countRows(linesMapFlop));
        } else if (currentStreet == Street.TURN) {
            Map<Line, List<PostFlopRow>> linesMapRiver = mapRows(rows, Street.RIVER, currentStreet);
            riverRows.addAll(getWinningRowsForStreet(Street.RIVER, linesMapRiver, current, currentStreet, ip));

            Map<Line, List<PostFlopRow>> linesMapTurn = mapRows(riverRows, Street.TURN, currentStreet);
            winningRows.addAll(getWinningRowsForStreet(Street.TURN, linesMapTurn, current, currentStreet, ip));
        } else if (currentStreet == Street.RIVER) {
            Map<Line, List<PostFlopRow>> linesMapRiver = mapRows(rows, Street.RIVER, currentStreet);
            winningRows.addAll(getWinningRowsForStreet(Street.RIVER, linesMapRiver, current, currentStreet, ip));
        }

        long end = System.currentTimeMillis();
        //System.out.println("WrongLinesRemover TOOK: " + ((start - end) / 1000) + " seconds");
        //System.out.println("Rows after removing wrongly played lines: " + winningRows.size());
        return winningRows;
    }

    private static int countRows(Map<Line, List<PostFlopRow>> mappedRows) {
        int count = 0;

        for (Map.Entry<Line, List<PostFlopRow>> entry : mappedRows.entrySet()) {
            List<PostFlopRow> mappedByTexture = entry.getValue();

            for (PostFlopRow r : mappedByTexture) {
                //System.out.println("" + r.actionsFlop + " : " + r.actionsTurn + " : " + r.actionsRiver);
            }
            System.out.println("-----------");
            count = count + mappedByTexture.size();

        }
        return count;
    }

    private static List<PostFlopRow> getWinningRowsForStreet(Street street, Map<Line, List<PostFlopRow>> linesMap, PostFlopRow current, Street currentStreet, boolean ip) {
        //System.out.println("WinningRows start " + street + " " + winningRows.size());
        List<PostFlopRow> winningRows = new ArrayList<>();
        for (Map.Entry<Line, List<PostFlopRow>> entry : linesMap.entrySet()) {
            List<PostFlopRow> mappedByTexture = entry.getValue();
            //System.out.println("mapped rows " + mappedByTexture.size());
            winningRows.addAll(getWinningRows(mappedByTexture, street, ip, currentStreet, current));
            //System.out.println("WinningRows " + street + " " + winningRows.size());
        }
        return winningRows;
        //System.out.println("WinningRows end " + street + " " + winningRows.size());
    }

    private static List<PostFlopRow> getWinningRows(List<PostFlopRow> mappedRows, Street street, boolean ip, Street currentStreet, PostFlopRow current) {
        List<List<String>> ipList = new ArrayList<>();
        ipList.add(Arrays.asList("BR", "BC", "BF"));
        ipList.add(Arrays.asList("RR", "RC", "RF"));
        ipList.add(Arrays.asList("B", "X"));
        ipList.add(Arrays.asList("R", "C", "F"));

        List<List<String>> oopList = new ArrayList<>();
        oopList.add(Arrays.asList("BRR", "BRC", "BRF"));
        oopList.add(Arrays.asList("XRR", "XRC", "XRF"));
        oopList.add(Arrays.asList("BR", "BC", "BF"));
        oopList.add(Arrays.asList("XR", "XC", "XF"));
        oopList.add(Arrays.asList("B", "X"));

        List<PostFlopRow> winningRows = new ArrayList<>();
        boolean isCurrentStreet = currentStreet == street;

        if (ip) {
            for (List<String> list : ipList) {

                winningRows.addAll(getWinningRowsWithPossibleActions(mappedRows, list, street, currentStreet, current));

            }
        } else {
            for (List<String> list : oopList) {
                winningRows.addAll(getWinningRowsWithPossibleActions(mappedRows, list, street, currentStreet, current));
            }
        }

        for (PostFlopRow row : mappedRows) {
            //System.out.println("" + row.actionsFlop + " : " + row.actionsTurn + " : " + row.actionsRiver);
            if (row.getActions(street).equals("-")) {
                winningRows.add(row);
            }
        }

        return winningRows;
    }

    private static List<PostFlopRow> getWinningRowsWithPossibleActions(List<PostFlopRow> mappedByTexture,
            List<String> actions, Street street, Street currentStreet, PostFlopRow current) {
        List<PostFlopRow> winningRows = new ArrayList<>();

        boolean isCurrentStreet = currentStreet == street;

        List<PostFlopRow> folds = new ArrayList<>();
        List<PostFlopRow> checks = new ArrayList<>();
        List<PostFlopRow> calls = new ArrayList<>();
        List<PostFlopRow> bets = new ArrayList<>();
        List<PostFlopRow> raises = new ArrayList<>();

        //List<PostFlopRow> rest = new ArrayList<>();
        Double foldsWonAMount = null;
        Double checksWonAMount = null;
        Double callsWonAMount = null;
        Double betsWonAMount = null;
        Double raisesWonAMount = null;

        for (PostFlopRow row : mappedByTexture) {
            //System.out.println("" + row.actionsFlop + " : " + row.actionsTurn + " : " + row.actionsRiver);
            if (row.getActions(street).equals("-")) {
                //winningRows.add(row);
                continue;
            }
            /*if (isCurrentStreet && ((actions.get(0).length() - 1) == current.getActions(street).length())) {
                //winningRows.add(row);
                continue;
            }*/
            for (String actionString : actions) {
                //String aa = row.getActions(street);
                //String cc = current.getActions(street);
                /* if (isCurrentStreet && ((actionString.length() - 1) == current.getActions(street).length())) {
                    //winningRows.add(row);
                    continue;
                }*/
                if (row.getActions(street).equals(actionString)) {
                    //System.out.println("actionString: " + actionString + " streetActions: " + row.getActions(street));
                    if (actionString.endsWith("F")) {
                        folds.add(row);
                        if (foldsWonAMount == null) {
                            foldsWonAMount = 0.0;
                        }
                        foldsWonAMount += row.wonBB;
                    } else if (actionString.endsWith("X")) {
                        checks.add(row);
                        if (checksWonAMount == null) {
                            checksWonAMount = 0.0;
                        }
                        checksWonAMount += row.wonBB;
                    } else if (actionString.endsWith("C")) {
                        calls.add(row);
                        if (callsWonAMount == null) {
                            callsWonAMount = 0.0;
                        }
                        callsWonAMount += row.wonBB;
                    } else if (actionString.endsWith("B")) {
                        bets.add(row);
                        if (betsWonAMount == null) {
                            betsWonAMount = 0.0;
                        }
                        betsWonAMount += row.wonBB;
                    } else if (actionString.endsWith("R")) {
                        raises.add(row);
                        if (raisesWonAMount == null) {
                            raisesWonAMount = 0.0;
                        }
                        raisesWonAMount += row.wonBB;
                    }
                    break;
                }

            }

        }

        /*if (folds.isEmpty() && checks.isEmpty() && calls.isEmpty() && bets.isEmpty() && raises.isEmpty()) {
            return winningRows;
        }*/
        int foldsSize = folds.size();
        int checksSize = checks.size();
        int callsSize = calls.size();
        int betsSize = bets.size();
        int raisesSize = raises.size();

        int minimumSize = 3;

        // Check if there is enough data, if not, return all
        /*if (!folds.isEmpty() && foldsSize < minimumSize
                || !checks.isEmpty() && checksSize < minimumSize
                || !calls.isEmpty() && callsSize < minimumSize
                || !bets.isEmpty() && betsSize < minimumSize
                || !raises.isEmpty() && raisesSize < minimumSize) {
            //System.out.println("RETURN ALLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
            return List.of();
        }*/
        MostProfitableActionAndWonAMount mostProfitableActionAndWonAmount = mostProfitableActionAndWonAmount(foldsSize, checksSize, callsSize, betsSize, raisesSize,
                foldsWonAMount, checksWonAMount, callsWonAMount, betsWonAMount, raisesWonAMount);
        PTActionType mostProfitableAction = mostProfitableActionAndWonAmount.action;
        double mostProfitableActionAvgWon = mostProfitableActionAndWonAmount.wonBB;

        if (mostProfitableAction != PTActionType.FOLD) {
            setWonAmountAndActions(folds, mostProfitableActionAvgWon, mostProfitableAction, street, currentStreet);
        }
        if (mostProfitableAction != PTActionType.CHECK) {
            setWonAmountAndActions(checks, mostProfitableActionAvgWon, mostProfitableAction, street, currentStreet);
        }
        if (mostProfitableAction != PTActionType.CALL) {
            setWonAmountAndActions(calls, mostProfitableActionAvgWon, mostProfitableAction, street, currentStreet);
        }
        if (mostProfitableAction != PTActionType.BET) {
            setWonAmountAndActions(bets, mostProfitableActionAvgWon, mostProfitableAction, street, currentStreet);
        }
        if (mostProfitableAction != PTActionType.RAISE) {
            setWonAmountAndActions(raises, mostProfitableActionAvgWon, mostProfitableAction, street, currentStreet);
        }

        winningRows.addAll(folds);
        winningRows.addAll(checks);
        winningRows.addAll(calls);
        winningRows.addAll(bets);
        winningRows.addAll(raises);

        System.out.println("mappedRows size = " + mappedByTexture.size() + " II winningRows size = " + winningRows.size());

        return winningRows;
    }

    private static void setWonAmountAndActions(List<PostFlopRow> rows, double amount, PTActionType mostProfitableAction, Street street, Street currentStreet) {
        rows.stream().map(row -> {
            if (amount > row.wonBB) {
                row.wonBB = amount;
            }
            /*String action = row.getActions(street);
            action = action.substring(0, action.length() - 1);
            row.setActions(street, action + mostProfitableAction.value());*/
            return row;
        }).forEachOrdered(row -> {
            if (null != currentStreet) {
                /*switch (currentStreet) {
                    case PREFLOP:
                        row.setActions(Street.FLOP, "T");
                        row.setActions(Street.TURN, "T");
                        row.setActions(Street.RIVER, "T");
                        break;
                    case FLOP:
                        row.setActions(Street.TURN, "T");
                        row.setActions(Street.RIVER, "T");
                        break;
                    case TURN:
                        row.setActions(Street.RIVER, "T");
                        break;
                    default:
                        break;
                }*/
            }

        });
    }

    private static class MostProfitableActionAndWonAMount {

        public PTActionType action;
        public Double wonBB;

        public MostProfitableActionAndWonAMount(PTActionType action, Double wonBB) {
            this.action = action;
            this.wonBB = wonBB;
        }
    }

    private static MostProfitableActionAndWonAMount mostProfitableActionAndWonAmount(int foldsCount, int checksCount, int callsCount, int betsCount, int raisesCount, Double foldsWonAMount,
            Double checksWonAMount,
            Double callsWonAMount,
            Double betsWonAMount,
            Double raisesWonAMount) {

        //System.out.println("folds: " + foldsCount + " checks: " + checksCount + " calls: " + callsCount + " bets: " + betsCount + " raises: " + raisesCount);
        PTActionType actionType = PTActionType.FOLD;
        double mostWon = -1000.0;

        Double avgFold = foldsWonAMount != null ? foldsWonAMount / foldsCount : null;
        Double avgCheck = checksWonAMount != null ? checksWonAMount / checksCount : null;
        Double avgCall = callsWonAMount != null ? callsWonAMount / callsCount : null;
        Double avgBet = betsWonAMount != null ? betsWonAMount / betsCount : null;
        Double avgRaise = raisesWonAMount != null ? raisesWonAMount / raisesCount : null;

        if (foldsWonAMount != null && avgFold > mostWon) {
            actionType = PTActionType.FOLD;
            mostWon = avgFold;
        }
        if (checksWonAMount != null && avgCheck > mostWon) {
            actionType = PTActionType.CHECK;
            mostWon = avgCheck;
        }
        if (callsWonAMount != null && avgCall > mostWon) {
            actionType = PTActionType.CALL;
            mostWon = avgCall;
        }
        if (betsWonAMount != null && avgBet > mostWon) {
            actionType = PTActionType.BET;
            mostWon = avgBet;
        }
        if (raisesWonAMount != null && avgRaise > mostWon) {
            actionType = PTActionType.RAISE;
            mostWon = avgRaise;
        }

        return new MostProfitableActionAndWonAMount(actionType, mostWon);
    }

    /*
    Map rows by current and future street textures
     */
    private static Map<Line, List<PostFlopRow>> mapRows(List<PostFlopRow> rows, Street street, Street currentStreet) {
        Map<Line, List<PostFlopRow>> linesMap = new HashMap<>();

        for (PostFlopRow row : rows) {
            Line line = null;
            if (street == currentStreet) {
                line = new Line(null, null, null, null, null, null, null, null);
            } else if (street == Street.FLOP) {
                line = new Line(row.boardTextureFlop, null, null, null, null, null, row.drawsFlop, null);
            } else if (street == Street.TURN) {
                line = new Line(row.boardTextureFlop, row.madeHandSpecificTurn, null, row.actionsFlop, null, null, row.drawsFlop, row.drawsTurn);
            } else if (street == Street.RIVER) {
                line = new Line(row.boardTextureFlop, row.madeHandSpecificTurn, row.madeHandSpecificRiver, row.actionsFlop, row.actionsTurn, null, row.drawsFlop, row.drawsTurn);
            }

            if (linesMap.containsKey(line)) {
                linesMap.get(line).add(row);
            } else {
                List<PostFlopRow> postFlopRows = new ArrayList<>();
                postFlopRows.add(row);
                linesMap.put(line, postFlopRows);
            }
        }

        return linesMap;
    }

    public static class Line {

        BoardTextureFlop flopTexture;
        MadeHandSpecific turnTexture;
        MadeHandSpecific riverTexture;

        String actionsFlop;
        String actionsTurn;
        String actionsRiver;

        Set<Draw> flopDraws;
        Set<Draw> turnDraws;

        public Line(
                BoardTextureFlop flopTexture,
                MadeHandSpecific turnTexture,
                MadeHandSpecific riverTexture,
                String actionsFlop,
                String actionsTurn,
                String actionsRiver,
                Set<Draw> flopDraws,
                Set<Draw> turnDraws
        ) {
            this.flopTexture = flopTexture;
            this.turnTexture = turnTexture;
            this.riverTexture = riverTexture;
            this.actionsFlop = actionsFlop;
            this.actionsTurn = actionsTurn;
            this.actionsRiver = actionsRiver;
            this.flopDraws = flopDraws;
            this.turnDraws = turnDraws;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 19 * hash + Objects.hashCode(this.flopTexture);
            hash = 19 * hash + Objects.hashCode(this.turnTexture);
            hash = 19 * hash + Objects.hashCode(this.riverTexture);
            hash = 19 * hash + Objects.hashCode(this.actionsFlop);
            hash = 19 * hash + Objects.hashCode(this.actionsTurn);
            hash = 19 * hash + Objects.hashCode(this.actionsRiver);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Line other = (Line) obj;
            if (!Objects.equals(this.actionsFlop, other.actionsFlop)) {
                return false;
            }
            if (!Objects.equals(this.actionsTurn, other.actionsTurn)) {
                return false;
            }
            if (!Objects.equals(this.actionsRiver, other.actionsRiver)) {
                return false;
            }
            if (!Objects.equals(this.flopTexture, other.flopTexture)) {
                return false;
            }
            if (!Objects.equals(this.turnTexture, other.turnTexture)) {
                return false;
            }
            if (!Objects.equals(this.riverTexture, other.riverTexture)) {
                return false;
            }

            if (!equalSets(other.flopDraws, this.flopDraws)) {
                return false;
            }

            if (!equalSets(other.turnDraws, this.turnDraws)) {
                return false;
            }
            return true;
        }

    }

    private static boolean equalSets(Set<Draw> one, Set<Draw> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        return one.equals(two);
    }
}
