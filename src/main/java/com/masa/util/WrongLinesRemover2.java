package com.masa.util;

import com.masa.pokertracker.model.PTActionType;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import com.masa.util.PostFlopRowGrouper.TextureAndActionsLine;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class WrongLinesRemover2 {

    public static List<PostFlopRow> removeLosingLines(List<PostFlopRow> rows, PostFlopRow current, Street currentStreet, boolean ip, CardSet holeCards) {
        //System.out.println("WrongLinesRemover start...");
        //System.out.println("Rows before removing wrongly played lines: " + rows.size());

        List<PostFlopRow> riverRows = new ArrayList<>();
        List<PostFlopRow> turnRows = new ArrayList<>();
        List<PostFlopRow> winningRows = new ArrayList<>();

        if (currentStreet == Street.PREFLOP) {
            /*getWinningRowsForStreet(Street.RIVER, linesMap, current, winningRows, currentStreet, ip);
            getWinningRowsForStreet(Street.TURN, linesMap, current, winningRows, currentStreet, ip);
            getWinningRowsForStreet(Street.FLOP, linesMap, current, winningRows, currentStreet, ip);
            getWinningRowsForStreet(Street.PREFLOP, linesMap, current, winningRows, currentStreet, ip);*/
        } else if (currentStreet == Street.FLOP) {
            Map<TextureAndActionsLine, List<PostFlopRow>> linesMapRiver = PostFlopRowGrouper.mapRowsByTextureAndActions(rows, Street.RIVER, currentStreet, holeCards);
            riverRows.addAll(getWinningRowsForStreet(Street.RIVER, linesMapRiver, current, currentStreet, ip));
            //System.out.println("Mapped count river: " + countRows(linesMapRiver));

            Map<TextureAndActionsLine, List<PostFlopRow>> linesMapTurn = PostFlopRowGrouper.mapRowsByTextureAndActions(riverRows, Street.TURN, currentStreet, holeCards);
            //System.out.println("Mapped count turn: " + countRows(linesMapTurn));
            turnRows.addAll(getWinningRowsForStreet(Street.TURN, linesMapTurn, current, currentStreet, ip));

            Map<TextureAndActionsLine, List<PostFlopRow>> linesMapFlop = PostFlopRowGrouper.mapRowsByTextureAndActions(turnRows, Street.FLOP, currentStreet, holeCards);
            winningRows.addAll(getWinningRowsForStreet(Street.FLOP, linesMapFlop, current, currentStreet, ip));

        } else if (currentStreet == Street.TURN) {
            Map<TextureAndActionsLine, List<PostFlopRow>> linesMapRiver = PostFlopRowGrouper.mapRowsByTextureAndActions(rows, Street.RIVER, currentStreet, holeCards);
            riverRows.addAll(getWinningRowsForStreet(Street.RIVER, linesMapRiver, current, currentStreet, ip));

            Map<TextureAndActionsLine, List<PostFlopRow>> linesMapTurn = PostFlopRowGrouper.mapRowsByTextureAndActions(riverRows, Street.TURN, currentStreet, holeCards);
            winningRows.addAll(getWinningRowsForStreet(Street.TURN, linesMapTurn, current, currentStreet, ip));
        } else if (currentStreet == Street.RIVER) {
            Map<TextureAndActionsLine, List<PostFlopRow>> linesMapRiver = PostFlopRowGrouper.mapRowsByTextureAndActions(rows, Street.RIVER, currentStreet, holeCards);
            winningRows.addAll(getWinningRowsForStreet(Street.RIVER, linesMapRiver, current, currentStreet, ip));
        }

        //System.out.println("WrongLinesRemover TOOK: " + ((start - end) / 1000) + " seconds");
        //System.out.println("Rows after removing wrongly played lines: " + winningRows.size());
        return winningRows;
    }

    private static List<PostFlopRow> getWinningRowsForStreet(Street street, Map<TextureAndActionsLine, List<PostFlopRow>> linesMap, PostFlopRow current, Street currentStreet, boolean ip) {
        //System.out.println("WinningRows start " + street + " " + winningRows.size());
        List<PostFlopRow> winningRows = new ArrayList<>();
        for (Map.Entry<TextureAndActionsLine, List<PostFlopRow>> entry : linesMap.entrySet()) {
            List<PostFlopRow> mappedByTexture = entry.getValue();
            //System.out.println("mapped rows " + mappedByTexture.size());
            winningRows.addAll(getWinningRows(mappedByTexture, street, ip, currentStreet, current));
            //System.out.println("WinningRows " + street + " " + winningRows.size());
        }
        return winningRows;
        //System.out.println("WinningRows end " + street + " " + winningRows.size());
    }

    private static List<PostFlopRow> getWinningRows(List<PostFlopRow> mappedRows, Street street, boolean ip, Street currentStreet, PostFlopRow current) {
        List<List<String>> ipList = new ArrayList<>();
        ipList.add(Arrays.asList("BR", "BC", "BF"));
        ipList.add(Arrays.asList("RR", "RC", "RF"));
        ipList.add(Arrays.asList("B", "X"));
        ipList.add(Arrays.asList("R", "C", "F"));

        List<List<String>> oopList = new ArrayList<>();
        oopList.add(Arrays.asList("BRR", "BRC", "BRF"));
        oopList.add(Arrays.asList("XRR", "XRC", "XRF"));
        oopList.add(Arrays.asList("BR", "BC", "BF"));
        oopList.add(Arrays.asList("XR", "XC", "XF"));
        oopList.add(Arrays.asList("B", "X"));

        List<PostFlopRow> winningRows = new ArrayList<>();

        if (ip) {
            for (List<String> list : ipList) {

                winningRows.addAll(getWinningRowsWithPossibleActions(mappedRows, list, street, currentStreet, current));

            }
        } else {
            for (List<String> list : oopList) {
                winningRows.addAll(getWinningRowsWithPossibleActions(mappedRows, list, street, currentStreet, current));
            }
        }

        if (currentStreet != street) {
            for (PostFlopRow row : mappedRows) {
                //System.out.println("" + row.actionsFlop + " : " + row.actionsTurn + " : " + row.actionsRiver);
                if (row.getActions(street).equals("-")) {
                    winningRows.add(row);
                }
            }
        }

        return winningRows;
    }

    private static List<PostFlopRow> getWinningRowsWithPossibleActions(List<PostFlopRow> mappedByTexture,
            List<String> actions, Street street, Street currentStreet, PostFlopRow current) {
        List<PostFlopRow> winningRows = new ArrayList<>();

        List<PostFlopRow> folds = new ArrayList<>();
        List<PostFlopRow> checks = new ArrayList<>();
        List<PostFlopRow> calls = new ArrayList<>();
        List<PostFlopRow> bets = new ArrayList<>();
        List<PostFlopRow> raises = new ArrayList<>();

        //List<PostFlopRow> rest = new ArrayList<>();
        Double foldsWonAMount = null;
        Double checksWonAMount = null;
        Double callsWonAMount = null;
        Double betsWonAMount = null;
        Double raisesWonAMount = null;

        for (PostFlopRow row : mappedByTexture) {
            //System.out.println("" + row.actionsFlop + " : " + row.actionsTurn + " : " + row.actionsRiver);
            if (row.getActions(street).equals("-")) {
                //winningRows.add(row);
                continue;
            }
            /*if (isCurrentStreet && ((actions.get(0).length() - 1) == current.getActions(street).length())) {
                //winningRows.add(row);
                continue;
            }*/
            for (String actionString : actions) {
                //String aa = row.getActions(street);
                //String cc = current.getActions(street);
                /* if (isCurrentStreet && ((actionString.length() - 1) == current.getActions(street).length())) {
                    //winningRows.add(row);
                    continue;
                }*/
                if (row.getActions(street).equals(actionString)) {
                    double betOrWonAmount = EVUtil.getBetOrCallAmount(row, actionString, street);
                    //System.out.println("actionString: " + actionString + " streetActions: " + row.getActions(street));
                    if (actionString.endsWith("F")) {
                        folds.add(row);
                        if (foldsWonAMount == null) {
                            foldsWonAMount = 0.0;
                        }
                        foldsWonAMount += row.wonBB;
                    } else if (actionString.endsWith("X")) {
                        checks.add(row);
                        if (checksWonAMount == null) {
                            checksWonAMount = 0.0;
                        }
                        checksWonAMount += EVUtil.getEVForCheck(mappedByTexture, row, actionString, currentStreet);
                    } else if (actionString.endsWith("C")) {
                        calls.add(row);
                        if (callsWonAMount == null) {
                            callsWonAMount = 0.0;
                        }
                        callsWonAMount += EVUtil.getEVForCall(row, actionString, street);
                    } else if (actionString.endsWith("B")) {
                        bets.add(row);
                        if (betsWonAMount == null) {
                            betsWonAMount = 0.0;
                        }
                        betsWonAMount += EVUtil.getEVForBet(row, actionString, street);
                    } else if (actionString.endsWith("R")) {
                        raises.add(row);
                        if (raisesWonAMount == null) {
                            raisesWonAMount = 0.0;
                        }
                        raisesWonAMount += EVUtil.getEVForBet(row, actionString, street);
                    }
                    break;
                }

            }

        }

        /*if (folds.isEmpty() && checks.isEmpty() && calls.isEmpty() && bets.isEmpty() && raises.isEmpty()) {
            return mappedByTexture;
        }*/
        int foldsSize = folds.size();
        int checksSize = checks.size();
        int callsSize = calls.size();
        int betsSize = bets.size();
        int raisesSize = raises.size();

        int minimumSize = 3;

        // Check if there is enough data, if not, return all
        /*if (!folds.isEmpty() && foldsSize < minimumSize
                || !checks.isEmpty() && checksSize < minimumSize
                || !calls.isEmpty() && callsSize < minimumSize
                || !bets.isEmpty() && betsSize < minimumSize
                || !raises.isEmpty() && raisesSize < minimumSize) {
            System.out.println("RETURN ALLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
            winningRows.addAll(folds);
            winningRows.addAll(checks);
            winningRows.addAll(calls);
            winningRows.addAll(bets);
            winningRows.addAll(raises);

            return winningRows;
        }*/
        MostProfitableActionAndWonAMount mostProfitableActionAndWonAmount = mostProfitableActionAndWonAmount(foldsSize, checksSize, callsSize, betsSize, raisesSize,
                foldsWonAMount, checksWonAMount, callsWonAMount, betsWonAMount, raisesWonAMount);
        PTActionType mostProfitableAction = mostProfitableActionAndWonAmount.action;
        //double mostProfitableActionAvgWon = mostProfitableActionAndWonAmount.wonBB;

        if (mostProfitableAction == PTActionType.FOLD) {
            winningRows.addAll(folds);
        }
        if (mostProfitableAction == PTActionType.CHECK) {
            winningRows.addAll(checks);
        }
        if (mostProfitableAction == PTActionType.CALL) {
            winningRows.addAll(calls);
        }
        if (mostProfitableAction == PTActionType.BET) {
            winningRows.addAll(bets);
        }
        if (mostProfitableAction == PTActionType.RAISE) {
            winningRows.addAll(raises);
        }

        return winningRows;
    }

    private static class MostProfitableActionAndWonAMount {

        public PTActionType action;
        public Double wonBB;

        public MostProfitableActionAndWonAMount(PTActionType action, Double wonBB) {
            this.action = action;
            this.wonBB = wonBB;
        }
    }

    private static MostProfitableActionAndWonAMount mostProfitableActionAndWonAmount(int foldsCount, int checksCount, int callsCount, int betsCount, int raisesCount, Double foldsWonAMount,
            Double checksWonAMount,
            Double callsWonAMount,
            Double betsWonAMount,
            Double raisesWonAMount) {

        //System.out.println("folds: " + foldsCount + " checks: " + checksCount + " calls: " + callsCount + " bets: " + betsCount + " raises: " + raisesCount);
        PTActionType actionType = PTActionType.FOLD;
        double mostWon = -1000.0;

        Double avgFold = foldsWonAMount != null ? foldsWonAMount / foldsCount : null;
        Double avgCheck = checksWonAMount != null ? checksWonAMount / checksCount : null;
        Double avgCall = callsWonAMount != null ? callsWonAMount / callsCount : null;
        Double avgBet = betsWonAMount != null ? betsWonAMount / betsCount : null;
        Double avgRaise = raisesWonAMount != null ? raisesWonAMount / raisesCount : null;

        if (foldsWonAMount != null && avgFold > mostWon) {
            actionType = PTActionType.FOLD;
            mostWon = avgFold;
        }
        if (checksWonAMount != null && avgCheck > mostWon) {
            actionType = PTActionType.CHECK;
            mostWon = avgCheck;
        }
        if (callsWonAMount != null && avgCall > mostWon) {
            actionType = PTActionType.CALL;
            mostWon = avgCall;
        }
        if (betsWonAMount != null && avgBet > mostWon) {
            actionType = PTActionType.BET;
            mostWon = avgBet;
        }
        if (raisesWonAMount != null && avgRaise > mostWon) {
            actionType = PTActionType.RAISE;
        }

        return new MostProfitableActionAndWonAMount(actionType, mostWon);
    }
}
