package com.masa.util;

import com.masa.type.MadeHandSpecific;
import static com.masa.util.HandStrengthAnalyzer.similarRanks;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class MadeHandStraightAnalyzer {

    public static MadeHandSpecific getSpecificStraight(CardSet board, CardSet bestHand, CardSet holeCards) {
        int nroOfHoleCardsOnBestHand = MadeHandUtil.countNroOfHandsInBestHand(bestHand, holeCards);
        CardSet allCards = new CardSet(board);
        allCards.addAll(holeCards);
        // holcards on best hand
        if (nroOfHoleCardsOnBestHand > 0) {
            return MadeHandSpecific.STRAIGHT;
        } else {
            return MadeHandSpecific.NO_MADE_HAND;
            /*
            // straight on board
            // set
            boolean pp = MadeHandUtil.isPocketPair(holeCards);
            if (HandStrengthAnalyzer.similarRanks(holeCards.get(0).rankOf(), allCards) == 3) {
                return MadeHandSpecific.SET;
            }
            if (HandStrengthAnalyzer.similarRanks(holeCards.get(1).rankOf(), allCards) == 3) {
                return MadeHandSpecific.SET;
            }
            // two pair
            if (MadeHandUtil.twoPairOnBoard(allCards)) {
                return MadeHandSpecific.TWO_PAIR;
            }
            // pair
            if (!pp) {
                int nroOfHoleCardsConnected = MadeHandUtil.countRankConnections(board, holeCards);
                if (nroOfHoleCardsConnected == 1) {
                    Card connectedHoleCard = MadeHandUtil.getConnectedHoleCard(board, holeCards);
                    if (isTopPairOneCardOnBestHand(board, connectedHoleCard)) {
                        return MadeHandSpecific.TOP_PAIR;
                    }
                    if (isMiddlePairOneCardOnBestHand(board, connectedHoleCard)) {
                        return MadeHandSpecific.MIDDLE_PAIR;
                    }
                    return MadeHandSpecific.WEAK_PAIR;
                }
            }
            if (pp) {
                Rank leftRank = holeCards.get(0).rankOf();
                Rank rightRank = holeCards.get(1).rankOf();
                if (isOverPair(board, leftRank, rightRank)) {
                    return MadeHandSpecific.OVERPAIR;
                }
                return MadeHandSpecific.NO_MADE_HAND;
            }
            // Ace high
            if (holeCards.containsRank(Rank.ACE)) {
                return MadeHandSpecific.ACE_HIGH;
            }
            // No made hand
            return MadeHandSpecific.NO_MADE_HAND;
             */
        }
    }

    private static boolean isTopPairOneCardOnBestHand(CardSet cards, Card holeCard) {
        if (similarRanks(holeCard.rankOf(), cards) > 0) {
            if (MadeHandUtil.highestRank(cards).equals(holeCard.rankOf())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMiddlePairOneCardOnBestHand(CardSet cards, Card holeCard) {
        if (similarRanks(holeCard.rankOf(), cards) > 0) {
            if (MadeHandUtil.secondHighestRank(cards).equals(holeCard.rankOf())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isOverPair(CardSet cards, Rank leftCardRank, Rank rightCardRank) {
        if (leftCardRank.equals(rightCardRank)) {
            for (Card card : cards) {
                if (leftCardRank.pipValue() < card.rankOf().pipValue()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static boolean isPPBelowTP(CardSet cards, Rank leftCardRank, Rank rightCardRank) {
        if (leftCardRank.pipValue() > MadeHandUtil.secondHighestRank(cards).pipValue()) {
            return true;
        }

        return false;
    }
}
