package com.masa.util;

import com.masa.type.MadeHandSpecific;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class MadeHandStraightFlushAnalyzer {

    public static MadeHandSpecific getSpecificStraightFlush(CardSet board, CardSet bestHand, CardSet holeCards) {
        int nroOfHoleCardsOnBestHand = MadeHandUtil.countNroOfHandsInBestHand(bestHand, holeCards);

        if (nroOfHoleCardsOnBestHand > 0) {
            return MadeHandSpecific.STRAIGHT_FLUSH;
        }

        return MadeHandSpecific.NO_MADE_HAND;
    }
}
