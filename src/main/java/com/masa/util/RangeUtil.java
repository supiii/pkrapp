package com.masa.util;

import static com.masa.pkrapp.App.board;
import static com.masa.pkrapp.App.comboCheckBoxValues;
import static com.masa.pkrapp.App.drawCheckBoxValues;
import static com.masa.pkrapp.App.holeCards;
import static com.masa.pkrapp.App.ranges;
import com.masa.type.Draw;
import com.masa.type.MadeHandSpecific;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class RangeUtil {

    public static double calculateEquityVsVillainContinueRange() {
        Set<CardSet> villainRangeCopy = new HashSet<>(ranges.villainRangeCardSet);
        RangeUtil.removeHandsFromRangeThatContainCardFromGivenCardSet(villainRangeCopy, holeCards);

        // Villain continueRange
        Set<CardSet> villainContinueRange = new HashSet<>();
        for (CardSet cardSet : villainRangeCopy) {
            MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, cardSet);

            boolean checked = comboCheckBoxValues.get(madeHandSpecific).checked;
            if (checked) {
                villainContinueRange.add(cardSet);
            }
            Set<Draw> draws = DrawAnalyzer.getDraws(board, cardSet, holeCards);
            for (Draw draw : draws) {
                boolean drawChecked = drawCheckBoxValues.get(draw).checked;
                if (drawChecked) {
                    villainContinueRange.add(cardSet);
                }
            }
        }

        String villainContinueRangeCopyString = RangeUtil.constructRangeString(villainContinueRange);
        System.out.println(villainContinueRangeCopyString);
        double equity = EquityHandler.calculateEquity(
                holeCards.toStringWithoutCommas(),
                villainContinueRangeCopyString,
                board.toStringWithoutCommas());

        return equity;
    }

    public static void removeHandsFromRangeThatContainCardFromGivenCardSet(Set<CardSet> range, CardSet cardSet) {
        Set<CardSet> removeFromVillainRangeCopy = new HashSet<>();

        for (CardSet villainCardSet : range) {
            if (villainCardSet.contains(cardSet.get(0)) || villainCardSet.contains(cardSet.get(1))) {
                removeFromVillainRangeCopy.add(villainCardSet);
            }
        }
        range.removeAll(removeFromVillainRangeCopy);
    }

    public static List<String> combineListsOfStrings(List<String>... hands) {
        List<String> all = new ArrayList<>();

        for (List<String> handList : hands) {
            for (String string : handList) {
                all.add(string);
            }
        }

        return all;
    }

    /*
    Constructs String range like "Th8d|AsAd|AsKh..."
     */
    public static String constructRangeString(Collection<CardSet> range) {
        // "Th8d|AsAd|AsKh..."
        String rangeString = "";
        int count = 0;
        for (CardSet cardSet : range) {
            if (count == 0) {
                rangeString = rangeString + cardSet.toStringWithoutCommas();
            } else {
                rangeString = rangeString + "|" + cardSet.toStringWithoutCommas();
            }
            count++;
        }
        return rangeString;
    }
}
