package com.masa.util;

import com.masa.type.MadeHandSpecific;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author compuuter
 */
public class MadeHandFlushAnalyzer {

    public static MadeHandSpecific getSpecificFlush(CardSet board, CardSet bestHand, CardSet holeCards) {
        int nroOfHoleCardsOnBestHand = MadeHandUtil.countNroOfHandsInBestHand(bestHand, holeCards);
        CardSet allCards = new CardSet(board);
        allCards.addAll(holeCards);
        // holcards on best hand
        if (nroOfHoleCardsOnBestHand > 0) {
            Suit flushSuit = bestHand.get(0).suitOf();
            Card connectedHoleCard = getBiggerConnectedFlushCard(board, bestHand, holeCards);
            CardSet remainingFlushCards = CardSet.freshDeck(flushSuit);
            remainingFlushCards.removeAll(bestHand);
            int nroOfBiggerRanks = 0;
            for (Card remainingFlushCard : remainingFlushCards) {
                if (remainingFlushCard.rankOf().pipValue() > connectedHoleCard.rankOf().pipValue()) {
                    nroOfBiggerRanks++;
                }
            }
            if (nroOfBiggerRanks == 0) {
                return MadeHandSpecific.FLUSH_NUT;
            }
            if (nroOfBiggerRanks == 1) {
                return MadeHandSpecific.FLUSH_2ND_NUT;
            }
            if (nroOfBiggerRanks == 2) {
                return MadeHandSpecific.FLUSH_3RD_NUT;
            }
            if (nroOfBiggerRanks == 3) {
                return MadeHandSpecific.FLUSH_4TH_NUT;
            }
            if (nroOfBiggerRanks > 3) {
                return MadeHandSpecific.FLUSH_WEAK;
            }
        } else {
            return MadeHandSpecific.NO_MADE_HAND;
        }
        return null;
    }

    private static Card getBiggerConnectedFlushCard(CardSet board, CardSet bestHand, CardSet holeCards) {
        Suit flushSuit = bestHand.get(0).suitOf();
        Card holeCard1 = holeCards.get(0);
        Card holeCard2 = holeCards.get(1);

        if (holeCard1.suitOf().equals(flushSuit) && holeCard2.suitOf().equals(flushSuit)) {
            return holeCard1.rankOf().pipValue() > holeCard2.rankOf().pipValue() ? holeCard1 : holeCard2;
        } else if (holeCard1.suitOf().equals(flushSuit)) {
            return holeCard1;
        } else if (holeCard2.suitOf().equals(flushSuit)) {
            return holeCard2;
        }

        return null;
    }
}
