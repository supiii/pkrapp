package com.masa.util;

import com.masa.pkrapp.App;
import static com.masa.pkrapp.App.df;
import static com.masa.pkrapp.App.firstFlopCard;
import static com.masa.pkrapp.App.holeCardLeft;
import static com.masa.pkrapp.App.holeCardRight;
import static com.masa.pkrapp.App.ranges;
import static com.masa.pkrapp.App.secondFlopCard;
import static com.masa.pkrapp.App.thirdFlopCard;
import com.masa.type.Draw;
import com.masa.type.MadeHand;
import com.masa.type.ThreeTypeValue;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class RangeConnectivityAnalyzer {
    // connections villain/all
    // Hero CO, Villain BB RC AsQd5c  351/611
    // Hero CO, Villain BB RC QdJdTs  799/613
    // Hero CO, Villain BB RC 5c6c7c  1008/632
    // Hero CO, Villain BB RC 7h8h9h  1064/615
    // Hero CO, Villain BB RC 5c6c7c  623/623
    // Hero CO, Villain BB RC QsQd5c  111/623
    // Hero BB, Villain EP RC AdKh4c  113/191
    // Hero BB, Villain BTN RRC Ah6d3c 59/197

    public static ThreeTypeValue calculateConnectivity() {
        Card firstCard = new Card(firstFlopCard);
        Card secondCard = new Card(secondFlopCard);
        Card thirdCard = new Card(thirdFlopCard);
        CardSet flopCardSet = new CardSet(firstCard, secondCard, thirdCard);

        Card holeCardLeftCard = new Card(holeCardLeft);
        Card holeCardRightCard = new Card(holeCardRight);

        return analyzeConnectivity(flopCardSet, ranges.villainRangeCardSet, new CardSet(holeCardLeftCard, holeCardRightCard));
    }

    public static ThreeTypeValue analyzeConnectivity(CardSet flop, Set<CardSet> range, CardSet deadCards) {
        boolean boardPaired = HandStrengthAnalyzer.similarRanks(flop.get(0).rankOf(), flop) == 2
                || HandStrengthAnalyzer.similarRanks(flop.get(1).rankOf(), flop) == 2
                || HandStrengthAnalyzer.similarRanks(flop.get(2).rankOf(), flop) == 2;

        boolean boardTrips = HandStrengthAnalyzer.similarRanks(flop.get(0).rankOf(), flop) == 3
                || HandStrengthAnalyzer.similarRanks(flop.get(1).rankOf(), flop) == 3
                || HandStrengthAnalyzer.similarRanks(flop.get(2).rankOf(), flop) == 3;

        int connections = 0;
        for (CardSet holeCards : range) {
            boolean pocketPair = HandStrengthAnalyzer.similarRanks(holeCards.get(0).rankOf(), holeCards) == 2;
            CardSet allCards = new CardSet(flop);
            allCards.add(holeCards.get(0));
            allCards.add(holeCards.get(1));
            MadeHand handValueOnFlop = MadeHandAnalyzer.getMadeHand(allCards);
            if (boardPaired && pocketPair && handValueOnFlop.ordinal() > MadeHand.THREE_OF_A_KIND.ordinal()) {
                connections++;
            }
            if (boardTrips && pocketPair && handValueOnFlop.ordinal() > MadeHand.FULL_HOUSE.ordinal()) {
                connections++;
            }
            if (boardPaired && !pocketPair && handValueOnFlop.ordinal() > MadeHand.PAIR.ordinal()) {
                connections++;
            }
            /*if (boardTrips && !pocketPair && handValueOnFlop != MadeHand.NO_PAIR && handValueOnFlop != MadeHand.PAIR
                    && handValueOnFlop != MadeHand.TWO_PAIR && handValueOnFlop != MadeHand.THREE_OF_A_KIND) {
                connections++;
            }*/
            if (!boardPaired && !boardTrips && !pocketPair && handValueOnFlop != MadeHand.NO_MADE_HAND) {
                connections++;
            }

            Set<Draw> draws = DrawAnalyzer.getDraws(flop, holeCards, deadCards);
            connections += draws.size();
        }

        double ratio = (double) connections / (double) range.size();
        App.villainRangeConnectivityLabel.setText("" + connections + "/" + range.size() + " " + df.format(ratio));
        // < >
        if (ratio < 0.6) {
            return ThreeTypeValue.SMALL;
        }
        if (ratio < 1.0) {
            return ThreeTypeValue.MEDIUM;
        }

        return ThreeTypeValue.BIG;
    }
}
