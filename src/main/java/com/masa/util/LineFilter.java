package com.masa.util;

import com.masa.pkrapp.App;
import com.masa.pokertracker.PTUtil;
import com.masa.type.BoardTextureFlop;
import com.masa.type.BoardTextureRiver;
import com.masa.type.BoardTextureTurn;
import com.masa.type.Draw;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author compuuter
 */
public class LineFilter {

    public static boolean filterBasic(String[] splitted, Street currentStreet, Set<String> possibleVillainPositions) {
        if (!filterStats(splitted)) {
            return false;
        }
        if (!filterDuplicateCards(splitted)) {
            return false;
        }
        if (!filterActions(splitted, currentStreet)) {
            //System.out.println("filterActions");
            return false;
        }
        if (!filterPosition(splitted, possibleVillainPositions)) {
            //System.out.println("filterPosition");
            return false;
        }
        if (!filterPlayerCount(splitted, currentStreet)) {
            //System.out.println("filterPlayerCount");
            return false;
        }

        return true;
    }

    private static boolean filterStats(String[] splitted) {
        // TAG
        int vpip = App.villainVPIP;
        int pfr = App.villainPFR;
        int threeBet = App.villain3Bet;
        int cBetFlop = 70;
        int cBetTurn = 55;
        int foldToCBetFlop = 65;
        int foldToCBetTurn = 60;

        System.out.println("" + vpip + " " + pfr + " " + threeBet);

        // FISH
        /*int vpip = 26;
        int pfr = 13;
        int threeBet = 4;
        int cBetFlop = 50;
        int cBetTurn = 55;
        int foldToCBetFlop = 50;
        int foldToCBetTurn = 60;*/
        // Loose/passive
        /*int vpip = 26;
        int pfr = 14;
        int threeBet = 5;
        int cBetFlop = 60;
        int cBetTurn = 40;
        int foldToCBetFlop = 30;
        int foldToCBetTurn = 30;*/
        int vpip2 = Util.getVillainVPIP(splitted);
        int pfr2 = Util.getVillainPFR(splitted);
        int threeBet2 = Util.getVillain3Bet(splitted);
        int cBetFlop2 = Util.getVillainCbetFlop(splitted);
        int cBetTurn2 = Util.getVillainCbetTurn(splitted);
        int foldToCBetFlop2 = Util.getFoldToCbetFlop(splitted);
        int foldToCBetTurn2 = Util.getFoldToCbetTurn(splitted);

        if (vpip2 >= vpip - 2 && vpip2 <= vpip + 2
                && pfr2 >= pfr - 2 && pfr2 <= pfr + 2
                && threeBet2 >= threeBet - 2 && threeBet2 <= threeBet + 2 /*&& cBetFlop2 >= cBetFlop - 10 && cBetFlop2 <= cBetFlop + 10*/ /*&& cBetTurn2 >= cBetTurn - 10 && cBetTurn2 <= cBetTurn + 10*/ /*&& foldToCBetFlop2 >= foldToCBetFlop - 10 && foldToCBetFlop2 <= foldToCBetFlop + 10*/ /*&& foldToCBetTurn2 >= foldToCBetTurn - 10 && foldToCBetTurn2 <= foldToCBetTurn + 10*/) {
            return true;
        }

        return false;
    }

    private static boolean filterDuplicateCards(String[] splitted) {
        int holeCard1 = PTUtil.getPTCardInt(App.holeCardLeft);
        int holeCard2 = PTUtil.getPTCardInt(App.holeCardRight);
        int flopCard1Id = intValue(Util.getFlopCard1(splitted));
        int flopCard2Id = intValue(Util.getFlopCard2(splitted));
        int flopCard3Id = intValue(Util.getFlopCard3(splitted));
        int turnCardId = intValue(Util.getTurnCard(splitted));
        int riverCardId = intValue(Util.getRiverCard(splitted));

        return holeCard1 != flopCard1Id && holeCard1 != flopCard2Id && holeCard1 != flopCard3Id && holeCard1 != turnCardId && holeCard1 != riverCardId
                && holeCard2 != flopCard1Id && holeCard2 != flopCard2Id && holeCard2 != flopCard3Id && holeCard2 != turnCardId && holeCard2 != riverCardId;
    }

    private static boolean filterPlayerCount(String[] splitted, Street currentStreet) {
        int playersPreflop = intValue(Util.getPlayersPreflop(splitted));
        int playersOnFlop = intValue(Util.getPlayersOnFlop(splitted));

        if (currentStreet == Street.PREFLOP && playersPreflop > 2) {
            return false;
        }
        if (currentStreet != Street.PREFLOP && playersOnFlop > 2) {
            return false;
        }

        return true;
    }

    private static boolean filterPosition(String[] splitted, Set<String> possibleVillainPositions) {
        String position = Util.getPosition(splitted);
        String villainPosition = Util.getVillainPosition(splitted);

        if (!App.heroPos.equalsIgnoreCase(position)) {
            return false;
        }

        if (App.villainPos != null && !possibleVillainPositions.contains(villainPosition)) {
            return false;
        }
        return true;
    }

    private static boolean filterActions(String[] splitted, Street currentStreet) {
        if (currentStreet == Street.PREFLOP) {
            if (!checkPreflopActions(splitted)) {
                return false;
            }
        } else if (currentStreet == Street.FLOP) {
            if (!checkFlopActions(splitted)) {
                return false;
            }
        } else if (currentStreet == Street.TURN) {
            if (!checkTurnActions(splitted)) {
                return false;
            }
        } else if (currentStreet == Street.RIVER) {
            if (!checkRiverActions(splitted)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkPreflopActions(String[] splitted) {
        String actionsPreflop = Util.getVillainActionsPreflop(splitted);
        String actionsVillainPreflop = Util.getVillainActionsPreflop(splitted);
        if (!actionsPreflop.startsWith(App.actionsHeroPreFlop)) {
            return false;
        }
        if (App.villainPos != null && !actionsVillainPreflop.startsWith(App.actionsHeroPreFlop)) {
            return false;
        }
        return true;
    }

    private static boolean checkRiverActions(String[] splitted) {
        String actionsPreflop = Util.getActionsPreflop(splitted);
        String actionsFlop = Util.getActionsFlop(splitted);
        String actionsTurn = Util.getActionsTurn(splitted);
        String actionsRiver = Util.getActionsRiver(splitted);
        String actionsVillainPreflop = Util.getVillainActionsPreflop(splitted);
        String actionsVillainFlop = Util.getVillainActionsFlop(splitted);
        String actionsVillainTurn = Util.getVillainActionsTurn(splitted);
        String actionsVillainRiver = Util.getVillainActionsRiver(splitted);
        if (!actionsPreflop.equals(App.actionsHeroPreFlop)) {
            return false;
        }
        if (!actionsVillainPreflop.equals(App.actionsVillainPreFlop)) {
            return false;
        }
        if (!actionsFlop.equals(App.actionsHeroFlop)) {
            return false;
        }
        if (!actionsVillainFlop.equals(App.actionsVillainFlop)) {
            return false;
        }
        if (!actionsTurn.equals(App.actionsHeroTurn)) {
            return false;
        }
        if (!actionsVillainTurn.equals(App.actionsVillainTurn)) {
            return false;
        }
        if (!actionsRiver.startsWith(App.actionsHeroRiver)) {
            return false;
        }
        if (!actionsVillainRiver.startsWith(App.actionsVillainRiver)) {
            return false;
        }
        return true;
    }

    private static boolean checkTurnActions(String[] splitted) {
        String actionsPreflop = Util.getActionsPreflop(splitted);
        String actionsFlop = Util.getActionsFlop(splitted);
        String actionsTurn = Util.getActionsTurn(splitted);
        String actionsVillainPreflop = Util.getVillainActionsPreflop(splitted);
        String actionsVillainFlop = Util.getVillainActionsFlop(splitted);
        String actionsVillainTurn = Util.getVillainActionsTurn(splitted);
        if (!App.actionsHeroPreFlop.equals(actionsPreflop)) {
            return false;
        }
        if (!App.actionsVillainPreFlop.equals(actionsVillainPreflop)) {
            return false;
        }
        if (!actionsFlop.equals(App.actionsHeroFlop)) {
            return false;
        }
        if (!actionsVillainFlop.equals(App.actionsVillainFlop)) {
            return false;
        }
        if (!actionsTurn.startsWith(App.actionsHeroTurn)) {
            return false;
        }
        if (!actionsVillainTurn.startsWith(App.actionsVillainTurn)) {
            return false;
        }
        return true;
    }

    private static boolean checkFlopActions(String[] splitted) {
        String actionsPreflop = Util.getActionsPreflop(splitted);
        String actionsFlop = Util.getActionsFlop(splitted);
        String actionsVillainPreflop = Util.getVillainActionsPreflop(splitted);
        String actionsVillainFlop = Util.getVillainActionsFlop(splitted);

        if (!App.actionsHeroPreFlop.equals(actionsPreflop)) {
            return false;
        }
        if (!App.actionsVillainPreFlop.equals(actionsVillainPreflop)) {
            return false;
        }
        if (!actionsFlop.startsWith(App.actionsHeroFlop)) {
            return false;
        }
        if (!actionsVillainFlop.startsWith(App.actionsVillainFlop)) {
            return false;
        }
        return true;
    }

    private static int intValue(String s) {
        return Integer.parseInt(s);
    }

    private static boolean equalSets(Set<Draw> one, Set<Draw> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        return one.equals(two);
    }

    public static List<PostFlopRow> filterNonShowdownHands(List<PostFlopRow> allRows, Street currentStreet) {
        List<PostFlopRow> rows = new ArrayList<>();

        // Group villain fold hands with board texture
        int numberOfVillainfFoldsToRemove = 0;

        Map<Line, List<PostFlopRow>> mappedByTexture = mapRows(allRows);

        // Count numberOfVillainfFoldsToRemove
        // remove villain folds
        return rows;
    }

    public static List<PostFlopRow> filterVillainFoldHands(List<PostFlopRow> allRows, Street currentStreet) {
        List<PostFlopRow> rows = new ArrayList<>();
        Map<Line, List<PostFlopRow>> mappedByTexture = mapRows(allRows);

        int countAll = 0;

        for (Map.Entry<Line, List<PostFlopRow>> entry : mappedByTexture.entrySet()) {
            System.out.println("NEW mappedByTexture.....");
            List<PostFlopRow> value = entry.getValue();

            int showdownHandsWithoutVillainHolecards = 0;

            for (PostFlopRow postFlopRow : value) {
                if (postFlopRow.showdown && (postFlopRow.villainHoleCard1Id == 0 || postFlopRow.villainHoleCard2Id == 0)) {
                    showdownHandsWithoutVillainHolecards = showdownHandsWithoutVillainHolecards + 1;
                }
            }

            System.out.println("showdownHandsWithoutVillainHolecards: " + showdownHandsWithoutVillainHolecards);

            if (showdownHandsWithoutVillainHolecards != 0) {
                int removedCount = 0;

                for (PostFlopRow postFlopRow : value) {
                    if (removedCount < showdownHandsWithoutVillainHolecards && someVillainsStreetEndsWith(postFlopRow, "F", currentStreet)) {
                        removedCount++;
                        System.out.println("removed: " + removedCount);
                    } else {
                        rows.add(postFlopRow);
                    }
                }
            } else {
                rows.addAll(value);
            }

            countAll = countAll + showdownHandsWithoutVillainHolecards;
        }

        System.out.println("countAll: " + countAll + " ero: " + (allRows.size() - rows.size()));

        // Count numberOfVillainfFoldsToRemove
        // remove villain folds
        return rows;
    }

    public static List<PostFlopRow> getAllBestLineRows(List<PostFlopRow> allRows, PostFlopRow current, Street currentStreet) {
        //List<PostFlopRow> filteredVillainFoldHands = filterVillainFoldHands(allRows, currentStreet);

        return WrongLinesRemover2.removeLosingLines(allRows, current, currentStreet, true, App.holeCards);
    }

    public static List<PostFlopRow> getSimpleBetCallRows(List<PostFlopRow> allRows, Street currentStreet) {
        List<PostFlopRow> rows = new ArrayList<>();

        for (PostFlopRow row : allRows) {
            if (!row.showdown || row.villainHoleCard1Id == 0 || row.villainHoleCard2Id == 0) {
                continue;
            }

            if (currentStreet == Street.FLOP) {
                if (allStreetsEquals(row, "B", currentStreet, true) && (allStreetsEquals(row, "C", Street.FLOP, false) || allStreetsEquals(row, "XC", Street.FLOP, false))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.TURN) {
                if (allStreetsEquals(row, "B", currentStreet, true) && (allStreetsEquals(row, "C", Street.TURN, false) || allStreetsEquals(row, "XC", Street.TURN, false))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.RIVER) {
                if (allStreetsEquals(row, "B", currentStreet, true) && (allStreetsEquals(row, "C", Street.RIVER, false) || allStreetsEquals(row, "XC", Street.RIVER, false))) {
                    rows.add(row);
                }
            }
        }

        return rows;
    }

    /*
    * Aggression
    * -Bet/Raise all lines
     */
    public static List<PostFlopRow> getAggressionRows(List<PostFlopRow> allRows, Street currentStreet) {
        List<PostFlopRow> rows = new ArrayList<>();

        for (PostFlopRow row : allRows) {
            if (!row.showdown) {
                continue;
            }

            if (currentStreet == Street.FLOP) {
                if (endsWithBOrR(row.getActions(Street.FLOP))
                        && endsWithBOrR(row.getActions(Street.TURN))
                        && endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.TURN) {
                if (endsWithBOrR(row.getActions(Street.TURN))
                        && endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.RIVER) {
                if (endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            }
        }

        return rows;
    }

    /*
    * Aggression/Call/Check
    * -All simple betting and raising lines
    * -Call/Check for villain aggression
     */
    public static List<PostFlopRow> getAggressionCallCheckRows(List<PostFlopRow> allRows, Street currentStreet) {
        List<PostFlopRow> rows = new ArrayList<>();

        for (PostFlopRow row : allRows) {
            if (!row.showdown) {
                continue;
            }

            if (currentStreet == Street.FLOP) {
                String flopActions = row.getActions(Street.FLOP);
                String turnActions = row.getActions(Street.TURN);
                String riverActions = row.getActions(Street.RIVER);
                // Hero aggressor flop, then villain
                if (containsBOrR(flopActions) && flopActions.endsWith("C")
                        && passive(turnActions)
                        && passive(riverActions)) {
                    rows.add(row);
                }// Hero aggressor flop and turn, then villain
                else if (endsWithBOrR(flopActions)
                        && containsBOrR(turnActions) && turnActions.endsWith("C")
                        && passive(riverActions)) {
                    rows.add(row);
                }// Hero aggressor flop, turn and river then villain
                else if (endsWithBOrR(flopActions) && endsWithBOrR(turnActions)
                        && containsBOrR(riverActions) && riverActions.endsWith("C")) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.TURN) {
                String turnActions = row.getActions(Street.TURN);
                String riverActions = row.getActions(Street.RIVER);
                // Hero aggressor turn, then villain
                if (containsBOrR(turnActions) && turnActions.endsWith("C")
                        && passive(riverActions)) {
                    rows.add(row);
                }// Hero aggressor turn and river then villain
                else if (endsWithBOrR(turnActions)
                        && containsBOrR(riverActions) && riverActions.endsWith("C")) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.RIVER) {
                String riverActions = row.getActions(Street.RIVER);
                if (containsBOrR(riverActions) && riverActions.endsWith("C")) {
                    rows.add(row);
                }
            }
        }

        return rows;
    }

    /*
    * Passive lines
    * -All check/call lines
    * -Call/Check for villain aggression
     */
    public static List<PostFlopRow> getPassiveRows(List<PostFlopRow> allRows, Street currentStreet) {
        List<PostFlopRow> rows = new ArrayList<>();

        for (PostFlopRow row : allRows) {
            if (!row.showdown) {
                continue;
            }

            if (currentStreet == Street.FLOP) {
                if (passive(row.getActions(Street.FLOP)) && passive(row.getActions(Street.TURN)) && passive(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.TURN) {
                if (passive(row.getActions(Street.TURN)) && passive(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.RIVER) {
                if (passive(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            }
        }

        return rows;
    }

    /*
    * Semi bluff lines
    * -All hands where villain fold except hands where hero hits his draw
    * -If hero hits his draw take all B/R/C lines from hero
    * -remove villain folds with same ratio as there are no villains hands on showdown
     */
    public static List<PostFlopRow> getSemiBluffRows(List<PostFlopRow> allRows, Street currentStreet, PostFlopRow current) {
        List<PostFlopRow> rows = new ArrayList<>();
        //List<PostFlopRow> filteredVillainFoldHands = filterVillainFoldHands(allRows, currentStreet);

        for (PostFlopRow row : allRows) {
            if (currentStreet == Street.FLOP) {
                if (endsWithBOrR(row.getActions(Street.FLOP))
                        && endsWithBOrR(row.getActions(Street.TURN))
                        && endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.TURN) {
                if (endsWithBOrR(row.getActions(Street.TURN))
                        && endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.RIVER) {
                if (endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            }
        }

        //return WrongLinesRemover2.removeLosingLines(rows, current, currentStreet, true, App.holeCards);
        return rows;
    }

    public static List<PostFlopRow> filterSDHandsWithoutVillainCards(List<PostFlopRow> allRows) {
        List<PostFlopRow> rows = new ArrayList<>();
        for (PostFlopRow row : allRows) {
            if (row.showdown == true && row.villainHoleCard1Id == 0) {
                continue;
            }
            rows.add(row);
        }

        return rows;
    }

    public static List<PostFlopRow> getSemiBluffRowsWithFiltering(List<PostFlopRow> allRows, Street currentStreet, PostFlopRow current) {
        List<PostFlopRow> rows = new ArrayList<>();
        //List<PostFlopRow> filteredVillainFoldHands = filterVillainFoldHands(allRows, currentStreet);

        //System.out.println("allRows: " + allRows.size() + " filtered: " + filteredVillainFoldHands.size());
        for (PostFlopRow row : allRows) {
            if (currentStreet == Street.FLOP) {
                if (endsWithBOrR(row.getActions(Street.FLOP))
                        && endsWithBOrR(row.getActions(Street.TURN))
                        && endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.TURN) {
                if (endsWithBOrR(row.getActions(Street.TURN))
                        && endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            } else if (currentStreet == Street.RIVER) {
                if (endsWithBOrR(row.getActions(Street.RIVER))) {
                    rows.add(row);
                }
            }
        }

        List<PostFlopRow> filteredVillainFoldHands = filterVillainFoldHands(rows, currentStreet);

        System.out.println("rows: " + rows.size() + " filtered: " + filteredVillainFoldHands.size());

        //return WrongLinesRemover2.removeLosingLines(rows, current, currentStreet, true, App.holeCards);
        return filteredVillainFoldHands;
    }

    private static boolean endsWithBOrR(String actions) {
        return actions.endsWith("B") || actions.endsWith("R");
    }

    private static boolean endsWithXOrC(String actions) {
        return actions.endsWith("X") || actions.endsWith("C");
    }

    private static boolean passive(String actions) {
        return actions.equals("X") || actions.equals("C") || actions.equals("XC");
    }

    private static boolean containsBOrR(String actions) {
        return actions.contains("B") || actions.contains("R");
    }

    private static boolean allStreetsEndsWith(PostFlopRow row, String action, Street currentStreet) {
        if (currentStreet == Street.PREFLOP) {
            return row.getActions(Street.PREFLOP).endsWith(action)
                    && row.getActions(Street.FLOP).endsWith(action)
                    && row.getActions(Street.TURN).endsWith(action)
                    && row.getActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.FLOP) {
            return row.getActions(Street.FLOP).endsWith(action)
                    && row.getActions(Street.TURN).endsWith(action)
                    && row.getActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.TURN) {
            return row.getActions(Street.TURN).endsWith(action)
                    && row.getActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.RIVER) {
            return row.getActions(Street.RIVER).endsWith(action);
        }

        return false;
    }

    public static boolean allStreetsEquals(PostFlopRow row, String action, Street currentStreet, boolean hero) {
        if (hero) {
            if (currentStreet == Street.PREFLOP) {
                return row.getActions(Street.PREFLOP).equals(action)
                        && row.getActions(Street.FLOP).equals(action)
                        && row.getActions(Street.TURN).equals(action)
                        && row.getActions(Street.RIVER).equals(action);
            } else if (currentStreet == Street.FLOP) {
                return row.getActions(Street.FLOP).equals(action)
                        && row.getActions(Street.TURN).equals(action)
                        && row.getActions(Street.RIVER).equals(action);
            } else if (currentStreet == Street.TURN) {
                return row.getActions(Street.TURN).equals(action)
                        && row.getActions(Street.RIVER).equals(action);
            } else if (currentStreet == Street.RIVER) {
                return row.getActions(Street.RIVER).equals(action);
            }
        } else {
            if (currentStreet == Street.PREFLOP) {
                return row.getVillainActions(Street.PREFLOP).equals(action)
                        && row.getVillainActions(Street.FLOP).endsWith(action)
                        && row.getVillainActions(Street.TURN).equals(action)
                        && row.getVillainActions(Street.RIVER).equals(action);
            } else if (currentStreet == Street.FLOP) {
                return row.getVillainActions(Street.FLOP).equals(action)
                        && row.getVillainActions(Street.TURN).equals(action)
                        && row.getVillainActions(Street.RIVER).equals(action);
            } else if (currentStreet == Street.TURN) {
                return row.getVillainActions(Street.TURN).equals(action)
                        && row.getVillainActions(Street.RIVER).equals(action);
            } else if (currentStreet == Street.RIVER) {
                return row.getVillainActions(Street.RIVER).equals(action);
            }
        }

        return false;
    }

    public static boolean someStreetEndsWith(PostFlopRow row, String action, Street currentStreet) {
        if (currentStreet == Street.PREFLOP) {
            return row.getActions(Street.PREFLOP).endsWith(action)
                    || row.getActions(Street.FLOP).endsWith(action)
                    || row.getActions(Street.TURN).endsWith(action)
                    || row.getActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.FLOP) {
            return row.getActions(Street.FLOP).endsWith(action)
                    || row.getActions(Street.TURN).endsWith(action)
                    || row.getActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.TURN) {
            return row.getActions(Street.TURN).endsWith(action)
                    || row.getActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.RIVER) {
            return row.getActions(Street.RIVER).endsWith(action);
        }

        return false;
    }

    public static boolean someVillainsStreetEndsWith(PostFlopRow row, String action, Street currentStreet) {
        if (currentStreet == Street.PREFLOP) {
            return row.getVillainActions(Street.PREFLOP).endsWith(action)
                    || row.getVillainActions(Street.FLOP).endsWith(action)
                    || row.getVillainActions(Street.TURN).endsWith(action)
                    || row.getVillainActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.FLOP) {
            return row.getVillainActions(Street.FLOP).endsWith(action)
                    || row.getVillainActions(Street.TURN).endsWith(action)
                    || row.getVillainActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.TURN) {
            return row.getVillainActions(Street.TURN).endsWith(action)
                    || row.getVillainActions(Street.RIVER).endsWith(action);
        } else if (currentStreet == Street.RIVER) {
            return row.getVillainActions(Street.RIVER).endsWith(action);
        }

        return false;
    }

    /*
    * Map rows by street textures
     */
    private static Map<Line, List<PostFlopRow>> mapRows(List<PostFlopRow> rows) {
        Map<Line, List<PostFlopRow>> linesMap = new HashMap<>();

        for (PostFlopRow row : rows) {
            Line line = new Line(row.boardTextureFlop, row.boardTextureTurn, row.boardTextureRiver);

            if (linesMap.containsKey(line)) {
                linesMap.get(line).add(row);
            } else {
                List<PostFlopRow> postFlopRows = new ArrayList<>();
                postFlopRows.add(row);
                linesMap.put(line, postFlopRows);
            }
        }

        return linesMap;
    }

    public static class Line {

        BoardTextureFlop flopTexture;
        BoardTextureTurn turnTexture;
        BoardTextureRiver riverTexture;

        public Line(
                BoardTextureFlop flopTexture,
                BoardTextureTurn turnTexture,
                BoardTextureRiver riverTexture
        ) {
            this.flopTexture = flopTexture;
            this.turnTexture = turnTexture;
            this.riverTexture = riverTexture;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 37 * hash + Objects.hashCode(this.flopTexture);
            hash = 37 * hash + Objects.hashCode(this.turnTexture);
            hash = 37 * hash + Objects.hashCode(this.riverTexture);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Line other = (Line) obj;
            if (!Objects.equals(this.flopTexture, other.flopTexture)) {
                return false;
            }
            if (!Objects.equals(this.turnTexture, other.turnTexture)) {
                return false;
            }
            if (!Objects.equals(this.riverTexture, other.riverTexture)) {
                return false;
            }
            return true;
        }

    }
}
