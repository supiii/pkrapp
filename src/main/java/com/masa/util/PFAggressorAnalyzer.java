package com.masa.util;

import com.masa.type.BetRange;
import com.masa.type.Draw;
import com.masa.type.FiveTypeValue;
import com.masa.type.Frequency;
import com.masa.type.Looseness;
import com.masa.type.MadeHandSpecific;
import com.masa.type.Ranges;
import com.masa.type.Street;
import com.masa.type.ThreeTypeValue;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class PFAggressorAnalyzer {

    public static BetRange getBettingRangeIP(CardSet board, CardSet holeCards, Ranges ranges, MadeHandSpecific villainMinimumContinueHand) {
        BetRange betRange = new BetRange();
        Set<CardSet> heroRangeCopy = new HashSet<>(ranges.heroRangeCardSet);
        Street street = getStreet(board);
        double bluffRatio = getBluffRatioIP(street, Looseness.NORMAL);

        // Villain continueRange
        Set<CardSet> villainContinueRange = new HashSet<>();
        for (CardSet cardSet : ranges.villainRangeCardSet) {
            MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, cardSet);
            if (madeHandSpecific.ordinal() > villainMinimumContinueHand.ordinal()) {
                villainContinueRange.add(cardSet);
            } else {
                // check draws
                // default flush draws and 4 and 8 out straight draws
                Set<Draw> draws = DrawAnalyzer.getDraws(board, cardSet, holeCards);
                if (draws.contains(Draw.FLUSH_DRAW) || draws.contains(Draw.OESD) || draws.contains(Draw.Gutshot)) {
                    villainContinueRange.add(cardSet);
                }
            }
        }

        // Value range
        // add combos that have more than 0.5 equity against villain's continue range
        // filter out combos if villain range does not have enough combos to call
        Set<CardSet> removeFromHeroRangeCopy = new HashSet<>();
        for (CardSet cardSet : heroRangeCopy) {
            /* Start of remove*/
            // remove Combos that have a card from cardSet
            Set<CardSet> villainContinueRangeCopy = new HashSet<>(villainContinueRange);
            Set<CardSet> removeFromVillainRangeCopy = new HashSet<>();

            for (CardSet villainCardSet : villainContinueRangeCopy) {
                if (villainCardSet.contains(cardSet.get(0)) || villainCardSet.contains(cardSet.get(1))) {
                    removeFromVillainRangeCopy.add(villainCardSet);
                }
            }
            villainContinueRangeCopy.removeAll(removeFromVillainRangeCopy);
            /* End of remove*/

            MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, cardSet);

            // Do not choose combo if villain does not have enough combos to call
            /* Start of check*/
            int villainHandsBetterThanCurrentHeros = 0;
            for (CardSet villainCardSet : villainContinueRangeCopy) {
                MadeHandSpecific villainsMadeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, villainCardSet);
                if (villainsMadeHandSpecific.ordinal() > madeHandSpecific.ordinal()) {
                    villainHandsBetterThanCurrentHeros++;
                }
            }
            double percentageOfVillainHandsContinuing = (double) villainHandsBetterThanCurrentHeros / villainContinueRangeCopy.size();
            // TODO: CHECK THIS PERCENTAGE
            if (percentageOfVillainHandsContinuing < 2.0) {
                continue;
            }
            /* End of check*/

            // add combos that have more than 0.5 equity against villain's continue range
            if (madeHandSpecific.ordinal() > villainMinimumContinueHand.ordinal()) {
                String villainContinueRangeCopyString = RangeUtil.constructRangeString(villainContinueRangeCopy);
                double equity = EquityHandler.calculateEquity(cardSet.toStringWithoutCommas(), villainContinueRangeCopyString, board.toStringWithoutCommas());
                if (equity > 0.50) {
                    betRange.value.add(cardSet);
                    removeFromHeroRangeCopy.add(cardSet);
                }
            }
        }
        heroRangeCopy.removeAll(removeFromHeroRangeCopy);
        removeFromHeroRangeCopy.clear();

        // Bluff range - draws and blockers
        // flush draw with straight 8 out draw
        // flush draw with straight 4 out draw
        // flush draw with backdoor straight draw
        // straight 8 out draw with backdoor flush draw
        // flush draw
        // straight 8 out draw
        // 4 out straight draws with blockers
        // 4 out straight draws
        // Backdoor flush with blockers
        // Backdoor straight with blockers
        // overcards with blockers
        // Backdoor flush
        // Backdoor straight
        // overcards with blockers
        double bluffHands = (bluffRatio / (1 - bluffRatio)) * betRange.value.size();
        addDraws(heroRangeCopy, betRange, board, Arrays.asList(Draw.FLUSH_DRAW, Draw.OESD));
        if (betRange.getBluffRatio() < bluffRatio) {
            addDraws(heroRangeCopy, betRange, board, Arrays.asList(Draw.FLUSH_DRAW, Draw.Gutshot));
        }
        if (betRange.getBluffRatio() < bluffRatio) {
            addDraws(heroRangeCopy, betRange, board, Arrays.asList(Draw.FLUSH_DRAW));
        }
        if (betRange.getBluffRatio() < bluffRatio) {
            addDraws(heroRangeCopy, betRange, board, Arrays.asList(Draw.OESD, Draw.FLUSH_DRAW_BACKDOOR));
        }

        return betRange;
    }

    private static void addDraws(Set<CardSet> heroRangeCopy, BetRange betRange, CardSet board, List<Draw> drawsToCheck) {
        Set<CardSet> removeFromHeroRangeCopy = new HashSet<>();
        for (CardSet cardSet : heroRangeCopy) {
            Set<Draw> draws = DrawAnalyzer.getDraws(board, cardSet, null);
            boolean contains = true;
            for (Draw drawToCheck : drawsToCheck) {
                if (!draws.contains(drawToCheck)) {
                    contains = false;
                }
            }
            if (contains) {
                betRange.bluff.add(cardSet);
                removeFromHeroRangeCopy.add(cardSet);
            }

        }
        heroRangeCopy.removeAll(removeFromHeroRangeCopy);
    }

    private static Street getStreet(CardSet board) {
        if (board.size() == 3) {
            return Street.FLOP;
        }
        if (board.size() == 4) {
            return Street.TURN;
        }
        if (board.size() == 5) {
            return Street.RIVER;
        }
        return null;
    }

    private static double getBluffRatioIP(Street street, Looseness villainLooseness) {
        // https://upswingpoker.com/what-is-bluff-to-value-ratio/
        // Todo: Board texture: on a lot of boards you should actually bluff slightly less than
        // the optimal ratio because of removal effects — when you are bluffing,
        // it’s slightly more likely he has a strong hand and vice-versa
        double defaultFlop = 0.60;
        double defaultTurn = 0.5;
        double defaultRiver = 0.30;
        double step = 0.15;
        if (villainLooseness == Looseness.NORMAL) {
            if (street == Street.FLOP) {
                return defaultFlop;
            }
            if (street == Street.TURN) {
                return defaultTurn;
            }
            if (street == Street.RIVER) {
                return defaultRiver;
            }
        }
        if (villainLooseness == Looseness.LOOSE) {
            if (street == Street.FLOP) {
                return defaultFlop - step;
            }
            if (street == Street.TURN) {
                return defaultTurn - step;
            }
            if (street == Street.RIVER) {
                return defaultRiver - step;
            }
        }
        if (villainLooseness == Looseness.VERY_LOOSE) {
            if (street == Street.FLOP) {
                return defaultFlop - (2 * step);
            }
            if (street == Street.TURN) {
                return defaultTurn - (2 * step);
            }
            if (street == Street.RIVER) {
                return defaultRiver - (2 * step);
            }
        }
        if (villainLooseness == Looseness.TIGHT) {
            if (street == Street.FLOP) {
                return defaultFlop + step;
            }
            if (street == Street.TURN) {
                return defaultTurn + step;
            }
            if (street == Street.RIVER) {
                return defaultRiver + step;
            }
        }
        if (villainLooseness == Looseness.VERY_TIGHT) {
            if (street == Street.FLOP) {
                return defaultFlop + (2 * step);
            }
            if (street == Street.TURN) {
                return defaultTurn + (2 * step);
            }
            if (street == Street.RIVER) {
                return defaultRiver + (2 * step);
            }
        }
        return 0.5d;
    }

    public static Frequency analyzeBetFrequenzy(
            FiveTypeValue rangeAdvantage,
            ThreeTypeValue nutAdvantage,
            ThreeTypeValue villainRangeConnectivity,
            boolean ip,
            double effectiveStackSize) {
        double adjustment = 0.0;

        if (!ip) {
            adjustment -= 0.15;
        }

        if (villainRangeConnectivity == ThreeTypeValue.SMALL.SMALL) {
            adjustment -= 0.05;
        } else if (villainRangeConnectivity == ThreeTypeValue.BIG) {
            adjustment += 0.05;
        }

        if (null != nutAdvantage) {
            switch (nutAdvantage) {
                case SMALL:
                    adjustment += 0.5;
                    break;
                case MEDIUM:
                    break;
                case BIG:
                    break;
                default:
                    break;
            }
        }

        if (rangeAdvantage == FiveTypeValue.VERY_BAD || rangeAdvantage == FiveTypeValue.BAD) {
            // Bet 100% Premium hands and draws
            // Check 100% Marginal hands and junk
            return new Frequency(0.0, 0.30);
        }

        return null;
    }
}
