package com.masa.util;

import com.masa.type.MadeHandSpecific;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class MadeHandSetAnalyzer {

    public static MadeHandSpecific getSpecificSet(CardSet board, CardSet bestHand, CardSet holeCards) {
        // board trips
        if (MadeHandUtil.threeOfAKindOnBoard(board)) {
            return MadeHandSpecific.NO_MADE_HAND;
        } else if (MadeHandUtil.pairOnBoard(board)) {
            return MadeHandSpecific.THREE_OF_A_KIND;
        } else {
            return MadeHandSpecific.SET;
        }
    }

}
