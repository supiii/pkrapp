package com.masa.util;

import com.masa.type.TheBigFive;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class TheBigFiveClaculator {

    public static TheBigFive calculate(
            String holeCards,
            String board,
            String heroRange,
            String villainRange,
            CardSet holeCardsCardSet,
            Set<CardSet> villainRangeCardSet,
            CardSet boardCardSet,
            boolean ip) {

        TheBigFive theBigFive = new TheBigFive();

        theBigFive.rangeAdvantage = EquityHandler.calculateEquityMonteCarlo(heroRange, villainRange, board);
        theBigFive.inPosition = ip;
        theBigFive.rawAdvantage = EquityHandler.calculateEquityMonteCarlo(holeCards, villainRange, board);
        theBigFive.vulnerability = VulnerabilityAnalyzer.analyzeVulnerabilityRatio(villainRangeCardSet, boardCardSet, holeCardsCardSet);
        theBigFive.villainEquityBucket = null;

        return null;
    }
}
