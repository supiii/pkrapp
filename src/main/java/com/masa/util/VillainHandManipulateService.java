package com.masa.util;

import com.masa.pkrapp.App;
import com.masa.pokertracker.PTUtil;
import com.masa.type.Draw;
import com.masa.type.MadeHandSpecific;
import com.masa.type.MadeHandSpecificAndDraw;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class VillainHandManipulateService {

    public static void manipulateVillainHands(List<PostFlopRow> rows, int level) {
        //System.out.println("ALL ROWS: " + rows.size());

        Set<CardSet> possibleCombos = getPossibleCombos();
        ArrayList<CardSet> possibleCombosList = new ArrayList<>(possibleCombos);
        Collections.shuffle(possibleCombosList);
        List<PostFlopRow> copyOfRows = new ArrayList<>(rows);

        for (PostFlopRow row : rows) {
            if (!row.showdown) {
                continue;
            }

            if (row.villainHoleCard1Id == 0) {
                // Filter hands by HUD stats

                if (level == 2) {
                    List<PostFlopRow> filteredByStats = new ArrayList<>();

                    int vpip = row.villainVPIP;
                    int pfr = row.villainPFR;
                    int threeBet = row.villain3Bet;
                    int cBetFlop = row.villainCbetFlop;
                    int cBetTurn = row.villainCbetTurn;
                    int foldToCBetFlop = row.villainFoldToCbetFlop;
                    int foldToCBetTurn = row.villainFoldToCbetTurn;

                    for (PostFlopRow copyOfRow : copyOfRows) {
                        int vpip2 = copyOfRow.villainVPIP;
                        int pfr2 = copyOfRow.villainPFR;
                        int threeBet2 = copyOfRow.villain3Bet;
                        int cBetFlop2 = copyOfRow.villainCbetFlop;
                        int cBetTurn2 = copyOfRow.villainCbetTurn;
                        int foldToCBetFlop2 = copyOfRow.villainFoldToCbetFlop;
                        int foldToCBetTurn2 = copyOfRow.villainFoldToCbetTurn;

                        // cbet = 70
                        // cbet2 = 50
                        // 23 >= 15 && 23 <= 25
                        if (vpip2 >= vpip - 4 && vpip2 <= vpip + 4
                                && pfr2 >= pfr - 4 && pfr2 <= pfr + 4
                                && threeBet2 >= threeBet - 3 && threeBet2 <= threeBet + 3 /*&& cBetFlop2 >= cBetFlop - 6 && cBetFlop2 <= cBetFlop + 6
                            && cBetTurn2 >= cBetTurn - 6 && cBetTurn2 <= cBetTurn + 6
                            && foldToCBetFlop2 >= foldToCBetFlop - 7 && foldToCBetFlop2 <= foldToCBetFlop + 7
                            && foldToCBetTurn2 >= foldToCBetTurn - 7 && foldToCBetTurn2 <= foldToCBetTurn + 7*/) {
                            filteredByStats.add(copyOfRow);
                        }

                    }

                    setPropableVillainHandIfMucked(row, filteredByStats, possibleCombosList, level);
                } else {
                    setPropableVillainHandIfMucked(row, rows, possibleCombosList, level);
                }
            }
        }
    }

    private static void setPropableVillainHandIfMucked(PostFlopRow row, List<PostFlopRow> rows, List<CardSet> possibleCombos, int level) {
        CardSet boardOnFlop = row.getBoardByStreet(Street.FLOP);
        CardSet boardOnTurn = row.getBoardByStreet(Street.TURN);
        CardSet boardOnRiver = row.getBoardByStreet(Street.RIVER);
        List<MadehandSpecificAndDrawsOnEveryStreet> allOccuredHands = getOccuredHandsForRow(row, rows, level);
        if (allOccuredHands.isEmpty()) {
            return;
        }
        MadehandSpecificAndDrawsOnEveryStreet randomMhsd = getRandomMhsad(allOccuredHands);

        /*if (randomMhsd.flop.madeHandSpecific == MadeHandSpecific.ACE_HIGH) {
            if (boardOnFlop.containsRank(Rank.ACE)) {

            }
        }*/
        //System.out.println("getRandomMhsad: " + randomMhsd.flop);
        CardSet villainPropableHandIfMucked = null;
        for (CardSet possibleCombo : possibleCombos) {
            if (boardOnRiver.contains(possibleCombo.get(0)) || boardOnRiver.contains(possibleCombo.get(1)) /*|| App.holeCards.contains(possibleCombo.get(0)) || App.holeCards.contains(possibleCombo.get(1))*/) {
                continue;
            }
            MadeHandSpecific villainMadeHandOnFlop = MadeHandAnalyzer.getMadeHandSpecific(boardOnFlop, possibleCombo);
            Set<Draw> drawsOnFlop = DrawAnalyzer.getDraws(boardOnFlop, possibleCombo, null);
            MadeHandSpecificAndDraw mhsdOnFlop = new MadeHandSpecificAndDraw(villainMadeHandOnFlop, drawsOnFlop);

            MadeHandSpecific villainMadeHandOnTurn = MadeHandAnalyzer.getMadeHandSpecific(boardOnTurn, possibleCombo);
            Set<Draw> drawsOnTurn = DrawAnalyzer.getDraws(boardOnTurn, possibleCombo, null);
            MadeHandSpecificAndDraw mhsdOnTurn = new MadeHandSpecificAndDraw(villainMadeHandOnTurn, drawsOnTurn);

            MadeHandSpecific madeHandOnRiver = MadeHandAnalyzer.getMadeHandSpecific(boardOnRiver, possibleCombo);

            MadehandSpecificAndDrawsOnEveryStreet mhsd = new MadehandSpecificAndDrawsOnEveryStreet(mhsdOnFlop, mhsdOnTurn, madeHandOnRiver);

            if (randomMhsd.equals(mhsd)) {
                //System.out.println("Chosen villain hand: " + mhsd + " " + possibleCombo.toStringWithoutCommas() + " " + streetBoard.toStringWithoutCommas());
                villainPropableHandIfMucked = possibleCombo;
                row.villainHoleCard1Id = PTUtil.getPTCardInt(villainPropableHandIfMucked.get(0).toString());
                row.villainHoleCard2Id = PTUtil.getPTCardInt(villainPropableHandIfMucked.get(1).toString());
                row.villainMadeHandSpecificAndDrawFlop = mhsdOnFlop;
                row.villainMadeHandSpecificAndDrawTurn = mhsdOnTurn;
                row.villainMadeHandSpecificRiver = madeHandOnRiver;
                row.alteredVillainHand = true;
                /*if (madeHandOnRiver == MadeHandSpecific.FLUSH_NUT || madeHandOnRiver == MadeHandSpecific.STRAIGHT) {
                    System.out.println("Villain propable hand found: " + villainPropableHandIfMucked + " - " + boardOnRiver + " " + mhsdOnFlop + " " + " " + mhsdOnTurn + " " + madeHandOnRiver);

                }*/
                break;
            }
        }

        /*if (villainPropableHandIfMucked != null) {
            //System.out.println("UPDATED VILLAIN HAND!!!!!!!!!!!!!");
            row.villainHoleCard1Id = PTUtil.getPTCardInt(villainPropableHandIfMucked.get(0).toString());
            row.villainHoleCard2Id = PTUtil.getPTCardInt(villainPropableHandIfMucked.get(1).toString());
            row.alteredVillainHand = true;
        }*/
    }

    private static List<MadehandSpecificAndDrawsOnEveryStreet> getOccuredHandsForRow(PostFlopRow row, List<PostFlopRow> copyOfRows, int level
    /*, CardSet boardOnFlop, CardSet boardOnTurn, CardSet boardOnRiver*/) {
        /*CardSet rowBoardOnFlop = row.getBoardByStreet(Street.FLOP);
        CardSet rowBoardOnTurn = row.getBoardByStreet(Street.TURN);
        CardSet rowBoardOnRiver = row.getBoardByStreet(Street.RIVER);*/
        List<MadehandSpecificAndDrawsOnEveryStreet> allOccuredHands = new ArrayList<>();
        // get occured hands for FLOP, TURN, RIVER
        for (PostFlopRow copyOfRow : copyOfRows) {
            if (allOccuredHands.size() > 300) {
                break;
            }
            if (!copyOfRow.showdown || copyOfRow.villainHoleCard1Id == 0) {
                continue;
            }
            CardSet boardOnFlop = copyOfRow.getBoardByStreet(Street.FLOP);
            CardSet boardOnTurn = copyOfRow.getBoardByStreet(Street.TURN);
            CardSet boardOnRiver = copyOfRow.getBoardByStreet(Street.RIVER);
            CardSet villainHand = new CardSet(PTUtil.getCard(copyOfRow.villainHoleCard1Id), PTUtil.getCard(copyOfRow.villainHoleCard2Id));

            if (checkActions(row, copyOfRow, level)) {
                continue;
            }
            if (checkTexture(row, copyOfRow, level)) {
                continue;
            }

            // check if villain made hand is high card and make sure current row board does not have one
            CardSet streetsBoard;
            MadeHandSpecific villainMadeHand;
            /*if (Util.isFlop()) {
                streetsBoard = rowBoardOnFlop;

            } else if (Util.isTurn()) {
                streetsBoard = rowBoardOnTurn;
            } else {
                streetsBoard = rowBoardOnRiver;
            }*/

            streetsBoard = copyOfRow.getBoardCardSet();

            villainMadeHand = MadeHandAnalyzer.getMadeHandSpecific(streetsBoard, villainHand);

            if (villainMadeHand == null) {
                continue;
            }

            if (villainMadeHand == MadeHandSpecific.ACE_HIGH && streetsBoard.containsRank(Rank.ACE)) {
                continue;
            }
            if (villainMadeHand == MadeHandSpecific.KING_HIGH && streetsBoard.containsRank(Rank.KING)) {
                continue;
            }
            if (villainMadeHand == MadeHandSpecific.QUEEN_HIGH && streetsBoard.containsRank(Rank.QUEEN)) {
                continue;
            }
            if (villainMadeHand == MadeHandSpecific.JACK_HIGH && streetsBoard.containsRank(Rank.JACK)) {
                continue;
            }
            if (villainMadeHand == MadeHandSpecific.MEDIUM_HIGH && (streetsBoard.containsRank(Rank.TEN)
                    || streetsBoard.containsRank(Rank.NINE) || streetsBoard.containsRank(Rank.EIGHT) || streetsBoard.containsRank(Rank.SEVEN))) {
                continue;
            }
            if (villainMadeHand == MadeHandSpecific.LOW_HIGH && (streetsBoard.containsRank(Rank.SIX)
                    || streetsBoard.containsRank(Rank.FIVE) || streetsBoard.containsRank(Rank.FOUR) || streetsBoard.containsRank(Rank.THREE)
                    || streetsBoard.containsRank(Rank.TWO))) {
                continue;
            }

            Set<Draw> drawsOnFlop = DrawAnalyzer.getDraws(boardOnFlop, villainHand, null);

            if (villainMadeHand.equals(MadeHandSpecific.NO_MADE_HAND)/* && drawsOnFlop.isEmpty()*/) {
                continue;
            }

            MadeHandSpecific villainMadeHandOnFlop = MadeHandAnalyzer.getMadeHandSpecific(boardOnFlop, villainHand);
            MadeHandSpecificAndDraw mhsdOnFlop = new MadeHandSpecificAndDraw(villainMadeHandOnFlop, drawsOnFlop);

            MadeHandSpecific villainMadeHandOnTurn = MadeHandAnalyzer.getMadeHandSpecific(boardOnTurn, villainHand);
            Set<Draw> drawsOnTurn = DrawAnalyzer.getDraws(boardOnTurn, villainHand, null);
            MadeHandSpecificAndDraw mhsdOnTurn = new MadeHandSpecificAndDraw(villainMadeHandOnTurn, drawsOnTurn);

            MadeHandSpecific madeHandOnRiver = MadeHandAnalyzer.getMadeHandSpecific(boardOnRiver, villainHand);

            allOccuredHands.add(new MadehandSpecificAndDrawsOnEveryStreet(mhsdOnFlop, mhsdOnTurn, madeHandOnRiver));

        }

        return allOccuredHands;
    }

    private static boolean checkActions(PostFlopRow row, PostFlopRow copyOfRow, int level) {
        if (!row.getVillainActions(Street.FLOP).equals(copyOfRow.getVillainActions(Street.FLOP))
                || !row.getVillainActions(Street.TURN).equals(copyOfRow.getVillainActions(Street.TURN))
                || !row.getVillainActions(Street.RIVER).equals(copyOfRow.getVillainActions(Street.RIVER))) {
            return true;
        }
        /*switch (level) {
            case 2:
                if (!row.getVillainActions(Street.FLOP).equals(copyOfRow.getVillainActions(Street.FLOP))
                        || !row.getVillainActions(Street.TURN).equals(copyOfRow.getVillainActions(Street.TURN))
                        || !row.getVillainActions(Street.RIVER).equals(copyOfRow.getVillainActions(Street.RIVER))) {
                    return true;
                }
                break;
            case 1:
                if (!row.getVillainActions(Street.FLOP).equals(copyOfRow.getVillainActions(Street.FLOP))
                        || !row.getVillainActions(Street.TURN).equals(copyOfRow.getVillainActions(Street.TURN))) {
                    return true;
                }
                break;
            case 0:
                if (!row.getVillainActions(Street.FLOP).equals(copyOfRow.getVillainActions(Street.FLOP))) {
                    return true;
                }
                break;
            default:
                break;
        }*/

        return false;
    }

    private static boolean checkTexture(PostFlopRow row, PostFlopRow copyOfRow, int level) {
        if (!row.boardTextureTurn.equals(copyOfRow.boardTextureTurn) || !row.boardTextureRiver.equals(copyOfRow.boardTextureRiver)) {
            return true;
        }
        /*switch (level) {
            case 2:
                if (!row.boardTextureTurn.equals(copyOfRow.boardTextureTurn) || !row.boardTextureRiver.equals(copyOfRow.boardTextureRiver)) {
                    return true;
                }
                break;
            case 1:
                if (!row.boardTextureTurn.equals(copyOfRow.boardTextureTurn)) {
                    return true;
                }
                break;
            case 0:
                if (!row.boardTextureTurn.equals(copyOfRow.boardTextureTurn)) {
                    return true;
                }
                break;
            default:
                break;
        }*/

        return false;
    }

    private static void calculateWonLoss(PostFlopRow row) {
        if (row.villainHoleCard1Id == 0 || row.villainHoleCard2Id == 0) {
            //System.out.println("No villins hand!!!!!!!!!!");
            return;
        }
        int holeCard1Id = PTUtil.getPTCardInt(App.holeCards.get(0).toString());
        int holeCard2Id = PTUtil.getPTCardInt(App.holeCards.get(1).toString());
        //CardSet rowBoardOnRiver = App.board;
        CardSet rowBoardOnRiver = row.getBoardByStreet(Street.RIVER);
        CardSet herosHand = new CardSet(PTUtil.getCard(holeCard1Id), PTUtil.getCard(holeCard2Id));
        herosHand.addAll(rowBoardOnRiver);
        CardSet villainsHand = new CardSet(PTUtil.getCard(row.villainHoleCard1Id), PTUtil.getCard(row.villainHoleCard2Id));
        CardSet villainHandWithBoard = new CardSet(villainsHand);
        villainHandWithBoard.addAll(rowBoardOnRiver);

        int herosResult = HandEval.handEval(herosHand);
        int villainsResult = HandEval.handEval(villainHandWithBoard);

        // If hero folded
        if (row.getActions(Street.FLOP).endsWith("F") || row.getActions(Street.TURN).endsWith("F") || row.getActions(Street.RIVER).endsWith("F")) {
            row.won = false;
            if (!isNegative(row.wonBB)) {
                row.wonBB = -row.wonBB;
            }
            System.out.println("6777---- HERO FOLDED row.won: " + row.won);
        } else if (herosResult > villainsResult) {
            row.won = true;
            row.wonBB = Math.abs(row.wonBB);
            System.out.println("Postflowservice 218 WON "
                    + herosHand.toStringWithoutCommas() + " vs " + villainsHand.toStringWithoutCommas() + " " + rowBoardOnRiver.toStringWithoutCommas()
                    + " WON wonBB After: " + row.wonBB + " heroActions: " + row.getAllActionsAsString());
        } else if (villainsResult >= herosResult) {
            row.won = false;
            if (!isNegative(row.wonBB)) {
                row.wonBB = -row.wonBB;
            }
            System.out.println("Postflowservice 218 LOST " + herosHand.toStringWithoutCommas() + " vs " + villainsHand.toStringWithoutCommas() + " " + rowBoardOnRiver.toStringWithoutCommas() + " LOST wonBB after: " + row.wonBB + " " + row.alteredVillainHand);
        }
    }

    private static MadehandSpecificAndDrawsOnEveryStreet getRandomMhsad(List<MadehandSpecificAndDrawsOnEveryStreet> allOccuredHands) {
        int min = 0;
        int max = allOccuredHands.size() - 1;
        Random random = new Random();
        int randomNumber = random.nextInt(max + 1 - min) + min;
        MadehandSpecificAndDrawsOnEveryStreet randomMhsd = allOccuredHands.get(randomNumber);
        return randomMhsd;
    }

    private static Set<CardSet> getPossibleCombos() {
        CardSet deck = CardSet.freshDeck();
        //deck.removeAll(App.holeCards);
        Set<CardSet> allPossibleCombosInDeck = new HashSet<>();
        for (Card card1 : deck) {
            for (Card card2 : deck) {
                if (card1.equals(card2)) {
                    continue;
                }
                if (card1.rankOf().pipValue() > card2.rankOf().pipValue()) {
                    allPossibleCombosInDeck.add(new CardSet(card1, card2));
                } else {
                    allPossibleCombosInDeck.add(new CardSet(card2, card1));
                }

            }
        }
        return allPossibleCombosInDeck;
    }

    private static boolean isNegative(double d) {
        return Double.compare(d, 0.0) < 0;
    }

    public static class MadehandSpecificAndDrawsOnEveryStreet {

        private final MadeHandSpecificAndDraw flop;
        private final MadeHandSpecificAndDraw turn;
        private final MadeHandSpecific river;

        public MadehandSpecificAndDrawsOnEveryStreet(MadeHandSpecificAndDraw flop, MadeHandSpecificAndDraw turn, MadeHandSpecific river) {
            this.flop = flop;
            this.turn = turn;
            this.river = river;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 41 * hash + Objects.hashCode(this.flop);
            hash = 41 * hash + Objects.hashCode(this.turn);
            hash = 41 * hash + Objects.hashCode(this.river);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final MadehandSpecificAndDrawsOnEveryStreet other = (MadehandSpecificAndDrawsOnEveryStreet) obj;
            if (!Objects.equals(this.flop, other.flop)) {
                return false;
            }
            if (!Objects.equals(this.turn, other.turn)) {
                return false;
            }
            if (!Objects.equals(this.river, other.river)) {
                return false;
            }
            return true;
        }

    }
}
