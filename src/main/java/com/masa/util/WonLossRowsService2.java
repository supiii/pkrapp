package com.masa.util;

import com.masa.pkrapp.App;
import com.masa.pokertracker.PTUtil;
import com.masa.type.MadeHandSpecific;
import com.masa.type.PostFlopRow;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;

/**
 *
 * @author compuuter
 */
public class WonLossRowsService2 {

    public static void calculateWonLossWithCurrentHand(Collection<PostFlopRow> rows, CardSet holeCards) {
        List<PostFlopRow> rowsToRemove = new ArrayList<>();

        int holeCard1Id = PTUtil.getPTCardInt(holeCards.get(0).toString());
        int holeCard2Id = PTUtil.getPTCardInt(holeCards.get(1).toString());

        System.out.println("ALL ROWS: " + rows.size());

        // get villain madehands and get most occured madehand and put in all the rows where villain hand was mucked
        Map<CardSet, Integer> villainHandsCount = new HashMap<>();
        Map<MadeHandSpecific, Integer> villainMadeHandsCount = new HashMap<>();
        List<MadeHandSpecific> allOccuredHands = new ArrayList<>();

        for (PostFlopRow row : rows) {
            CardSet board = row.getBoardCardSet();

            if (row.villainHoleCard1Id > 0) {
                CardSet villainHand = new CardSet(PTUtil.getCard(row.villainHoleCard1Id), PTUtil.getCard(row.villainHoleCard2Id));

                MadeHandSpecific villainMadeHand = MadeHandAnalyzer.getMadeHandSpecific(board, villainHand);

                if (villainMadeHand == null) {
                    System.out.println("WonLossService villainMadeHand is NULL");
                } else {
                    allOccuredHands.add(villainMadeHand);
                }

                if (villainMadeHandsCount.containsKey(villainMadeHand)) {
                    Integer value = villainMadeHandsCount.get(villainMadeHand);
                    if (value == null) {
                        value = 1;
                    } else {
                        value = value + 1;
                    }
                    villainMadeHandsCount.put(villainMadeHand, value);
                } else {
                    Integer value = villainMadeHandsCount.get(villainMadeHand);
                    villainMadeHandsCount.put(villainMadeHand, value);
                }

                if (villainHandsCount.containsKey(villainHand)) {
                    Integer value = villainHandsCount.get(villainHand);
                    if (value == null) {
                        value = 1;
                    } else {
                        value = value + 1;
                    }
                    villainHandsCount.put(villainHand, value);
                } else {
                    Integer value = villainHandsCount.get(villainHand);
                    villainHandsCount.put(villainHand, value);
                }
            }
        }

        CardSet deck = CardSet.freshDeck();

        deck.removeAll(App.holeCards);

        Set<CardSet> allPossibleCombosInDeck = new HashSet<>();
        for (Card card1 : deck) {
            for (Card card2 : deck) {
                if (card1.equals(card2)) {
                    continue;
                }
                if (card1.rankOf().pipValue() > card2.rankOf().pipValue()) {
                    allPossibleCombosInDeck.add(new CardSet(card1, card2));
                } else {
                    allPossibleCombosInDeck.add(new CardSet(card2, card1));
                }

            }
        }

        for (PostFlopRow row : rows) {
            CardSet board = row.getBoardCardSet();

            int min = 0;
            int max = allOccuredHands.size() - 1;
            Random random = new Random();

            int randomNumber = random.nextInt(max + 1 - min) + min;
            MadeHandSpecific mhs = allOccuredHands.get(randomNumber);

            CardSet villainPropableHandIfMucked = null;

            for (CardSet possibleCombo : allPossibleCombosInDeck) {
                if (board.contains(possibleCombo.get(0)) || board.contains(possibleCombo.get(1))) {
                    continue;
                }
                MadeHandSpecific villainMadeHand = MadeHandAnalyzer.getMadeHandSpecific(board, possibleCombo);
                if (mhs.equals(villainMadeHand)) {
                    villainPropableHandIfMucked = possibleCombo;
                    break;
                }
            }

            // mucked hand
            if (row.showdown && row.villainHoleCard1Id == 0 && villainPropableHandIfMucked != null) {
                row.villainHoleCard1Id = PTUtil.getPTCardInt(villainPropableHandIfMucked.get(0).toString());
                row.villainHoleCard2Id = PTUtil.getPTCardInt(villainPropableHandIfMucked.get(1).toString());
            }

            if (row.showdown && row.villainHoleCard1Id != 0 && row.villainHoleCard2Id != 0) {

                // check if current hole cards is on board and remove hand if so
                if (holeCard1Id == row.flopCard1Id
                        || holeCard1Id == row.flopCard2Id
                        || holeCard1Id == row.flopCard3Id
                        || holeCard2Id == row.flopCard1Id
                        || holeCard2Id == row.flopCard2Id
                        || holeCard2Id == row.flopCard3Id
                        || holeCard1Id == row.turnCardId
                        || holeCard1Id == row.riverCardId
                        || holeCard2Id == row.riverCardId
                        || holeCard2Id == row.riverCardId) {
                    rowsToRemove.add(row);

                    continue;
                }

                CardSet herosHand = new CardSet(PTUtil.getCard(holeCard1Id), PTUtil.getCard(holeCard2Id));
                herosHand.addAll(board);
                CardSet villainsHand = new CardSet(PTUtil.getCard(row.villainHoleCard1Id), PTUtil.getCard(row.villainHoleCard2Id));
                villainsHand.addAll(board);

                if (HandEval.handEval(herosHand) > HandEval.handEval(villainsHand)) {
                    //System.out.println("WON wonBB before: " + row.wonBB);
                    //won
                    row.won = true;
                    row.wonBB = Math.abs(row.wonBB);
                    //System.out.println("WON wonBB After: " + row.wonBB);
                    System.out.println("Postflowservice 218 WON " + herosHand.toStringWithoutCommas() + " vs " + villainsHand.toStringWithoutCommas() + "WON wonBB After: " + row.wonBB);
                } else {
                    // loss
                    //System.out.println("LOST wonBB before: " + row.wonBB);
                    row.won = false;
                    if (!isNegative(row.wonBB)) {
                        row.wonBB = -row.wonBB;
                    }
                    //System.out.println("LOST wonBB after: " + row.wonBB);
                    System.out.println("Postflowservice 218 LOST " + herosHand.toStringWithoutCommas() + " vs " + villainsHand.toStringWithoutCommas() + "LOST wonBB after: " + row.wonBB);
                }
            }
        }

        System.out.println("ROWS TO REMOVE SIZE: " + rowsToRemove.size());
        rows.removeAll(rowsToRemove);
    }

    private static boolean isNegative(double d) {
        return Double.compare(d, 0.0) < 0;
    }
}
