package com.masa.util;

import com.masa.type.MadeHandSpecific;
import static com.masa.util.HandStrengthAnalyzer.similarRanks;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class MadeHandQuadsAnalyzer {

    public static MadeHandSpecific getSpecificQuads(CardSet board, CardSet bestHand, CardSet holeCards) {
        int nroOfHoleCardsOnBestHand = MadeHandUtil.countNroOfHandsInBestHand(bestHand, holeCards);
        CardSet allCards = new CardSet(board);
        allCards.addAll(holeCards);

        if (similarRanks(holeCards.get(0).rankOf(), bestHand) > 3) {
            return MadeHandSpecific.QUADS;
        }
        if (similarRanks(holeCards.get(1).rankOf(), bestHand) > 3) {
            return MadeHandSpecific.QUADS;
        }

        if (MadeHandUtil.fourOfAKindOnBoard(board)) {
            Rank higherHoleCardRank = MadeHandUtil.highestRank(holeCards);
            if (board.size() == 4) {
                if (higherHoleCardRank.pipValue() == Rank.ACE.pipValue()) {
                    return MadeHandSpecific.ACE_HIGH;
                }
                if (higherHoleCardRank.pipValue() == Rank.KING.pipValue()) {
                    return MadeHandSpecific.KING_HIGH;
                }
                if (higherHoleCardRank.pipValue() == Rank.QUEEN.pipValue()) {
                    return MadeHandSpecific.QUEEN_HIGH;
                }
                if (higherHoleCardRank.pipValue() == Rank.JACK.pipValue()) {
                    return MadeHandSpecific.JACK_HIGH;
                }
                if (higherHoleCardRank.pipValue() == Rank.TEN.pipValue()) {
                    return MadeHandSpecific.MEDIUM_HIGH;
                }
            } else {
                Rank singleRankOnBoard = null;

                for (Card card : board) {
                    if (similarRanks(card.rankOf(), board) == 1) {
                        singleRankOnBoard = card.rankOf();
                        break;
                    }
                }
                if (singleRankOnBoard.pipValue() < higherHoleCardRank.pipValue()) {
                    if (higherHoleCardRank.pipValue() == Rank.ACE.pipValue()) {
                        return MadeHandSpecific.ACE_HIGH;
                    }
                    if (higherHoleCardRank.pipValue() == Rank.KING.pipValue()) {
                        return MadeHandSpecific.KING_HIGH;
                    }
                    if (higherHoleCardRank.pipValue() == Rank.QUEEN.pipValue()) {
                        return MadeHandSpecific.QUEEN_HIGH;
                    }
                    if (higherHoleCardRank.pipValue() == Rank.JACK.pipValue()) {
                        return MadeHandSpecific.JACK_HIGH;
                    }
                    if (higherHoleCardRank.pipValue() == Rank.TEN.pipValue()) {
                        return MadeHandSpecific.MEDIUM_HIGH;
                    }
                }

            }
        }
        return null;
    }
}
