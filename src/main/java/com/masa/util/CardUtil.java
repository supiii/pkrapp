package com.masa.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author compuuter
 */
public class CardUtil {

    public static Set<CardSet> getPossibleFlops() {
        Set<CardSet> possibleFlops = new HashSet();
        CardSet firstDeck = CardSet.freshDeck();

        for (Card firstCard : firstDeck) {
            CardSet secondDeck = CardSet.freshDeck();
            secondDeck.remove(firstCard);

            for (Card secondCard : secondDeck) {
                CardSet thirdDeck = CardSet.freshDeck();
                if (firstCard.equals(secondCard)) {
                    continue;
                }

                for (Card thirdCard : thirdDeck) {
                    if (firstCard.equals(thirdCard) || secondCard.equals(thirdCard)) {
                        continue;
                    }

                    List<Card> flop = new ArrayList<>();
                    flop.add(firstCard);
                    flop.add(secondCard);
                    flop.add(thirdCard);
                    Collections.sort(flop);

                    // check suits
                    CardSet cs = new CardSet((ArrayList<Card>) checkSuits(flop));
                    possibleFlops.add(cs);
                }
            }
        }

        return possibleFlops;
    }

    public static Set<List> getPossibleFlopsWithList() {
        Set<List> possibleFlops = new HashSet();
        CardSet firstDeck = CardSet.freshDeck();

        for (Card firstCard : firstDeck) {
            CardSet secondDeck = CardSet.freshDeck();
            secondDeck.remove(firstCard);

            for (Card secondCard : secondDeck) {
                CardSet thirdDeck = CardSet.freshDeck();
                if (firstCard.equals(secondCard)) {
                    continue;
                }

                for (Card thirdCard : thirdDeck) {
                    if (firstCard.equals(thirdCard) || secondCard.equals(thirdCard)) {
                        continue;
                    }

                    List<Card> flop = new ArrayList<>();
                    flop.add(firstCard);
                    flop.add(secondCard);
                    flop.add(thirdCard);
                    Collections.sort(flop);

                    // check suits
                    possibleFlops.add(checkSuits(flop));
                }
            }
        }

        return possibleFlops;
    }

    /*
    Converts suits with order: Clubs, Diamonds, Hearts
     */
    private static List<Card> checkSuits(List<Card> flop) {
        List<Card> flopWithNewSuits = new ArrayList<>();
        Card c1 = flop.get(0);
        Card c2 = flop.get(1);
        Card c3 = flop.get(2);
        Suit s1 = c1.suitOf();
        Suit s2 = c2.suitOf();
        Suit s3 = c3.suitOf();

        // rainbow
        if (s1 != s2 && s1 != s3 && s2 != s3) {
            flopWithNewSuits.add(new Card(c1.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c2.rankOf(), Suit.DIAMOND));
            flopWithNewSuits.add(new Card(c3.rankOf(), Suit.HEART));
        } else // all same suit
        if (s1 == s2 && s1 == s3 && s2 == s3) {
            flopWithNewSuits.add(new Card(c1.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c2.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c3.rankOf(), Suit.CLUB));
        } else // first and second same suit
        if (s1 == s2) {
            flopWithNewSuits.add(new Card(c1.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c2.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c3.rankOf(), Suit.DIAMOND));
        } else // first and third same suit
        if (s1 == s3) {
            flopWithNewSuits.add(new Card(c1.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c2.rankOf(), Suit.DIAMOND));
            flopWithNewSuits.add(new Card(c3.rankOf(), Suit.CLUB));
        } else // second and third same suit
        if (s2 == s3) {
            flopWithNewSuits.add(new Card(c1.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c2.rankOf(), Suit.CLUB));
            flopWithNewSuits.add(new Card(c3.rankOf(), Suit.DIAMOND));
        }

        return flopWithNewSuits;
    }

    /*
    All possible hands eg 22, 33, 9To, 9Ts
     */
    public static Set<String> getPossibleHands() {
        Set<String> possibleHands = new HashSet();

        for (Rank rank1 : Rank.values()) {
            for (Rank rank2 : Rank.values()) {
                String r1 = String.valueOf(rank1.toChar());
                String r2 = String.valueOf(rank2.toChar());
                if (rank1.equals(rank2)) {
                    possibleHands.add(r1 + r2);
                } else if (rank1.pipValue() > rank2.pipValue()) {
                    possibleHands.add(r1 + r2 + "o");
                    possibleHands.add(r1 + r2 + "s");
                }
            }
        }

        //System.out.println("PossibleHands size: " + possibleHands.size());
        return possibleHands;
    }

    /*
    All possible hands eg 22, 33, 9To, 9Ts
     */
    public static Set<CardSet> getPossibleHandsAsCardSets() {
        Set<CardSet> possibleHands = new HashSet();

        for (Rank rank1 : Rank.values()) {
            for (Rank rank2 : Rank.values()) {
                String r1 = String.valueOf(rank1.toChar());
                String r2 = String.valueOf(rank2.toChar());
                if (rank1.equals(rank2)) {
                    CardSet cs = new CardSet(new Card(r1 + "c"), new Card(r2 + "d"));
                    possibleHands.add(cs);
                } else if (rank1.pipValue() > rank2.pipValue()) {
                    CardSet cs = new CardSet(new Card(r1 + "c"), new Card(r2 + "d"));
                    possibleHands.add(cs);
                }
            }
        }

        //System.out.println("PossibleHands size: " + possibleHands.size());
        return possibleHands;
    }
}
