package com.masa.util;

import com.masa.pkrapp.App;
import com.masa.pokertracker.PTUtil;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;

/**
 *
 * @author compuuter
 */
public class WonLossRowsService {

    public static void calculateWonLossWithCurrentHand(List<PostFlopRow> rows) {
        for (PostFlopRow row : rows) {
            /*if (!row.showdown) {
                continue;
            }*/
            if (row.getActions(Street.FLOP).endsWith("F") || row.getActions(Street.TURN).endsWith("F") || row.getActions(Street.RIVER).endsWith("F")) {
                row.won = false;
                if (!isNegative(row.wonBB)) {
                    row.wonBB = 0.0;
                }
                System.out.println("6777---- HERO FOLDED row.won: " + row.won);
                continue;
            }
            calculateWonLoss(row);
        }
    }

    private static void calculateWonLoss(PostFlopRow row) {
        if (row.villainHoleCard1Id == 0 || row.villainHoleCard2Id == 0) {
            //System.out.println("No villins hand!!!!!!!!!!");
            return;
        }
        int holeCard1Id = PTUtil.getPTCardInt(App.holeCards.get(0).toString());
        int holeCard2Id = PTUtil.getPTCardInt(App.holeCards.get(1).toString());
        //CardSet rowBoardOnRiver = App.board;
        CardSet rowBoardOnRiver = row.getBoardByStreet(Street.RIVER);
        CardSet herosHand = new CardSet(PTUtil.getCard(holeCard1Id), PTUtil.getCard(holeCard2Id));
        herosHand.addAll(rowBoardOnRiver);
        CardSet villainsHand = new CardSet(PTUtil.getCard(row.villainHoleCard1Id), PTUtil.getCard(row.villainHoleCard2Id));
        CardSet villainHandWithBoard = new CardSet(villainsHand);
        villainHandWithBoard.addAll(rowBoardOnRiver);

        int herosResult = HandEval.handEval(herosHand);
        int villainsResult = HandEval.handEval(villainHandWithBoard);

        if (herosResult > villainsResult) {
            row.won = true;
            row.wonBB = Math.abs(row.wonBB);
            System.out.println("Postflowservice 218 WON "
                    + herosHand.toStringWithoutCommas() + " vs " + villainsHand.toStringWithoutCommas() + " " + rowBoardOnRiver.toStringWithoutCommas()
                    + " WON wonBB After: " + row.wonBB + " heroActions: " + row.getAllActionsAsString());
        } else if (villainsResult >= herosResult) {
            row.won = false;
            if (!isNegative(row.wonBB)) {
                row.wonBB = -row.wonBB;
            }
            System.out.println("Postflowservice 218 LOST " + herosHand.toStringWithoutCommas() + " vs " + villainsHand.toStringWithoutCommas() + " " + rowBoardOnRiver.toStringWithoutCommas() + " LOST wonBB after: " + row.wonBB + " " + row.alteredVillainHand);
        }
    }

    private static boolean isNegative(double d) {
        return Double.compare(d, 0.0) < 0;
    }

}
