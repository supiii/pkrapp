package com.masa.util;

import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author compuuter
 */
public class VillainFoldedHandsRemover {

    public static List<PostFlopRow> removeFoldedHands(List<PostFlopRow> rows, Street currentStreet) {
        List<PostFlopRow> filteredRows = new ArrayList<>();
        // group by folds on certain street
        if (currentStreet == Street.PREFLOP) {
        } else if (currentStreet == Street.FLOP) {
            // FLOP
            filteredRows = removeFoldedHandsForGroup(rows, currentStreet);

            // TURN
            List<PostFlopRow> sawTurnRows = filterSawTurn(filteredRows);
            Map<PostFlopRowGrouper.TextureLine, List<PostFlopRow>> mapRowsByTextureTurn = PostFlopRowGrouper.mapRowsByTexture(sawTurnRows, Street.TURN, currentStreet);
            for (Map.Entry<PostFlopRowGrouper.TextureLine, List<PostFlopRow>> entry : mapRowsByTextureTurn.entrySet()) {
                List<PostFlopRow> value = entry.getValue();
                filteredRows.addAll(removeFoldedHandsForGroup(value, currentStreet));
            }
            // RIVER
            List<PostFlopRow> sawRiverRows = filterSawRiver(filteredRows);
            Map<PostFlopRowGrouper.TextureLine, List<PostFlopRow>> mapRowsByTextureRiver = PostFlopRowGrouper.mapRowsByTexture(sawRiverRows, Street.RIVER, currentStreet);
            for (Map.Entry<PostFlopRowGrouper.TextureLine, List<PostFlopRow>> entry : mapRowsByTextureRiver.entrySet()) {
                List<PostFlopRow> value = entry.getValue();
                filteredRows.addAll(removeFoldedHandsForGroup(value, currentStreet));
            }
        } else if (currentStreet == Street.TURN) {
            // TURN
            filteredRows = removeFoldedHandsForGroup(rows, currentStreet);

            // RIVER
            List<PostFlopRow> sawRiverRows = filterSawRiver(filteredRows);
            Map<PostFlopRowGrouper.TextureLine, List<PostFlopRow>> mapRowsByTextureRiver = PostFlopRowGrouper.mapRowsByTexture(sawRiverRows, Street.RIVER, currentStreet);
            for (Map.Entry<PostFlopRowGrouper.TextureLine, List<PostFlopRow>> entry : mapRowsByTextureRiver.entrySet()) {
                List<PostFlopRow> value = entry.getValue();
                filteredRows.addAll(removeFoldedHandsForGroup(value, currentStreet));
            }
        } else if (currentStreet == Street.RIVER) {
            filteredRows = removeFoldedHandsForGroup(rows, currentStreet);
        }

        return filteredRows;
    }

    private static List<PostFlopRow> removeFoldedHandsForGroup(List<PostFlopRow> rows, Street currentStreet) {
        List<PostFlopRow> sdWithoutVillainHandFolds = new ArrayList<>();

        double sdWithoutVillainHandSize = 0;
        double allVillainFoldsSize = 0;

        for (PostFlopRow row : rows) {
            if (!row.showdown) {
                sdWithoutVillainHandSize++;
                if (LineFilter.someVillainsStreetEndsWith(row, "F", currentStreet)) {
                    sdWithoutVillainHandFolds.add(row);
                }
            }
            if (row.showdown && row.villainHoleCard1Id == 0) {
                sdWithoutVillainHandSize++;
            }
            if (LineFilter.someVillainsStreetEndsWith(row, "F", currentStreet)) {
                allVillainFoldsSize++;
            }
        }

        double rowsSize = rows.size();
        double allHandsVillainFoldsRatio = allVillainFoldsSize / rowsSize;

        double wantedFoldsCountDoouble = allHandsVillainFoldsRatio * sdWithoutVillainHandSize;
        int wantedFoldsCount = (int) Math.round(wantedFoldsCountDoouble);

        List<PostFlopRow> remove = new ArrayList<>();
        int count = 0;
        int ffff = (int) Math.round(sdWithoutVillainHandSize) - wantedFoldsCount;
        for (PostFlopRow sdWithoutVillainHandFold : sdWithoutVillainHandFolds) {
            if (count < ffff) {
                remove.add(sdWithoutVillainHandFold);
                count++;
            } else {
                break;
            }
        }

        rows.removeAll(remove);
        return rows;
    }

    private static List<PostFlopRow> filterSawTurn(List<PostFlopRow> rows) {
        return rows.stream().filter(r -> r.sawTurn()).collect(Collectors.toList());
    }

    private static List<PostFlopRow> filterSawRiver(List<PostFlopRow> rows) {
        return rows.stream().filter(r -> r.sawRiver()).collect(Collectors.toList());
    }
}
