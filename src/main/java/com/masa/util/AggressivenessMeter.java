package com.masa.util;

import com.masa.type.FiveTypeValue;

/**
 *
 * @author compuuter
 */
public class AggressivenessMeter {

    private static final double STEP = 0.125;

    public static double determineAggressiveness(FiveTypeValue rangeAdvantage,
            FiveTypeValue nutAdvantage,
            FiveTypeValue vulnerability,
            boolean IP) {
        // -1 - 1
        double aggression = 0.0;

        aggression = rangeAdvantage(rangeAdvantage, aggression);
        aggression = nutAdvantage(nutAdvantage, aggression);
        aggression = vulnerability(vulnerability, aggression);
        aggression = position(IP, aggression);

        return aggression;
    }

    private static double position(boolean IP, double aggression) {
        // Position
        if (IP) {
            aggression += 2 * STEP;
        } else {
            aggression -= 2 * STEP;
        }
        return aggression;
    }

    private static double rangeAdvantage(FiveTypeValue rangeAdvantage, double aggression) {
        if (null != rangeAdvantage) {
            switch (rangeAdvantage) {
                case VERY_GOOD:
                    aggression += 2 * STEP;
                    break;
                case GOOD:
                    aggression += STEP;
                    break;
                case BAD:
                    aggression -= STEP;
                    break;
                case VERY_BAD:
                    aggression -= 2 * STEP;
                    break;
                default:
                    break;
            }
        }
        return aggression;
    }

    private static double nutAdvantage(FiveTypeValue nutAdvantage, double aggression) {
        if (null != nutAdvantage) {
            switch (nutAdvantage) {
                case VERY_GOOD:
                    aggression += 2 * STEP;
                    break;
                case GOOD:
                    aggression += STEP;
                    break;
                case BAD:
                    aggression -= STEP;
                    break;
                case VERY_BAD:
                    aggression -= 2 * STEP;
                    break;
                default:
                    break;
            }
        }
        return aggression;
    }

    private static double vulnerability(FiveTypeValue vulnerability, double aggression) {
        if (null != vulnerability) {
            switch (vulnerability) {
                case VERY_BAD:
                    aggression += 2 * STEP;
                    break;
                case BAD:
                    aggression += STEP;
                    break;
                case GOOD:
                    aggression -= STEP;
                    break;
                case VERY_GOOD:
                    aggression -= 2 * STEP;
                    break;
                default:
                    break;
            }
        }
        return aggression;
    }

}
