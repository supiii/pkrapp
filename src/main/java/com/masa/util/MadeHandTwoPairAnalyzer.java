package com.masa.util;

import com.masa.type.MadeHandSpecific;
import static com.masa.util.HandStrengthAnalyzer.similarRanks;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Rank;

/**
 *
 * @author compuuter
 */
public class MadeHandTwoPairAnalyzer {

    public static MadeHandSpecific getSpecificTwoPair(CardSet board, CardSet fiveBestCards, CardSet holeCards) {
        CardSet allCards = new CardSet(board);
        allCards.addAll(holeCards);
        Rank leftRank = holeCards.get(0).rankOf();
        Rank rightRank = holeCards.get(1).rankOf();
        int nroOfHoleCardsInBestHand = MadeHandUtil.countNroOfHandsInBestHand(fiveBestCards, holeCards);
        int nroOfHoleCardsConnected = MadeHandUtil.countRankConnections(fiveBestCards, holeCards);
        boolean pp = MadeHandUtil.isPocketPair(leftRank, rightRank);
        Rank highestRank = MadeHandUtil.highestRank(fiveBestCards);
        Rank connectedHoleCardRank = null;
        if (nroOfHoleCardsInBestHand == 1) {

        }
        //boolean twoPairOnBoard = twoPairOnBoard(board);
        if (nroOfHoleCardsInBestHand == 0) {
            return MadeHandSpecific.NO_MADE_HAND;
        }

        if (nroOfHoleCardsInBestHand == 1 && nroOfHoleCardsConnected == 0) {
            Card card = null;
            if (fiveBestCards.contains(holeCards.get(0))) {
                card = holeCards.get(0);
            } else if (fiveBestCards.contains(holeCards.get(1))) {
                card = holeCards.get(1);
            }
            return card.rankOf().equals(Rank.ACE) ? MadeHandSpecific.ACE_HIGH : MadeHandSpecific.NO_MADE_HAND;
        }

        if (pp && nroOfHoleCardsInBestHand == 2) {
            if (isOverPair(board, leftRank, rightRank)) {
                return MadeHandSpecific.OVERPAIR;
            }
            if (isPPBelowTP(board, leftRank, rightRank)) {
                return MadeHandSpecific.PP_BELOW_TP;
            }
            return MadeHandSpecific.WEAK_PAIR;
        } else if (nroOfHoleCardsConnected == 1 && nroOfHoleCardsInBestHand == 1) {
            Rank connectedRank = similarRanks(leftRank, fiveBestCards) > 0 ? leftRank : rightRank;
            int nroOfSameRanks = similarRanks(connectedRank, fiveBestCards);
            if (nroOfSameRanks == 1) {
                return connectedRank.equals(Rank.ACE) ? MadeHandSpecific.ACE_HIGH : MadeHandSpecific.NO_MADE_HAND;
            } else {
                if (isTopPair(allCards, connectedRank)) {
                    return MadeHandSpecific.TOP_PAIR;
                }
                if (isMiddlePair(allCards, connectedRank)) {
                    return MadeHandSpecific.MIDDLE_PAIR;
                }
                /*if (isWeakPair(board, connectedRank)) {*/
                return MadeHandSpecific.WEAK_PAIR;
                /*}*/
            }
        } else if (nroOfHoleCardsConnected == 1 && nroOfHoleCardsInBestHand == 2) {
            Card connectedHoleCard = MadeHandUtil.getConnectedHoleCard(fiveBestCards, holeCards);
            Card notConnectedHoleCard = MadeHandUtil.getNotConnectedHoleCard(connectedHoleCard, holeCards);
            Rank connectedRank = similarRanks(leftRank, fiveBestCards) > 0 ? leftRank : rightRank;
            int nroOfSameRanks = similarRanks(connectedRank, fiveBestCards);
            if (nroOfSameRanks == 1) {
                return connectedRank.equals(Rank.ACE) ? MadeHandSpecific.ACE_HIGH : MadeHandSpecific.NO_MADE_HAND;
            } else {
                if (isTopPair(allCards, connectedRank, notConnectedHoleCard)) {
                    return MadeHandSpecific.TOP_PAIR;
                }
                if (isMiddlePair(allCards, connectedRank, notConnectedHoleCard)) {
                    return MadeHandSpecific.MIDDLE_PAIR;
                }
                /*if (isWeakPair(board, connectedRank)) {*/
                return MadeHandSpecific.WEAK_PAIR;
                /*}*/
            }
        } else if (nroOfHoleCardsConnected == 2) {
            return MadeHandSpecific.TWO_PAIR;
        }

        return MadeHandSpecific.NO_MADE_HAND;
    }

    private static boolean isOverPair(CardSet cards, Rank leftCardRank, Rank rightCardRank) {
        if (leftCardRank.equals(rightCardRank)) {
            for (Card card : cards) {
                if (leftCardRank.pipValue() < card.rankOf().pipValue()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static boolean isTopPair(CardSet cards, Rank cardRank) {
        if (similarRanks(cardRank, cards) > 0) {
            if (highestRank(cards).equals(cardRank)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isTopPair(CardSet cards, Rank cardRank, Card notConnectedHoleCard) {
        CardSet cardsRemoved = new CardSet(cards);
        cardsRemoved.remove(notConnectedHoleCard);
        if (similarRanks(cardRank, cardsRemoved) > 0) {
            if (highestRank(cardsRemoved).equals(cardRank)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isPPBelowTP(CardSet cards, Rank leftCardRank, Rank rightCardRank) {
        if (MadeHandUtil.isPocketPair(leftCardRank, rightCardRank)) {
            if (leftCardRank.pipValue() > secondHighestRank(cards).pipValue()) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMiddlePair(CardSet cards, Rank cardRank) {
        if (similarRanks(cardRank, cards) == 2) {
            if (secondHighestRank(cards).equals(cardRank)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMiddlePair(CardSet cards, Rank cardRank, Card notConnectedHoleCard) {
        CardSet cardsRemoved = new CardSet(cards);
        cardsRemoved.remove(notConnectedHoleCard);
        if (similarRanks(cardRank, cardsRemoved) > 0) {
            if (secondHighestRank(cardsRemoved).equals(cardRank)) {
                return true;
            }
        }
        return false;
    }

    public static Rank highestRank(CardSet cards) {
        List<Card> sortedList = new ArrayList<>(cards);
        Collections.sort(sortedList);
        return sortedList.get(sortedList.size() - 1).rankOf();
    }

    private static Rank secondHighestRank(CardSet cards) {
        Set<Rank> sortedSet = new HashSet<>();
        for (Card card : cards) {
            sortedSet.add(card.rankOf());
        }
        List<Rank> sortedList = new ArrayList<>(sortedSet);
        Collections.sort(sortedList);
        return sortedList.get(sortedList.size() - 2);
    }
}
