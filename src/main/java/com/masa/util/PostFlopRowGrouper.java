package com.masa.util;

import com.masa.type.BoardTextureFlop;
import com.masa.type.BoardTextureRiver;
import com.masa.type.BoardTextureTurn;
import com.masa.type.Draw;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class PostFlopRowGrouper {

    public static Map<TextureAndActionsLine, List<PostFlopRow>> mapRowsByTextureAndActions(List<PostFlopRow> rows, Street street, Street currentStreet, CardSet holeCards) {
        Map<TextureAndActionsLine, List<PostFlopRow>> linesMap = new HashMap<>();

        for (PostFlopRow row : rows) {
            TextureAndActionsLine line = null;
            if (street == currentStreet) {
                line = new TextureAndActionsLine(null, null, null, null, null, null, null, null);
            } else if (street == Street.FLOP) {
                line = new TextureAndActionsLine(row.boardTextureFlop, null, null,
                        null, null, null, row.drawsFlop, null);
            } else if (street == Street.TURN) {
                line = new TextureAndActionsLine(row.boardTextureFlop, row.boardTextureTurn, null,
                        row.actionsFlop, null, null, null, row.drawsTurn);
            } else if (street == Street.RIVER) {
                line = new TextureAndActionsLine(null, null, row.boardTextureRiver,
                        row.actionsFlop, row.actionsTurn, null, null, null);
            }

            if (linesMap.containsKey(line)) {
                linesMap.get(line).add(row);
            } else {
                List<PostFlopRow> postFlopRows = new ArrayList<>();
                postFlopRows.add(row);
                linesMap.put(line, postFlopRows);
            }
        }

        return linesMap;
    }

    public static Map<TextureLine, List<PostFlopRow>> mapRowsByTexture(List<PostFlopRow> rows, Street street, Street currentStreet) {
        Map<TextureLine, List<PostFlopRow>> linesMap = new HashMap<>();

        for (PostFlopRow row : rows) {
            TextureLine line = null;
            if (street == currentStreet) {
                line = new TextureLine(null, null, null);
            } else if (street == Street.FLOP) {
                line = new TextureLine(row.boardTextureFlop, null, null);
            } else if (street == Street.TURN) {
                line = new TextureLine(row.boardTextureFlop, row.boardTextureTurn, null);
            } else if (street == Street.RIVER) {
                line = new TextureLine(null, null, row.boardTextureRiver);
            }

            if (linesMap.containsKey(line)) {
                linesMap.get(line).add(row);
            } else {
                List<PostFlopRow> postFlopRows = new ArrayList<>();
                postFlopRows.add(row);
                linesMap.put(line, postFlopRows);
            }
        }

        return linesMap;
    }

    public static class TextureAndActionsLine {

        BoardTextureFlop flopTexture;
        BoardTextureTurn turnTexture;
        BoardTextureRiver riverTexture;

        String actionsFlop;
        String actionsTurn;
        String actionsRiver;

        Set<Draw> flopDraws;
        Set<Draw> turnDraws;

        public TextureAndActionsLine(
                BoardTextureFlop flopTexture,
                BoardTextureTurn turnTexture,
                BoardTextureRiver riverTexture,
                String actionsFlop,
                String actionsTurn,
                String actionsRiver,
                Set<Draw> flopDraws,
                Set<Draw> turnDraws
        ) {
            this.flopTexture = flopTexture;
            this.turnTexture = turnTexture;
            this.riverTexture = riverTexture;
            this.actionsFlop = actionsFlop;
            this.actionsTurn = actionsTurn;
            this.actionsRiver = actionsRiver;
            this.flopDraws = flopDraws;
            this.turnDraws = turnDraws;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 37 * hash + Objects.hashCode(this.flopTexture);
            hash = 37 * hash + Objects.hashCode(this.turnTexture);
            hash = 37 * hash + Objects.hashCode(this.riverTexture);
            hash = 37 * hash + Objects.hashCode(this.actionsFlop);
            hash = 37 * hash + Objects.hashCode(this.actionsTurn);
            hash = 37 * hash + Objects.hashCode(this.actionsRiver);
            hash = 37 * hash + Objects.hashCode(this.flopDraws);
            hash = 37 * hash + Objects.hashCode(this.turnDraws);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final TextureAndActionsLine other = (TextureAndActionsLine) obj;
            if (!Objects.equals(this.actionsFlop, other.actionsFlop)) {
                return false;
            }
            if (!Objects.equals(this.actionsTurn, other.actionsTurn)) {
                return false;
            }
            if (!Objects.equals(this.actionsRiver, other.actionsRiver)) {
                return false;
            }
            if (!Objects.equals(this.flopTexture, other.flopTexture)) {
                return false;
            }
            if (!Objects.equals(this.turnTexture, other.turnTexture)) {
                return false;
            }
            if (!Objects.equals(this.riverTexture, other.riverTexture)) {
                return false;
            }
            if (!equalSets(other.flopDraws, this.flopDraws)) {
                return false;
            }

            if (!equalSets(other.turnDraws, this.turnDraws)) {
                return false;
            }
            return true;
        }

    }

    public static class TextureLine {

        BoardTextureFlop flopTexture;
        BoardTextureTurn turnTexture;
        BoardTextureRiver riverTexture;

        public TextureLine(
                BoardTextureFlop flopTexture,
                BoardTextureTurn turnTexture,
                BoardTextureRiver riverTexture) {
            this.flopTexture = flopTexture;
            this.turnTexture = turnTexture;
            this.riverTexture = riverTexture;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 37 * hash + Objects.hashCode(this.flopTexture);
            hash = 37 * hash + Objects.hashCode(this.turnTexture);
            hash = 37 * hash + Objects.hashCode(this.riverTexture);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final TextureLine other = (TextureLine) obj;
            if (!Objects.equals(this.flopTexture, other.flopTexture)) {
                return false;
            }
            if (!Objects.equals(this.turnTexture, other.turnTexture)) {
                return false;
            }
            if (!Objects.equals(this.riverTexture, other.riverTexture)) {
                return false;
            }
            return true;
        }

    }

    private static boolean equalSets(Set<Draw> one, Set<Draw> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        return one.equals(two);
    }
}
