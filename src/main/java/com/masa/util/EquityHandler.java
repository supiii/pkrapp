package com.masa.util;

import com.masa.pokertracker.PTUtil;
import com.masa.type.PreflopHandVsHandEquity;
import com.masa.type.WeightedCombination;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import mi.poker.calculation.EquityCalculation;
import mi.poker.calculation.HandParser;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author Matti
 */
public class EquityHandler {

    private static List<PreflopHandVsHandEquity> preflopHandEquities;

    public static double calculateEquity(String range1, String range2, String boardCards) {
        return EquityCalculation.calculateExhaustiveEnumration(range1 + "," + range2, boardCards, "").getHandInfo(0).getEquity();
    }

    public static double calculateEquityMonteCarlo(String range1, String range2, String boardCards) {
        return EquityCalculation.calculateMonteCarlo(range1 + "," + range2, boardCards, "").getHandInfo(0).getEquity();
    }

    public static double calculateEquity(List<WeightedCombination> range1, List<WeightedCombination> range2, String boardCards) {
        double equity = 0;
        double allCombinations = 0;

        for (WeightedCombination weightedCombination : range1) {
            allCombinations = allCombinations + weightedCombination.weightedCombinations;
        }

        for (WeightedCombination weightedCombination : range1) {
            String hand = weightedCombination.hand;

            double eq = calculateEquity(hand, range2, boardCards);
            equity = equity + ((weightedCombination.weightedCombinations / allCombinations) * eq);
        }

        return equity;
    }

    public static double calculateEquityWithManualRemoval(List<WeightedCombination> range1, List<WeightedCombination> range2, String boardCards) {
        String weightedHands = "";

        for (WeightedCombination weightedCombination : range1) {
            String hand = weightedCombination.hand;
            CardSet[] possibleHands = HandParser.parsePossibleHands(hand);
            int include = (int) Math.round(weightedCombination.weightedCombinations);
            for (int i = 0; i < include; i++) {
                String s = possibleHands[i].toStringWithoutCommas();
                System.out.println("s: " + s);
                if (weightedHands.isEmpty()) {
                    weightedHands = s;
                } else {
                    weightedHands = weightedHands + "|" + s;
                }
            }

        }

        return calculateEquity("" + weightedHands, range2, boardCards);
    }

    public static double calculateEquity(String handString, List<WeightedCombination> range2, String boardCards) {

        String hand = handString;

        double equity = 0;
        double allCombinations = 0;

        for (WeightedCombination weightedCombination : range2) {
            allCombinations = allCombinations + weightedCombination.weightedCombinations;
        }

        if (boardCards.length() == 0) {
            loadPreflopHandVsHandEquities();
            for (WeightedCombination weightedCombination : range2) {
                double eq = 0;
                for (PreflopHandVsHandEquity handVsHand : preflopHandEquities) {
                    if (hand.equals(handVsHand.hand1) && weightedCombination.hand.equals(handVsHand.hand2)) {
                        eq = handVsHand.hand1Equity;
                        break;
                    } else if (hand.equals(handVsHand.hand2) && weightedCombination.hand.equals(handVsHand.hand1)) {
                        eq = handVsHand.hand2Equity;
                        break;
                    }
                }

                equity = equity + ((weightedCombination.weightedCombinations / allCombinations) * eq);
            }
        } else {
            for (WeightedCombination weightedCombination : range2) {
                double eq = EquityCalculation.calculateExhaustiveEnumration(hand + "," + weightedCombination.hand, boardCards, "")
                        .getHandInfo(0).getEquity();
                equity = equity + ((weightedCombination.weightedCombinations / allCombinations) * eq);

            }
        }

        return equity;
    }

    public static double calculateEquityPreflop(String hand1String, String hand2String) {
        loadPreflopHandVsHandEquities();
        //String hand = handString;
        double equity = 0;
        String rangeCombo1 = PTUtil.convertToRangeCombo(hand1String);
        String rangeCombo2 = PTUtil.convertToRangeCombo(hand2String);

        for (PreflopHandVsHandEquity handVsHand : preflopHandEquities) {
            if (rangeCombo1.equals(handVsHand.hand1) && rangeCombo2.equals(handVsHand.hand2)) {
                equity = handVsHand.hand1Equity;
                break;
            } else if (rangeCombo1.equals(handVsHand.hand2) && rangeCombo2.equals(handVsHand.hand1)) {
                equity = handVsHand.hand2Equity;
                break;
            }
        }

        return equity;
    }

    private static void loadPreflopHandVsHandEquities() {
        preflopHandEquities = new ArrayList<>();
        try {
            File file = new File("preflop_lookup.txt");    //creates a new file instance
            FileReader fr = new FileReader(file);   //reads the file
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            String line;
            while ((line = br.readLine()) != null) {
                // AA A8s 0.880337 0.119663
                String[] s = line.split(" ");
                //System.out.println(s[0] + s[1] + s[2]);
                preflopHandEquities.add(new PreflopHandVsHandEquity(s[0], s[1], Double.parseDouble(s[2]), Double.parseDouble(s[3])));
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int getCombinations(String hand) {
        // KK
        if (hand.length() == 2) {
            return 6;
        } // ex. T9o
        else if (hand.substring(hand.length() - 1).equals("o")) {
            return 4;
        }

        // T9s
        return 12;
    }
}
