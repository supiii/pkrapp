package com.masa.util;

import static com.masa.pkrapp.App.df;
import static com.masa.pkrapp.App.firstFlopCard;
import static com.masa.pkrapp.App.nutAdvantageLabel;
import static com.masa.pkrapp.App.ranges;
import static com.masa.pkrapp.App.secondFlopCard;
import static com.masa.pkrapp.App.thirdFlopCard;
import static com.masa.pkrapp.App.villainNutAdvantageLabel;
import com.masa.type.FiveTypeValue;
import com.masa.type.MadeHand;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;

/**
 *
 * @author compuuter
 */
public class NutAdvantageAnalyzer {

    public static FiveTypeValue calculateSimpleNutAdvantage() {
        double heroNutPercentage = getNutCombinations(ranges.heroRangeCardSet);
        double villainNutPercentage = getNutCombinations(ranges.villainRangeCardSet);

        double ratio = heroNutPercentage / villainNutPercentage;

        nutAdvantageLabel.setText("" + (int) (heroNutPercentage * 100.0) + " %" + "  " + df.format(ratio) + "x");
        villainNutAdvantageLabel.setText("" + (int) (villainNutPercentage * 100.0) + "%");

        FiveTypeValue nutAdvantage = null;

        if (ratio < 0.75) {
            nutAdvantage = FiveTypeValue.VERY_BAD;
        } else if (ratio < 1.00) {
            nutAdvantage = FiveTypeValue.BAD;
        } else if (ratio < 1.15) {
            nutAdvantage = FiveTypeValue.MEDIUM;
        } else if (ratio < 1.50) {
            nutAdvantage = FiveTypeValue.GOOD;
        } else {
            nutAdvantage = FiveTypeValue.VERY_GOOD;
        }

        System.out.println("NutAdvantage: " + ratio + " " + nutAdvantage);

        return nutAdvantage;
    }

    private static double getNutCombinations(Set<CardSet> hands) {
        Card firstCard = new Card(firstFlopCard);
        Card secondCard = new Card(secondFlopCard);
        Card thirdCard = new Card(thirdFlopCard);
        CardSet flopCardSet = new CardSet();
        int nutCombos = 0;
        int allCombos = 0;

        for (CardSet hand : hands) {
            //CardSet[] parsedHand = HandParser.parsePatternSuit(hand);
            //for (CardSet uniqueHandsParsed1 : parsedHand) {
            flopCardSet.clear();
            Card holeCard1 = hand.get(0);
            Card holeCard2 = hand.get(1);

            flopCardSet.add(holeCard1);
            flopCardSet.add(holeCard2);
            flopCardSet.add(firstCard);
            flopCardSet.add(secondCard);
            flopCardSet.add(thirdCard);

            MadeHand madeHand = MadeHandAnalyzer.getMadeHand(flopCardSet);
            if (madeHand != null && madeHand != MadeHand.NO_MADE_HAND && madeHand != MadeHand.PAIR) {
                nutCombos++;
            }
            allCombos++;
            //}
        }

        return (double) nutCombos / (double) allCombos;
    }
}
