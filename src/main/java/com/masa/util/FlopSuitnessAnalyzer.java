package com.masa.util;

import com.masa.type.FlopSuitness;
import java.util.Collection;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.Suit;

/**
 *
 * @author compuuter
 */
public class FlopSuitnessAnalyzer {

    public static FlopSuitness getFlopSuitness(CardSet boardCardSet) {
        int highestNro = highestNumberOfSameSuits(boardCardSet);

        switch (highestNro) {
            case 1:
                return FlopSuitness.RAINBOW;
            case 2:
                return FlopSuitness.TWO_TONE;
            case 3:
                return FlopSuitness.MONOTONE;
            default:
                break;
        }

        return null;
    }

    private static int highestNumberOfSameSuits(CardSet boardCardSet) {
        int highestNro = 0;
        for (Suit suit : Suit.values()) {
            int similarSuits = similarSuits(suit, boardCardSet);
            if (similarSuits > highestNro) {
                highestNro = similarSuits;
            }
        }
        return highestNro;
    }

    private static int similarSuits(Suit suit, Collection<Card> cards) {
        int times = 0;
        for (Card card : cards) {
            if (suit == card.suitOf()) {
                times++;
            }
        }
        return times;
    }
}
