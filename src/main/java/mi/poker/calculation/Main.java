/**
 * @author m1
 */
package mi.poker.calculation;

public class Main {

    public static void main(String[] args) {

        //HandRangeUtil handRange = new HandRangeUtil();
        long start = System.currentTimeMillis();
//		Result result = EquityCalculation.calculateMonteCarlo("KK,AKo,9s8s,3h3s","4h8hQc","2c3c3d");
//                Result result = EquityCalculation.calculate("AhAd,KK+|AKs|AKo,8d9d","Kh6d7d","");
        //Result result = EquityCalculation.calculateExhaustiveEnumration("AhKd,9c8c,3h3s","","");
//                Result result = EquityCalculation.calculate("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");
//                Result result = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");
//        Result result = EquityCalculation.calculateMonteCarlo(handRange.getOpeningRange(22.0d) + ",7c2d", "Kh6dJd", "");
//        Result result = EquityCalculation.calculateMonteCarlo("AsKd,Kh6d", "KsKc3d", "");                      // 800ms 10k games
//        Result result = EquityCalculation.calculateExhaustiveEnumration("AsKd,Kh6d", "KsKc3d", "");              // 400ms 84k games

//        Result result = EquityCalculation.calculateMonteCarlo("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");            // 780ms 10k eq 60
//        Result result = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");      // 280ms 110k eq 60
//        Result result = EquityCalculation.calculate("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");                         // 750ms montecarlo 10k eq 60
//                Result result = EquityCalculation.calculateMonteCarlo("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");            // 780ms 10k eq 60
//        Result result = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7dKd", "");      // 280ms 110k eq 60
//        Result result = EquityCalculation.calculate("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7dKd", "");                         // 750ms montecarlo 10k eq 60
        Result result1 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");
        Result result2 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo,9d2d", "Kh6d7d", "");
        Result result3 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs,9d2d", "Kh6d7d", "");
        Result result4 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+,9d2d", "Kh6d7d", "");
        Result result5 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+,9d2d", "Kh6d7d", "");
        Result result6 = EquityCalculation.calculateExhaustiveEnumration("99|88,9d2d", "Kh6d7d", "");
        Result result7 = EquityCalculation.calculateExhaustiveEnumration("99,9d2d", "Kh6d7d", "");
        Result result8 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");
        Result result9 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");
        Result result10 = EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,9d2d", "Kh6d7d", "");

        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsAd", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo,AsAd", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs,AsAd", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+,AsAd", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+,AsAd", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88,As9d", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99,As2d", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,As5d", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,As9d", "Kh6d7d", "");
        EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", "");

        long end = System.currentTimeMillis();
        end = end - start;
        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);
        System.out.println(result5);
        System.out.println(result6);
        System.out.println(result7);
        System.out.println(result8);
        System.out.println(result9);
        System.out.println(result10);
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d9s", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7d", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Qh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "KhQd7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh9d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh8dAdKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh8d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "KhAd7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh2d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh3d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh4d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh5d7dKs", ""));
        System.out.println(EquityCalculation.calculateExhaustiveEnumration("99|88|ATs+|KTs+|QJs|AJo|KQo,AsKd", "Kh6d7dKsKc", ""));
        System.out.println("\n" + "time: " + end);
    }
}
