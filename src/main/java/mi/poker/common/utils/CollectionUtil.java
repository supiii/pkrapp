package mi.poker.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author m1
 */
public class CollectionUtil {

    public static <T> List<List<T>> chopIntoParts(final List<T> ls, final int iParts) {
        final List<List<T>> lsParts = new ArrayList<List<T>>();
        final int iChunkSize = ls.size() / iParts;
        int iLeftOver = ls.size() % iParts;
        int iTake = iChunkSize;

        for (int i = 0, iT = ls.size(); i < iT; i += iTake) {
            if (iLeftOver > 0) {
                iLeftOver--;

                iTake = iChunkSize + 1;
            } else {
                iTake = iChunkSize;
            }

            lsParts.add(new ArrayList<T>(ls.subList(i, Math.min(iT, i + iTake))));
        }

        return lsParts;
    }

    public static <T> List<T> buildListFromArray(T[] array) {
        List<T> col = new LinkedList<T>();
        col.addAll(Arrays.asList(array));

        return col;
    }

    public static <T> void fillArray(T[] arr, List<T> col) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = col.get(i);
        }
    }
}
