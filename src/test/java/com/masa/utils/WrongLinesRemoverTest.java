package com.masa.utils;

import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import com.masa.util.BoardTextureAnalyzer;
import com.masa.util.MadeHandAnalyzer;
import com.masa.util.WrongLinesRemover2;
import java.util.ArrayList;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class WrongLinesRemoverTest {

    @Test
    public void test_1() throws Exception {
        /*
        Hero BTN, Villain BB
        Hero is dealt AdQh
        Preflop Hero R, Villain C
            Flop Td6s4h, Villain XC, Hero R
                -Turn Qs
                    -Villain XC Hero B
                    -Villain XC Hero X
                        -River Qd
                            -Villain XC Hero B, Hero wins 100
                            -Villain XC Hero X, Hero wins 10

         */
        // Point of interesr heros FLOP action

        CardSet boardFlop = new CardSet(c("Td"), c("6s"), c("4h"));
        CardSet holeCards = new CardSet(c("Qh"), c("Js"));

        PostFlopRow current = new PostFlopRow();
        current.actionsPreflop = "R";
        current.actionsFlop = "";
        current.actionsTurn = "";
        current.actionsRiver = "";
        BoardTextureAnalyzer.analyzeFlop(current, boardFlop);

        List<PostFlopRow> rows = new ArrayList<>();

        // block 1 #########
        // Node 1 - Cbets flop, cbets turn, river cbet opportunity
        PostFlopRow r11 = addRow(80.00, holeCards, "R", "B", "B", "B", boardFlop, "Qs", "Qd", rows, 1);
        PostFlopRow r12 = addRow(50.00, holeCards, "R", "B", "B", "X", boardFlop, "Qs", "Qd", rows, 2);

        // Node 2 - Cbets flop, cbets turn, faces bet on river
        PostFlopRow r21 = addRow(70.00, holeCards, "R", "B", "B", "C", boardFlop, "Qs", "Qd", rows, 3);
        PostFlopRow r22 = addRow(100.00, holeCards, "R", "B", "B", "R", boardFlop, "Qs", "Qd", rows, 4);
        PostFlopRow r23 = addRow(-25.00, holeCards, "R", "B", "B", "F", boardFlop, "Qs", "Qd", rows, 5);

        // Node 3 - Cbets flop, faces bet on turn
        PostFlopRow r31 = addRow(80.00, holeCards, "R", "B", "R", "X", boardFlop, "Qs", "Qd", rows, 6);
        PostFlopRow r32 = addRow(25.00, holeCards, "R", "B", "C", "X", boardFlop, "Qs", "Qd", rows, 7);
        PostFlopRow r33 = addRow(-80.00, holeCards, "R", "B", "F", "-", boardFlop, "Qs", null, rows, 8);

        // Node 4 - Cbets flop, raise opp on turn
        PostFlopRow r41 = addRow(100.00, holeCards, "R", "B", "R", "R", boardFlop, "Qs", "Qd", rows, 9);//
        PostFlopRow r42 = addRow(50.00, holeCards, "R", "B", "R", "-", boardFlop, "Qs", null, rows, 10);//

        PostFlopRow r44 = addRow(-10.00, holeCards, "R", "B", "F", "-", boardFlop, "Qs", null, rows, 12);
        PostFlopRow r45 = addRow(70.00, holeCards, "R", "B", "C", "X", boardFlop, "Qs", "Qd", rows, 13);
        PostFlopRow r46 = addRow(80.00, holeCards, "R", "B", "C", "C", boardFlop, "Qs", "Qd", rows, 14);
        PostFlopRow r47 = addRow(100.00, holeCards, "R", "B", "RC", "-", boardFlop, "Qs", "Qd", rows, 15);//
        PostFlopRow r48 = addRow(-60.00, holeCards, "R", "B", "RF", "-", boardFlop, "Qs", null, rows, 16);
        PostFlopRow r43 = addRow(75.00, holeCards, "R", "B", "R", "X", boardFlop, "Qs", "Qd", rows, 11);
        PostFlopRow r49 = addRow(95.00, holeCards, "R", "B", "R", "B", boardFlop, "Qs", "Qd", rows, 17);
        addRow(150.00, holeCards, "R", "B", "-", "-", boardFlop, null, null, rows, 99);
        addRow(-23.00, holeCards, "R", "F", "-", "-", boardFlop, null, null, rows, 100);
        addRow(50.00, holeCards, "R", "R", "-", "-", boardFlop, null, null, rows, 101);

        String turnCard2 = "As";

        // block 2 #########
        // Node 5 - Cbets flop, faces raise turn
        PostFlopRow r51 = addRow(-80.00, holeCards, "R", "B", "R", "B", boardFlop, turnCard2, "Ad", rows, 18);
        PostFlopRow r52 = addRow(-50.00, holeCards, "R", "B", "C", "C", boardFlop, turnCard2, "Ad", rows, 19);
        PostFlopRow r53 = addRow(-20.00, holeCards, "R", "B", "F", "-", boardFlop, turnCard2, null, rows, 20);
        PostFlopRow r54 = addRow(-22.00, holeCards, "R", "B", "F", "-", boardFlop, turnCard2, null, rows, 21);
        //List<PostFlopRow> winningLines = WrongLinesRemover.removeLosingLines(rows, current, Street.FLOP, true);
        List<PostFlopRow> winningLines = WrongLinesRemover2.removeLosingLines(rows, current, Street.FLOP, true, holeCards);

        assertRowFound(99, winningLines);
        assertRowNotFound(100, winningLines);
        assertRowFound(101, winningLines);

        // Node 1
        assertRowFound(1, winningLines);
        assertRowNotFound(2, winningLines);
        // Node 2
        assertRowNotFound(3, winningLines);
        assertRowFound(4, winningLines);
        assertRowNotFound(5, winningLines);
        // Node 3
        assertRowNotFound(6, winningLines);
        assertRowNotFound(7, winningLines);
        assertRowNotFound(8, winningLines);
        // Node 4
        assertRowFound(9, winningLines);
        assertRowFound(10, winningLines);

        assertRowNotFound(11, winningLines);
        assertRowNotFound(12, winningLines);
        assertRowNotFound(13, winningLines);
        assertRowNotFound(14, winningLines);
        assertRowFound(15, winningLines);
        assertRowNotFound(16, winningLines);
        assertRowFound(17, winningLines);
        assertRowNotFound(18, winningLines);
        assertRowNotFound(19, winningLines);
        assertRowFound(20, winningLines);
        assertRowFound(21, winningLines);
    }

    private void assertRowFound(int id, List<PostFlopRow> rows) {
        boolean found = false;
        for (PostFlopRow row : rows) {
            if (row.getId() == id) {
                found = true;
            }
        }

        assertTrue(found, "Should contain " + id);
    }

    private void assertRowNotFound(int id, List<PostFlopRow> rows) {
        boolean found = false;
        for (PostFlopRow row : rows) {
            if (row.getId() == id) {
                found = true;
            }
        }

        assertFalse(found, "Should not contain " + id);
    }

    private PostFlopRow addRow(double wonBB, CardSet holeCards, String actionsPreflop, String actionsFlop, String actionsTurn, String actionsRiver,
            CardSet boardFlop, String turnCardString, String riverCardString, List<PostFlopRow> rows, int id) {
        PostFlopRow row = new PostFlopRow();
        row.wonBB = wonBB;
        row.actionsPreflop = actionsPreflop;
        row.actionsFlop = actionsFlop;
        row.actionsTurn = actionsTurn;
        row.actionsRiver = actionsRiver;
        row.setId(id);
        BoardTextureAnalyzer.analyzeFlop(row, boardFlop);
        row.madeHandSpecificFlop = MadeHandAnalyzer.getMadeHandSpecific(boardFlop, holeCards);

        if (turnCardString != null) {
            CardSet boardTurn = new CardSet(boardFlop);
            boardTurn.add(c(turnCardString));
            BoardTextureAnalyzer.analyzeTurn(row, boardTurn, c(turnCardString));
            row.madeHandSpecificTurn = MadeHandAnalyzer.getMadeHandSpecific(boardTurn, holeCards);
        }
        if (riverCardString != null) {
            CardSet boardTurn = new CardSet(boardFlop);
            boardTurn.add(c(turnCardString));
            CardSet boardRiver = new CardSet(boardTurn);
            boardRiver.add(c(riverCardString));
            BoardTextureAnalyzer.analyzeRiver(row, boardRiver, c(riverCardString));
            row.madeHandSpecificRiver = MadeHandAnalyzer.getMadeHandSpecific(boardRiver, holeCards);
        }

        rows.add(row);
        return row;
    }

    public Card c(String cardString) {
        return new Card(cardString);
    }

}
