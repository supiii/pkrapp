package com.masa.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import mi.poker.common.utils.CollectionUtil;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class TheBigFiveCalculatorTest {

    /*@Test
    public void testFlopBucket() throws Exception {
        // String folder = "PS_new_skin\\card_screenshots\\fs1643908889630.png";

        Set<CardSet> possibleFlops = CardUtil.getPossibleFlops();
        //Set<String> possibleHands = getPossibleHands();

        for (CardSet possibleFlop : possibleFlops) {
            String flop = possibleFlop.get(0).toString() + possibleFlop.get(1).toString() + possibleFlop.get(2).toString();

            if (possibleFlop.size() == 3) {
                FlopBucket flopBucket = FlopBucketAnalyzer.getFlopBucket(possibleFlop);
                System.out.println(flop + " " + flopBucket.value());
            } else {
                System.out.println("possibleFlop.size(): " + possibleFlop.size());
            }

        }

        System.out.println("Possible flops size: " + possibleFlops.size());
    }*/
    @Test
    public void testChopIntoParts() {
        List<Integer> numbers = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20));

        List<List<Integer>> chopIntoParts = CollectionUtil.chopIntoParts(numbers, 4);

        System.out.println("parts: " + chopIntoParts.size());

        for (List<Integer> chopIntoPart : chopIntoParts) {
            System.out.println("-");
            for (Integer integer : chopIntoPart) {
                System.out.println("" + integer);
            }
        }

    }
}
