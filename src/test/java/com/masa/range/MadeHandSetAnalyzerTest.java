package com.masa.range;

import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class MadeHandSetAnalyzerTest {

    @Test
    public void testGetMadeHandSpecificSetTest() {
        assertEquals(MadeHandSpecific.SET, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qs"), c("8d"), c("5h"), c("2c")),
                new CardSet(c("Qh"), c("Qc"))));
    }

    @Test
    public void testGetMadeHandSpecificThreeOfAKindTest() {
        assertEquals(MadeHandSpecific.THREE_OF_A_KIND, getMadeHandSpecific(
                new CardSet(c("Qc"), c("8s"), c("8d"), c("5h")),
                new CardSet(c("8h"), c("Jc"))));
    }

    @Test
    public void testGetMadeHandSpecificNoMadeHandTest() {
        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("8c"), c("8s"), c("8d"), c("5h")),
                new CardSet(c("Ah"), c("Jc"))));
    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        return MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
    }
}
