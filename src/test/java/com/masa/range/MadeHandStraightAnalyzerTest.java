package com.masa.range;

import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class MadeHandStraightAnalyzerTest {

    @Test
    public void testGetMadeHandSpecificStraightTest() {
        assertEquals(MadeHandSpecific.STRAIGHT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qs"), c("Jd"), c("Th"), c("2c")),
                new CardSet(c("Ah"), c("Qc"))));

        assertEquals(MadeHandSpecific.STRAIGHT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qs"), c("Td"), c("9h"), c("2c")),
                new CardSet(c("Ah"), c("Jc"))));

        assertEquals(MadeHandSpecific.STRAIGHT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qs"), c("Jd"), c("3h"), c("2c")),
                new CardSet(c("Th"), c("9c"))));

        assertEquals(MadeHandSpecific.STRAIGHT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qs"), c("Jd"), c("Th"), c("9c")),
                new CardSet(c("Ah"), c("Kd"))));
    }

    @Test
    public void testGetMadeHandSpecificSetTest() {
        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qs"), c("Jd"), c("Th"), c("9d")),
                new CardSet(c("Qh"), c("Qc"))));
    }

    @Test
    public void testGetMadeHandSpecificTwoPairTest() {
        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qs"), c("Jd"), c("Th"), c("9d")),
                new CardSet(c("Kh"), c("Qc"))));
    }

    @Test
    public void testGetMadeHandSpecificPairTest() {
        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("Qs"), c("Jd"), c("Th"), c("9d"), c("8s")),
                new CardSet(c("Ah"), c("Ac"))));

        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("Qs"), c("Jd"), c("Th"), c("9d"), c("8s")),
                new CardSet(c("Ah"), c("Ac"))));

        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("Qs"), c("Jd"), c("Th"), c("9d"), c("8s")),
                new CardSet(c("6h"), c("6c"))));
    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        return MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
    }
}
