package com.masa.range;

import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class MadeHandFlullhouseAnalyzerTest {

    @Test
    public void testGetMadeHandSpecificThreeOfAKindOnBoardTest() {

        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("7c"), c("4c"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_AA, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("Ac"), c("Ad"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_KK, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("Kc"), c("Kd"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_JJ, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("Jc"), c("Jd"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_66_99, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("6c"), c("6d"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_66_99, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("9c"), c("9d"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_22_55, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("2c"), c("2d"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_22_55, getMadeHandSpecific(
                new CardSet(c("Ah"), c("Ac"), c("Ad")),
                new CardSet(c("5c"), c("5d"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_SET, getMadeHandSpecific(
                new CardSet(c("Jh"), c("Jc"), c("Jd"), c("Kh")),
                new CardSet(c("Kc"), c("Kd"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_PP_BELOW_TP, getMadeHandSpecific(
                new CardSet(c("Jh"), c("Jc"), c("Jd"), c("Kh"), c("9d")),
                new CardSet(c("Qc"), c("Qd"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_WEAK_PAIR, getMadeHandSpecific(
                new CardSet(c("Jh"), c("Jc"), c("Jd"), c("Kh"), c("9d")),
                new CardSet(c("Tc"), c("Td"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_WEAK_PAIR, getMadeHandSpecific(
                new CardSet(c("Jh"), c("Jc"), c("Jd"), c("Kh"), c("Ad")),
                new CardSet(c("Qc"), c("Qd"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_WEAK_PAIR, getMadeHandSpecific(
                new CardSet(c("Jh"), c("Jc"), c("Jd"), c("Kh"), c("Ad")),
                new CardSet(c("Qc"), c("Qd"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_TP, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qh"), c("Qc"), c("Qd"), c("Th")),
                new CardSet(c("Kh"), c("9d"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_MIDDLE_PAIR, getMadeHandSpecific(
                new CardSet(c("Jc"), c("Qh"), c("Qc"), c("Qd"), c("Th")),
                new CardSet(c("Jh"), c("Td"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_MIDDLE_PAIR, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd"), c("Th")),
                new CardSet(c("Jh"), c("Td"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_WEAK_PAIR, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qh"), c("Qc"), c("Qd"), c("Th")),
                new CardSet(c("Jh"), c("Td"))));
    }

    @Test
    public void testGetMadeHandSpecificTwoPairOnBoardTest() {
        assertEquals(MadeHandSpecific.FULL_HOUSE_TOP, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d")),
                new CardSet(c("Qd"), c("4c"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_TOP, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d")),
                new CardSet(c("Qd"), c("9h"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_BOTTOM, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d")),
                new CardSet(c("9h"), c("4c"))));

        // PP
        assertEquals(MadeHandSpecific.FULL_HOUSE_STRONG, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d"), c("Ah")),
                new CardSet(c("Ac"), c("Ad"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_OTHER, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d"), c("Jh")),
                new CardSet(c("Jc"), c("Jd"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_OTHER, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d"), c("7h")),
                new CardSet(c("7c"), c("7d"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_TOP, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d"), c("7h")),
                new CardSet(c("Qd"), c("4c"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_TOP, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d"), c("7h")),
                new CardSet(c("Qd"), c("9h"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE_BOTTOM, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("9c"), c("9d"), c("7h")),
                new CardSet(c("9h"), c("4c"))));
    }

    @Test
    public void testGetMadeHandSpecificFullHousePairOnBoardTest() {
        assertEquals(MadeHandSpecific.FULL_HOUSE, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Tc"), c("7h"), c("4d")),
                new CardSet(c("Qd"), c("Th"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Tc"), c("7h"), c("4d")),
                new CardSet(c("Qd"), c("7d"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Tc"), c("7h"), c("4d")),
                new CardSet(c("Qd"), c("4h"))));

        assertEquals(MadeHandSpecific.FULL_HOUSE, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Tc"), c("7h"), c("4d")),
                new CardSet(c("Td"), c("Th"))));
    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        return MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
    }
}
