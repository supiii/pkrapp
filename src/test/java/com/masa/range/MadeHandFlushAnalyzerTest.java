package com.masa.range;

import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class MadeHandFlushAnalyzerTest {

    @Test
    public void testGetMadeHandSpecificSetTest() {
        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qc"), c("Tc"), c("9c"), c("8c")),
                new CardSet(c("7c"), c("4c"))));

        assertEquals(MadeHandSpecific.FLUSH_NUT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Qc"), c("Tc"), c("9c"), c("8c")),
                new CardSet(c("Ac"), c("4c"))));

        assertEquals(MadeHandSpecific.FLUSH_2ND_NUT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Jc"), c("9c"), c("7c"), c("5c")),
                new CardSet(c("Qc"), c("4c"))));

        assertEquals(MadeHandSpecific.FLUSH_3RD_NUT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Jc"), c("9c"), c("7c"), c("5c")),
                new CardSet(c("Tc"), c("4c"))));

        assertEquals(MadeHandSpecific.FLUSH_4TH_NUT, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Jc"), c("9c"), c("7c"), c("5c")),
                new CardSet(c("8c"), c("4c"))));

        assertEquals(MadeHandSpecific.FLUSH_WEAK, getMadeHandSpecific(
                new CardSet(c("Kc"), c("Jc"), c("9c"), c("7c"), c("5c")),
                new CardSet(c("6c"), c("3c"))));

    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        return MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
    }
}
