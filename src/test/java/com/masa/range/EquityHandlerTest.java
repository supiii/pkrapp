package com.masa.range;

import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import mi.poker.common.model.testbed.klaatu.HandEval;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class EquityHandlerTest {

    /*@Test
    public void testEVCalculationHandVsWeightedCombination() throws Exception {
        CardSet flush = new CardSet();
        flush.add(new Card(Rank.ACE, Suit.CLUB));
        flush.add(new Card(Rank.KING, Suit.CLUB));
        flush.add(new Card(Rank.NINE, Suit.CLUB));
        flush.add(new Card(Rank.EIGHT, Suit.CLUB));
        flush.add(new Card(Rank.SEVEN, Suit.CLUB));
        int flushInt = HandEval.handEval(flush);
        System.out.println(flushInt);

        CardSet flush2 = new CardSet();
        flush2.add(new Card(Rank.TEN, Suit.CLUB));
        flush2.add(new Card(Rank.KING, Suit.CLUB));
        flush2.add(new Card(Rank.NINE, Suit.CLUB));
        flush2.add(new Card(Rank.EIGHT, Suit.CLUB));
        flush2.add(new Card(Rank.SEVEN, Suit.CLUB));
        int flushInt2 = HandEval.handEval(flush2);
        System.out.println(flushInt2);

        CardSet twoPairs = new CardSet();
        twoPairs.add(new Card(Rank.FOUR, Suit.CLUB));
        twoPairs.add(new Card(Rank.FIVE, Suit.CLUB));
        twoPairs.add(new Card(Rank.SIX, Suit.CLUB));
        twoPairs.add(new Card(Rank.NINE, Suit.CLUB));
        twoPairs.add(new Card(Rank.NINE, Suit.HEART));
        int straightFlushInt = HandEval.handEval(twoPairs);
        System.out.println(straightFlushInt);
    }*/
    @Test
    public void testHandStrength() {
        System.out.println("" + HandEval.handEval(
                new CardSet(c("3h"), c("2h"), c("8h"), c("9h"), c("Qd"), c("4h"))));
        System.out.println("" + HandEval.handEval(
                new CardSet(c("Ac"), c("2c"), c("3c"), c("4c"), c("6c"), c("2d"))));
        System.out.println("" + HandEval.handEval(
                new CardSet(c("9h"), c("Jh"), c("Qh"), c("Kh"), c("Ah"))));
        System.out.println("" + HandEval.handEval(
                new CardSet(c("8d"), c("9h"), c("Jh"), c("Qh"), c("Kh"), c("Ah"))));
        System.out.println("" + HandEval.handEval(
                new CardSet(c("7d"), c("8d"), c("9h"), c("Jh"), c("Qh"), c("Kh"), c("Ah"))));

    }

    /*@Test
    public void testRangeConnectivity() {
        CardSet flop1 = new CardSet(c("Qc"), c("Jc"), c("Td"));
        CardSet deadCards = new CardSet();
        Set<CardSet> range = new HashSet<>(CardDistributionUtil.getAllPossibleHandsReturnSet());

        range.add(new CardSet(c("8d"), c("9c")));
        range.add(new CardSet(c("8d"), c("9h")));
        range.add(new CardSet(c("8d"), c("9c")));
        range.add(new CardSet(c("8d"), c("9s")));
        range.add(new CardSet(c("Js"), c("8h")));
        range.add(new CardSet(c("Tc"), c("6c")));

        RangeConnectivityAnalyzer.analyzeConnectivity(flop1, range, deadCards);
    }*/
    private Card c(String cardString) {
        return new Card(cardString);
    }
}
