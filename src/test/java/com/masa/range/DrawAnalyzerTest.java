package com.masa.range;

import com.masa.type.Draw;
import com.masa.util.DrawAnalyzer;
import java.util.Set;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class DrawAnalyzerTest {

    @Test
    public void testGetFlushDrawsTest() {
        // No flush draw
        assertEquals(Set.of(), getDraws(
                cs("Qh", "Jh", "7h"),
                cs("Ah", "4h"),
                null));

        assertEquals(Set.of(), getDraws(
                cs("Qh", "Jh", "7h", "5h"),
                cs("Ah", "Jc"),
                null));

        // Flush draw
        assertEquals(Set.of(Draw.FLUSH_DRAW), getDraws(
                cs("Qh", "Jh", "7d"),
                cs("Ah", "4h"),
                null));
        assertEquals(Set.of(Draw.FLUSH_DRAW), getDraws(
                cs("Qh", "Jh", "7d", "5d"),
                cs("Ah", "4h"),
                null));

        // Nut flushdraw
        assertEquals(Set.of(Draw.FLUSH_DRAW_NUT), getDraws(
                cs("Qh", "Jh", "7h"),
                cs("Ah", "4d"),
                null));
        assertEquals(Set.of(Draw.FLUSH_DRAW_NUT), getDraws(
                cs("Ah", "Jh", "7h"),
                cs("Kh", "4d"),
                null));
        assertEquals(Set.of(Draw.Gutshot, Draw.FLUSH_DRAW_NUT), getDraws(
                cs("Ah", "Kh", "Jh"),
                cs("Qh", "4d"),
                null));

        // 2nd nut flushdraw
        assertEquals(Set.of(Draw.FLUSH_DRAW_2ND_NUT), getDraws(
                cs("Ah", "Jh", "8h", "3c"),
                cs("Qh", "4d"),
                null));
        assertEquals(Set.of(Draw.FLUSH_DRAW_2ND_NUT), getDraws(
                cs("Ah", "Kh", "8h"),
                cs("Jh", "4d"),
                null));

        // 3rd nut flushdraw
        assertEquals(Set.of(Draw.FLUSH_DRAW_3RD_NUT), getDraws(
                cs("Ah", "Kh", "8h"),
                cs("Th", "4d"),
                null));
        assertEquals(Set.of(Draw.FLUSH_DRAW_3RD_NUT), getDraws(
                cs("Th", "8h", "2h"),
                cs("Qh", "4d"),
                null));

        // flushdraw weak
        assertEquals(Set.of(Draw.FLUSH_DRAW_WEAK), getDraws(
                cs("Th", "8h", "2h"),
                cs("Jh", "4d"),
                null));

        // flushdraw 2 card backdoor
        assertEquals(Set.of(Draw.FLUSH_DRAW_BACKDOOR), getDraws(
                cs("Qh", "Jc", "7c"),
                cs("Ah", "4h"),
                null));

        // overcards
        assertEquals(Set.of(Draw.Gutshot, Draw.OVERCARDS), getDraws(
                cs("Qh", "Jc", "7c"),
                cs("Ah", "Kd"),
                null));
        assertEquals(Set.of(Draw.OVERCARDS), getDraws(
                cs("Jh", "7c", "3c"),
                cs("Kh", "Qd"),
                null));
    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private CardSet cs(String... cardStrings) {
        CardSet cards = new CardSet();
        for (String cardString : cardStrings) {
            cards.add(c(cardString));
        }
        return cards;
    }

    private Set<Draw> getDraws(CardSet board, CardSet holeCards, CardSet deadCards) {
        return DrawAnalyzer.getDraws(board, holeCards, deadCards);
    }
}
