package com.masa.range;

import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class MadeHandStraightFlushAnalyzerTest {

    @Test
    public void testGetMadeHandSpecificStraightFlushTest() {
        assertEquals(MadeHandSpecific.STRAIGHT_FLUSH, getMadeHandSpecific(
                new CardSet(c("8c"), c("9c"), c("Tc")),
                new CardSet(c("6c"), c("7c"))));

        assertEquals(MadeHandSpecific.STRAIGHT_FLUSH, getMadeHandSpecific(
                new CardSet(c("8c"), c("9c"), c("Tc")),
                new CardSet(c("Jc"), c("7c"))));

        assertEquals(MadeHandSpecific.STRAIGHT_FLUSH, getMadeHandSpecific(
                new CardSet(c("8c"), c("9c"), c("Tc"), c("Jc")),
                new CardSet(c("Qc"), c("Ad"))));

        assertEquals(MadeHandSpecific.NO_MADE_HAND, getMadeHandSpecific(
                new CardSet(c("8c"), c("9c"), c("Tc"), c("Jc"), c("Qc")),
                new CardSet(c("7c"), c("Ad"))));
    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        return MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
    }
}
