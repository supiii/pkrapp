package com.masa.range;

import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class MadeHandQuadsAnalyzerTest {

    @Test
    public void testGetMadeHandSpecificQaudsTest() {

        assertEquals(MadeHandSpecific.QUADS, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd")),
                new CardSet(c("Qs"), c("4c"))));

        assertEquals(MadeHandSpecific.QUADS, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd"), c("4d")),
                new CardSet(c("Qs"), c("4c"))));

        assertEquals(MadeHandSpecific.QUADS, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Kd")),
                new CardSet(c("Qs"), c("Qd"))));

        assertEquals(MadeHandSpecific.QUADS, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Kd"), c("Jh")),
                new CardSet(c("Qs"), c("Qd"))));

        // Quads on board
        assertEquals(MadeHandSpecific.ACE_HIGH, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd"), c("Qs")),
                new CardSet(c("As"), c("4c"))));

        assertEquals(MadeHandSpecific.ACE_HIGH, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd"), c("Qs")),
                new CardSet(c("As"), c("Ac"))));

        assertEquals(MadeHandSpecific.KING_HIGH, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd"), c("Qs")),
                new CardSet(c("Ks"), c("Jc"))));

        assertEquals(MadeHandSpecific.QUEEN_HIGH, getMadeHandSpecific(
                new CardSet(c("Ah"), c("Ac"), c("Ad"), c("As")),
                new CardSet(c("Qs"), c("Tc"))));

        assertEquals(MadeHandSpecific.JACK_HIGH, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd"), c("Qs")),
                new CardSet(c("Js"), c("Tc"))));

        assertEquals(MadeHandSpecific.MEDIUM_HIGH, getMadeHandSpecific(
                new CardSet(c("Qh"), c("Qc"), c("Qd"), c("Qs")),
                new CardSet(c("Ts"), c("9c"))));

    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        return MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
    }
}
