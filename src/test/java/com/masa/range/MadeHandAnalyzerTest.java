package com.masa.range;

import com.masa.type.MadeHandSpecific;
import com.masa.util.MadeHandAnalyzer;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class MadeHandAnalyzerTest {

    @Test
    public void testGetMadeHandSpecificPairTopPair() {
        CardSet board = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards = new CardSet(c("Kh"), c("Ac"));
        MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.TOP_PAIR, madeHandSpecific);
    }

    @Test
    public void testGetMadeHandSpecificPairMiddlePair() {
        CardSet board = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards = new CardSet(c("Th"), c("Qd"));
        MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.MIDDLE_PAIR, madeHandSpecific);

        CardSet board2 = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards2 = new CardSet(c("Ah"), c("Qd"));
        MadeHandSpecific madeHandSpecific2 = MadeHandAnalyzer.getMadeHandSpecific(board2, holeCards2);
        assertEquals(MadeHandSpecific.MIDDLE_PAIR, madeHandSpecific2);

        CardSet board3 = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Jc"));
        CardSet holeCards3 = new CardSet(c("Qh"), c("Jd"));
        MadeHandSpecific madeHandSpecific3 = MadeHandAnalyzer.getMadeHandSpecific(board3, holeCards3);
        assertEquals(MadeHandSpecific.MIDDLE_PAIR, madeHandSpecific3);
    }

    @Test
    public void testGetMadeHandSpecificPairWeakPair() {
        CardSet board = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards = new CardSet(c("Ah"), c("8c"));
        MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.WEAK_PAIR, madeHandSpecific);

        CardSet board2 = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards2 = new CardSet(c("Th"), c("8c"));
        MadeHandSpecific madeHandSpecific2 = MadeHandAnalyzer.getMadeHandSpecific(board2, holeCards2);
        assertEquals(MadeHandSpecific.WEAK_PAIR, madeHandSpecific2);

        CardSet board3 = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards3 = new CardSet(c("Jh"), c("8c"));
        MadeHandSpecific madeHandSpecific3 = MadeHandAnalyzer.getMadeHandSpecific(board3, holeCards3);
        assertEquals(MadeHandSpecific.WEAK_PAIR, madeHandSpecific3);

        assertEquals(MadeHandSpecific.WEAK_PAIR, getMadeHandSpecific(
                new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Ac")),
                new CardSet(c("Qh"), c("Qc"))));
    }

    @Test
    public void testGetMadeHandSpecificPairNoMadeHand() {
        CardSet board = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards = new CardSet(c("Jh"), c("6c"));
        MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.NO_MADE_HAND, madeHandSpecific);

        CardSet board2 = new CardSet(c("Kc"), c("4s"), c("8d"), c("Qh"), c("Qc"));
        CardSet holeCards2 = new CardSet(c("Jh"), c("6c"));
        MadeHandSpecific madeHandSpecific2 = MadeHandAnalyzer.getMadeHandSpecific(board2, holeCards2);
        assertEquals(MadeHandSpecific.NO_MADE_HAND, madeHandSpecific2);
    }

    @Test
    public void testGetMadeHandSpecificOverpairPair() {
        MadeHandSpecific madeHandSpecific
                = getMadeHandSpecific(new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc")),
                        new CardSet(c("Ah"), c("Ac")));
        assertEquals(MadeHandSpecific.OVERPAIR, madeHandSpecific);
    }

    @Test
    public void testGetMadeHandSpecificPairPPBelowTopPairPair() {
        assertEquals(MadeHandSpecific.PP_BELOW_TP, getMadeHandSpecific(
                new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Jc")),
                new CardSet(c("Qh"), c("Qc"))));
    }

    @Test
    public void testGetMadeHandSpecificTwoPair() {
        // holecards connect both
        CardSet board = new CardSet(c("Kc"), c("4s"), c("8d"), c("5h"), c("Qc"));
        CardSet holeCards = new CardSet(c("Kh"), c("4c"));
        MadeHandSpecific madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.TWO_PAIR, madeHandSpecific);

        // holecards connect 1 top pair
        CardSet board1 = new CardSet(c("Kc"), c("4s"), c("4d"), c("5h"), c("Qc"));
        CardSet holeCards1 = new CardSet(c("Kh"), c("2c"));
        MadeHandSpecific madeHandSpecific1 = MadeHandAnalyzer.getMadeHandSpecific(board1, holeCards1);
        assertEquals(MadeHandSpecific.TOP_PAIR, madeHandSpecific1);

        // holecards connect 1 top pair
        CardSet board11 = new CardSet(c("Kc"), c("4s"), c("4d"), c("5h"), c("Qc"));
        CardSet holeCards11 = new CardSet(c("Kh"), c("Ac"));
        MadeHandSpecific madeHandSpecific11 = MadeHandAnalyzer.getMadeHandSpecific(board11, holeCards11);
        assertEquals(MadeHandSpecific.TOP_PAIR, madeHandSpecific11);

        // holecards connect 1 middle pair
        CardSet board2 = new CardSet(c("Kc"), c("4s"), c("4d"), c("5h"), c("Tc"));
        CardSet holeCards2 = new CardSet(c("Th"), c("2c"));
        MadeHandSpecific madeHandSpecific2 = MadeHandAnalyzer.getMadeHandSpecific(board2, holeCards2);
        assertEquals(MadeHandSpecific.MIDDLE_PAIR, madeHandSpecific2);

        // holecards connect 1 weak pair
        // K5544
        CardSet board3 = new CardSet(c("Kc"), c("4s"), c("4d"), c("5h"), c("Tc"));
        CardSet holeCards3 = new CardSet(c("5c"), c("2c"));
        MadeHandSpecific madeHandSpecific3 = MadeHandAnalyzer.getMadeHandSpecific(board3, holeCards3);
        assertEquals(MadeHandSpecific.WEAK_PAIR, madeHandSpecific3);

        // holecards connect 1 No made hand
        board = new CardSet(c("Kc"), c("Kd"), c("4d"), c("4s"), c("3c"));
        holeCards = new CardSet(c("5h"), c("3s"));
        madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.NO_MADE_HAND, madeHandSpecific);

        // holecards connect 0
        board = new CardSet(c("Kc"), c("Kh"), c("8c"), c("8d"), c("Qc"));
        holeCards = new CardSet(c("5h"), c("4c"));
        madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.NO_MADE_HAND, madeHandSpecific);
        // holecards PP PP below TP
        board = new CardSet(c("Kc"), c("Kd"), c("4c"), c("4d"), c("3c"));
        holeCards = new CardSet(c("Th"), c("Tc"));
        madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.PP_BELOW_TP, madeHandSpecific);

        // holecards PP Overpair
        board = new CardSet(c("Kc"), c("Kd"), c("4c"), c("4d"), c("3c"));
        holeCards = new CardSet(c("Ah"), c("Ac"));
        madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.OVERPAIR, madeHandSpecific);

        // holecards PP Weak pair
        board = new CardSet(c("Kc"), c("Kd"), c("Qc"), c("4d"), c("3c"));
        holeCards = new CardSet(c("Th"), c("Tc"));
        madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.WEAK_PAIR, madeHandSpecific);
        // holecards connect 1 Ace high
        board = new CardSet(c("Kc"), c("Kh"), c("8c"), c("8d"), c("Qc"));
        holeCards = new CardSet(c("Ah"), c("4c"));
        madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        assertEquals(MadeHandSpecific.ACE_HIGH, madeHandSpecific);
    }

    private Card c(String cardString) {
        return new Card(cardString);
    }

    private MadeHandSpecific getMadeHandSpecific(CardSet board, CardSet holeCards) {
        return MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
    }
}
