package com.masa.pokertracker;

import com.masa.pokertracker.model.PTActionType;
import com.masa.txt.service.PostFlopRowService;
import com.masa.type.BoardTextureFlop;
import com.masa.type.NextPostFlopMoves;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import com.masa.util.BoardTextureAnalyzer;
import com.masa.util.DrawAnalyzer;
import com.masa.util.MadeHandAnalyzer;
import com.masa.util.Util;
import java.util.List;
import mi.poker.common.model.testbed.klaatu.Card;
import mi.poker.common.model.testbed.klaatu.CardSet;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Matti
 */
public class PostFlopServiceTest {

    /*
    private final static String[] cards = {
        "2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "Tc", "Jc", "Qc", "Kc", "Ac",
        "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "Td", "Jd", "Qd", "Kd", "Ad",
        "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "Th", "Jh", "Qh", "Kh", "Ah",
        "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "Ts", "Js", "Qs", "Ks", "As"
    }; // Js2h - J2o
     */
    // Ac 13
    // Ad 26
    // As 47
    // BOARD
    // Ah 39
    // Jc 10
    // 4d 16
    // Jd 23
    // board
    // 39 10 16 23 14
    //Villain hand
    // 1 2
    @Test
    public void testReadPostFlopRowsFromTextFile() throws Exception {
        PostFlopRowService postFlopService = new PostFlopRowService();
        Street currentStreet = Street.FLOP;

        CardSet holeCards = new CardSet();
        holeCards.add(new Card("As"));
        holeCards.add(new Card("Ad"));
        CardSet board = new CardSet();
        board.add(new Card("Ah"));
        board.add(new Card("Jc"));
        board.add(new Card("4d"));

        long start = System.currentTimeMillis();

        PostFlopRow current = new PostFlopRow();
        //5,3
        current.position = "BTN";
        current.villainPosition = "BB";
        current.actionsPreflop = "R";
        current.actionsFlop = "B";
        current.actionsTurn = "-";
        current.actionsRiver = "";
        current.actionsVillainPreflop = "C";
        current.actionsVillainFlop = "X";
        current.actionsVillainTurn = "-";
        current.actionsVillainRiver = "-";
        current.madeHandSpecificFlop = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
        current.drawsFlop = DrawAnalyzer.getDraws(board, holeCards, null);
        Util.mapDraws(current, current.drawsFlop, Street.FLOP);
        BoardTextureAnalyzer.analyzeFlop(current, board);
        current.boardTextureFlop = new BoardTextureFlop(current);

        List<PostFlopRow> rows = postFlopService.readAndFilterWithBasicInfo(current, currentStreet);
        NextPostFlopMoves nextMoves = new NextPostFlopMoves();
        divideToNextMoves(current, rows, nextMoves, currentStreet);

        long end = System.currentTimeMillis();

        /*for (PostFlopRow row : rows) {
            System.out.println(row.actionsIdFlop);
            System.out.println(row.actionsIdTurn);
            System.out.println(row.actionsIdRiver);
        }*/
        System.out.println("TOOK: " + ((start - end)) + " ms");

        System.out.println("Size: " + rows.size());
    }

    public static void divideToNextMoves(PostFlopRow current, List<PostFlopRow> rows, NextPostFlopMoves nextMoves, Street currentStreet) {
        String foldAction = current.getActions(currentStreet) + PTActionType.FOLD.value();
        String checkAction = current.getActions(currentStreet) + PTActionType.CHECK.value();
        String callAction = current.getActions(currentStreet) + PTActionType.CALL.value();
        String betAction = current.getActions(currentStreet) + PTActionType.BET.value();
        String raiseAction = current.getActions(currentStreet) + PTActionType.RAISE.value();

        for (PostFlopRow row : rows) {

            String rowActions = row.getActions(currentStreet);
            //System.out.println("Divide actions, current street: " + rowActions + " " + row.wonBB);
            if (rowActions.startsWith(foldAction)) {
                nextMoves.getFoldMoves().add(row.wonBB);
            } else if (rowActions.startsWith(checkAction)) {
                nextMoves.getCheckMoves().add(row.wonBB);
            } else if (rowActions.startsWith(callAction)) {
                nextMoves.getCallMoves().add(row.wonBB);
            } else if (rowActions.startsWith(betAction) || rowActions.startsWith(raiseAction)) {
                nextMoves.getRaiseMoves().add(row.wonBB);
            }
        }
    }

    public static void calculate(NextPostFlopMoves nextPostFlopMoves) {

        int foldMovesCount = nextPostFlopMoves.getFoldMoves().count;
        double foldAmountBB = nextPostFlopMoves.getFoldMoves().bbAmountWonOrLoss;
        int checkMovesCount = nextPostFlopMoves.getCheckMoves().count;
        double checkAmountBB = nextPostFlopMoves.getCheckMoves().bbAmountWonOrLoss;
        int callMovesCount = nextPostFlopMoves.getCallMoves().count;
        double callAmountBB = nextPostFlopMoves.getCallMoves().bbAmountWonOrLoss;
        int raiseMovesCount = nextPostFlopMoves.getRaiseMoves().count;
        double raiseAmountBB = nextPostFlopMoves.getRaiseMoves().bbAmountWonOrLoss;

        System.out.println("Fold: (" + foldMovesCount + ") " + foldAmountBB + " " + (foldAmountBB / foldMovesCount));
        System.out.println("Check: (" + checkMovesCount + ") " + checkAmountBB + " " + (checkAmountBB / checkMovesCount));
        System.out.println("Call: (" + callMovesCount + ") " + callAmountBB + " " + (callAmountBB / callMovesCount));
        System.out.println("Raise: (" + raiseMovesCount + ") " + raiseAmountBB + " " + (raiseAmountBB / raiseMovesCount));

    }
}
