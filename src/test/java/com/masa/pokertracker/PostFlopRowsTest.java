package com.masa.pokertracker;

import com.masa.txt.service.PostFlopRowService;
import com.masa.type.HUDStat;
import com.masa.type.NLLimit;
import com.masa.type.PostFlopRow;
import com.masa.type.Street;
import com.masa.util.BoardTextureAnalyzer;
import com.masa.util.HUDStatUtil;
import com.masa.util.Util;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mi.poker.common.model.testbed.klaatu.CardSet;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Matti
 */
public class PostFlopRowsTest {

    private final PokerTrackerRepository pokerTrackerRepository = new PokerTrackerRepository();
    //private final WinningPlayersMovesRepository winningPlayersRepository = new WinningPlayersMovesRepository();
    //private final AllMovesRepository allMovesRepository = new AllMovesRepository();
    Map<HUDStat, Double> defaultHUDStats = new HashMap<>();
    Map<Integer, Map<HUDStat, Double>> playersHUDStats = new HashMap<>();

    @Test
    public void testHandleFullPostflop() throws Exception {
        defaultHUDStats.putAll(HUDStatUtil.getDefaultHUDStats(NLLimit.NL5));
        boolean postflop = true;
        int step = 800000;
        //int step = 7000;

        for (int i = 0; i < 30000; i++) {
            System.out.println("Iteration nro: " + i);
            long start = System.currentTimeMillis();
            List<Map<String, Object>> response = new ArrayList<>();

            response.addAll(pokerTrackerRepository.fetchWinningPlayersHandReport(0, step, postflop, i * step));
            List<PostFlopRow> postflopRows = new ArrayList<>();

            if (response.isEmpty()) {
                System.out.println("EMPTY RESPONSE");
                break;
            }

            //System.out.println("response: " + response.toString());
            fetchDataForP1AndStatsForP2AndCombinePostflopRows(response, postflopRows, postflop);
            System.out.println("DONE FETCHING HUD STATS...");

            for (PostFlopRow postflopRow : postflopRows) {
                //CardSet board = PTUtil.getBoardCardSet(postflopRow.flopCard1Id, postflopRow.flopCard2Id, postflopRow.flopCard3Id, postflopRow.turnCardId, postflopRow.riverCardId);
                /*if (board == null || board.isEmpty()) {
                continue;
            }*/

                //CardSet holeCards = PTUtil.getHoleCardSet(postflopRow.holeCard1Id, postflopRow.holeCard2Id);
                CardSet boardFlop = PTUtil.getBoardCardSet(postflopRow.flopCard1Id, postflopRow.flopCard2Id, postflopRow.flopCard3Id, 0, 0);
                if (postflopRow.flopCard1Id > 0) {
                    //postflopRow.madeHandSpecificFlop = MadeHandAnalyzer.getMadeHandSpecific(boardFlop, holeCards);
                    //postflopRow.flopBucket = FlopBucketAnalyzer.getFlopBucket(boardFlop);
                    BoardTextureAnalyzer.analyzeFlop(postflopRow, boardFlop);
                    //postflopRow.flopSuitness = FlopSuitnessAnalyzer.getFlopSuitness(boardFlop);
                    //postflopRow.drawsFlop = DrawAnalyzer.getDraws(boardFlop, holeCards, null);
                    Util.mapDraws(postflopRow, postflopRow.drawsFlop, Street.FLOP);
                    // THE BIG FIVE
                    /*postflopRow.ip = Util.isHeroIP();
                postflopRow.rangeEquityFlop = EquityHandler.calculateEquityMonteCarlo(heroRange, villainRange, board);
                postflopRow.rawEquityFlop = EquityHandler.calculateEquityMonteCarlo(holeCards, villainRange, board);
                postflopRow.vulnerablityFlop = VulnerabilityAnalyzer.analyzeVulnerabilityRatio(villainRangeCardSet, boardCardSet, holeCardsCardSet);
                     */
                }
                CardSet boardTurn = null;
                CardSet boardRiver = null;
                if (postflopRow.turnCardId > 0) {
                    boardTurn = PTUtil.getBoardCardSet(postflopRow.flopCard1Id, postflopRow.flopCard2Id, postflopRow.flopCard3Id, postflopRow.turnCardId, 0);
                    if (boardTurn.size() < 4) {
                        System.out.println("DEBUG");
                    }
                    BoardTextureAnalyzer.analyzeTurn(postflopRow, boardTurn, PTUtil.getCard(postflopRow.turnCardId));
                    //postflopRow.drawsTurn = DrawAnalyzer.getDraws(boardTurn, holeCards, null);
                    Util.mapDraws(postflopRow, postflopRow.drawsTurn, Street.TURN);
                    //postflopRow.madeHandSpecificTurn = MadeHandAnalyzer.getMadeHandSpecific(boardTurn, holeCards);
                }
                if (postflopRow.riverCardId > 0) {
                    boardRiver = PTUtil.getBoardCardSet(postflopRow.flopCard1Id, postflopRow.flopCard2Id, postflopRow.flopCard3Id, postflopRow.turnCardId, postflopRow.riverCardId);
                    BoardTextureAnalyzer.analyzeRiver(postflopRow, boardRiver, PTUtil.getCard(postflopRow.riverCardId));
                    //postflopRow.madeHandSpecificRiver = MadeHandAnalyzer.getMadeHandSpecific(boardRiver, holeCards);
                }

                // equities
                // Preflop
                /*if (postflopRow.villainHoleCard1Id > 0 && postflopRow.villainHoleCard2Id > 0) {
                String holeCard1String = PTUtil.getCard(postflopRow.holeCard1Id).toString();
                String holeCard2String = PTUtil.getCard(postflopRow.holeCard2Id).toString();
                String villainHoleCard1String = PTUtil.getCard(postflopRow.villainHoleCard1Id).toString();
                String villainHoleCard2String = PTUtil.getCard(postflopRow.villainHoleCard2Id).toString();

                String holeCardsString = holeCard1String + holeCard2String;
                String villainHoleCardsString = villainHoleCard1String + villainHoleCard2String;

                double equityPreflop = EquityHandler.calculateEquityPreflop(holeCardsString, villainHoleCardsString);
                postflopRow.equityPreflop = equityPreflop;

                if (boardFlop != null) {
                    double equityFlop = EquityHandler.calculateEquityMonteCarlo(holeCardsString, villainHoleCardsString, boardFlop.toStringWithoutCommas());
                    postflopRow.equityFlop = equityFlop;
                }
                if (boardTurn != null) {
                    double equityTurn = EquityHandler.calculateEquityMonteCarlo(holeCardsString, villainHoleCardsString, boardTurn.toStringWithoutCommas());
                    postflopRow.equityTurn = equityTurn;
                }
                if (boardRiver != null) {
                    double equityRiver = EquityHandler.calculateEquityMonteCarlo(holeCardsString, villainHoleCardsString, boardRiver.toStringWithoutCommas());
                    postflopRow.equityRiver = equityRiver;
                }
            }*/
            }

            PostFlopRowService postFlopService = new PostFlopRowService();
            postFlopService.write(postflopRows, postflop);

            System.out.println("result: " + postflopRows.size());

            long end = System.currentTimeMillis();
            System.out.println("TOOK: " + ((start - end) / 1000) + " seconds");
        }
    }

    private void fetchDataForP1AndStatsForP2AndCombinePostflopRows(List<Map<String, Object>> response, List<PostFlopRow> postflopRows, boolean postflop) throws Exception {
        for (Map<String, Object> responseMap : response) {
            PostFlopRow postflopRow = new PostFlopRow();

            //String handNo = (String) responseMap.get("hand_no");
            //postflopRow.handNo = handNo;
            int playersFlop = (int) responseMap.get("cnt_players_f");
            postflopRow.playersOnFlop = playersFlop;

            int id_limit = (int) responseMap.get("id_limit");
            Double bb = PTUtil.getBB(id_limit);

            if (bb == null) {
                continue;
            }

            fetchDataForP1PostflopRows(responseMap, postflopRow, postflop);
            fetchStatsForOthersPostflopRows(responseMap, postflopRow);

            int id_holecard1 = (int) responseMap.get("id_holecard1");
            int id_holecard2 = (int) responseMap.get("id_holecard2");

            postflopRow.holeCard1Id = id_holecard1;
            postflopRow.holeCard2Id = id_holecard2;
            postflopRow.flopCard1Id = (int) responseMap.get("id_flop1");
            postflopRow.flopCard2Id = (int) responseMap.get("id_flop2");
            postflopRow.flopCard3Id = (int) responseMap.get("id_flop3");
            postflopRow.turnCardId = (int) responseMap.get("id_turn");
            postflopRow.riverCardId = (int) responseMap.get("id_river");
            /*CardSet holeCards = new CardSet();
            holeCards.add(PTUtil.getCard(id_holecard1));
            holeCards.add(PTUtil.getCard(id_holecard2));
            String holeCardsString = holeCards.get(0).toString() + holeCards.get(1).toString();
            String rangeCombo = PTUtil.convertToRangeCombo(holeCardsString);
            postflopRow.rangeComboId = PTUtil.getRangeComboInt(rangeCombo);*/
            postflopRow.showdown = (boolean) responseMap.get("flg_showdown");

            postflopRows.add(postflopRow);
        }
    }

    private void fetchDataForP1PostflopRows(Map<String, Object> responseMap, PostFlopRow postflopRow, boolean postflop) {
        BigDecimal amountWon = (BigDecimal) responseMap.get("amt_won");
        int valPositionTypeInt = (int) responseMap.get("val_position_type");
        postflopRow.position = PTUtil.getPTPositionString(valPositionTypeInt);

        boolean p1Won = amountWon != null && amountWon.doubleValue() > 0.00;
        double amountWonBB = 0.0;

        int id_limit = (int) responseMap.get("id_limit");
        Double bb = PTUtil.getBB(id_limit);

        if (amountWon != null) {
            amountWonBB = amountWon.doubleValue() / bb;
        }

        postflopRow.won = p1Won;
        postflopRow.wonBB = amountWonBB;

        postflopRow.potPreflopBB = getPotDoubleValue(responseMap.get("amt_pot_p"), bb);
        postflopRow.potFlopBB = getPotDoubleValue(responseMap.get("amt_pot_f"), bb);
        postflopRow.potTurnBB = getPotDoubleValue(responseMap.get("amt_pot_t"), bb);
        postflopRow.potRiverBB = getPotDoubleValue(responseMap.get("amt_pot_r"), bb);
        postflopRow.raiseMadePreFlopPct = getDoubleValue(responseMap.get("val_p_raise_made_pct"));
        postflopRow.raise2MadePreFlopPct = getDoubleValue(responseMap.get("val_p_raise_made_2_pct"));
        postflopRow.betMadeFlopPct = getDoubleValue(responseMap.get("val_f_bet_made_pct"));
        postflopRow.raiseMadeFlopPct = getDoubleValue(responseMap.get("val_f_raise_made_pct"));
        postflopRow.raise2MadeFlopPct = getDoubleValue(responseMap.get("val_f_raise_made_2_pct"));
        postflopRow.betMadeTurnPct = getDoubleValue(responseMap.get("val_t_bet_made_pct"));
        postflopRow.raiseMadeTurnPct = getDoubleValue(responseMap.get("val_t_raise_made_pct"));
        postflopRow.raise2MadeTurnPct = getDoubleValue(responseMap.get("val_t_raise_made_2_pct"));
        postflopRow.betMadeRiverPct = getDoubleValue(responseMap.get("val_r_bet_made_pct"));
        postflopRow.raiseMadeRiverPct = getDoubleValue(responseMap.get("val_r_raise_made_pct"));
        postflopRow.raise2MadeRiverPct = getDoubleValue(responseMap.get("val_r_raise_made_2_pct"));

        int hahah = (int) responseMap.get("id_action_p");

        postflopRow.actionsPreflop = PTUtil.getActionsString((int) responseMap.get("id_action_p"));
        if (postflop) {
            postflopRow.actionsFlop = PTUtil.getActionsString((int) responseMap.get("id_action_f"));
            postflopRow.actionsTurn = PTUtil.getActionsString((int) responseMap.get("id_action_t"));
            postflopRow.actionsRiver = PTUtil.getActionsString((int) responseMap.get("id_action_r"));
        }
    }

    private double getPotDoubleValue(Object object, double bb) {
        if (object != null) {
            return ((BigDecimal) object).doubleValue() / bb;
        } else {
            return 0.0;
        }
    }

    private double getDoubleValue(Object object) {
        if (object != null) {
            return ((BigDecimal) object).doubleValue();
        } else {
            return 0.00;
        }
    }

    private double getVillainDoubleValue(Object object) {
        if (object != null) {
            return (double) object;
        } else {
            return 0.00;
        }
    }

    private void fetchStatsForOthersPostflopRows(Map<String, Object> responseMap, PostFlopRow postflopRow) {
        int handId = (Integer) responseMap.get("id_hand");
        int p1Id = (Integer) responseMap.get("player_id");

        List<Map<String, Object>> otherVillainsList = pokerTrackerRepository.getOtherVillains(handId, p1Id);

        // Actors preflop
        int actorsPreflop = 1;

        for (Map<String, Object> otherVillains : otherVillainsList) {
            boolean sawFlop = (boolean) otherVillains.get("flg_f_saw");

            int actionsPreflop = (int) otherVillains.get("id_action_p");
            String otherVillainActionString = PTUtil.getActionsString(actionsPreflop);
            if (otherVillainActionString.equalsIgnoreCase("-") || otherVillainActionString.equalsIgnoreCase("F")) {
                continue;
            } else {
                actorsPreflop++;
            }
            if (!sawFlop) {
                continue;
            }
            //String end = "_" + (String) otherVillains.get("position");
            int id_holecard1 = 0;
            int id_holecard2 = 0;
            if (otherVillains.get("id_holecard1") != null) {
                id_holecard1 = (int) otherVillains.get("id_holecard1");
                id_holecard2 = (int) otherVillains.get("id_holecard2");
            }

            postflopRow.villainHoleCard1Id = id_holecard1;
            postflopRow.villainHoleCard2Id = id_holecard2;

            //int rangeComboId = 0;
            /*if (id_holecard1 > 0) {
                String holeCardsString = PTUtil.getCard(id_holecard1).toString() + PTUtil.getCard(id_holecard2).toString();
                String rangeCombo = PTUtil.convertToRangeCombo(holeCardsString);
                rangeComboId = PTUtil.getRangeComboInt(rangeCombo);
            }*/
            //postflopRow.rangeComboId = rangeComboId;
            int valVillainPositionTypeInt = (int) otherVillains.get("val_position_type");
            postflopRow.villainPosition = PTUtil.getPTPositionString(valVillainPositionTypeInt);

            postflopRow.actionsVillainPreflop = PTUtil.getActionsString((int) otherVillains.get("id_action_p"));
            postflopRow.actionsVillainFlop = PTUtil.getActionsString((int) otherVillains.get("id_action_f"));
            postflopRow.actionsVillainTurn = PTUtil.getActionsString((int) otherVillains.get("id_action_t"));
            postflopRow.actionsVillainRiver = PTUtil.getActionsString((int) otherVillains.get("id_action_r"));
            postflopRow.villainRaiseMadePreFlopPct = getVillainDoubleValue(otherVillains.get("val_p_raise_made_pct"));
            postflopRow.villainRaise2MadePreFlopPct = getVillainDoubleValue(otherVillains.get("val_p_raise_made_2_pct"));
            postflopRow.villainBetMadeFlopPct = getVillainDoubleValue(otherVillains.get("val_f_bet_made_pct"));
            postflopRow.villainRaiseMadeFlopPct = getVillainDoubleValue(otherVillains.get("val_f_raise_made_pct"));
            postflopRow.villainRaise2MadeFlopPct = getVillainDoubleValue(otherVillains.get("val_f_raise_made_2_pct"));
            postflopRow.villainBetMadeTurnPct = getVillainDoubleValue(otherVillains.get("val_t_bet_made_pct"));
            postflopRow.villainRaiseMadeTurnPct = getVillainDoubleValue(otherVillains.get("val_t_raise_made_pct"));
            postflopRow.villainRaise2MadeTurnPct = getVillainDoubleValue(otherVillains.get("val_t_raise_made_2_pct"));
            postflopRow.villainBetMadeRiverPct = getVillainDoubleValue(otherVillains.get("val_r_bet_made_pct"));
            postflopRow.villainRaiseMadeRiverPct = getVillainDoubleValue(otherVillains.get("val_r_raise_made_pct"));
            postflopRow.villainRaise2MadeRiverPct = getVillainDoubleValue(otherVillains.get("val_r_raise_made_2_pct"));

            int p2Id = (Integer) otherVillains.get("id_player");
            Map<HUDStat, Double> hudStats;
            if (playersHUDStats.containsKey(p2Id)) {
                hudStats = playersHUDStats.get(p2Id);
            } else {
                hudStats = pokerTrackerRepository.getAllHudStats(String.valueOf(p2Id));
                playersHUDStats.put(p2Id, hudStats);
            }

            if (hudStats != null) {
                Double hands = hudStats.get(HUDStat.HANDS);
                postflopRow.villainVPIP = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_VPIP, hands);
                postflopRow.villainPFR = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_PFR, hands);
                postflopRow.villainAF = 0;
                postflopRow.villain3Bet = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_3BET, hands);
                postflopRow.villainFoldTo3Bet = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_3BET, hands);
                postflopRow.villain4Bet = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_4BET, hands);
                postflopRow.villainFoldTo4Bet = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_4BET, hands);
                postflopRow.villainCbetFlop = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_CBET, hands);
                postflopRow.villainCbetTurn = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_CBET, hands);
                postflopRow.villainFoldToCbetFlop = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_FOLD_TO_CBET, hands);
                postflopRow.villainFoldToCbetTurn = HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_FOLD_TO_CBET, hands);

            }

            //System.out.println(handId + " villain actions flop: " + postflopRow.actionsVillainFlop + " hand: " + postflopRow.villainHoleCard1Id + postflopRow.villainHoleCard2Id);

            /*map.put("vpip" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_VPIP, hands));
            map.put("pfr" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_PFR, hands));
            map.put("att_to_steal" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_ATT_TO_STEAL, hands));
            map.put("fold_to_steal" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_STEAL, hands));
            map.put("threebet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_3BET, hands));
            map.put("fold_to_threebet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_3BET, hands));
            map.put("fourbet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_4BET, hands));
            map.put("fold_to_fourbet" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_4BET, hands));
            map.put("cbet_f" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_CBET, hands));
            map.put("fold_to_cbet_f" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_FOLD_TO_CBET, hands));
            map.put("cbet_t" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_CBET, hands));
            map.put("fold_to_cbet_t" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_FOLD_TO_CBET, hands));*/
        }

        postflopRow.playersPreflop = actorsPreflop;
    }

    private String bs(boolean b) {
        return b == true ? "1" : "0";
    }

}
