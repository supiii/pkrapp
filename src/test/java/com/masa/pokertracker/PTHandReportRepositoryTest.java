package com.masa.pokertracker;

//import com.masa.pokertracker.repository.AllMovesRepository;
//import com.masa.pokertracker.repository.WinningPlayersMovesRepository;
import com.masa.txt.service.PostFlopRowService;
import com.masa.type.HUDStat;
import com.masa.type.NLLimit;
import com.masa.type.PostFlopRow;
import com.masa.util.DrawAnalyzer;
import com.masa.util.FlopBucketAnalyzer;
import com.masa.util.FlopSuitnessAnalyzer;
import com.masa.util.HUDStatUtil;
import com.masa.util.MadeHandAnalyzer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mi.poker.common.model.testbed.klaatu.CardSet;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Matti
 */
public class PTHandReportRepositoryTest {

    private final PokerTrackerRepository pokerTrackerRepository = new PokerTrackerRepository();
    //private final WinningPlayersMovesRepository winningPlayersRepository = new WinningPlayersMovesRepository();
    //private final AllMovesRepository allMovesRepository = new AllMovesRepository();
    Map<HUDStat, Double> defaultHUDStats = new HashMap<>();
    Map<Integer, Map<HUDStat, Double>> playersHUDStats = new HashMap<>();

    @Disabled
    @Test
    public void testDelete() throws Exception {
        //winningPlayersRepository.deleteAllDataFromTable("winning_players_moves");
    }

    @Disabled
    @Test
    public void testGetAllHUDStats() throws Exception {
        Map<HUDStat, Double> hudStats = pokerTrackerRepository.getAllHudStats("4");
    }

    @Disabled
    @Test
    public void testFetchAndInsertWinningPlayersData() throws Exception {
        //winningPlayersRepository.fetchAndInsertWinningPlayersData();
    }

    @Test
    public void testHandleFullPreflop() throws Exception {
        defaultHUDStats.putAll(HUDStatUtil.getDefaultHUDStats(NLLimit.NL5));
        boolean postflop = false;
        int step = 500;

        for (int i = 0; i < 10000; i++) {
            long start = System.currentTimeMillis();
            List<Map<String, Object>> response = new ArrayList<>();
            List<Map<String, Object>> insertData = new ArrayList<>();

            response.addAll(pokerTrackerRepository.fetchWinningPlayersHandReport(0, step));
            if (response.isEmpty()) {
                System.out.println("EMPTY RESPONSE");
                break;
            }

            //System.out.println("response: " + response.toString());
            fetchDataForP1AndStatsForP2AndCombine(response, insertData, postflop);
            System.out.println("DONE FETCHING HUD STATS...");
            /*for (Map.Entry<String, Object> entry : insertData.get(0).entrySet()) {
                String key = entry.getKey();
                System.out.println(key);
            }*/
 /*for (Map<String, Object> map : insertData) {
                System.out.println("-------");
                for (Map.Entry<String, Object> entry : map.entrySet()) {

                    Object key = entry.getKey();
                    Object val = entry.getValue();
                    System.out.println(key + " " + val);
                }
            }*/
            pokerTrackerRepository.insertGenericTableDataOneByOne(insertData, "labelled_pf");
            System.out.println("result: " + insertData.size());

            long end = System.currentTimeMillis();
            System.out.println("TOOK: " + ((start - end) / 1000) + " seconds");
        }
    }

    @Test
    public void testHandleFullPostflop() throws Exception {
        defaultHUDStats.putAll(HUDStatUtil.getDefaultHUDStats(NLLimit.NL5));
        boolean postflop = true;
        int step = 500;

        for (int i = 0; i < 10000; i++) {
            long start = System.currentTimeMillis();
            List<Map<String, Object>> response = new ArrayList<>();

            response.addAll(pokerTrackerRepository.fetchWinningPlayersHandReport(0, step));
            List<PostFlopRow> postflopRows = new ArrayList<>();

            if (response.isEmpty()) {
                System.out.println("EMPTY RESPONSE");
                break;
            }

            //System.out.println("response: " + response.toString());
            fetchDataForP1AndStatsForP2AndCombinePostflopRows(response, postflopRows, postflop);
            System.out.println("DONE FETCHING HUD STATS...");
            /*for (Map.Entry<String, Object> entry : insertData.get(0).entrySet()) {
                String key = entry.getKey();
                System.out.println(key);
            }*/
 /*for (Map<String, Object> map : insertData) {
                System.out.println("-------");
                for (Map.Entry<String, Object> entry : map.entrySet()) {

                    Object key = entry.getKey();
                    Object val = entry.getValue();
                    System.out.println(key + " " + val);
                }
            }*/
            //pokerTrackerRepository.insertGenericTableDataOneByOne(insertData, "labelled_pf");

            for (PostFlopRow postflopRow : postflopRows) {
                CardSet board = PTUtil.getBoardCardSet(postflopRow.flopCard1Id, postflopRow.flopCard2Id, postflopRow.flopCard3Id, postflopRow.turnCardId, postflopRow.riverCardId);
                CardSet holeCards = PTUtil.getHoleCardSet(postflopRow.holeCard1Id, postflopRow.holeCard2Id);
                postflopRow.madeHandSpecific = MadeHandAnalyzer.getMadeHandSpecific(board, holeCards);
                postflopRow.flopBucket = FlopBucketAnalyzer.getFlopBucket(board);
                postflopRow.flopSuitness = FlopSuitnessAnalyzer.getFlopSuitness(board);
                postflopRow.draws = DrawAnalyzer.getDraws(board, holeCards, null);
            }

            PostFlopRowService postFlopService = new PostFlopRowService();
            postFlopService.write(postflopRows);

            System.out.println("result: " + postflopRows.size());

            long end = System.currentTimeMillis();
            System.out.println("TOOK: " + ((start - end) / 1000) + " seconds");
        }
    }

    private void fetchDataForP1AndStatsForP2AndCombinePostflopRows(List<Map<String, Object>> response, List<PostFlopRow> postflopRows, boolean postflop) throws Exception {
        for (Map<String, Object> responseMap : response) {
            //Map<String, Object> map = new HashMap<>();
            PostFlopRow postflopRow = new PostFlopRow();

            fetchDataForP1PostflopRows(responseMap, postflopRow, postflop);
            fetchStatsForOthersPostflopRows(responseMap, postflopRow);

            //convertResponseToPosTextDraw(responseMap, map);
            int id_holecard1 = (int) responseMap.get("id_holecard1");
            int id_holecard2 = (int) responseMap.get("id_holecard2");

            //map.put("id_holecard1", id_holecard1);
            //map.put("id_holecard2", id_holecard2);
            postflopRow.holeCard1Id = id_holecard1;
            postflopRow.holeCard2Id = id_holecard2;
            postflopRow.flopCard1Id = (int) responseMap.get("id_flop1");
            postflopRow.flopCard2Id = (int) responseMap.get("id_flop2");
            postflopRow.flopCard3Id = (int) responseMap.get("id_flop3");
            postflopRow.turnCardId = (int) responseMap.get("id_turn");
            postflopRow.riverCardId = (int) responseMap.get("id_river");
            CardSet holeCards = new CardSet();
            holeCards.add(PTUtil.getCard(id_holecard1));
            holeCards.add(PTUtil.getCard(id_holecard2));
            String holeCardsString = holeCards.get(0).toString() + holeCards.get(1).toString();
            String rangeCombo = PTUtil.convertToRangeCombo(holeCardsString);
            //map.put("id_rangecombo", PTUtil.getRangeComboInt(rangeCombo));
            postflopRow.rangeComboId = PTUtil.getRangeComboInt(rangeCombo);
            postflopRows.add(postflopRow);
        }
    }

    private void fetchDataForP1AndStatsForP2AndCombine(List<Map<String, Object>> response, List<Map<String, Object>> combined, boolean postflop) throws Exception {
        for (Map<String, Object> responseMap : response) {
            Map<String, Object> map = new HashMap<>();

            fetchDataForP1(responseMap, map, postflop);
            fetchStatsForOthers(responseMap, map);

            //convertResponseToPosTextDraw(responseMap, map);
            int id_holecard1 = (int) responseMap.get("id_holecard1");
            int id_holecard2 = (int) responseMap.get("id_holecard2");

            map.put("id_holecard1", id_holecard1);
            map.put("id_holecard2", id_holecard2);
            CardSet holeCards = new CardSet();
            holeCards.add(PTUtil.getCard(id_holecard1));
            holeCards.add(PTUtil.getCard(id_holecard2));
            String holeCardsString = holeCards.get(0).toString() + holeCards.get(1).toString();
            String rangeCombo = PTUtil.convertToRangeCombo(holeCardsString);
            map.put("id_rangecombo", PTUtil.getRangeComboInt(rangeCombo));
            combined.add(map);
        }
    }

    private void fetchDataForP1PostflopRows(Map<String, Object> responseMap, PostFlopRow postflopRow, boolean postflop) {
        BigDecimal amountWon = (BigDecimal) responseMap.get("amt_won");
        //map.put("val_position_type", (int) responseMap.get("val_position_type"));
        postflopRow.valPositionType = (int) responseMap.get("val_position_type");
        boolean p1Won = amountWon != null && amountWon.doubleValue() > 0.00;
        double amountWonBB = 0.0;
        if (amountWon != null) {
            amountWonBB = amountWon.doubleValue() / 0.10d;
        }
        //map.put("p1_won", p1Won);
        postflopRow.won = p1Won;
        //map.put("p1_won_bb", amountWonBB);
        postflopRow.wonBB = amountWonBB;
        //map.put("id_limit", responseMap.get("id_limit"));
        //map.put("id_action_p", (int) responseMap.get("id_action_p"));
        postflopRow.actionsPreflop = (int) responseMap.get("id_action_p");
        if (postflop) {
            postflopRow.actionsFlop = (int) responseMap.get("id_action_f");
            postflopRow.actionsTurn = (int) responseMap.get("id_action_t");
            postflopRow.actionsRiver = (int) responseMap.get("id_action_r");
            //map.put("id_action_f", (int) responseMap.get("id_action_f"));
            //map.put("id_action_t", (int) responseMap.get("id_action_t"));
            //map.put("id_action_r", (int) responseMap.get("id_action_r"));
        }
    }

    private void fetchDataForP1(Map<String, Object> responseMap, Map<String, Object> map, boolean postflop) {
        BigDecimal amountWon = (BigDecimal) responseMap.get("amt_won");
        map.put("val_position_type", (int) responseMap.get("val_position_type"));
        boolean p1Won = amountWon != null && amountWon.doubleValue() > 0.00;
        double amountWonBB = 0.0;
        if (amountWon != null) {
            amountWonBB = amountWon.doubleValue() / 0.10d;
        }
        map.put("p1_won", p1Won);
        map.put("p1_won_bb", amountWonBB);
        //map.put("id_limit", responseMap.get("id_limit"));
        map.put("id_action_p", (int) responseMap.get("id_action_p"));

        if (postflop) {
            map.put("id_action_f", (int) responseMap.get("id_action_f"));
            map.put("id_action_t", (int) responseMap.get("id_action_t"));
            map.put("id_action_r", (int) responseMap.get("id_action_r"));
        }
    }

    private void fetchStatsForOthersPostflopRows(Map<String, Object> responseMap, PostFlopRow postflopRow) {
        int handId = (Integer) responseMap.get("id_hand");
        //map.put("id_hand", handId);
        //map.put("hand_no", (String) responseMap.get("hand_no"));
        int p1Id = (Integer) responseMap.get("player_id");

        List<Map<String, Object>> otherVillainsList = pokerTrackerRepository.getOtherVillains(handId, p1Id);

        for (Map<String, Object> otherVillains : otherVillainsList) {
            String end = "_" + (String) otherVillains.get("position");
            if (handId == 5385) {
                System.out.println("DEBUG");
            }
            postflopRow.valPositionTypeVillain = (int) otherVillains.get("val_position_type");
            postflopRow.actionsVillainPreflop = (int) otherVillains.get("id_action_p");
            postflopRow.actionsVillainFlop = (int) otherVillains.get("id_action_f");
            postflopRow.actionsVillainTurn = (int) otherVillains.get("id_action_t");
            postflopRow.actionsVillainRiver = (int) otherVillains.get("id_action_r");
            //map.put("id_action_p" + end, otherVillains.get("id_action_p"));
            int p2Id = (Integer) otherVillains.get("id_player");
            Map<HUDStat, Double> hudStats;
            if (playersHUDStats.containsKey(p2Id)) {
                hudStats = playersHUDStats.get(p2Id);
            } else {
                hudStats = pokerTrackerRepository.getAllHudStats(String.valueOf(p2Id));
                playersHUDStats.put(p2Id, hudStats);
            }
            /*
            Double hands = hudStats.get(HUDStat.HANDS);
            map.put("vpip" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_VPIP, hands));
            map.put("pfr" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_PFR, hands));
            map.put("att_to_steal" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_ATT_TO_STEAL, hands));
            map.put("fold_to_steal" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_STEAL, hands));
            map.put("threebet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_3BET, hands));
            map.put("fold_to_threebet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_3BET, hands));
            map.put("fourbet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_4BET, hands));
            map.put("fold_to_fourbet" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_4BET, hands));
            map.put("cbet_f" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_CBET, hands));
            map.put("fold_to_cbet_f" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_FOLD_TO_CBET, hands));
            map.put("cbet_t" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_CBET, hands));
            map.put("fold_to_cbet_t" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_FOLD_TO_CBET, hands));
             */
        }
    }

    private void fetchStatsForOthers(Map<String, Object> responseMap, Map<String, Object> map) {
        int handId = (Integer) responseMap.get("id_hand");
        map.put("id_hand", handId);
        map.put("hand_no", (String) responseMap.get("hand_no"));
        int p1Id = (Integer) responseMap.get("player_id");

        List<Map<String, Object>> otherVillainsList = pokerTrackerRepository.getOtherVillains(handId, p1Id);

        for (Map<String, Object> otherVillains : otherVillainsList) {
            String end = "_" + (String) otherVillains.get("position");
            if (handId == 5385) {
                System.out.println("DEBUG");
            }
            map.put("id_action_p" + end, otherVillains.get("id_action_p"));
            int p2Id = (Integer) otherVillains.get("id_player");
            Map<HUDStat, Double> hudStats;
            if (playersHUDStats.containsKey(p2Id)) {
                hudStats = playersHUDStats.get(p2Id);
            } else {
                hudStats = pokerTrackerRepository.getAllHudStats(String.valueOf(p2Id));
                playersHUDStats.put(p2Id, hudStats);
            }

            Double hands = hudStats.get(HUDStat.HANDS);
            map.put("vpip" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_VPIP, hands));
            map.put("pfr" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_PFR, hands));
            map.put("att_to_steal" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_ATT_TO_STEAL, hands));
            map.put("fold_to_steal" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_STEAL, hands));
            map.put("threebet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_3BET, hands));
            map.put("fold_to_threebet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_3BET, hands));
            map.put("fourbet_pf" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_4BET, hands));
            map.put("fold_to_fourbet" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.PREFLOP_FOLD_TO_4BET, hands));
            map.put("cbet_f" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_CBET, hands));
            map.put("fold_to_cbet_f" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.FLOP_FOLD_TO_CBET, hands));
            map.put("cbet_t" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_CBET, hands));
            map.put("fold_to_cbet_t" + end, HUDStatUtil.getHUDStatCompensated(hudStats, defaultHUDStats, HUDStat.TURN_FOLD_TO_CBET, hands));
        }
    }

//    private void convertResponseToPosTextDraw(Map<String, Object> responseMap, Map<String, Object> map) {
//        int position_p1 = (int) responseMap.get("val_position_type");
//        int position_p2 = (int) responseMap.get("position_p2");
//
//        String posTexDrawFlopString = "";
//        String posTexDrawTurnString = "";
//        String posTexDrawRiverString = "";
//
//        //TODO: ctaegoryHandStrengthId
//        //TODO: Draws
//        // FLOP
//        int id_flop1 = (int) responseMap.get("id_flop1");
//        int id_flop2 = (int) responseMap.get("id_flop2");
//        int id_flop3 = (int) responseMap.get("id_flop3");
//        CardSet flopSet = new CardSet();
//        flopSet.add(PTUtil.getCard(id_flop1));
//        flopSet.add(PTUtil.getCard(id_flop2));
//        flopSet.add(PTUtil.getCard(id_flop3));
//
//        posTexDrawFlopString = PTUtil.getPosTextDrawFlop(position_p1, position_p2, flopSet);
//
//        // TURN
//        int id_turn = (int) responseMap.get("id_turn");
//        if (id_turn > 0) {
//            CardSet turnSet = new CardSet(flopSet);
//            turnSet.add(PTUtil.getCard(id_turn));
//            posTexDrawTurnString = PTUtil.getPosTextDrawTurn(position_p1, position_p2, flopSet, turnSet);
//        } else {
//            posTexDrawTurnString = ""
//                    + position_p1
//                    + position_p2
//                    + "000000";
//        }
//
//        //RIVER
//        int id_river = (int) responseMap.get("id_river");
//        if (id_turn > 0 && id_river > 0) {
//            CardSet riverSet = new CardSet(flopSet);
//            riverSet.add(PTUtil.getCard(id_turn));
//            riverSet.add(PTUtil.getCard(id_river));
//            CardSet turnSet = new CardSet(flopSet);
//            turnSet.add(PTUtil.getCard(id_turn));
//            posTexDrawRiverString = PTUtil.getPosTextDrawTurn(position_p1, position_p2, turnSet, riverSet);
//        } else {
//            posTexDrawRiverString = ""
//                    + position_p1
//                    + position_p2
//                    + "000000000";
//        }
//
//        //30000000000
//        int posTexDrawFlop = Integer.parseInt(posTexDrawFlopString);
//        int posTexDrawTurn = Integer.parseInt(posTexDrawTurnString);
//        long posTexDrawRiver = Long.parseLong(posTexDrawRiverString);
//
//        map.put("pos_text_draw_f", posTexDrawFlop);
//        map.put("pos_text_draw_t", posTexDrawTurn);
//        map.put("pos_text_draw_r", posTexDrawRiver);
//
//    }
    private String bs(boolean b) {
        return b == true ? "1" : "0";
    }

}
