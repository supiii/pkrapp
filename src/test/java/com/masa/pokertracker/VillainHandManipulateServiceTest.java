package com.masa.pokertracker;

import com.masa.txt.service.PostFlopRowService;
import com.masa.type.PostFlopRow;
import com.masa.util.Util;
import com.masa.util.VillainHandManipulateService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;


/*
 For manipulating heros hands
 */
public class VillainHandManipulateServiceTest {

    private static final String FILES_FOLDER = ".\\files\\";
    private static final String MANIPULATED_FILES_FOLDER = ".\\files_manipulated\\";
    PostFlopRowService postflopRoeService = new PostFlopRowService();

    @Test
    public void testCalculateWonLoss() throws Exception {
        Set<String> fileNames = listFiles(FILES_FOLDER);
        Set<String> manipulatedFileNames = listFiles(MANIPULATED_FILES_FOLDER);

        for (String fileName : fileNames) {
            if (manipulatedFileNames.contains(fileName)) {
                System.out.println("FOUND ALREADY MANIPULATED FILENAME: " + fileName);
                continue;
            }
            System.out.println("Start manipulating file: " + fileName);
            List<PostFlopRow> rows = readRows(fileName);
            VillainHandManipulateService.manipulateVillainHands(rows, 2);
            VillainHandManipulateService.manipulateVillainHands(rows, 1);
            //WonLossRowsService.calculateWonLossWithCurrentHand(rows, 0, true);

            System.out.println("--------------ROWS: " + rows.size());

            /*int manipulated = 0;
            int showdownhands = 0;
            int villainFoldHands = 0;
            List<PostFlopRow> remove = new ArrayList<>();
            for (PostFlopRow row : rows) {
                if (row.showdown) {
                    showdownhands++;
                } else if (LineFilter.someVillainsStreetEndsWith(row, "F", Street.FLOP)) {
                    villainFoldHands++;
                }
                if (row.alteredVillainHand == true) {
                    manipulated++;
                }
                if (row.showdown && (row.villainHoleCard1Id == 0 || row.villainHoleCard2Id == 0)) {
                    remove.add(row);
                }

            }

            int rowsWithoutVillainHandRatio = 0;
            if (remove.size() > 0 && showdownhands > 0) {
                rowsWithoutVillainHandRatio = remove.size() / showdownhands;
            }

            int villainFoldHandsToRemove = villainFoldHands * rowsWithoutVillainHandRatio;

            int removedVillainFoldHands = 0;
            for (PostFlopRow row : rows) {
                if (removedVillainFoldHands > villainFoldHandsToRemove) {
                    break;
                }
                if (LineFilter.someVillainsStreetEndsWith(row, "F", Street.FLOP)) {
                    remove.add(row);
                    removedVillainFoldHands++;
                }
            }

            System.out.println("--------------ROWS WITHOUT VILLAIN CARDS: " + remove.size() + " showdownHands: " + showdownhands + " manipulated: " + manipulated);

            rows.removeAll(remove);*/
            postflopRoeService.writeIntoFile(MANIPULATED_FILES_FOLDER + fileName, rows);

        }
    }

    public List<PostFlopRow> readRows(String fileName) throws FileNotFoundException, IOException {
        List<PostFlopRow> rows = new ArrayList<>();
        BufferedReader objReader = null;

        try {
            String filename = FILES_FOLDER + fileName;
            //System.out.println("FILENAME: " + filename);
            objReader = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = objReader.readLine()) != null) {
                PostFlopRow row = new PostFlopRow();
                Util.createPostFlopRow(line, row);
                rows.add(row);
            }
            objReader = null;

        } finally {
            if (objReader != null) {
                objReader.close();
            }
        }

        return rows;
    }

    public Set<String> listFiles(String dir) {
        return Stream.of(new File(dir).listFiles())
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toSet());
    }

}
