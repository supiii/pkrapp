package com.masa.screenreader;

import com.masa.type.Seat;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.junit.jupiter.api.Test;

/**
 *
 * @author compuuter
 */
public class TableReaderTest {

    private final ScreenReader screenReader = new ScreenReader();
    private final TableReader tableReader = new TableReader();

    /*@Test
    public void testHandleTable_2() throws Exception {
        String folder = "PS_new_skin\\card_screenshots\\fs1643908889630.png";
        screenReader.setFullScreenImage(getImageResource(folder));
        tableReader.readHoleCardset(screenReader.getFullScreenImage());
    }*/
    @Test
    public void testHandleTable_btn_seat() throws Exception {
        String folder = "PS_new_skin\\card_screenshots\\btn_seat2.png";
        screenReader.setFullScreenImage(getImageResource(folder));
        tableReader.readHoleCardset(screenReader.getFullScreenImage());
        Seat buttonSeat = tableReader.readButtonSeat(screenReader.getFullScreenImage());
        System.out.println("Button seat: " + buttonSeat);
    }

    protected BufferedImage getImageResource(String filename) {
        BufferedImage bufferedImage = null;
//        try {
//            originalImage = ImageIO.read(new File("C:\\Users\\Matti\\Documents\\NetBeansProjects\\MasaMaven\\src\\main\\resources\\card_4.png"));
//        } catch (IOException ex) {
//            Logger.getLogger(CardReaderTest.class.getName()).log(Level.SEVERE, null, ex);
//        }

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(filename);
        try {
            bufferedImage = ImageIO.read(is);
        } catch (IOException ex) {
            Logger.getLogger(TableReaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bufferedImage;

    }
}
